#ifndef BMESH_H
#define BMESH_H
#include "bskeleton.h"
#include <geometry/topologicalmesh.h>
#include <animation/vertex3dskin.h>
namespace module
{

namespace bmeshes
{


class BMesh
{
public:
    BMesh(const std::string& mesh_name);
    void computeInitMesh(unsigned int inbetween_step);
    void evolveMesh();
    void setRootIndex(size_t index);
    inline std::shared_ptr<skyengine::geometry::TopologicalMesh<skyengine::animation::Vertex3DSkin>> getMesh();
    void AddSphere(float radius, const glm::vec3& center);
    void AddConnection(size_t id_0, size_t id_1);
private:
    std::shared_ptr<skyengine::geometry::TopologicalMesh<skyengine::animation::Vertex3DSkin>> m_mesh;
    BSkeleton m_skeleton;
    size_t m_root_sphere_id;

    void computeInitMeshRec(skyengine::utility::GraphNode<BSphere>* sphere, std::unordered_map<skyengine::utility::GraphNode<BSphere>* ,skyengine::utility::GraphNode<BSphere>* >& dealt_nodes, unsigned int inbetween_step);

};

inline std::shared_ptr<skyengine::geometry::TopologicalMesh<skyengine::animation::Vertex3DSkin>> BMesh::getMesh()
{
    return m_mesh;
}

}
}
#endif // BMESH_H
