#ifndef DOODLEGUI_H
#define DOODLEGUI_H

#include <core/task.h>
#include "TrajectoryManipulator.h"
#include "../modules/gui/widgets/skywidget.h"
#include "AnimationModifier/LabanTrajectoryModifier.h"
struct GLFWwindow;
namespace module
{

namespace doodle
{


class MotionDoodleControls : public module::gui::SkyWidget
{
public:
    MotionDoodleControls(const std::string& name,module::gui::SkyWindow* sky_win,std::shared_ptr<module::gui::ActiveObjectEvent> act_evt,
                         std::shared_ptr<module::gui::SelectObjectEvent> select_evt,DoodleManipulator* doodle_manipulator);
    virtual ~MotionDoodleControls();
    void draw();

private:
    DoodleManipulator* m_doodle_manipulator;    
    std::shared_ptr<ImVec2> m_trajectory_win_size;
    std::shared_ptr<ImVec2> m_detection_win_size;
    std::vector<std::vector<motionDoodle::Vector7D>> m_training_vec;
    std::vector<std::shared_ptr<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>>> m_training_curves;

    int m_current_file_name;
    bool m_show_geo_feat;
    bool m_is_training;
    char m_cv_buff[128];

    void ShowSegmenterProperties();
    void ShowGeometryFeatures();
    void LearnActionExpression();

};

}
}
#endif
