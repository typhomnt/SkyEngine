#ifndef KEYFRAME_H
#define KEYFRAME_H

#include "utils/skyengine_dll.h"

#include "timeparameter.h"
#include "animationmacros.h"
#include <vector>
#include <map>
#include "math/mathutils.h"
#include "animation.h"
#include "utils/utilities.h"

namespace skyengine
{

namespace animation
{
class Animation;
class SKYENGINE_API KeyFrame
{
public:
    //Constructors & Destructors-------------
    KeyFrame(float time_stamp);
    virtual ~KeyFrame();
    //---------------------------------------

    //Getters--------------------------------
    float getTimeStamp() const;
    inline std::map<std::string,BaseTimeParameter*>& getFrameKeys();
    inline bool containsKey(const std::string& key_name);
    //---------------------------------------

    //Setters & Adders-----------------------
    void setTimeStamp(float time_stamp);
    void setAnimation(Animation* animation);
    bool addKeyParameter(const std::string& key_name, BaseTimeParameter* parameter);
    //---------------------------------------

    static float keyframeDistance(const KeyFrame& keyframe_1, const KeyFrame& keyframe_2);

protected:
    float m_time_stamp;
    std::map<std::string,BaseTimeParameter*> m_frame_keys;
    Animation* m_animation;
};

inline std::map<std::string,BaseTimeParameter*>& KeyFrame::getFrameKeys()
{
    return m_frame_keys;
}

inline bool KeyFrame::containsKey(const std::string& key_name)
{
    return utility::MapContains(m_frame_keys,key_name);
}

}
}
#endif // KEYFRAME_H
