#include "state.h"
namespace skyengine
{

namespace utility
{
State::State(StateMachine* context, bool is_instable): m_context(context), m_is_instable(is_instable)
{

}

void State::addNextState(State* state)
{
    m_next_states.push_back(state);
}

void State::setContext(StateMachine* context)
{
    m_context = context;
}

void State::setPrecState(State* prec)
{
    m_prec = prec;
}

void State::setIsInstable(bool is_instable)
{
    m_is_instable = is_instable;
}

bool State::getIsInstable() const
{
    return m_is_instable;
}



}
}
