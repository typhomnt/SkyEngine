#include "animatorinterface.h"


namespace skyengine
{

namespace animation
{
AnimatorInterface::AnimatorInterface() :  m_time_stamp(0.0f), m_start_time(0.0f), m_end_time(0.0f), m_play_speed(0.04f), m_is_looping(true)
{

}

void AnimatorInterface::setSceneGraph(scene::SceneGraph* scene_graph)
{
    m_scene_graph = scene_graph;
}



void AnimatorInterface::setTimeStamp(float time)
{
    m_time_stamp = time;
}

void AnimatorInterface::setStartTime(float time)
{
    m_start_time = time;
}

void AnimatorInterface::setEndTime(float time)
{
    m_end_time = time;
}

void AnimatorInterface::setPlaySpeed(float speed)
{
    m_play_speed = speed;
}

void AnimatorInterface::setIsLooping(bool is_looping)
{
    m_is_looping = is_looping;
}

void AnimatorInterface::stopAnimation(core::SkyEngine* play_engine)
{
    play_engine->removeTask("Animator");
}

}
}
