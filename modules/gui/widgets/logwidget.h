#ifndef __LOG_WIDGET_H__
#define __LOG_WIDGET_H__

#include "skywidget.h"


namespace module
{

namespace gui
{

class LogWidget : public SkyWidget
{
public:
    LogWidget(const std::string& name, SkyWindow* window, std::shared_ptr<ActiveObjectEvent> act_evt, std::shared_ptr<SelectObjectEvent> select_evt, ImGuiWindowFlags flags = 0);

    void draw() override;

private:
    ImGuiTextFilter m_filter;

    void clear();
};

} // namespace gui

} // namespace module

#endif // __LOG_WIDGET_H__
