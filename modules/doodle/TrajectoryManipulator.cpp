#include "TrajectoryManipulator.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/norm.hpp>
#include "Utils/utility.h"
#include "render/defaultMesh.h"
#include "scene/scenegraphmanager.h"
#include "Utils/actiongeneratorutility.h"
#include "doodleutils.h"
#include <time.h>



namespace module {
namespace doodle {

TrajectoryManipulator::TrajectoryManipulator(skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D> *node, ext_dvc::slate::SlateManager* manager) :
    ext_dvc::slate::SlateManagerListener(manager,false),
    m_node(node)
{
}

TrajectoryManipulator::~TrajectoryManipulator()
{
}

//Trajectory
void TrajectoryManipulator::listened(const ext_dvc::slate::HardwareEvent* e)
{
    /* if (e->hardwareEventType() == 3) {
        m_enabled = !m_enabled;
        if (m_enabled) {
            {
                std::unique_lock<std::mutex> lock(m_node->trajectory()->getMutex());
                unsigned int count = m_node->trajectory()->GetSize();
                m_node->trajectory()->RemovePoints();
                m_node->trajectory()->NotifyKeyframesRemoved(0, count - 1);

                m_start_timestamp = QDateTime::currentMSecsSinceEpoch();
            }
            return true;
        } else {
            m_node->trajectory()->NotifyTrajectoryFinished();
        }
    }
    return false;*/
}

void TrajectoryManipulator::listened(const ext_dvc::slate::Object3DEvent* e)
{
    /*if (m_enabled) {
       AABBox b = SlateManagerListener::target_->active_bounding_box();

        float actualBoxSize = 50.0f;
        glm::vec3 p = actualBoxSize * e->position();

        float rot_x = M_PI / 2.0 -M_PI * e->rotation()[0] / 180.f;
        float rot_y = M_PI-M_PI * e->rotation()[1] / 180.f;
        {
            std::unique_lock<std::mutex> lock(m_node->trajectory()->getMutex());
            uint t = QDateTime::currentMSecsSinceEpoch();

            m_node->trajectory()->AddPoint(Pose(Vector(p.y, actualBoxSize - p.z, actualBoxSize - p.x), Vector(rot_x, rot_y, 0.f),(float) (t - m_start_timestamp)));
        }
        m_node->trajectory()->NotifyKeyframesAdded(m_node->trajectory()->GetSize() - 1, m_node->trajectory()->GetSize() - 1);
        return true;
    }
    return false;*/
}
//DOODLE

DoodleManipulator::DoodleManipulator(skyengine::animation::GameObjectSkinned *node, skyengine::core::SkyEngine * engine, skyengine::animation::SkeletalAnimator *animator, ext_dvc::slate::SlateManager *manager):
    ext_dvc::slate::SlateManagerListener(manager,false),
    m_node(node),
    m_doodle_manager(std::make_shared<motionDoodle::MotionManager>(new motionDoodle::MotionSegmenter(SLATE_MEAN_FREQ))),
    m_engine(engine),
    m_animator(animator),
    m_path_animation(skyengine::ressource::Ressource::Create<skyengine::animation::GameObjectAnimation>("doodle_path",dynamic_cast<skyengine::scene::GameObject3D*>(m_node),m_node->getGraphOwner())),
    m_doodle_scenario(std::make_shared<motionDoodle::MotionScenario>()),
    m_no_clamp(false),
    m_drawing_mode(false),
    m_last_played_action(""),
    m_last_accepted_time(0.0f),
    m_last_event_time(0.0f),
    m_current_freq(0.0f),
    m_action_count(0),
    m_drawing_node(std::make_shared<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>>("drawing doodle",
                                                                                                          /*dynamic_cast<skyengine::render::DefaultMesh*>( std::dynamic_pointer_cast<skyengine::scene::GameObject3D>(
                                                                                                           skyengine::scene::SceneGraphManager::Get("default_scene")->getNode("Cube-frame"))->getMesh().get())*/
                                                                                                          skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>::LINES)),
    m_frame_node(std::make_shared<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>>("frame doodle",
                                                                                                          dynamic_cast<skyengine::render::DefaultMesh*>(std::dynamic_pointer_cast<skyengine::scene::GameObject3D>(
                                                                                                          animator->getSceneGraph()->getNode("Cube-frame"))->getMesh().get())
                                                                                                         )),
    m_last_pushed_nbr(0),
    m_doodle_machine(DoodleMachine(this)),
    m_is_in_cycle(false)
{   
    m_animator->getSceneGraph()->addNode("drawing doodle",m_drawing_node);
    m_animator->getSceneGraph()->addNode("frame doodle",m_frame_node);
    m_frame_node->setMaterial(std::dynamic_pointer_cast<skyengine::scene::GameObject3D>(
                                  animator->getSceneGraph()->getNode("Cube-frame"))->getMaterial());
    m_animator->setStartTime(m_engine->getTotalTime());
    m_animator->setIsLooping(false);
    std::shared_ptr<skyengine::scene::GameObject3D> cube = std::make_shared<skyengine::scene::GameObject3D>("doodle_ground_cube");
    cube->setMesh(skyengine::geometry::CubePrimitive(0.2f));
    m_ground_cube = cube.get();
    skyengine::physics::PhysicsManager::GetSingleton()->AddBoxCollider(m_ground_cube,0.2f*glm::vec3(1.0f,1.0f,1.0f),m_ground_cube->computeMassCenter<skyengine::render::vertex3D>(),1.0f);
    m_animator->getSceneGraph()->addNode(cube);
    m_ground_cube->setMaterial(skyengine::ressource::Ressource::Create<skyengine::ressource::PhongMaterial>("phy_mat",
                                                                                                            glm::vec4(0,0,0,0),
                                                                                                            glm::vec4(1,0,0,1),
                                                                                                            glm::vec4(1,0,0,1),
                                                                                                            glm::vec4(1,1,1,1),
                                                                                                            0.2f));
    m_doodle_machine.setCurrentState(std::make_shared<InitState>(&m_doodle_machine));

}

void DoodleManipulator::ImportActions(const std::string& action_path)
{
    m_doodle_scenario->LoadMotionStyleSheetFromFile(action_path,"style_sheet");
    m_doodle_manager->addStyleSheetAction(m_doodle_scenario->getMotionStyleSheetRef());
    //Properties
    m_doodle_scenario->getMotionStyleSheetRef().getActionByName("Armature|Walk_final").setProcessFunction(motionDoodle::utility::flattenSpaceTimeCurve);
    motionDoodle::utility::setWalkActionCondition(m_doodle_scenario->getMotionStyleSheetRef().getActionByName("Armature|Walk_final"));
    motionDoodle::utility::setRunActionCondition(m_doodle_scenario->getMotionStyleSheetRef().getActionByName("Armature|Run"));
    m_doodle_scenario->getMotionStyleSheetRef().getActionByName("Armature|Jump").setProcessFunction(motionDoodle::utility::computeSplineJump);
}

void DoodleManipulator::LoadCurveFile(const char* file_name)
{
    m_drawing_node->clearTrajectory();
    m_frame_node->clearTrajectory();
    m_doodle_curve = motionDoodle::actionGeneratorUtility::LoadActionCurve(file_name);
    for(skyengine::animation::KeyPose3D& point : DoodleTrajToAnimationTraj(m_doodle_curve,glm::vec3(EPS2,0,0)).GetPoints())
    {
        m_drawing_node->AddPoint(skyengine::animation::KeyPose3D(skyengine::animation::Pose3D(glm::vec3(1,1,1),glm::quat(point.value.rotation),point.value.position),point.time));
        m_frame_node->AddPoint(skyengine::animation::KeyPose3D(skyengine::animation::Pose3D(glm::vec3(1,1,1),glm::quat(point.value.rotation),point.value.position),point.time));
    }
}

void DoodleManipulator::SaveCurve(const char* file_name)
{
    if(!m_drawing_mode)
        motionDoodle::actionGeneratorUtility::SaveActionCurve(m_doodle_manager->getMotionSegmenter()->getMotionCurve(),file_name);
    else
        motionDoodle::actionGeneratorUtility::SaveActionCurve(m_doodle_curve,file_name);
}

void DoodleManipulator::listened(const ext_dvc::slate::HardwareEvent* e)
{
    if (e->hardwareEventType() == 2)
        m_animator->clear();
    if (e->hardwareEventType() == 3)
    {
        ext_dvc::slate::Object3DListener::setListenerActive(!ext_dvc::slate::Object3DListener::isListenerActive);
    }
        if(ext_dvc::slate::Object3DListener::isListenerActive)
        {
            m_register_time = m_engine->getGLFWTime();// - m_last_event_time;
            m_doodle_machine.setCurrentState(std::make_shared<InitState>(&m_doodle_machine));
            ManageMachine();
        }

}

void DoodleManipulator::listened(const ext_dvc::slate::Object3DEvent* e)
{
    if (ext_dvc::slate::Object3DListener::isListenerActive)
    {
        //Event features----------------------------------
        float time = m_engine->getGLFWTime() - m_register_time;
        float new_freq =  time - m_last_accepted_time;
        m_last_event_time = time;
        m_current_freq = 1.0/new_freq;

        ext_dvc::slate::AABBox b = ext_dvc::slate::SlateManager::GetSingleton()->active_bounding_box();
        float actualBoxSize = 80.0f;
        glm::vec3 p = actualBoxSize * (e->position()) - m_translation_shift;

        //good formula for gameobjects because the ring is horizontal
        float rot_x = M_PI / 2.0 -M_PI * e->rotation()[0] / 180.f;
        float rot_y = M_PI-M_PI * e->rotation()[1] / 180.f;
        //--------------------------------------------------

        //Event treatment-----------------------------------
        //We can choose to clamp the event frequency to 180hz
        if(m_last_event_time - m_last_accepted_time > 1.0f/180.0f || m_no_clamp)
        {
            m_last_accepted_time = m_last_event_time;
            m_last_accepted_point = motionDoodle::Vector7D(p.y, actualBoxSize - p.z, actualBoxSize - p.x,rot_y, 0.0f,rot_x,m_last_event_time);
            //manage event
            ManageMachine();
        }
        //End Treatment-------------------------------------------

    }
}

void DoodleManipulator::ManageMachine()
{
    do
    {
        m_doodle_machine.handleCurrentState();
    }while(m_doodle_machine.m_current_state->getIsInstable());
}

void DoodleManipulator::InitState::handleState()
{
    DoodleMachine* machine = dynamic_cast<DoodleMachine*>(m_context);
    machine->m_manipulator->m_last_accepted_time = 0.0f;
    machine->m_manipulator->m_drawing_node->clearTrajectory();
    machine->m_manipulator->m_frame_node->clearTrajectory();
    machine->m_manipulator->m_last_curve.clear();
    machine->m_manipulator->m_animator->clearAnimationCompoEls();
    machine->m_manipulator->m_animator->clearTransitions();
    machine->m_manipulator->m_animator->reset();
    machine->m_manipulator->m_animator->clearAnimatedCompBones();
    machine->m_manipulator->m_animator->clearMotionPaths();
    machine->m_manipulator->m_path_animation->clearPath();
    machine->m_manipulator->m_doodle_manager->ReinitializeManager();
    machine->m_manipulator->m_doodle_curve.clear();
    if(machine->m_manipulator->m_drawing_mode)
        machine->setCurrentState(std::make_shared<Drawing>(machine));
    else
        machine->setCurrentState(std::make_shared<ComputeTransfer>(machine));
}

void DoodleManipulator::ComputeTransfer::handleState()
{
    DoodleMachine* machine = dynamic_cast<DoodleMachine*>(m_context);
    DoodleManipulator* manipulator = machine->m_manipulator;
    manipulator->m_animator->setPlaySpeed((manipulator->m_current_freq*manipulator->m_engine->getDeltaTime())/manipulator->m_current_freq);
    //we accept the event
    if(manipulator->m_doodle_manager->getIsInCycle() ||  manipulator->m_last_accepted_point.time > manipulator->m_animator->getEndTime())
    {
        manipulator->m_doodle_manager->UpdateManager(manipulator->m_last_accepted_point);
        manipulator->m_lasted_pushed_token = manipulator->m_doodle_manager->getMotionSegmenter()->getCurrentState();

        //if new action pushed or in cycle
        if(manipulator->m_doodle_manager->getPushedActionNbr() != manipulator->m_last_pushed_nbr
                && !(manipulator->m_doodle_manager->getLastPushedAction().getActionName() == motionDoodle::MotionManager::unknown_action.getActionName() && manipulator->m_last_played_action.find("unknown")))
        {
            machine->setCurrentState(std::make_shared<NotInCycle>(machine));
        }
        else
        {
            machine->setCurrentState(std::make_shared<InCycle>(machine));
        }
    }
    else
        machine->setCurrentState(std::make_shared<ComputeTransfer>(machine));


}

void DoodleManipulator::PushAnimation::handleState()
{
    DoodleMachine* machine = dynamic_cast<DoodleMachine*>(m_context);
    machine->setCurrentState(std::make_shared<ComputeTransfer>(machine));
}

void DoodleManipulator::InCycle::handleState()
{
    DoodleMachine* machine = dynamic_cast<DoodleMachine*>(m_context);
    DoodleManipulator* manipulator = machine->m_manipulator;
    //m_path_animation->addPoint(new_pose);
    if(manipulator->m_doodle_manager->getIsInCycle())
    {
        skyengine::animation::KeyPose3D new_pose(skyengine::animation::Pose3D(glm::vec3(1,1,1),glm::quat(glm::vec3(manipulator->m_last_accepted_point.vector.gab.y,manipulator->m_last_accepted_point.vector.gab.x,manipulator->m_last_accepted_point.vector.gab.z)),glm::vec3(manipulator->m_last_accepted_point.vector.xyz.x,0,manipulator->m_last_accepted_point.vector.xyz.z)),manipulator->m_last_accepted_point.time);
        manipulator->m_path_animation->addPoint(new_pose);
        manipulator->m_animator->setElementEndTime("doodle_path",manipulator->m_last_accepted_point.time);
        manipulator->m_animator->setElementEndTime(manipulator->m_last_played_action + motionDoodle::utility::typeToString<unsigned int>(manipulator->m_action_count),manipulator->m_last_accepted_point.time);
        manipulator->m_animator->clampToEnd();
        manipulator->m_last_curve.push_back(new_pose);
        manipulator->m_doodle_curve.push_back(manipulator->m_last_accepted_point);
    }
    machine->setCurrentState(std::make_shared<ComputeTransfer>(machine));
}

void DoodleManipulator::NotInCycle::handleState()
{
    DoodleMachine* machine = dynamic_cast<DoodleMachine*>(m_context);
    DoodleManipulator* manipulator = machine->m_manipulator;
    std::string elem_name;
    manipulator->m_doodle_curve.push_back(manipulator->m_last_accepted_point);
    //Action--------------------------------
    manipulator->m_last_pushed_nbr = manipulator->m_doodle_manager->getPushedActionNbr();
    motionDoodle::MotionAction last_pushed_action = manipulator->m_doodle_manager->getLastPushedAction();
    //TODO remove and  find something more generic
    if(manipulator->m_doodle_manager->getLastPushedAction().getActionName() == motionDoodle::MotionManager::unknown_action.getActionName())
    {
        last_pushed_action.setActionName("Armature|Walk_final");
        last_pushed_action.setIsCyclic(true);
        last_pushed_action.setFollowPath(true);
        elem_name = "unknown";
    }
    else
        elem_name = last_pushed_action.getActionName();
    manipulator->m_last_played_action = elem_name;
    manipulator->m_action_count++;
    //----------------------------------------

    //Curve-----------------------------------
    manipulator->m_last_curve.clear();
    skyengine::animation::AnimatedPoseTrajectory last_geo_curve;
    if(last_pushed_action.getIsJumpy())
        last_geo_curve = DoodleTrajToAnimationTraj(last_pushed_action.getMotionCurve(),
                                                   glm::vec3(0,-last_pushed_action.getMotionCurve()[0].vector.xyz.y,
                                                   0));
    else
        last_geo_curve = DoodleTrajToAnimationTraj(last_pushed_action.getMotionCurve());

    /* Smooth
     * std::vector<skyengine::animation::KeyPose3D> smooth_poses;
    skyengine::geometry::SmoothCurve<skyengine::animation::KeyPose3D>(last_geo_curve.GetPoints(),smooth_poses);
    for(unsigned int i  = 0 ; i < smooth_poses.size() ;++i)
        last_geo_curve.SetPoint(i,smooth_poses[i]);*/
    manipulator->m_last_curve = last_geo_curve.GetPoints();
    //---------------------------------------

    //Path and duration----------------------
    float animation_duration = manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getDuration();
    float curve_duration = last_geo_curve.GetEnd().time - last_geo_curve.GetPoint(0).time;
    float final_duration = std::max(curve_duration,animation_duration*0.5f);
    //TODO remove
    if(last_pushed_action.getIsCyclic())
        final_duration = curve_duration;
    manipulator->AdapatUniformKeyFramCurve(last_geo_curve,final_duration);
    //assert(skyengine::math::ApproxEqual((last_geo_curve.GetEnd().time - last_geo_curve.GetStart().time), final_duration,0.001f));
    //animation stage percentages-----------
    float anticipation_per = 0.0f;
    float follow_per = 0.0f;

    if(manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getAnticipationTime() != -1.0f)
    {
        std::pair<float,float> times =  manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getAnticipationStartEnd();
        anticipation_per = (times.second - times.first)/(animation_duration*manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getTickPerSecond());
    }
    if(manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getFollowThroughTime() != -1.0f)
    {
        std::pair<float,float> times =  manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getFollowThroughStartEnd();
        follow_per = (times.second - times.first)/(animation_duration*manipulator->m_node->getAnimationPt(last_pushed_action.getActionName())->getTickPerSecond());
    }
    //animation stage application
    if(last_geo_curve.GetSize() > 2)
    {
        if(anticipation_per != 0.0f)
        {
            float ant_time = last_geo_curve.GetStart().time + anticipation_per*(last_geo_curve.GetEnd().time - last_geo_curve.GetStart().time);
            for(unsigned int i = 1 ; i < last_geo_curve.GetSize() ; ++i)
                if(last_geo_curve.GetPoint(i).time < ant_time)
                    last_geo_curve.GetPoint(i).value = last_geo_curve.GetStart().value;
                else
                    break;
        }
        if(follow_per != 0.0f)
        {
            float follow_time = last_geo_curve.GetEnd().time - follow_per*(last_geo_curve.GetEnd().time - last_geo_curve.GetStart().time);
            for(unsigned int i = last_geo_curve.GetSize() - 2 ; i > 0  ; --i)
                if(last_geo_curve.GetPoint(i).time > follow_time)
                    last_geo_curve.GetPoint(i).value = last_geo_curve.GetEnd().value;
                else
                    break;
        }
    }
    //Final process to be sure that this curve begin at the end of the prec animation
    if(last_geo_curve.GetStart().time < manipulator->m_animator->getEndTime())
    {
        float time_diff = manipulator->m_animator->getEndTime() - last_geo_curve.GetStart().time;
        for(unsigned int i = 0 ; i < last_geo_curve.GetSize() ; ++i)
            last_geo_curve.GetPoint(i).time += time_diff;
    }
    //--------------------------------------
    if(last_pushed_action.getFollowPath())
    {
        manipulator->m_path_animation->setPath(last_geo_curve);
    }
    else
    {
        for(unsigned int i = 0 ; i <  last_geo_curve.GetSize() ; i++)
        {
            //last_geo_curve.GetPoint(0).time = last_geo_curve.GetPoint(i).time;
            manipulator->m_path_animation->addPoint(last_geo_curve.GetPoint(0));
        }
    }
    //----------------------------------------

    //Laban Analysis
    if(motionDoodle::MotionManager::m_laban_traj_analyser->getIsTrained())
    {
        last_pushed_action.setLabanVector(motionDoodle::utility::LabanWordTupleToLabanVector(motionDoodle::MotionManager::m_laban_traj_analyser->Classify(last_pushed_action.getMotionCurve())));
    }
    //Animations
    if(!manipulator->m_animator->containsElement("doodle_path"))
        manipulator->m_animator->addAnimation("doodle_path",last_geo_curve.GetPoint(0).time,last_geo_curve.GetEnd().time,manipulator->m_path_animation.get(),0,std::vector<std::string>{manipulator->m_node->getPSkeleton()->getRootBone()->bone_name});
    manipulator->m_animator->setElementEndTime("doodle_path",manipulator->m_last_accepted_point.time);
    if(!last_pushed_action.getIsCyclic())
    {
        manipulator->m_animator->addAnimation(manipulator->m_last_played_action + motionDoodle::utility::typeToString<unsigned int>(manipulator->m_action_count),last_geo_curve.GetStart().time,last_geo_curve.GetEnd().time,manipulator->m_node->getAnimationPt(last_pushed_action.getActionName()),1,manipulator->createSkelModifier(last_pushed_action,final_duration),true);
    }
    else
        manipulator->m_animator->addAnimation(manipulator->m_last_played_action + motionDoodle::utility::typeToString<unsigned int>(manipulator->m_action_count),last_geo_curve.GetStart().time,last_geo_curve.GetEnd().time,manipulator->m_node->getAnimationPt(last_pushed_action.getActionName()),1,nullptr,true);
    //------------------------------------
    manipulator->m_animator->clampToEnd();
    machine->setCurrentState(std::make_shared<PushAnimation>(machine));

}

void DoodleManipulator::Drawing::handleState()
{
    DoodleMachine* machine = dynamic_cast<DoodleMachine*>(m_context);
    machine->m_manipulator->m_doodle_curve.push_back(machine->m_manipulator->m_last_accepted_point);
    machine->m_manipulator->m_drawing_node->AddPoint(skyengine::animation::KeyPose3D(
                                                         skyengine::animation::Pose3D(
                                                             glm::vec3(1,1,1),
                                                             glm::quat(glm::vec3(machine->m_manipulator->m_last_accepted_point.vector.gab.y,machine->m_manipulator->m_last_accepted_point.vector.gab.x, machine->m_manipulator->m_last_accepted_point.vector.gab.z)),
                                                             glm::vec3(machine->m_manipulator->m_last_accepted_point.vector.xyz.x, machine->m_manipulator->m_last_accepted_point.vector.xyz.y, machine->m_manipulator->m_last_accepted_point.vector.xyz.z)),
                                                         machine->m_manipulator->m_last_accepted_point.time));
    if(machine->m_manipulator->m_frame_node->GetSize() == 0 ||
            machine->m_manipulator->m_last_accepted_point.time - machine->m_manipulator->m_frame_node->GetEnd().time
            > 0.15f)
    machine->m_manipulator->m_frame_node->AddPoint(skyengine::animation::KeyPose3D(
                                                         skyengine::animation::Pose3D(
                                                             glm::vec3(1,1,1),
                                                             glm::quat(glm::vec3(machine->m_manipulator->m_last_accepted_point.vector.gab.y,machine->m_manipulator->m_last_accepted_point.vector.gab.x, machine->m_manipulator->m_last_accepted_point.vector.gab.z)),
                                                             glm::vec3(machine->m_manipulator->m_last_accepted_point.vector.xyz.x, machine->m_manipulator->m_last_accepted_point.vector.xyz.y, machine->m_manipulator->m_last_accepted_point.vector.xyz.z)),
                                                         machine->m_manipulator->m_last_accepted_point.time));

    machine->m_manipulator->m_last_curve.push_back(skyengine::animation::KeyPose3D(
                                                       skyengine::animation::Pose3D(
                                                           glm::vec3(1,1,1),
                                                           glm::quat(glm::vec3(machine->m_manipulator->m_last_accepted_point.vector.gab.y, machine->m_manipulator->m_last_accepted_point.vector.gab.x,machine->m_manipulator-> m_last_accepted_point.vector.gab.z)),
                                                           glm::vec3(machine->m_manipulator->m_last_accepted_point.vector.xyz.x, machine->m_manipulator->m_last_accepted_point.vector.xyz.y, machine->m_manipulator->m_last_accepted_point.vector.xyz.z)),
                                                       machine->m_manipulator->m_last_accepted_point.time));
    machine->setCurrentState(std::make_shared<Drawing>(machine));
}

void DoodleManipulator::UpdateFromDrawing()
{
    //drawing mode to false because machine cannot work otherwise
    m_drawing_mode = false;
    m_doodle_machine.setCurrentState(std::make_shared<InitState>(&m_doodle_machine));
    m_last_curve.clear();
    float time_shift = 0.0f;

    std::vector<motionDoodle::Vector7D> smth_curve;
    skyengine::geometry::HermiteFiltering(m_doodle_curve,smth_curve);

    std::vector<skyengine::animation::KeyPose3D> smth_draw;
    skyengine::geometry::HermiteFiltering(m_drawing_node->GetPoints(),smth_draw);
    m_drawing_node->setPoints(smth_draw);
    m_frame_node->setPoints(smth_draw);

    for(const motionDoodle::Vector7D& vec :smth_curve)
    {
        if(m_animator->getEndTime() > vec.time + time_shift)
            time_shift += m_animator->getEndTime() - (vec.time + time_shift);

        m_last_accepted_time = vec.time + time_shift;

        m_last_accepted_point = motionDoodle::Vector7D(vec.vector.xyz.x,vec.vector.xyz.y,vec.vector.xyz.z,
                                                       vec.vector.gab.x,vec.vector.gab.y,vec.vector.gab.z
                                                       ,m_last_accepted_time);
        ManageMachine();
    }
    m_drawing_mode = true;
}

void DoodleManipulator::setAnimator(skyengine::animation::SkeletalAnimator* animator)
{
    m_animator = animator;
}

std::shared_ptr<skyengine::animation::SkeletalAnimationModifier> DoodleManipulator::createSkelModifier(const motionDoodle::MotionAction& action, float duration)
{
    std::shared_ptr<skyengine::animation::SkeletalAnimationModifier> modifier = std::make_shared<skyengine::animation::SkeletalAnimationModifier>();
    motionDoodle::LabanVector action_lab = action.getLabanVector();
    modifier->setLabanFactors(glm::vec3(action_lab.space,action_lab.time,action_lab.weight));
    modifier->setGlobalDurationFactor(duration);
    return modifier;

}


skyengine::geometry::GeometryCurve<skyengine::animation::TimedParameter<glm::vec3>> DoodleManipulator::DoodleTrajToCurve(const std::vector<motionDoodle::Vector7D>& path, const glm::vec3 pos_shift)
{
    skyengine::geometry::GeometryCurve<skyengine::animation::TimedParameter<glm::vec3>> curve;
    for(const motionDoodle::Vector7D& point : path)
        curve.AddPoint(skyengine::animation::TimedParameter<glm::vec3>(glm::vec3(point.vector.xyz.x,point.vector.xyz.y*(pos_shift != glm::vec3()),point.vector.xyz.z) + pos_shift,point.time));

    return curve;
}


//SCENE

NodeManipulator::NodeManipulator(skyengine::scene::SceneGraphNode *node, ext_dvc::slate::SlateManager *manager):
    SlateManagerListener(manager,false),
    m_node(node)
{

}


void NodeManipulator::listened(const ext_dvc::slate::HardwareEvent* e)
{
    if (e->hardwareEventType() == 3)
        ext_dvc::slate::Object3DListener::setListenerActive(!ext_dvc::slate::Object3DListener::isListenerActive);
}

void NodeManipulator::listened(const ext_dvc::slate::Object3DEvent* e)
{
    if (ext_dvc::slate::Object3DListener::isListenerActive)
    {
        ext_dvc::slate::AABBox b = ext_dvc::slate::SlateManager::GetSingleton()->active_bounding_box();

        float actualBoxSize = 80.0f;
        glm::vec3 p = actualBoxSize * (e->position()) - m_translation_shift;

        float rot_x = M_PI / 2.0 -M_PI * e->rotation()[0] / 180.f;
        float rot_y = M_PI-M_PI * e->rotation()[1] / 180.f;
        {
            m_node->setWorldMatrix(glm::translate(glm::mat4(1.0f),glm::vec3(p.y, actualBoxSize - p.z, actualBoxSize - p.x))*glm::eulerAngleXY(rot_x, rot_y));
        }

    }
}


}

}
