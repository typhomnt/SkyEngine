#include "commons/sampleskyengine.h"

#include "scene/scenegraphmanager.h"

#include "core/functioncalltask.h"
#include "core/manipulator.h"

#include "gui/widgets/logwidget.h"
#include "gui/widgets/nodelist.h"
#include "gui/widgets/resourcelist.h"
#include "gui/widgets/skywindow.h"
#include "gui/widgets/viewport.h"

using namespace skyengine;


class TestEmpty : public samples::SampleSkyEngine
{
public:
    TestEmpty();
    ~TestEmpty();

    void initScene() override;
    void initUserRessources() override;
    void initTasks() override;

private:
    std::shared_ptr<module::gui::GuiManager> m_guiManager;
    std::shared_ptr<module::gui::SkyEditor> m_editor;    
    std::shared_ptr<scene::Camera> m_camera;
};

TestEmpty::TestEmpty() : samples::SampleSkyEngine()
{}

TestEmpty::~TestEmpty()
{}

void TestEmpty::initScene()
{
    auto sg = std::make_shared<scene::SceneGraph>();
    scene::SceneGraphManager::Add("default_scene", sg);
    sg->addNode(m_camera);

    m_guiManager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());
    m_editor = std::make_shared<module::gui::SkyEditor>(
        this,
        m_guiManager
    );

    module::gui::SkyWindow* window = m_editor->getSkyWindow();
    window->addWidget<module::gui::NodeList>(
        "Node List",
        module::gui::SkyWindow::DockSlot::Left,
        nullptr
    );
    window->addWidget<module::gui::ResourceList>(
        "Resources",
        module::gui::SkyWindow::DockSlot::Left
    );
    window->addWidget<module::gui::LogWidget>(
        "Logger",
        module::gui::SkyWindow::DockSlot::Footer
    );

    auto viewport = window->addWidget<module::gui::Viewport>(
        "Viewport",
        module::gui::SkyWindow::DockSlot::Center
    );


}

void TestEmpty::initUserRessources()
{
    m_camera = std::make_shared<scene::Camera>("camera");
    m_camera->setPosition(glm::vec3(-10,3,0));
    m_camera->setDirection(glm::vec3(0,0,1.0));
}

void TestEmpty::initTasks()
{
    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame, m_guiManager));
    m_editor->addTasksToEngine();
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender, m_guiManager));

    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}


int main()
{
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);

    TestEmpty engineInstance;
    engineInstance.init();
    engineInstance.run();

    return 0;
}
