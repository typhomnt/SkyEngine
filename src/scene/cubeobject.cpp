#include "cubeobject.h"

#include "render/Vertex3D.h"


namespace skyengine
{

namespace scene
{

CubeObject::CubeObject(const std::string& name,
                       SceneGraph* owner,
                       float size)
    : GameObject3D(name, owner, Type::GameObject)
    , m_cubeSize(size)
{
    build();
}


void CubeObject::build()
{
    auto pMesh = std::make_shared<ressource::Mesh<render::vertex3D>>("Cube_" + m_name);
    m_mesh = pMesh;

    // Points
    float hSize = 0.5f * m_cubeSize;
    glm::vec3 P0( hSize, -hSize, -hSize);
    glm::vec3 P1( hSize,  hSize, -hSize);
    glm::vec3 P2( hSize,  hSize,  hSize);
    glm::vec3 P3( hSize, -hSize,  hSize);
    glm::vec3 P4(-hSize, -hSize, -hSize);
    glm::vec3 P5(-hSize,  hSize, -hSize);
    glm::vec3 P6(-hSize,  hSize,  hSize);
    glm::vec3 P7(-hSize, -hSize,  hSize);

    // Normals
    glm::vec3 Nf( 1.0f,  0.0f,  0.0f);
    glm::vec3 Nl( 0.0f, -1.0f,  0.0f);
    glm::vec3 Nb(-1.0f,  0.0f,  0.0f);
    glm::vec3 Nr( 0.0f,  1.0f,  0.0f);
    glm::vec3 Nt( 0.0f,  0.0f,  1.0f);
    glm::vec3 Nd( 0.0f,  0.0f, -1.0f);

    // UV
    // TODO : chose one type of cube texture to compute correctly UVs
    glm::vec2 UV;

    // Front face
    pMesh->addVertex(render::vertex3D(P0, UV, Nf));
    pMesh->addVertex(render::vertex3D(P1, UV, Nf));
    pMesh->addVertex(render::vertex3D(P2, UV, Nf));
    pMesh->addVertex(render::vertex3D(P3, UV, Nf));
    pMesh->addIndex(0);
    pMesh->addIndex(1);
    pMesh->addIndex(2);
    pMesh->addIndex(0);
    pMesh->addIndex(2);
    pMesh->addIndex(3);

    // Left face
    pMesh->addVertex(render::vertex3D(P4, UV, Nl));
    pMesh->addVertex(render::vertex3D(P0, UV, Nl));
    pMesh->addVertex(render::vertex3D(P3, UV, Nl));
    pMesh->addVertex(render::vertex3D(P7, UV, Nl));
    pMesh->addIndex(4);
    pMesh->addIndex(5);
    pMesh->addIndex(6);
    pMesh->addIndex(4);
    pMesh->addIndex(6);
    pMesh->addIndex(7);

    // Behind face
    pMesh->addVertex(render::vertex3D(P5, UV, Nb));
    pMesh->addVertex(render::vertex3D(P4, UV, Nb));
    pMesh->addVertex(render::vertex3D(P7, UV, Nb));
    pMesh->addVertex(render::vertex3D(P6, UV, Nb));
    pMesh->addIndex(8);
    pMesh->addIndex(9);
    pMesh->addIndex(10);
    pMesh->addIndex(8);
    pMesh->addIndex(10);
    pMesh->addIndex(11);

    // Right face
    pMesh->addVertex(render::vertex3D(P1, UV, Nr));
    pMesh->addVertex(render::vertex3D(P5, UV, Nr));
    pMesh->addVertex(render::vertex3D(P6, UV, Nr));
    pMesh->addVertex(render::vertex3D(P2, UV, Nr));
    pMesh->addIndex(12);
    pMesh->addIndex(13);
    pMesh->addIndex(14);
    pMesh->addIndex(12);
    pMesh->addIndex(14);
    pMesh->addIndex(15);

    // Top face
    pMesh->addVertex(render::vertex3D(P3, UV, Nt));
    pMesh->addVertex(render::vertex3D(P2, UV, Nt));
    pMesh->addVertex(render::vertex3D(P6, UV, Nt));
    pMesh->addVertex(render::vertex3D(P7, UV, Nt));
    pMesh->addIndex(16);
    pMesh->addIndex(17);
    pMesh->addIndex(18);
    pMesh->addIndex(16);
    pMesh->addIndex(18);
    pMesh->addIndex(19);

    // Bottom face
    pMesh->addVertex(render::vertex3D(P4, UV, Nd));
    pMesh->addVertex(render::vertex3D(P5, UV, Nd));
    pMesh->addVertex(render::vertex3D(P1, UV, Nd));
    pMesh->addVertex(render::vertex3D(P0, UV, Nd));
    pMesh->addIndex(20);
    pMesh->addIndex(21);
    pMesh->addIndex(22);
    pMesh->addIndex(20);
    pMesh->addIndex(22);
    pMesh->addIndex(23);
}

std::shared_ptr<rapidjson::Value> CubeObject::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = GameObject3D::SerializeProp(al);
    value->AddMember(rapidjson::Value("Size", al), rapidjson::Value(m_cubeSize), al);

    return value;
}

void CubeObject::LoadProp(const rapidjson::Value& value)
{
    GameObject3D::LoadProp(value);
    m_cubeSize = value["Size"].GetFloat();
}

} // namespace scene

} // namespace skyengine
