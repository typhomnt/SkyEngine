#ifndef MODELIMPORTERMANAGER_H
#define MODELIMPORTERMANAGER_H

#include "utils/skyengine_dll.h"

#include <string>
#include "utils/singleton.h"
#include "animation/gameobjectskinned.h"
#include "animation/animationmanager.h"
#include "scene/scenegraph.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

namespace skyengine
{

namespace core
{



enum LoadMode {NORMAL_3D, SKINNED_3D};
//TODO Singleton
class SKYENGINE_API ModelImporterManager : public utility::Singleton<ModelImporterManager>
{
public:
    friend ModelImporterManager* Singleton<ModelImporterManager>::GetSingleton();

    ModelImporterManager(const ModelImporterManager&) = delete;
    ModelImporterManager& operator=(const ModelImporterManager&) = delete;

    ~ModelImporterManager();

    void setSceneGraph(scene::SceneGraph* scene_graph);
    void loadModel(const std::string& model_path, scene::SceneGraph *scene_graph = nullptr, scene::SceneGraphNode *parent_node = nullptr);
    inline scene::SceneGraph* getSceneGraph();

    void ExportScene();

private:

    Assimp::Importer importer;
    aiScene* ai_scene;
    scene::SceneGraph* m_scene_graph;



    void processNode(aiNode* node, aiScene* scene, const std::string &model_path,scene::SceneGraphNode* parent_node = nullptr);
    void skeletonCleanProcess();
    void processAnimations(aiScene* scene);
    void processAnimation(aiAnimation* animation);
    void processMesh(aiMesh* mesh, const aiScene* scene, const std::string &mesh_name, aiNode* node, const LoadMode& loadMode, std::shared_ptr<scene::GameObject3D> object);
    void processTextures(const aiScene* scene);

    void loadMaterialTextures(aiMaterial* mat, aiTextureType type, bool &has_texture,std::shared_ptr<scene::GameObject3D> object);

    glm::mat4 convertAiMat4ToGLMMat4(const aiMatrix4x4 &mat4);
    glm::vec3 convertAiVector3DToGLMVec3(const aiVector3D& vec3);
    glm::quat convertAiQuaternionToGLMQuat(const aiQuaternion& quat);

    void BuildSkeleton(const aiScene *scene, const std::vector<animation::Bone3D> &bones, std::shared_ptr<animation::Skeleton> skel_to_build);
    animation::GameObjectSkinned *FindBoneInSkeletons(const std::string& bone_name);
    bool FindBoneInSkeleton(const std::string bone_name, const animation::GameObjectSkinned& object);
    void fillBoneHierarchy(aiNode* node, std::unordered_map<std::string, std::shared_ptr<animation::Bone3D> > &bone_mapping, animation::Skeleton *skel_to_build);

    ModelImporterManager();
    ModelImporterManager(scene::SceneGraph* scene_graph);
};

    inline scene::SceneGraph* ModelImporterManager::getSceneGraph()
    {
        return m_scene_graph;
    }

}
}

#endif // MODELIMPORTERMANAGER_H
