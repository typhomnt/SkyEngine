#ifndef QUITLISTENER_H
#define QUITLISTENER_H

#include "core/eventlisteners/windoweventlistener.h"
#include "core/eventlisteners/userinputlistener.h"

namespace skyengine {

namespace core {
    class SkyEngine;
}

namespace samples {

class QuitListener : public core::WindowEventListener, public core::UserInputListener
{
public:
    QuitListener(
            core::SkyEngine* engine,
            std::shared_ptr<core::WindowEventManager> wem,
            std::shared_ptr<core::UserInputManager> uim) :
        core::WindowEventListener(wem),
        core::UserInputListener(uim),
        engine_(engine)
    {}

    virtual ~QuitListener(){}

    virtual void listened(const core::CloseEvent*);
    virtual void listened(const core::KeyEvent*);

    virtual void onClose();

protected:
    core::SkyEngine* engine_;
};

}
}

#endif // QUITLISTENER_H
