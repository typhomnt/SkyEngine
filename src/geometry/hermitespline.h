#ifndef HERMITESPLINE_H
#define HERMITESPLINE_H
#include <vector>
namespace skyengine
{

namespace geometry
{
template<typename Point_T>
class HermiteSpline
{
public:
    HermiteSpline(const Point_T& st_point = Point_T(),
                  const Point_T& ed_point = Point_T(),
                  const Point_T& st_tangent = Point_T(),
                  const Point_T& ed_tangent = Point_T())
        :
          m_st_point(st_point),
          m_ed_point(ed_point),
          m_st_tangent(st_tangent),
          m_ed_tangent(ed_tangent) {}

    inline Point_T Evaluate(float s)
    {
        float s2 = s*s;
        float s3 = s2*s;
        return (2.0f*s3 - 3.0f*s2 + 1.0f)*m_st_point
                + (s3 - 2.0f*s2 + s)*m_st_tangent
                + (s3- s2)*m_ed_tangent
                + (-2.0f*s3 + 3.0f*s2)*m_ed_point;
    }

    std::vector<Point_T> ComputePointSpline(unsigned int nb_samples)
    {
        std::vector<Point_T> points;
        for(unsigned int i = 0 ; i < nb_samples ; ++i)
            points.push_back(Evaluate(static_cast<float>(i)/static_cast<float>(nb_samples - 1)));
        return points;
    }


protected:
    Point_T m_st_point;
    Point_T m_ed_point;
    Point_T m_st_tangent;
    Point_T m_ed_tangent;
};
}
}
#endif // HERMITESPLINE_H
