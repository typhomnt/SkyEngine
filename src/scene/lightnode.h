#ifndef __LIGHTNODE_H__
#define __LIGHTNODE_H__

#include "gameobject3d.h"

#include <memory>

#include <glm/glm.hpp>

#include "scenegraph.h"
#include "../ressources/lights.h"


namespace skyengine
{

namespace scene
{

class LightNode : public GameObject3D
{
public:
    LightNode(
        const std::string& name,
        SceneGraph* owner = nullptr,
        unsigned int type = Type::GameObject,
        const glm::mat4& worldMatrix = glm::mat4(1.0f),
        const std::shared_ptr<ressource::Light>& light = nullptr
    );

    const std::shared_ptr<ressource::Light>& getLight() const { return m_light; }

    void setLight(const std::shared_ptr<ressource::Light>& light) { m_light = light; }

    std::string getTag() override { return "LightNode"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);

private:
    std::shared_ptr<ressource::Light> m_light;

    void LoadProp(const rapidjson::Value& value) override;
};

} // namespace scene

} // namespace skyengine

#endif // __LIGHTNODE_H__
