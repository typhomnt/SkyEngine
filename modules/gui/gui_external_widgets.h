#ifndef GUI_EXTERNAL_WIDGETS_H
#define GUI_EXTERNAL_WIDGETS_H
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <functional>

// original source of timeline code is
// https://github.com/nem0/LumixEngine
//
// modified further according to imgui issue 76
// and adding a panzoomer
// https://github.com/ocornut/imgui/issues/76
//

namespace ImGui
{

bool BeginTimeline(const char* str_id, float pixel_offset, float max_value, int num_visible_rows);
bool TimelineEvent(const char* str_id, float &val1, float &val2,unsigned int layer =0);
void EndTimeline(int num_vertical_grid_lines, double & time_in, double & time_out);


} // ImGui



#endif // GUI_EXTERNAL_WIDGETS_H
