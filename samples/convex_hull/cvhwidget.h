#ifndef __CVHWIDGET_H__
#define __CVHWIDGET_H__

#include "gui/widgets/skywidget.h"


class CVHWidget final : public module::gui::SkyWidget
{
public:
    CVHWidget(
        const std::string& name,
        module::gui::SkyWindow* window,
        std::shared_ptr<module::gui::ActiveObjectEvent> aoEvent,
        std::shared_ptr<module::gui::SelectObjectEvent> soEvent,
        skyengine::scene::SceneGraph* sceneGraph,
        ImGuiWindowFlags flags = 0
    );

    void draw() override;

private:
    skyengine::scene::SceneGraph* m_sceneGraph;
    int m_meshIndex;
    skyengine::render::Renderable* m_currentMesh;
};

#endif // __CVHWIDGET_H__
