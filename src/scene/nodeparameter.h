#ifndef OBJECTPARAMETER_H
#define OBJECTPARAMETER_H
#include <string>
namespace skyengine
{

namespace scene
{


///
/// \brief The baseObjectParameter struct
///
struct baseNodeParameter
{
    baseNodeParameter(const std::string& name, unsigned int size=1) : name(name), size(size) {}
    virtual ~baseNodeParameter(){}

    std::string name;
    unsigned int size;
    //virtual std::string getValueString() = 0;

};


///
/// \brief The ObjectParameter struc defines a template struct containing a value associated with a variable name
///
template<typename T>
struct NodeParameter: public baseNodeParameter
{
    T value;
    //std::string getValueString(){return std::to_string(value);}
    NodeParameter(const std::string& name, unsigned int size=1) :baseNodeParameter(name,size) {}
    NodeParameter(const std::string& name, unsigned int size, const T& val) :baseNodeParameter(name,size),value(val) {}
};
}
}
#endif // OBJECTPARAMETER_H
