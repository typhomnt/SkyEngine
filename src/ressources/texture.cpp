#include "texture.h"

#include <cassert>

#include "ressourcemanager.h"
#include "utils/glcheck.h"
#include "utils/logger.h"
#include "utils/utilities.h"

namespace skyengine
{
namespace ressource
{

Texture::Texture(const std::string& name, GLenum target)
    : Ressource(name)
    , m_texID(0)
    , m_target(target)
    , m_embed_file("")
{
    glGenTextures(1, &m_texID);
    assert(m_texID != 0);
}

Texture::Texture(const std::string& name, GLenum internal_format,
                 GLenum format,
                 GLenum type,
                 unsigned int width,
                 unsigned int height)
    : Ressource(name)
    , m_texID(0)
    , m_target(GL_TEXTURE_2D)
    , m_internal_format(internal_format)
    , m_format(format)
    , m_type(type)
    , m_width(width)
    , m_height(height)
    , m_embed_file("")
{
    GLCHECK(glGenTextures(1, &m_texID));
    assert(m_texID != 0);

    glBindTexture(m_target, m_texID);
    //TODO Propose  GL_CLAMP_TO_EDGE/GL_REPEAT choice.
    glTexParameteri(m_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(m_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GLCHECK(glTexImage2D(m_target, 0, internal_format, width, height, 0, format, type, nullptr));
}

Texture::Texture(const std::string& name, const std::string& texFileName, GLenum target)
    : Ressource(name)
    , m_texID(0)
    , m_target(target)
    , m_embed_file("")
{
    glGenTextures(1, &m_texID);
    assert(m_texID != 0);

    loadFromFile(texFileName);
}

Texture::Texture(const std::string& name,
                 const std::shared_ptr<Image>& image,
                 GLenum target)
    : Ressource(name)
    , m_texID(0)
    , m_target(target)
    , m_embed_file("")
{
    glGenTextures(1, &m_texID);
    assert(m_texID != 0);

    loadFromImage(image->getName());
}

void Texture::loadFromImage(const std::shared_ptr<Image>& image)
{
    // Load texture
    const unsigned char* data = image->getData();
    GLenum dataFormat = image->getDataFormat();

    GLCHECK(glBindTexture(m_target, m_texID));
    GLCHECK(glTexParameteri(m_target, GL_TEXTURE_WRAP_S, GL_REPEAT));
    GLCHECK(glTexParameteri(m_target, GL_TEXTURE_WRAP_T, GL_REPEAT));
    GLCHECK(glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GLCHECK(glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

    switch (m_target)
    {
        case GL_TEXTURE_2D:
            GLCHECK(glTexImage2D(m_target,
                                 0,
                                 dataFormat,
                                 image->getWidth(),
                                 image->getHeight(),
                                 0,
                                 dataFormat,
                                 GL_UNSIGNED_BYTE,
                                 data));
            m_internal_format = dataFormat;
            m_format = dataFormat;
            m_type = GL_UNSIGNED_BYTE ;
            m_width = image->getWidth();
            m_height = image->getHeight();
        break;
        default:
            break;
    }
}

void Texture::loadFromImage(const std::string& imageName)
{
    auto resource = RessourceManager::Get(imageName);
    if (!resource)
    {
        utility::Logger::Error("Texture::loadFromImage : invalid image name");
        return;
    }

    auto image = std::dynamic_pointer_cast<Image>(resource);
    loadFromImage(image);
}

void Texture::loadFromFile(const std::string& fileName)
{
    // Create image and add it to resources
    std::shared_ptr<Image> image = Ressource::Create<Image>(
        m_name + std::string("_image"),
        fileName
    );

    // We assume the image is strongly linked with the texture.
    // The image will be created when loading texture.
    image->setSerializable(false);

    loadFromImage(image);

    m_embed_file = fileName;
}

void Texture::bind(GLenum textureUnit)
{
    GLCHECK(glActiveTexture(textureUnit));
    GLCHECK(glBindTexture(m_target, m_texID));
}

void Texture::unbind()
{
    GLCHECK(glBindTexture(m_target, 0));
}

Texture::~Texture()
{
    if (m_texID)
    {
        GLCHECK(glDeleteTextures(1, &m_texID));
        m_texID = 0;
    }
}

std::shared_ptr<rapidjson::Value> Texture::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Ressource::SerializeProp(al);

    if (m_embed_file.empty())
    {
        value->AddMember(rapidjson::Value("InternalFormat", al), rapidjson::Value(m_internal_format), al);
        value->AddMember(rapidjson::Value("Format", al), rapidjson::Value(m_format), al);
        value->AddMember(rapidjson::Value("Type", al), rapidjson::Value(m_type), al);
        value->AddMember(rapidjson::Value("Width", al), rapidjson::Value(m_width), al);
        value->AddMember(rapidjson::Value("Height", al), rapidjson::Value(m_height), al);

        // TODO : how to serialize data ?
    }
    else
    {
        // Parameters are "included" in file (width, height ...)
        // No need to serialize them.
        utility::JSONHelper::SaveString(*value, "File", m_embed_file, al);
    }

    return value;
}

bool Texture::LoadProp(const rapidjson::Value& value)
{
    if (value.HasMember("File"))
    {
        // All parameters will be filled while loading file.
        loadFromFile(value["File"].GetString());
    }
    else
    {
        m_internal_format = static_cast<GLenum>(value["InternalFormat"].GetUint());
        m_format = static_cast<GLenum>(value["Format"].GetUint());
        m_type = static_cast<GLenum>(value["Type"].GetUint());
        m_width = value["Width"].GetUint();
        m_height = value["Height"].GetUint();

        // TODO : how to load data ?
    }

    return false;
}

} // namespace ressource
} // namespace skyengine
