#ifndef ABSTRACTCURVE_H
#define ABSTRACTCURVE_H

#include "utils/skyengine_dll.h"
namespace skyengine
{

namespace geometry
{

class SKYENGINE_API AbstractCurve
{
public:
    AbstractCurve();
    ~AbstractCurve();

    virtual unsigned int GetSize() const = 0;
};

}
}

#endif // ABSTRACTCURVE_H
