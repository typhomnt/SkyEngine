#ifndef ANIMATORINTERFACE_H
#define ANIMATORINTERFACE_H
#include "scene/scenegraphnode.h"
#include "core/skyengine.h"
namespace skyengine
{

namespace animation
{
class AnimatorInterface
{
public:
    AnimatorInterface();

    void setSceneGraph(scene::SceneGraph* scene_graph);
    inline scene::SceneGraph* getSceneGraph();
    virtual void playAnimation(core::SkyEngine* play_engine) = 0;
    virtual void stopAnimation(core::SkyEngine* play_engine);
    virtual void computeAnimation(float time_stamp) = 0;

    void setTimeStamp(float time);
    void setStartTime(float time);
    void setEndTime(float time);
    void setPlaySpeed(float speed);
    void setIsLooping(bool is_looping);
    inline float getTimeStamp() const;
    inline float getStartTime() const;
    inline float getEndTime() const;
    inline float getPlaySpeed() const;
    inline bool getIsLooping() const;

protected:

    scene::SceneGraph* m_scene_graph;

    float m_time_stamp;
    float m_start_time;
    float m_end_time;
    float m_play_speed;
    bool m_is_looping;

    virtual void play() = 0;

};



inline float AnimatorInterface::getTimeStamp() const
{
    return m_time_stamp;
}

inline float AnimatorInterface::getStartTime() const
{
    return m_start_time;
}

inline float AnimatorInterface::getEndTime() const
{
    return m_end_time;
}

inline float AnimatorInterface::getPlaySpeed() const
{
    return m_play_speed;
}

inline bool AnimatorInterface::getIsLooping() const
{
    return m_is_looping;
}

inline scene::SceneGraph* AnimatorInterface::getSceneGraph()
{
    return m_scene_graph;
}



}
}
#endif // ANIMATORINTERFACE_H
