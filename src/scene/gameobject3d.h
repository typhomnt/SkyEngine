#ifndef GAMEOBJECT3D_H
#define GAMEOBJECT3D_H

#include "utils/skyengine_dll.h"
#include "../render/renderable.h"

#include <iostream>

#include "ressources/material.h"
#include "utils/utilities.h"

#include "core/updateable.h"

namespace skyengine
{

namespace scene
{

class SKYENGINE_API GameObject3D : public render::Renderable, public core::Updatable
{
public:
    GameObject3D(const std::string& name, scene::SceneGraph *owner = nullptr,
                 const unsigned int type = Type::GameObject,
                 const glm::mat4& world_matrix = glm::mat4(1.0f));
    ~GameObject3D();

    void addRenderShaderUniformVal(const std::string &param_name, baseNodeParameter* uniform_val);

    template<typename uniform_type>
    void updateRenderShaderUniformVal(baseNodeParameter* uniform_val)
    {
        bool updated = false;
        for(auto& parameter : render_shader_uniform_values)
        {
            if(parameter.second->name == uniform_val->name)
            {
                dynamic_cast<NodeParameter<uniform_type>*>(parameter.second)->value = dynamic_cast<NodeParameter<uniform_type>*>(uniform_val)->value;
                updated = true;
            }
        }
        if(!updated)
            std::cout << "Uniform value: " << uniform_val->name << " does not exist, please use add function instead" << std::endl;
    }

    void setMaterial(const std::shared_ptr<ressource::Material>& material);
    void setShaderUniforms();
    void setDefaultShaderUniforms();
    void setWorldMatrix(const glm::mat4& world_matrix);    

    inline std::shared_ptr<ressource::Material> getMaterial();

    //TODO template and use position() function
    template<typename Point_T>
    glm::vec3 computeMassCenter() const
    {
        glm::vec3 center;
        std::shared_ptr<ressource::Mesh<Point_T> > mesh = std::dynamic_pointer_cast<ressource::Mesh<Point_T>>(m_mesh);
        if( mesh != nullptr)
        {
           for(const Point_T& vertex : mesh->vertices())
               center += vertex.position();
           center /= mesh->vertices().size();
        }
        return center;
    }
    virtual std::shared_ptr<SceneGraphNode> Duplicate();
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);
    virtual std::string getTag() {return "GmObj";}

protected:
    std::shared_ptr<ressource::Material> m_material;

    bool uniformValueAlreadyExist(const std::string& parameter_name) const;

    void LoadProp(const rapidjson::Value& value) override;
};

inline std::shared_ptr<ressource::Material> GameObject3D::getMaterial()
{
    return m_material;
}

}
}

#endif // GAMEOBJECT3D_H
