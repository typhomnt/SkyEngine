#ifndef META_H
#define META_H

#include <type_traits>


namespace skyengine {
namespace utility {

template<class T0, class T1>
struct TypePair
{
    using type_first = T0;
    using type_second = T1;
};



template<class T, class Cmp0, class Cmp1, class ...Ts>
struct TypeSwitch
{
    using type = typename std::conditional<std::is_same<T, typename Cmp0::type_first>::value, typename Cmp0::type_second, TypeSwitch<T, Cmp1, Ts...>>::type;
};

template<class T, class Cmp, class Default>
struct TypeSwitch<T, Cmp, Default>
{
    using type = typename std::conditional<std::is_same<T, typename Cmp::type_first>::value, typename Cmp::type_second, Default>::type;
};

}
}


#endif // META_H
