#include "utilities.h"

#include <cstdio>
#include <string>

#if defined(_MSC_VER)
#include <windows.h>
#endif // defined(_MSC_VER)

#include "math/mathutils.h"
#include "animation/vertex3dskin.h"
#include "ressources/pointcloud.h"
#include "utils/instancetransforms.h"


namespace skyengine
{

namespace utility
{


bool getSimilarVertexIndex(
        glm::vec3 & in_vertex,
        glm::vec2 & in_uv,
        glm::vec3 & in_normal,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals,
        unsigned short & result
        )
{
    // Lame linear search
    for ( unsigned int i=0; i<out_vertices.size(); i++ ){
        if (
                math::ApproxEqual(in_vertex.x, out_vertices[i].x, 0.001f) &&
                math::ApproxEqual(in_vertex.y, out_vertices[i].y, 0.001f) &&
                math::ApproxEqual(in_vertex.z, out_vertices[i].z, 0.001f) &&
                math::ApproxEqual(in_uv.x    , out_uvs     [i].x, 0.001f) &&
                math::ApproxEqual(in_uv.y    , out_uvs     [i].y, 0.001f) &&
                math::ApproxEqual(in_normal.x, out_normals [i].x, 0.001f) &&
                math::ApproxEqual(in_normal.y, out_normals [i].y, 0.001f) &&
                math::ApproxEqual(in_normal.z, out_normals [i].z, 0.001f)
                ){
            result = i;
            return true;
        }
    }
    // No other vertex could be used instead.
    // Looks like we'll have to add it to the VBO.
    return false;
}


void computeTangentBasis(
        // inputs
        std::vector<glm::vec3> & vertices,
        std::vector<glm::vec2> & uvs,
        std::vector<glm::vec3> & normals,
        // outputs
        std::vector<glm::vec3> & tangents,
        std::vector<glm::vec3> & bitangents
        )
{
    for (unsigned int i=0; i < vertices.size(); i+=3)
    {

        // Shortcuts for vertices
        glm::vec3 & v0 = vertices[i+0];
        glm::vec3 & v1 = vertices[i+1];
        glm::vec3 & v2 = vertices[i+2];

        // Shortcuts for UVs
        glm::vec2 & uv0 = uvs[i+0];
        glm::vec2 & uv1 = uvs[i+1];
        glm::vec2 & uv2 = uvs[i+2];

        // Edges of the triangle : postion delta
        glm::vec3 deltaPos1 = v1-v0;
        glm::vec3 deltaPos2 = v2-v0;

        // UV delta
        glm::vec2 deltaUV1 = uv1-uv0;
        glm::vec2 deltaUV2 = uv2-uv0;

        float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        glm::vec3 tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
        glm::vec3 bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;

        // Set the same tangent for all three vertices of the triangle.
        // They will be merged later, in vboindexer.cpp
        tangents.push_back(tangent);
        tangents.push_back(tangent);
        tangents.push_back(tangent);

        // Same thing for binormals
        bitangents.push_back(bitangent);
        bitangents.push_back(bitangent);
        bitangents.push_back(bitangent);
    }
    for (unsigned int i=0; i<vertices.size(); i+=1 )
    {
        glm::vec3 & n = normals[i];
        glm::vec3 & t = tangents[i];
        glm::vec3 & b = bitangents[i];

        // Gram-Schmidt orthogonalize
        t = glm::normalize(t - n * glm::dot(n, t));

        // Calculate handedness
        if (glm::dot(glm::cross(n, t), b) < 0.0f){
            t = t * -1.0f;
        }
    }
}


void indexVBO_NM(
        std::vector<glm::vec3> & in_vertices,
        std::vector<glm::vec2> & in_uvs,
        std::vector<glm::vec3> & in_normals,
        std::vector<glm::vec3> & in_tangents,
        std::vector<glm::vec3> & in_bitangents,
        std::vector<unsigned short> & out_indices,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals,
        std::vector<glm::vec3> & out_tangents,
        std::vector<glm::vec3> & out_bitangents
        )
{
    // For each input vertex
    for ( unsigned int i=0; i<in_vertices.size(); i++ ){

        // Try to find a similar vertex in out_XXXX
        unsigned short index;
        bool found = getSimilarVertexIndex(in_vertices[i], in_uvs[i], in_normals[i],     out_vertices, out_uvs, out_normals, index);

        if ( found ){ // A similar vertex is already in the VBO, use it instead !
            out_indices.push_back( index );
            out_tangents[index] += in_tangents[i];
            out_bitangents[index] += in_bitangents[i];
        }else{ // If not, it needs to be added in the output data.
            out_vertices.push_back( in_vertices[i]);
            out_uvs     .push_back( in_uvs[i]);
            out_normals .push_back( in_normals[i]);
            out_tangents .push_back( in_tangents[i]);
            out_bitangents .push_back( in_bitangents[i]);
            out_indices .push_back( (unsigned short)out_vertices.size() - 1 );
        }
    }
}


void indexVBO(
        std::vector<glm::vec3> & in_vertices,
        std::vector<glm::vec2> & in_uvs,
        std::vector<glm::vec3> & in_normals,
        std::vector<unsigned short> & out_indices,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals
        )
{
    // For each input vertex
    for ( unsigned int i=0; i<in_vertices.size(); i++ ){

        // Try to find a similar vertex in out_XXXX
        unsigned short index;
        bool found = getSimilarVertexIndex(in_vertices[i], in_uvs[i], in_normals[i],     out_vertices, out_uvs, out_normals, index);

        if ( found ){ // A similar vertex is already in the VBO, use it instead !
            out_indices.push_back( index );
        }else{ // If not, it needs to be added in the output data.
            out_vertices.push_back( in_vertices[i]);
            out_uvs     .push_back( in_uvs[i]);
            out_normals .push_back( in_normals[i]);
            out_indices .push_back( (unsigned short)out_vertices.size() - 1 );
        }
    }
}

void InitRandomSeed()
{
    srand ((unsigned int)time(NULL));
}

glm::vec3 HSLToRGB(const glm::vec3& hsl_col)
{
    float c =  (1.0f - fabs(2.0f*hsl_col[2] - 1.0f))*hsl_col[1];
    float x = c*(1.0f - abs((int(hsl_col[0]/60) % 2) - 1));
    float m = hsl_col[2] - hsl_col[1]/2.0f;
    glm::vec3 rgb_inter;
    if(0.0f <= hsl_col[0] && hsl_col[0] < 60.0f)
        rgb_inter =  glm::vec3(c,x,0.0f);
    else if(60.0f <= hsl_col[0] && hsl_col[0] < 120.0f)
        rgb_inter =  glm::vec3(x,c,0.0f);
    else if(120.0f <= hsl_col[0] && hsl_col[0] < 180.0f)
        rgb_inter =  glm::vec3(0.0f,c,x);
    else if(180.0f <= hsl_col[0] && hsl_col[0] < 240.0f)
        rgb_inter =  glm::vec3(0.0f,x,c);
    else if(240.0f <= hsl_col[0] && hsl_col[0] < 300.0f)
        rgb_inter =  glm::vec3(x,0.0f,c);
    else if(300.0f <= hsl_col[0] && hsl_col[0] < 360.0f)
        rgb_inter =  glm::vec3(c,0.0f,x);

    return glm::vec3((rgb_inter[0]+m)*255,(rgb_inter[1]+m)*255,(rgb_inter[2]+m)*255);
}

glm::vec3 getRandomColor()
{
    glm::vec3 hsl_col;
    hsl_col[0] = 360.0f*float(rand())/float(RAND_MAX);
    hsl_col[1] = 90.0f + 10.0f*float(rand())/float(RAND_MAX);
    hsl_col[2] = 60 + 10.0f*float(rand())/float(RAND_MAX);
    return HSLToRGB(hsl_col);
}

unsigned int RGBToInt(const glm::vec3& rbg_col, bool alpha_chanel)
{
    if(alpha_chanel)
        return (((unsigned int)(rbg_col[0]) << 24) + ((unsigned int)(rbg_col[1]) << 16) + ((unsigned int)(rbg_col[2]) << 8) + 255);
    else
        return (((unsigned int)(rbg_col[0]) << 16) + ((unsigned int)(rbg_col[1]) << 8) + ((unsigned int)(rbg_col[2])));
}


std::string stringTrim(const std::string& s, char char_trim)
{
    std::string trimmed_string = s;
    trimmed_string.erase(std::remove(trimmed_string.begin(),trimmed_string.end(),char_trim),trimmed_string.end());
    return trimmed_string;
}

std::vector<std::string> stringSplit(const std::string &s, char delim, bool keep_delim)
{
    std::vector<std::string> elems;
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
        if(keep_delim)
            elems.push_back(std::string(1,delim));
    }
    return elems;
}

void stringUpperCase(std::string& input)
{
    for (std::string::iterator it = input.begin(); it != input.end(); ++ it)
      *it = toupper((unsigned char)*it);
}

std::string extractFileNameFromPath(const std::string& path)
{
    unsigned int begin = (unsigned int)(-1);
    unsigned int end = (unsigned int)(-1);
    for(int i = (int)path.size() - 1 ; i >= 0 ; i--)
    {
        if(path[i] == '.')
            end = i;
        else if(path[i] == '/' || path[i] == '\\' ||  i == 0)
        {
            begin = i;
            break;
        }
    }
    if(end == (unsigned int)(-1) || end<=begin)
        return std::string();
    return path.substr(begin+1, end-begin-1);
}

std::string extractFileExtensionFromPath(const std::string& path)
{

    unsigned int begin = (unsigned int)(-1);
    unsigned int end = (unsigned int)(-1);
    for(int i = (int)path.size() - 1 ; i >= 0 ; i--)
    {
        if(path[i] == '.')
        {
            begin = i;
            end = (int)path.size() - 1;
            break;

        }
    }
    if(end == (unsigned int)(-1) || end<=begin)
        return std::string();
    return path.substr(begin+1, end);
}

std::vector<std::string> getFilesInDir(const std::string& dir_path, const std::initializer_list<std::string>& filters)
{
    std::vector<std::string> files;

#if defined(_MSC_VER)
    /**
     * MSVC version (Windows API).
     * TODO !
     */

    WIN32_FIND_DATA fileData;
    HANDLE hFind = FindFirstFile(dir_path + "/*", &fileData);
    if (hFind != INVALID_HANDLE_VALUE)
    {
        do {

        } while (FindNextFile(hFind, &fileData));

        FindClose(hFind);
    }
    else
        Logger::Error("Failed to open directory " + dir_path);

#else
    /**
     * POSIX systems version.
     */

    DIR* dir = opendir(dir_path.c_str());
    struct dirent* ent;
    if (dir)
    {
        while((ent = readdir(dir)))
        {
            std::string extension = extractFileExtensionFromPath(ent->d_name);
            if (std::find(filters.begin(), filters.end(), extension) != filters.end())
                files.push_back(dir_path + "/" + ent->d_name);
        }

        closedir(dir);
    }
    else
        Logger::Error("Failed to open directory " + dir_path);

#endif // defined(_MSC_VER)

    return files;
}

}

std::ostream& operator<< (std::ostream& stream, const ressource::PCVertex& v)
{
    stream << v.pos;
    stream << v.size;
    stream << v.color;
    return stream;
}
std::ostream& operator<< (std::ostream& stream, const render::vertex3D& v)
{
    stream << v.pos;
    stream << v.uvs;
    stream << v.normal;
    return stream;
}

std::ostream& operator<< (std::ostream& stream, const glm::mat4& m)
{
    for(int j = 0 ; j < m[0].length(); ++j)
        for(int i = 0 ; i < m.length(); ++i)
            stream << m[i][j];

    return stream;
}

std::ostream& operator<< (std::ostream& stream, const utility::Tmat4& m)
{
    for(int j = 0 ; j < m[0].length(); ++j)
        for(int i = 0 ; i < m.length(); ++i)
            stream << m[i][j];

    return stream;

}
std::ostream& operator<< (std::ostream& stream, const utility::Tvec3& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream << v[i];

    return stream;

}

std::ostream& operator<< (std::ostream& stream, const glm::vec4& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream << v[i];

    return stream;

}
std::ostream& operator<< (std::ostream& stream, const glm::vec3& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream << v[i];

    return stream;

}
std::ostream& operator<< (std::ostream& stream, const glm::vec2& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream << v[i];

    return stream;

}

std::ostream& operator<< (std::ostream& stream, const animation::Vertex3DSkin& v)
{
    stream << render::vertex3D(v);
    for(unsigned int i = 0 ; i< NUM_BONES_PER_VEREX; ++i)
        stream << v.bones_IDs[i];
    for(unsigned int i = 0 ; i< NUM_BONES_PER_VEREX; ++i)
        stream << v.weights[i];
    stream << v.vertex_ID;

    return stream;
}


//Istream
std::istream& operator>> (std::istream& stream, const render::vertex3D& v)
{
    stream >> v.pos;
    stream >> v.uvs;
    stream >> v.normal;
    return stream;
}

std::istream& operator>> (std::istream& stream, const utility::Tmat4& m)
{
    for(int j = 0 ; j < m[0].length(); ++j)
        for(int i = 0 ; i < m.length(); ++i)
            stream >> m[i][j];

    return stream;
}

std::istream& operator>> (std::istream& stream, const utility::Tvec3& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream >> v[i];

    return stream;

}

std::istream& operator>> (std::istream& stream, const ressource::PCVertex& v)
{
    stream >> v.pos;
    stream >> v.size;
    stream >> v.color;
    return stream;
}

std::istream& operator>> (std::istream& stream, const animation::Vertex3DSkin& v)
{
    stream >> render::vertex3D(v);
    for(unsigned int i = 0 ; i< NUM_BONES_PER_VEREX; ++i)
        stream >> v.bones_IDs[i];
    for(unsigned int i = 0 ; i< NUM_BONES_PER_VEREX; ++i)
        stream >> v.weights[i];
    stream >> v.vertex_ID;

    return stream;
}

std::istream& operator>> (std::istream& stream, const glm::mat4& m)
{
    for(int j = 0 ; j < m[0].length(); ++j)
        for(int i = 0 ; i < m.length(); ++i)
            stream >> m[i][j];

    return stream;
}

std::istream& operator>> (std::istream& stream, const glm::vec4& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream >> v[i];

    return stream;
}

std::istream& operator>> (std::istream& stream, const glm::vec3& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream >> v[i];

    return stream;
}

std::istream& operator>> (std::istream& stream, const glm::vec2& v)
{
    for(int i = 0 ; i < v.length(); ++i)
        stream >> v[i];

    return stream;
}

}
