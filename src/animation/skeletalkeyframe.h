#ifndef SKELETALKEYFRAME_H
#define SKELETALKEYFRAME_H
#include "keyframe.h"
#include "skeletalanimation.h"
#include "animatedbone3d.h"
namespace skyengine
{

namespace animation
{
class SkeletalAnimation;
class SkeletalKeyFrame : public KeyFrame
{
public:
    SkeletalKeyFrame(SkeletalAnimation* animation = nullptr, float time_stamp = 0);
    ~SkeletalKeyFrame();

    static float keyframeDistance(SkeletalKeyFrame &keyframe_1, SkeletalKeyFrame &keyframe_2);




};
}
}
#endif // SKELETALKEYFRAME_H
