#ifndef HALFEDGE_H
#define HALFEDGE_H

#include <glm/glm.hpp>


namespace skyengine
{

namespace geometry
{

struct Halfedge;
struct TVertex;
struct Face;

struct Halfedge
{
    unsigned int id;

    Halfedge*  prev;
    Halfedge* next;
    Halfedge* opposite;
    TVertex* v;
    Face* f;

    Halfedge(unsigned int _id) : id(_id) {}
};

struct TVertex
{
    unsigned int id;

    Halfedge* e;
    glm::vec3 p; // <! World Position

    TVertex(unsigned int _id) : id(_id) {}
};

struct Face
{
    unsigned int id;

    Halfedge* e;

    Face(unsigned int _id) : id(_id) {}
};

} // namespace geometry

} // namespace skyengine

#endif // HALFEDGE_H
