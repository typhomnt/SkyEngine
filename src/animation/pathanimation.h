#ifndef PATHANIMATION_H
#define PATHANIMATION_H
#include "animation.h"
#include "geometry/geometrycurve.h"

namespace skyengine
{

namespace animation
{
class PathAnimation : public Animation
{
public:
    PathAnimation(const std::string& animation_name);
    virtual ~PathAnimation();

    void setPath(const geometry::GeometryCurve<TimedParameter<glm::vec3>>& path,const std::string key_name = "");
    void setPath(const geometry::GeometryCurve<glm::vec3>& path, const std::string key_name = "");
    void addPoint(const glm::vec3& point, const std::string &key_name = "");
    void addPoint(const TimedParameter<glm::vec3>& point, const std::string &key_name = "");
    void addKey(const std::string& key_name, std::shared_ptr<BaseTimeParameter> key_value);
    void clearPath();

    virtual void ComputeAnimation(float time_stamp);

private:
    std::shared_ptr<geometry::GeometryCurve<TimedParameter<glm::vec3>> > m_path;
};
}
}
#endif // PATHANIMATION_H
