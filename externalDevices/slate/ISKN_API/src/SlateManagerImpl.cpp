//#include "SlateManagerImpl.h"

//#include <ctime>
//#include <iostream>

//using namespace std;

//namespace ext_dvc
//{

//namespace slate
//{

//SlateManagerImpl::SlateManagerImpl() :
//    m_manager(nullptr),
//    m_slate(nullptr)
//{
//    initSlate();
//}

//void SlateManagerImpl::initSingletonSlate()
//{
//    SlateManagerImpl::GetSingleton()->initSlate();
//}

//void SlateManagerImpl::initSlate()
//{
//    try
//    {
//        cout<<"Attempting to connect to iSketchnote device..."<<endl;

//        // Create m_manager and Device
//        m_manager = new ISKN_API::SlateManager();
//        m_slate     = &m_manager->getDevice();

//        // Register events Listener
//        m_manager->registerListener(this);

//        // Connect to the Slate
//        if (m_manager->connect())
//        {
//            cout<<"Scanning devices..."<<endl;
//        }
//        else
//        {
//            cout<<"No device found"<<endl;
////            std::cout << "Error : Slate is not connected. Check your USB connection." << std::endl;
////            cout<<"Could not connect... "<<endl;
////            cout<<"Did you close the other apps using the Slate ?"<<endl;

////            exit(0);
//        }
//    }
//    catch (ISKN_API::Error &err)
//    {
//        wcout << err.Message() << endl;
//    }
//}

//void SlateManagerImpl::connectionStatusChanged(bool connected)
//{
//    if(connected) {

//        // Request Slate description
//        m_manager->request(ISKN_API::REQ_DESCRIPTION);

//        // Subscribe to events (Status, Pen Status, Function Call and Pen_3D)
//        m_manager->subscribe(
//                    ISKN_API::AUTO_STATUS |
//                    ISKN_API::AUTO_SOFTWARE_EVENTS |
//                    ISKN_API::AUTO_HARDWARE_EVENTS |
//                    ISKN_API::AUTO_PEN_2D |
//                    ISKN_API::AUTO_PEN_3D |
//                    ISKN_API::AUTO_OBJECT_3D
//                    );

////        printf("connected to slate %s\n", m_slate->getDeviceName());

//    }
//    else
//    {
//    }
//}

//void SlateManagerImpl::processEvent(ISKN_API::Event &e, unsigned int timecode)
//{
//    ISKN_API::Size size;
//    ISKN_API::Rect rect;
//    ISKN_API::Vector2D rotation;
//    ISKN_API::Vector2D pos2d;
//    ISKN_API::Vector3D pos3d;
//    ISKN_API::ISKNDate id;
//    ISKN_API::ISKNTime it;

//    switch (e.Type)
//    {
//    case ISKN_API::EVT_STATUS:  m_status_event->send(e.Status.getBattery(), e.Status.isBatteryInCharge()); break;
//    case ISKN_API::EVT_DESCRIPTION:
//        size = e.Description.getSlateSize();
//        rect = e.Description.getActiveZone();
//        m_description_event->send(e.Description.getDeviceName(), e.Description.getFirmwareVersion(), glm::vec2(size.Width, size.Height),AABBox(glm::vec3(rect.Left,rect.Top, -190.0), glm::vec3(rect.Left + rect.Width, rect.Top + rect.Height, -.0)));
//        break;
//    case ISKN_API::EVT_SOFTWARE: m_software_event->send(e.SoftwareEvent.getObjectID(), e.SoftwareEvent.getSoftwareEventType()); break;
//    case ISKN_API::EVT_PEN_2D:
//        pos2d = e.Pen2D.getPosition();
//        rotation = e.Pen2D.getRotation();
//        m_pen2d_event->send((glm::vec2(pos2d.X, pos2d.Y) - glm::vec2(m_active_box.min()[0],m_active_box.min()[1])) / glm::vec2(m_active_box.width(), m_active_box.height()), glm::vec2(rotation.X, rotation.Y), e.Pen2D.Touch());
//        break;
//    case ISKN_API::EVT_PEN_3D :
//        pos3d = e.Pen3D.getPosition();
//        rotation = e.Pen3D.getRotation();
//        m_pen3d_event->send((glm::vec3(pos3d.X, pos3d.Y, pos3d.Z) - m_active_box.min()) / glm::vec3(m_active_box.width(), m_active_box.height(), m_active_box.depth()), glm::vec2(rotation.X, rotation.Y), e.Pen3D.getZPaper(), e.Pen3D.Touch());
//        break;
//    case ISKN_API::EVT_OBJECT_3D:
//        pos3d = e.Object3D.getPosition();
//        rotation = e.Object3D.getRotation();
//      m_object3d_event->send((glm::vec3(pos3d.X, pos3d.Y, pos3d.Z) - m_active_box.min()) / glm::vec3(m_active_box.width(), m_active_box.height(), m_active_box.depth()), glm::vec2(rotation.X, rotation.Y));
//        break;
//    case ISKN_API::EVT_ACCELEROMETER:
//        pos3d = e.Acceleration.getAccelerometer();
//        m_accelerometer_event->send((glm::vec3(pos3d.X, pos3d.Y, pos3d.Z)));
//        break;
//    case ISKN_API::EVT_HARDWARE: m_hardware_event->send(e.HardwareEvent.getHardwareEventType()); break;
//    case ISKN_API::EVT_DISK_STATUS: m_diskstatus_event->send(e.DiskStatus.getFreeDiskSpace(),e.DiskStatus.getFilesNumber(),e.DiskStatus.isDiskOnline(),e.DiskStatus.isDiskReadable(),e.DiskStatus.isDiskWriteable(),e.DiskStatus.isDiskChanged(),e.DiskStatus.isSDCardInserted()); break;
//    case ISKN_API::EVT_FILE_DESCRIPTOR:
//        id = e.FileDescriptor.getDate();
//        it = e.FileDescriptor.getTime();
//        tm t;
//        t.tm_hour = it.Hour;
//        t.tm_min = it.Minute;
//        t.tm_sec = it.Second;
//        t.tm_mday = id.Day;
//        t.tm_mon = id.Month;
//        t.tm_year = id.Year;
//        t.tm_wday = id.DayOfWeek;
//        m_filedescriptor_event->send(e.FileDescriptor.getSize(), e.FileDescriptor.getId(), mktime(&t)); break;
//    case ISKN_API::EVT_FILE_CONTENT: m_filecontent_event->send(e.FileContent.Id, e.FileContent.Status, e.FileContent.NumPart, e.FileContent.NbParts, e.FileContent.Size, e.FileContent.buffer, FILE_BLOCK_SIZE); break;
//    case ISKN_API::EVT_FILE_CONTENTS:
//        for (unsigned int i = 0; i < e.FileContents.Events->size(); ++i) {
//            processEvent(*e.FileContents.Events->at(i), timecode);
//        }
////        fileContentsEvent(e.FileContents);
//        break;

//    case ISKN_API::EVT_OPERATION_STATUS: m_fileoperation_event->send(e.FileOperation.Operation, e.FileOperation.Status); break;
//    case ISKN_API::EVT_ERROR: m_error_event->send(e.Error.ErrorCode, e.Error.Parameter); break;
//    }
//}


//}

//}

