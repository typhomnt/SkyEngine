#ifndef SKELETALANIMATIONMODIFIER_H
#define SKELETALANIMATIONMODIFIER_H

#include "utils/skyengine_dll.h"

#include "geometry/transformmodifier.h"
#include "animation/skeletalanimation.h"
#include <unordered_set>

#ifdef __GUI__
namespace module
{

namespace gui
{
class ActionAnimator;
}
}
#endif


namespace skyengine
{

namespace animation
{
class SKYENGINE_API SkeletalAnimationModifier : public geometry::TransformModifier<SkeletalAnimation,glm::vec3,glm::vec3>
{
#ifdef __GUI__
    friend class module::gui::ActionAnimator;
#endif
public:
    SkeletalAnimationModifier();


    virtual void apply_scale();
    virtual void apply_translate();
    virtual void apply_rotate();
    virtual void apply_shape();
    virtual void apply();

    /**
     * @brief apply_effort
     * Laban effort modifier. It relies on the following principles:
     * Space: Scale, Rotations and translations are interpolated using a cardinal spline function where
     * tangent equals to T(Pk) = (1-C(space))*((Pk+1 - Pk+1)/(tk+1 - tk-1) where c(space) = (-space + 1)*0.5
     * an additionnal scale can be applied using the apply_scale function
     * Time: timing is modified using scale frame timing function with an affine by chunk function
     * (for t in [0,t_a],[t_a,t_action_end],[t_action_end,t_fol_through] t(T) = n(T)(t_extreme - t) + t)
     * If T >= 0 an additionnal scale is applied with coeff 1/(a*(T+1)) and a is near 1)
     * Weight: If W <= 0 for t in [0,t_a] t = (t_a - t)^n + t avec 4>n(w) > 1
     *                   for t in [t_a,t_inf] t = (t_inf - t)^m + t avec m(w) < 1
     * If W > 0 for all t t_i = (dt_moy - (t_i - t_(i-1))^p + t_i avec p(w) < 1 (possibly use affine speed to uniformize)
     *
     * @param space
     * @param time
     * @param weight
     */
    void apply_effort();
    void apply_naive_effort();
    void scale_to_duration();
    void scale_frame_timing(unsigned int start_index,unsigned int end_index, unsigned int pivot_index, bool keep_coherence_offset, float scale_factor, float expos_factor=1.0f);
    /**
     *  Modifiers are based on Laban Motion Analysis (LMA)
     *  There are two kinds of modifiers, those which change timing and those which modify space movement
     *  This classification is necessary because coupling the two aspects together implies unwanted dephasing most of the time
     *  Moreover we will consider 5 laban modifiers
     *  Effort modifier altering space, time, weight and flow
     *  Space modifier matching movement to a shape
     *  Phrasing altering timing of the  animation stages and preparring a transition with next animations
     *  Body altering body flow during animation (space mainly)
     *  Shape altering body main pose on horizontal,vertical and sagital plane
     *
     */



    void setScaleFactor(float factor);
    void setRotateFactor(float factor);
    void setTranslateFactor(float factor);
    void setRisingFactor(float factor);
    void setSpreadingFactor(float factor);
    void setLabanFactors(glm::vec3 factors);
    void setGlobalDurationFactor(float factor);

    inline float getScaleFactor() const;
    inline float getRotateFactor() const;
    inline float getTranslateFactor() const;
    inline float getRisingFactor() const;
    inline float getSpreadingFactor() const;
    inline void resetFactors();


    void setModifiedAnimation(SkeletalAnimation* animation);

private:

    float m_scale_factor;
    float m_rotate_factor;
    float m_translate_factor;

    float m_rising_factor;
    float m_spreading_factor;
    glm::vec3 m_laban_factors;

    float m_global_duration_factor;

    SkeletalAnimation* m_modified_animation;


    void scalePhase(SkeletalAnimation &data_scale
                    ,unsigned int start = 0
                    ,unsigned int end = 0
                    ,const std::unordered_set<std::string>& scale_bones = std::unordered_set<std::string>());
    virtual void scale(const glm::vec3& pivot_point,  SkeletalAnimation& data_scale);
    virtual void translate(const glm::vec3& translate_vector,  SkeletalAnimation& data_translate);
    virtual void rotate(const glm::vec3& pivot_point, SkeletalAnimation &data_rotate);

    virtual void scale_axis(const glm::vec3& pivot_point,const glm::vec3& pivot_axis, SkeletalAnimation &data_scale);
    virtual void rotate_axis(const glm::vec3& pivot_point,const glm::vec3& pivot_axis,  SkeletalAnimation& data_rotate);

    void applyStrong();
    void applyLight();
    void applySudden();
    void applySustained();
    void applyDirect();
    void applyIndirect();

};

inline float SkeletalAnimationModifier::getScaleFactor() const
{
    return m_scale_factor;
}

inline float SkeletalAnimationModifier::getRotateFactor() const
{
    return m_rotate_factor;
}
inline float SkeletalAnimationModifier::getTranslateFactor() const
{
    return m_translate_factor;
}

inline float SkeletalAnimationModifier::getRisingFactor() const
{
    return m_rising_factor;
}

inline float SkeletalAnimationModifier::getSpreadingFactor() const
{
    return m_spreading_factor;
}

inline void SkeletalAnimationModifier::resetFactors()
{
    m_scale_factor = 1.0f;
    m_rotate_factor = 1.0f;
    m_translate_factor = 1.0f;
    m_rising_factor = 0.0f;
    m_spreading_factor = 0.0f;
    m_global_duration_factor = 0.0f;
    m_laban_factors = glm::vec3();
}

}
}
#endif // SKELETALANIMATIONMODIFIER_H
