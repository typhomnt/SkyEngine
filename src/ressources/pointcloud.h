#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include "ressources/mesh.h"

#include "ressources/bufferattrib.h"
#include "math/mathutils.h"
namespace skyengine
{

namespace ressource
{

struct PCVertex
{
    glm::vec3 pos;   /*!< Position, in local space */
    float size;      /*!< Point size */
    glm::vec4 color; /*!< Point color */

    /**
     * @brief Constructs a Point Cloud vertex
     * @param _pos   Position, in local space (set to origin by default)
     * @param _size  Size (set to 1.0 by default)
     * @param _color Color (set to white by default)
     */
    PCVertex(const glm::vec3& _pos = glm::vec3(0.0f),
             const float _size = 1.0f,
             const glm::vec4& _color = glm::vec4(0.0f,0.0f,1.0f,1.0f))
        : pos(_pos)
        , size(_size)
        , color(_color)
    {}

    const glm::vec3& position() const { return pos; }

    static std::vector<BufferAttrib> getBufferAttribs()
    {
        PCVertex tmp;
        char* tmp_p = (char*) &tmp;
#define ind(elem) ((char*)(&tmp.elem) - tmp_p)
        std::vector<BufferAttrib> attribs;
        attribs.push_back(BufferAttrib(0, 3, GL_FLOAT, ind(pos))); // Position
        attribs.push_back(BufferAttrib(1, 1, GL_FLOAT, ind(size))); // Size
        attribs.push_back(BufferAttrib(2, 4, GL_FLOAT, ind(color))); // Color
        return attribs;
    }

    static size_t BufferSize()
    {
        return sizeof(pos) + sizeof(size) + sizeof(color);
    }

    static unsigned int MagicNumber()
    {
        return 2;
    }

    static PCVertex readFromStream(std::ifstream& in_s)
    {
        PCVertex new_v;
        utility::binary_read(in_s,new_v.pos);
        utility::binary_read(in_s,new_v.size);
        utility::binary_read(in_s,new_v.color);
        return new_v;
    }

    void writeStream(std::ostream& in_s)
    {
        utility::binary_write(in_s,pos);
        utility::binary_write(in_s,size);
        utility::binary_write(in_s,color);
    }

};

class PointCloud : public Mesh<PCVertex>
{
public:
    PointCloud(const std::string& name, GLenum usage = GL_STATIC_DRAW, GLuint instanceId = 0)
        : Mesh(name, usage, GL_POINTS, instanceId)
    {}

    inline IndexType addVertex(const VertexType& vertex)
    {
        IndexType idx = Mesh<PCVertex>::addVertex(vertex);
        addIndex(idx);
        return idx;
    }
};

} // namespace ressource
} // namespace skyengine

#endif // POINTCLOUD_H
