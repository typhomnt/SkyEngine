#ifndef ANIMATORGUITASK_H
#define ANIMATORGUITASK_H

#include <core/task.h>
#include <animation/skeletalanimator.h>
#include <animation/gameobjectskinned.h>
#include <animation/armature.h>
#include "skywidget.h"

namespace module
{

namespace gui
{
class AnimationActiveEvent : public  skyengine::core::Listenable<AnimationActiveEvent>
{
public:
    AnimationActiveEvent() {}
    virtual ~AnimationActiveEvent() {}

    virtual void send(skyengine::animation::Animation* animation)
    { animation_ = animation; notifyListeners(); }

    skyengine::animation::Animation* animation() const {return animation_;}

protected:
    skyengine::animation::Animation* animation_;
};
using AnimationActiveListener = skyengine::core::Listener<AnimationActiveEvent>;

class ActionAnimator : public SkyWidget
{
public:
    ActionAnimator(const std::string& name, SkyWindow* sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                   std::shared_ptr<SelectObjectEvent> select_evt,skyengine::animation::Animator* animator);
    virtual ~ActionAnimator();
    void draw() override;
    //Skeletal Specific----------------------------
    void SkeletonWindow(skyengine::animation::Skeleton *skeleton);
    //---------------------------------------------
protected:
    skyengine::animation::Animator* m_animator;
    std::shared_ptr<skyengine::animation::SkeletalAnimation> m_current_anim;

    std::map<std::string,glm::vec3> m_elem_laban;

};

class ActionPlayer : public SkyWidget
{
public:
    ActionPlayer(const std::string& name, SkyWindow *sky_win, std::shared_ptr<ActiveObjectEvent> act_evt, std::shared_ptr<SelectObjectEvent> select_evt, skyengine::animation::SkeletalAnimator* animator, skyengine::scene::SceneGraph* sc_g, skyengine::animation::GameObjectSkinned* anim_object = nullptr);
    void draw() override;

    void setAnimatedObject(skyengine::animation::GameObjectSkinned* anim_object);
    void listened(const ActiveObjectEvent*);
    inline std::shared_ptr<AnimationActiveEvent> getAnimationActiveEvent(){return m_anim_activ_event;}
private:
    std::shared_ptr<skyengine::animation::SkeletalAnimator> m_preview_animator;
    skyengine::animation::SkeletalAnimator* m_animator;
    skyengine::animation::GameObjectSkinned* m_anim_object;
    skyengine::animation::Animation*  m_current_anim;
    int m_current_displayed_animation;
    int m_curr_layer;
    std::shared_ptr<AnimationActiveEvent> m_anim_activ_event;
};

class AnimationEditor: public SkyWidget, public skyengine::core::Listener<AnimationActiveEvent>
{
public:
    AnimationEditor(const std::string& name, SkyWindow *sky_win, std::shared_ptr<ActiveObjectEvent> act_evt, std::shared_ptr<SelectObjectEvent> select_evt, std::shared_ptr<AnimationActiveEvent>);
    void draw() override;

    void setAnimation(skyengine::animation::Animation* animation);
     virtual void listened(const AnimationActiveEvent* e){setAnimation(e->animation());}
private:
    skyengine::animation::Animation* m_animation;
    std::unordered_map<std::string,std::pair<int,unsigned int*>> m_curr_anim_bones; //Chain length and index of target
    int m_current_bone;
    int m_curr_ch_lght;
    char m_tg_buff[128];

    void HighLevelModif();
    void AnimConstraintMenu();
};

class ArmatureDisplay: public SkyWidget
{
public:
    ArmatureDisplay(const std::string& name, SkyWindow *sky_win, std::shared_ptr<ActiveObjectEvent> act_evt, std::shared_ptr<SelectObjectEvent> select_evt, skyengine::animation::Armature* armature = nullptr);
    void draw() override;

    void setArmature(skyengine::animation::Armature* m_armature);

private:
    skyengine::animation::Armature* m_armature;
    int m_current_root_bone;
    int m_current_core_bone;
    int m_current_head_bone;

    void displaySkelRec(skyengine::animation::Bone3D* bone);
};

}
}
#endif // ANIMATORGUITASK_H
