#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "utils/skyengine_dll.h"
#include "animation.h"
#include "animatorinterface.h"
#include "../geometry/transformmodifier.h"
#include "../core/skyengine.h"


#ifdef __GUI__
namespace module
{

namespace gui
{
class ActionAnimator;
}
}
#endif

namespace skyengine
{

namespace animation
{

class SKYENGINE_API Animator : public AnimatorInterface
{
#ifdef __GUI__
    friend class module::gui::ActionAnimator;
#endif
protected:
    struct LayeredAnimationElement
    {
        LayeredAnimationElement(const std::string& name): name(name), start_time(0.0f),end_time(0.0f),layer(0),animation(nullptr),modifier(nullptr),modified_animation(nullptr){}
        LayeredAnimationElement(const std::string& name, float start_time, float end_time, unsigned int layer, Animation* animation, std::shared_ptr<geometry::AbstractTransformModifier> modifier):name(name), start_time(start_time),end_time(end_time),layer(layer),animation(animation),modifier(modifier),modified_animation(nullptr){}
        std::string name;
        float start_time;
        float end_time;
        unsigned int layer;
        Animation* animation;
        std::shared_ptr<geometry::AbstractTransformModifier> modifier;
        std::shared_ptr<Animation> modified_animation;
    };

    struct Transition
    {
        Transition(const std::string& name, float start_time, float end_time, LayeredAnimationElement* source_anim, LayeredAnimationElement* target_anim): name(name),start_time(start_time),end_time(end_time),source_anim(source_anim),target_anim(target_anim){}
        std::string name;
        float start_time;
        float end_time;
        LayeredAnimationElement* source_anim;
        LayeredAnimationElement* target_anim;
    };

public:
    Animator();

    virtual void addAnimationModifier(std::shared_ptr<geometry::AbstractTransformModifier> animation_modifier, const std::string& modifier_name);
    virtual void addAnimation(const std::string& element_name, float start_time, float end_time, Animation* animation, unsigned int layered_order, std::shared_ptr<geometry::AbstractTransformModifier> modifier = nullptr,bool add_transition = false);
    virtual bool addTransition(const std::string& trans_name,float start_time, float end_time, const std::string& src_el_name,const std::string& trg_el_name);
    virtual void removeAnimation(const std::string& animation_name);

    inline std::unordered_map<std::string,Animation*> getPlayedAnimations(float time_stamp);
    inline std::unordered_map<std::string,std::shared_ptr<LayeredAnimationElement>>& getAnimatorsElements();
    inline std::unordered_map<std::string, std::shared_ptr<Transition> > &getAnimatorsTransitions();
    inline std::shared_ptr<geometry::AbstractTransformModifier> getModifier(const std::string& modifier_name);
    inline bool containsElement(const std::string& element_name);

    virtual void setElementStartTime(const std::string& element_name, float start_time);
    virtual void setElementEndTime(const std::string& element_name, float end_time);
    virtual bool setElementModifier(const std::string& element_name, std::shared_ptr<geometry::AbstractTransformModifier> modifier);
    virtual bool applyElementModifier(const std::string& element_name);
    virtual bool clampElementToEnd(const std::string& element_name);

    virtual void clampToEnd();
    //TODO are those function really pure ?
    virtual void computeAnimation(float time_stamp);
    virtual void updateMotionPaths() = 0;
    virtual void saveCurrentCompoAsAnim(const std::string& anim_name) = 0;

    virtual void clear();
    void clearModifiers();
    void clearModifierOrdering();
    void clearAnimationCompoEls();
    void clearTransitions();
    void reset();
    void resetTimeStamp();

    #ifdef __GUI__
    inline unsigned int getId() const;
    inline float* getPlaySpeed();
    void setId(unsigned int id);
    #endif


protected:


    std::unordered_map<std::string,std::shared_ptr<geometry::AbstractTransformModifier>> m_animation_modifiers;
    std::map<unsigned int,LayeredAnimationElement*> m_animation_layering_order;
    std::vector<std::string> m_modifier_layering_order;
    std::unordered_map<std::string,std::shared_ptr<LayeredAnimationElement>> m_composed_anim_els;
    std::unordered_map<std::string,std::shared_ptr<Transition>> m_transitions;
    std::map<unsigned int,Transition*> m_played_trans;
    #ifdef __GUI__
    unsigned int m_id;
    #endif

    void ComputeElement(const std::string& element_name,float time_stamp);


};

#ifdef __GUI__
inline unsigned int Animator::getId() const
{
    return m_id;
}

inline float* Animator::getPlaySpeed()
{
    return &m_play_speed;
}

#endif

inline std::shared_ptr<geometry::AbstractTransformModifier> Animator::getModifier(const std::string& modifier_name)
{
    auto modif_it = m_animation_modifiers.find(modifier_name);
    if(modif_it == m_animation_modifiers.end()) {
        std::cerr << "[Error] trying to access inexistant modifier: " << modifier_name << std::endl;
        return nullptr;
    }
    else
        return modif_it->second;
}


inline std::unordered_map<std::string,Animation*> Animator::getPlayedAnimations(float time_stamp)
{
    std::unordered_map<std::string,Animation*> elem_anim;

    for(auto& anim_el : m_composed_anim_els)
    {
        if(time_stamp <= anim_el.second->end_time
                && time_stamp >= anim_el.second->start_time)
            utility::MapInsert<std::string,Animation*>(elem_anim,anim_el.first,anim_el.second->animation);
    }

    return elem_anim;
}

inline std::unordered_map<std::string,std::shared_ptr<Animator::LayeredAnimationElement>>& Animator::getAnimatorsElements()
{
    return m_composed_anim_els;
}

inline std::unordered_map<std::string,std::shared_ptr<Animator::Transition>>& Animator::getAnimatorsTransitions()
{
    return m_transitions;
}

inline bool Animator::containsElement(const std::string& element_name)
{
    return utility::MapContains(m_composed_anim_els,element_name);
}


}
}

#endif // ANIMATOR_H
