#version 330 core
// Input variables
in vec3 fragPos;
in vec3 fragNormal;
in vec2 fragUV;
// Output variable
out vec4 fragColor;



struct Material
{
    vec3  albedo;
    float metallic;
    float roughness;
    float ao;
};

// Struct definitions
struct DirLight
{
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct PointLight
{
    vec3 position;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
};

struct SpotLight
{
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
    float cutoff;
};

// Uniform lights.
// TODO : for now, only 1 DirLight and 1 PointLight
#define NB_LIGHTS 1
uniform DirLight dirLight[NB_LIGHTS];
uniform PointLight pointLight[NB_LIGHTS];

uniform Material material;
uniform vec3 camPosition;

uniform sampler2D myTextureSampler;
uniform sampler2D myTextureSampler2;

const float PI = 3.14159265359;
const float mu_s = 4.0; //scattering coefficient, float

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float glowFunc(float dist_to_light)
{
    float t = 1.0 - clamp(dist_to_light, 0.0, 0.2) / 0.2;
    t *= t;

    return t;
}


vec3 computeReflectance(vec3 N, vec3 V, vec3 F0, vec3 albedo, vec3 mu_a, vec3 L, vec3 H, vec3 diffuse, float attenuation)
{
    vec3 radiance = diffuse * attenuation;

    // cook-torrance brdf
    float NDF = DistributionGGX(N, H, material.roughness);
    float G   = GeometrySmith(N, V, L, material.roughness);
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - material.metallic;

    vec3 nominator    = NDF * G * F;
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001;
    vec3 specular     = nominator / denominator;

    // add to outgoing radiance Lo
    float NdotL = max(dot(N, L), 0.0);
    //Test absorb and scaterring
    //low border of clamp is the abbsobness of the object
    vec3 absorb = vec3(clamp(exp2(-2.0*dot(V,N)*(1.0 - material.metallic)*mu_a),0.2,1.0)); //amount not absorbed
    float scatter = exp2(-2.0*length(N)*mu_s); //amount not scattered
    float glow = glowFunc(1.0 - 1.0/attenuation);
    return (kD * albedo*absorb*vec3(1.0 - scatter)*vec3(glow)/ PI + specular*absorb*vec3(1.0 - scatter)*vec3(glow)) * radiance * NdotL;
}

void main()
{

    vec3 albedo = material.albedo;
    albedo *= texture(myTextureSampler, fragUV).rgb;
    vec3 N = normalize(fragNormal);
    vec3 V = normalize(camPosition - fragPos);

    //Test absorb
    vec3 mu_a = vec3(1.0f)/albedo;


    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, material.metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < NB_LIGHTS; ++i)
    {
        // calculate per-light radiance
        vec3 L = normalize(pointLight[i].position - fragPos);
        vec3 H = normalize(V + L);
        float distance    = length(pointLight[i].position - fragPos);
        float attenuation = 1.0 / (distance * distance);
        Lo += computeReflectance(N,V,F0,albedo,mu_a,L,H,pointLight[i].diffuse.rgb,attenuation);
    }
    for(int i = 0; i < NB_LIGHTS; ++i)
    {
        // calculate per-light radiance
        //vec3 L = normalize(pointLight[i].position - fragPos);
        vec3 L = normalize(-dirLight[i].direction);
        vec3 H = normalize(V + L);
        float attenuation = 1.0;
        Lo += computeReflectance(N,V,F0,albedo,mu_a,L,H,dirLight[i].diffuse.rgb,attenuation);
    }

    vec3 ambient = vec3(0.03) * albedo * material.ao;
    vec3 color = ambient + Lo;

    //HDR
    color = color / (color + vec3(1.0));
     //Gamma. Done in post process
    //color = pow(color, vec3(1.0/2.2));

    fragColor = vec4(color, 1.0);
}
