#include "resourcelist.h"

#include "ressources/ressourcemanager.h"


namespace module
{

namespace gui
{

ResourceList::ResourceList(const std::string& name, SkyWindow *sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                           std::shared_ptr<SelectObjectEvent> select_evt)
    : SkyWidget(name,sky_win,act_evt,select_evt)
{}


void ResourceList::draw()
{
    for(auto& res : skyengine::ressource::RessourceManager::GetSingleton()->getRessources())
        ImGui::Text(res.first.c_str());
}

} // namespace gui

} // namespace module
