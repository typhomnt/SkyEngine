#include "scenegraph.h"
#include "core/updateable.h"
#include "core/functioncalltask.h"
#include <iostream>


namespace skyengine
{

namespace scene
{

SceneGraph::SceneGraph(const std::shared_ptr<SceneGraphNode>& root) : 
    m_root(nullptr)
{
    if(root == nullptr)
        setRoot(std::make_shared<SceneGraphNode>("Root_node"));
    else
        setRoot(root);
}


void SceneGraph::setRoot(const std::shared_ptr<SceneGraphNode>& root)
{
    m_root = root;
    //TO Improve. We test if the root is inside the node map
    if(!containsNode(root->getName()))
        addNode(m_root);
}

void SceneGraph::addNode(const std::shared_ptr<SceneGraphNode>& node)
{
    addNode(m_root->getName(),node);
}


void SceneGraph::addNode(const std::string& parent_name, const std::shared_ptr<SceneGraphNode>& node)
{
    auto result = m_scene_nodes.insert({node->m_name, node});
    node->m_scene_graph = this;
    if (result.second && node->getName() != parent_name)
    {
        changeParent(node->getName(), parent_name);
    }
}

void SceneGraph::deleteNode(const std::string& node_name)
{
    if(utility::MapContains<std::string,std::shared_ptr<SceneGraphNode> >(m_scene_nodes,node_name))
    {
        //delete all children
        for(SceneGraphNode* child: m_scene_nodes[node_name]->getChildren())
            deleteNode(child->m_name);

        //delete from parent
        if(m_scene_nodes[node_name]->getParent() !=  nullptr)
                m_scene_nodes[node_name]->getParent()->removeChild(node_name);
        m_scene_nodes.erase(node_name);
    }

}

void SceneGraph::changeParent(const std::string& node_name, const std::string& new_parent_name)
{
    auto node = m_scene_nodes.find(node_name);
    if (node == m_scene_nodes.end())
    {
        std::cerr << "[ERROR] : The node " << node_name << " does not exist" << std::endl;
        return;
    }

    auto parent = m_scene_nodes.find(new_parent_name);
    if (parent == m_scene_nodes.end())
    {
        std::cerr << "[ERROR] : The node " << new_parent_name << " does not exist" << std::endl;
        return;
    }
    if(node->second->getParent() != nullptr)
        node->second->getParent()->removeChild(node_name);

    node->second->setParent(parent->second.get());
    parent->second->m_children.push_back(node->second.get());
}

void SceneGraph::Serialize(rapidjson::Value &curr_obj, rapidjson::MemoryPoolAllocator<> &al)
{
    std::shared_ptr<rapidjson::Value> obj = Serializable::SerializeProp(al);
    m_root->Serialize(*(obj.get()),al);

    curr_obj.AddMember(
        rapidjson::Value("Scene", al),
        *(obj.get()),
        al
    );
}

void SceneGraph::LoadProp(const rapidjson::Value& scene)
{
    assert(scene.HasMember("Root_node"));
    SceneGraphNode::Load("Root_node", scene["Root_node"], this);
}


} // namespace scene

} // namespace skyengine
