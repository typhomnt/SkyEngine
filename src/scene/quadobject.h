#ifndef QUADOBJECT_H
#define QUADOBJECT_H

#include "utils/skyengine_dll.h"

#include "gameobject3d.h"
namespace skyengine
{

namespace scene
{

class SKYENGINE_API QuadObject: public GameObject3D
{
public:
    QuadObject(const std::string& name, scene::SceneGraph *owner = nullptr,
               const unsigned int type = 0,
               const glm::mat4& world_matrix = glm::mat4(1.0f),
               const glm::vec3& center_position = glm::vec3(0,0,0),
               float width = 2,
               float height = 2);

    virtual std::string getTag() {return "QuadObject";}
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);

private:
    glm::vec3 m_center_position;
    float m_width;
    float m_height;

    void build();

    void LoadProp(const rapidjson::Value& value) override;
};

}
}
#endif // QUADOBJECT_H
