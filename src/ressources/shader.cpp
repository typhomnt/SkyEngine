#include "shader.h"

#include <fstream>
#include <iostream>

#include "animation/animationmacros.h"
#include "ressources/ressourcemanager.h"
#include "ressources/texture.h"
#include "utils/logger.h"
#include "utils/utilities.h"


static bool _ReadShaderSource(const std::string& fileName, std::string& oSource)
{
    using skyengine::utility::Logger;

    std::ifstream stream(fileName);
    if (!stream.is_open())
    {
        Logger::Error(std::string("Failed to open shader ") + fileName);
        return false;
    }

    oSource.clear();
    std::string line;
    while (getline(stream, line))
        oSource += line + "\n";

    stream.close();
    return true;
}

static bool _CompileShader(const GLuint shader, const std::string& shaderFile)
{
    using skyengine::utility::Logger;

    std::string shaderSource;
    if (!_ReadShaderSource(shaderFile, shaderSource))
        return false;

    const GLchar* source = static_cast<const GLchar*>(shaderSource.c_str());
    GLCHECK(glShaderSource(shader, 1, &source, nullptr));
    GLCHECK(glCompileShader(shader));

    // Check errors
    GLint status = 0;
    GLCHECK(glGetShaderiv(shader, GL_COMPILE_STATUS, &status));
    if (status == GL_FALSE)
    {
        GLint logLength = 0;
        GLCHECK(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength));

        GLchar* log = new GLchar[logLength + 1];
        if (log)
        {
            GLCHECK(glGetShaderInfoLog(shader, logLength, &logLength, log));

            Logger::Error(shaderFile + std::string(" : compilation failed ! (see below for details)"));
            std::cerr << log << std::endl;

            delete[] log;
            return false;
        }
    }

    return true;
}


namespace skyengine
{

namespace ressource
{

void Shader::InitDefaultShader()
{
}

Shader::Shader(const std::string& name)
    : Ressource { name }
    , m_vertexFile { "" }
    , m_geomFile { "" }
    , m_fragmentFile { "" }
    , m_pid(0)
{}

Shader::Shader(const std::string& name,
               const std::string& vertexFile,
               const std::string& fragmentFile,
               const std::string& geomFile)
    : Ressource(name)
    , m_vertexFile(vertexFile)
    , m_geomFile(geomFile)
    , m_fragmentFile(fragmentFile)
    , m_pid(0)
{}

Shader::~Shader()
{
    if (m_pid)
    {
        GLCHECK(glDeleteProgram(m_pid));
        m_pid = 0;
    }

    m_uniforms.clear();
    texture_to_sampler.clear();
}

bool Shader::compileAndLink()
{
    // Create program if necessary
    if (m_pid == 0)
    {
        m_pid = glCreateProgram();
        if (m_pid == 0)
            utility::Logger::Fatal("Failed to create OpenGL shader program");
    }

    // Create shaders
    GLuint vid = glCreateShader(GL_VERTEX_SHADER);
    if (vid == 0)
        utility::Logger::Fatal("Failed to create OpenGL vertex shader");

    GLuint gid = 0;
    if (!m_geomFile.empty())
    {
        gid = glCreateShader(GL_GEOMETRY_SHADER);
        if (gid == 0)
            utility::Logger::Fatal("Failed to create OpenGL geometry shader");
    }

    GLuint fid = glCreateShader(GL_FRAGMENT_SHADER);
    if (fid == 0)
        utility::Logger::Fatal("Failed to create OpenGL fragment shader");

    // Compile
    bool compileOk = true, linkOk = true;
    compileOk = compileOk && _CompileShader(vid, m_vertexFile);
    compileOk = compileOk && _CompileShader(fid, m_fragmentFile);

    if (gid)
        compileOk = compileOk && _CompileShader(gid, m_geomFile);

    if (compileOk)
    {
        // Link
        GLCHECK(glAttachShader(m_pid, vid));
        GLCHECK(glAttachShader(m_pid, fid));
        if (gid)
            GLCHECK(glAttachShader(m_pid, gid));

        GLCHECK(glLinkProgram(m_pid));

        GLint status = 0;
        GLCHECK(glGetProgramiv(m_pid, GL_LINK_STATUS, &status));
        if (status == GL_FALSE)
        {
            GLint logLength = 0;
            GLCHECK(glGetProgramiv(m_pid, GL_INFO_LOG_LENGTH, &logLength));

            GLchar* log = new GLchar[logLength + 1];
            if (log)
            {
                GLCHECK(glGetProgramInfoLog(m_pid, logLength, &logLength, log));

                utility::Logger::Error("Program link failed ! (see below for details)");
                std::cerr << log << std::endl;

                delete[] log;
                linkOk = false;
            }
        }

        GLCHECK(glDetachShader(m_pid, vid));
        GLCHECK(glDetachShader(m_pid, fid));
        if (gid)
            GLCHECK(glDetachShader(m_pid, gid));
    }

    GLCHECK(glDeleteShader(vid));
    GLCHECK(glDeleteShader(fid));
    if (gid)
        GLCHECK(glDeleteShader(gid));

    if (compileOk && linkOk)
    {
        getShaderUniforms();
        return true;
    }
    return false;
}

GLint Shader::getUniformLocation(const char* variableName) const
{
    auto uniformIt = m_uniforms.find(variableName);
    if (uniformIt == m_uniforms.end()) {
        return -1;
    }
    else
        return uniformIt->second.location;
}

unsigned int Shader::getUniformSize(const char* variableName) const
{
    auto uniformIt = m_uniforms.find(variableName);
    if (uniformIt == m_uniforms.end()) {
        return 0;
    }
    else
        return uniformIt->second.size;
}

UniformType Shader::getUniformType(const std::string &uniform_name) const
{
    auto uniformIt = m_uniforms.find(uniform_name);
    if (uniformIt == m_uniforms.end()) {
        return NO_TYPE;
    }
    else
        return uniformIt->second.type;
}

bool Shader::containsUniform(const std::string& uniform_name) const
{
    return utility::MapContains(m_uniforms,uniform_name);
}

GLint Shader::getSamplerUnit(GLuint texture_location) const
{
    auto it = texture_to_sampler.find(texture_location);
    if (it == texture_to_sampler.end())
        return -1;
    else
        return it->second;
}

void Shader::setTextureToSampler(const std::string &texture_name, const std::string& uniform_sampler)
{
    m_sampler_linked_texture[m_uniform_sampler[uniform_sampler]] = texture_name;
}

void Shader::bindTextures()
{
    std::multimap<GLuint,std::string> sampler_uniform = utility::MapFlip<std::string,GLuint>(m_uniform_sampler);
    for(auto& sp_tex : m_sampler_linked_texture)
    {
        std::dynamic_pointer_cast<Texture>(ressource::RessourceManager::Get(sp_tex.second))->bind(GL_TEXTURE0 +sp_tex.first);
        setUniformValue1i(sampler_uniform.equal_range(sp_tex.first).first->second.c_str(), sp_tex.first);
    }
}

void Shader::setUniformValue1i(const char* uniform_name, GLuint value) const
{
    GLCHECK(glUniform1i(getUniformLocation(uniform_name), value));
}

void Shader::setUniformValue1f(const char *uniform_name, float value) const
{
    GLCHECK(glUniform1f(getUniformLocation(uniform_name), value));
}

void Shader::setUniformValue2f(const char* uniform_name, const glm::vec2 &value) const
{
    GLCHECK(glUniform2fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), &value[0]));
}

void Shader::setUniformValue3f(const char* uniform_name, const glm::vec3 &value) const
{
    GLCHECK(glUniform3fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), &value[0]));
}

void Shader::setUniformValue4f(const char *uniform_name, const glm::vec4 &value) const
{
    GLCHECK(glUniform4fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), &value[0]));
}

void Shader::setUniformValueMatrix3fv(const char* uniform_name, const glm::mat3 &value) const
{
    GLCHECK(glUniformMatrix3fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), GL_FALSE, &value[0][0]));
}

void Shader::setUniformValueMatrix4fv(const char* uniform_name, const glm::mat4 &value) const
{
    GLCHECK(glUniformMatrix4fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), GL_FALSE, &value[0][0]));
}

void Shader::setUniformValue1i(const char* uniform_name, GLuint* value) const
{
    GLCHECK(glUniform1i(getUniformLocation(uniform_name), *value));
}

void Shader::setUniformValue1f(const char* uniform_name, float* value) const
{
    GLCHECK(glUniform1f(getUniformLocation(uniform_name), *value));
}

void Shader::setUniformValue2f(const char *uniform_name, const glm::vec2 *value) const
{
    GLCHECK(glUniform2fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), ((float*)value)));
}

void Shader::setUniformValue3f(const char* uniform_name, const glm::vec3 *value) const
{
    GLCHECK(glUniform3fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), ((float*)value)));
}

void Shader::setUniformValue4f(const char* uniform_name, const glm::vec4 *value) const
{
    GLCHECK(glUniform4fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), ((float*)value)));
}

void Shader::setUniformValueMatrix3fv(const char *uniform_name, const glm::mat3 *value) const
{
    GLCHECK(glUniformMatrix3fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), GL_FALSE, ((float*)value)));
}


void Shader::setUniformValueMatrix4fv(const char *uniform_name,const glm::mat4* value) const
{
    GLCHECK(glUniformMatrix4fv(getUniformLocation(uniform_name), getUniformSize(uniform_name), GL_FALSE, ((float*)value)));
}


void Shader::setUniformValue(scene::baseNodeParameter* uniform_parameter)
{
    ressource::UniformType param_type = getUniformType(uniform_parameter->name);

    switch (param_type)
    {
    case ressource::FLOAT:
        if(uniform_parameter->size >= 1)
            setUniformValue1f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<float*>*>(uniform_parameter)->value);
        else
            setUniformValue1f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<float>*>(uniform_parameter)->value);
        break;
    case ressource::FLOAT_VEC2:
        if(uniform_parameter->size >= 1)
            setUniformValue2f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::vec2*>*>(uniform_parameter)->value);
        else
            setUniformValue2f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::vec2>*>(uniform_parameter)->value);
        break;
    case ressource::FLOAT_VEC3:
        if(uniform_parameter->size >= 1)
            setUniformValue3f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::vec3*>*>(uniform_parameter)->value);
        else
            setUniformValue3f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::vec3>*>(uniform_parameter)->value);
        break;
    case ressource::FLOAT_VEC4:
        if(uniform_parameter->size >= 1)
            setUniformValue4f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::vec4>*>(uniform_parameter)->value);
        else
            setUniformValue4f(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::vec4>*>(uniform_parameter)->value);
        break;
    case ressource::FLOAT_MAT4:
        if(uniform_parameter->size >= 1)
            setUniformValueMatrix4fv(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::mat4*>*>(uniform_parameter)->value);
        else
            setUniformValueMatrix4fv(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::mat4>*>(uniform_parameter)->value);
        break;
    case ressource::FLOAT_MAT3:
        if(uniform_parameter->size >= 1)
            setUniformValueMatrix3fv(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<glm::mat3>*>(uniform_parameter)->value);
        break;
    case ressource::TEXTURE_2D:
        if(uniform_parameter->size >= 1)
            setUniformValue1i(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<GLuint*>*>(uniform_parameter)->value);
        else
            setUniformValue1i(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<GLuint>*>(uniform_parameter)->value);
        break;
    case ressource::INT:
    case ressource::BOOL:
        if(uniform_parameter->size >= 1)
            setUniformValue1i(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<GLuint*>*>(uniform_parameter)->value);
        else
            setUniformValue1i(uniform_parameter->name.c_str(), dynamic_cast<scene::NodeParameter<GLuint>*>(uniform_parameter)->value);
        break;
    default:
        //LOG ERROR
        utility::Logger::GetSingleton()->Error("Uniform " + uniform_parameter->name +" of unknown type");
        break;
    }
}

std::shared_ptr<rapidjson::Value> Shader::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Ressource::SerializeProp(al);

    utility::JSONHelper::SaveString(*value, "Vertex", m_vertexFile, al);
    utility::JSONHelper::SaveString(*value, "Fragment", m_fragmentFile, al);
    if (!m_geomFile.empty())
        utility::JSONHelper::SaveString(*value, "Geometry", m_geomFile, al);

    return value;
}

void Shader::addSamplerUnitTextureLocation(GLuint texture_location, GLuint sampler_number)
{
    texture_to_sampler.insert(std::pair<GLuint,GLuint>(texture_location,sampler_number));
}

void Shader::setSamplerUnitTextureLocation(GLuint texture_location, GLuint sampler_number)
{
    texture_to_sampler[texture_location] = sampler_number;
}

void Shader::getShaderUniforms()
{
    m_uniforms.clear();

    // Number of shader uniforms
    GLint uniformsCount = 0;
    GLuint samplerCount = 0;

    GLCHECK(glGetProgramiv(m_pid, GL_ACTIVE_UNIFORMS, &uniformsCount));

    for (GLint i = 0; i < uniformsCount; ++i)
    {
        // Uniform values
        GLsizei nameLength = 0;
        GLint size = 0;
        GLenum type;
        GLchar name[MAX_BUFFER_SIZE];


        GLCHECK(glGetActiveUniform(
            m_pid, i, MAX_BUFFER_SIZE,
            &nameLength, &size, &type, name));

        GLint location = glGetUniformLocation(m_pid, name);
        m_uniforms.insert({
                              std::string(name),
                              Uniform(location, size, (UniformType)type)
                          });

        //Link sampler nbr to uniform sample
        if((UniformType)type == UniformType::TEXTURE_2D)
        {
            m_uniform_sampler[std::string(name)] = samplerCount;
            samplerCount++;
        }
    }
}

bool Shader::LoadProp(const rapidjson::Value& value)
{
    m_vertexFile = value["Vertex"].GetString();
    m_fragmentFile = value["Fragment"].GetString();

    if (value.HasMember("Geometry"))
        m_geomFile = value["Geometry"].GetString();

    compileAndLink();
    return false;
}


} // namespace ressource

} // namespace skyengine
