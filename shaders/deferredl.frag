#version 330 core
layout(location = 0) out vec4 fragColor;

in vec2 UV;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

// Struct definitions
struct DirLight
{
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct PointLight
{
    vec3 position;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
};

struct SpotLight
{
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
    float cutoff;
};
#define NB_LIGHTS 1
uniform DirLight dirLight[NB_LIGHTS];
uniform PointLight pointLight[NB_LIGHTS];


uniform vec3 camPosition;



vec4 computeDirLighting(DirLight dirLight,vec3 fragPos ,vec3 fragNormal,vec3 Albedo, float spec)
{    
    // Ambient
    vec4 color = vec4(Albedo*0.1,dirLight.ambient.a);

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(-dirLight.direction);

    float NdotL = max(dot(N, L),0.0);
    if (NdotL > 0.0)
    {
        color += vec4(Albedo* dirLight.diffuse.rgb* NdotL,dirLight.diffuse.a);

        // Specular
        vec3 V = normalize(camPosition - fragPos);
        vec3 R = normalize(reflect(-L, N));

        float RdotV = max(dot(R, V), 0.0);

        color += vec4(Albedo * dirLight.specular.rgb* pow(RdotV, spec),dirLight.specular.a);
    }
    return color;
}


vec4 computePointLighting(PointLight pointLight,vec3 fragPos ,vec3 fragNormal,vec3 Albedo, float spec)
{
    // Ambient
    vec4 color = vec4(0.1f*Albedo,1.0f)* pointLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(pointLight.position - fragPos);

    float NdotL = dot(N, L);
    if (NdotL > 0.0)
    {

        color += vec4(Albedo,1.0f) * pointLight.diffuse * NdotL;

        // Specular
        vec3 V = normalize(camPosition - fragPos);
        vec3 R = normalize(reflect(-L, N));

        float RdotV = max(dot(R, V), 0.0);
        color += vec4(Albedo,1.0f) * pointLight.specular * pow(RdotV, spec);
    }

    // Attenuation factor
    float d = length(pointLight.position - fragPos);
    float att =   pointLight.constant
                + pointLight.linear * d
                + pointLight.quadratic * d * d;

    return att * color;
}


vec4 computeSpotLighting(SpotLight spotLight,vec3 fragPos ,vec3 fragNormal,vec3 Albedo, float spec)
{
    // Ambient
    vec4 color = vec4(0.1f*Albedo,1.0f)* spotLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(spotLight.position - fragPos);

    float spotFactor = dot(-L, normalize(spotLight.direction));
    if (spotFactor > spotLight.cutoff)
    {
        float NdotL = dot(N, L);
        if (NdotL > 0.0)
        {
            color += vec4(Albedo,1.0f) * spotLight.diffuse * NdotL;

            // Specular
            vec3 V = normalize(camPosition - fragPos);
            vec3 R = normalize(reflect(-L, N));

            float RdotV = max(dot(R, V), 0.0);
            color += vec4(Albedo,1.0f) * spotLight.specular * pow(RdotV, spec);
        }

        // Attenuate color with spot factor
        color *= spotFactor;
    }

    // Attenuation factor
    float d = length(spotLight.position - fragPos);
    float att =   spotLight.constant
                + spotLight.linear * d
                + spotLight.quadratic * d * d;

    return att * color;
}
void main()
{

    fragColor = vec4(0,0,0,0);
    // retrieve data from G-buffer
    if(texture(gAlbedoSpec, UV).a == 1.0f)
        discard;
    vec3 fragPos = texture(gPosition, UV).xyz;
    vec3 fragNormal = texture(gNormal, UV).xyz;
    vec3 Albedo = texture(gAlbedoSpec, UV).xyz;
    float Specular = texture(gNormal, UV).a;

    for (int i = 0; i < NB_LIGHTS; ++i)
        fragColor += computeDirLighting(dirLight[i],fragPos,fragNormal,Albedo,Specular);
   // for (int i = 0; i < NB_LIGHTS; ++i)
      //  fragColor += computePointLighting(pointLight[i],fragPos,fragNormal,Albedo,Specular);


        fragColor =clamp(fragColor , 0.0,1.0);
}
