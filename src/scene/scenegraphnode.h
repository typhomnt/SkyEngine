#ifndef SCENEGRAPH_NODE_H
#define SCENEGRAPH_NODE_H

#include "utils/skyengine_dll.h"

#include <memory>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <iostream>
#include <unordered_map>
#include "../utils/utilities.h"
#include "scenegraph.h"
#include "nodeparameter.h"
#include "core/serializable.h"
namespace skyengine
{

namespace scene
{
class SceneGraph;
class SKYENGINE_API SceneGraphNode: public core::Serializable
{
public:
    // The different "types" a SceneGraphNode can have
    enum Type
    {
        Renderable = 1 << 0,
        GameObject = 1 << 1,
        GameObjectSkinned = 1 << 2,
        Updatable = 1 << 3
    };

    SceneGraphNode(const std::string& name, SceneGraph *owner = nullptr,
                   const unsigned int type = 0,
                   const glm::mat4& world_matrix = glm::mat4(1.0f));
    virtual ~SceneGraphNode();
    inline const std::string& getName() const;
    inline SceneGraphNode* getParent();
    inline const std::vector<SceneGraphNode*>& getChildren() const;
    inline unsigned int getType() const;
    inline SceneGraph* getGraphOwner();
    virtual std::shared_ptr<SceneGraphNode> Duplicate();
    std::string getDuplicateName(const std::string& input_name);
    bool hasParameter(const std::string& p_name);

    template<class T>
    void addParameter(std::shared_ptr<NodeParameter<T>> parameter)
    {
        utility::MapInsert<std::string,std::shared_ptr<baseNodeParameter> >(m_parameters,parameter->name,parameter,std::string("addParameter : Parameter ") + parameter->name + std::string(" already exists"));
    }

    inline std::shared_ptr<baseNodeParameter> getParameter(const std::string& p_name);

    void removeParameter(const std::string& parameter_name);

    //Matrix Getters
    inline const glm::mat4& getParentMatrix() const;
    inline const glm::mat4& getLocalMatrix() const;
    inline const glm::mat4& getWorldMatrix() const;

    inline const glm::vec3& getCenter() const;

    void setType(unsigned int type);
    void setParent(SceneGraphNode* parent);

    void removeChild(const std::string& child_name);

    //Matrix setters
    virtual void setParentMatrix(const glm::mat4& parent_matrix);
    virtual void setLocalMatrix(const glm::mat4& local_matrix);
    virtual void setWorldMatrix(const glm::mat4& world_matrix);

    void setCenter(const glm::vec3& center);

    inline bool isRenderable() const;

    void addChild(const std::shared_ptr<SceneGraphNode>& child);

    virtual void Serialize(rapidjson::Value&,rapidjson::MemoryPoolAllocator<>&);
    virtual std::string getTag() {return "SceneGraphNode";}

    static std::shared_ptr<SceneGraphNode> Load(
        const std::string& name,
        const rapidjson::Value& value,
        SceneGraph* const sceneGraph
    );

protected:
    // The name acts as the node unique identifier
    std::string m_name;

    // This tag indicates the node "type" (e.g it is a renderable object)
    unsigned int m_type;



    glm::mat4 m_parent_matrix;
    glm::mat4 m_local_matrix;
    glm::mat4 m_world_matrix;

    glm::vec3 m_center;

    // Node parameters which are plugs of indetermined type
    std::unordered_map<std::string,std::shared_ptr<baseNodeParameter> > m_parameters;

    // Hierarchy attributes
    SceneGraphNode* m_parent;
    std::vector<SceneGraphNode* > m_children;

    //SceneGraph Container
    SceneGraph* m_scene_graph;

    //World Matrix update by going through the hierarchy
    void UpdateHierarchicalWorldMatrix();

    //Load property function => to Override and call from derived clss
    virtual void LoadProp(const rapidjson::Value& value);
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);

    friend class SceneGraph;
};


inline SceneGraphNode* SceneGraphNode::getParent()
{
    return m_parent;
}
inline unsigned int SceneGraphNode::getType() const
{
    return m_type;
}

inline const glm::mat4& SceneGraphNode::getParentMatrix() const
{
    return m_parent_matrix;
}

inline const glm::mat4& SceneGraphNode::getLocalMatrix() const
{
    return m_local_matrix;
}

inline const glm::mat4& SceneGraphNode::getWorldMatrix() const
{
    return m_world_matrix;
}

inline const glm::vec3& SceneGraphNode::getCenter() const
{
    return m_center;
}

inline const std::string& SceneGraphNode::getName() const
{
    return m_name;
}


inline bool SceneGraphNode::isRenderable() const
{
    return m_type & (
        Type::Renderable |
        Type::GameObject |
        Type::GameObjectSkinned
    );
}

inline const std::vector<SceneGraphNode*>& SceneGraphNode::getChildren() const
{
    return m_children;
}

inline SceneGraph* SceneGraphNode::getGraphOwner()
{
    return m_scene_graph;
}

inline std::shared_ptr<baseNodeParameter> SceneGraphNode::getParameter(const std::string& p_name)
{
    std::shared_ptr<baseNodeParameter> param = nullptr;
    utility::MapFind(
        m_parameters,
        p_name,
        param,
        "SceneGraphNode::getParameter : param " + p_name + " not found"
    );
    return param;
}

} // namespace scene

} // namespace skyengine

#endif // SCENEGRAPH_NODE_H
