#version 330 core
uniform vec4 color;

// Output variable
out vec4 fragColor;



void main()
{

    fragColor = clamp(color, 0.0, 1.0);
}
