#include "animationcomposer.h"
#include "core/functioncalltask.h"
namespace skyengine
{

namespace animation
{
AnimationComposer::AnimationComposer() : AnimatorInterface()
{

}

void AnimationComposer::AddAnimator(Animator* animator)
{
    AnimationComposer* composer = AnimationComposer::GetSingleton();
    composer->m_composed_animator.push_back(animator);
}

void AnimationComposer::RemoveAnimator(Animator* animator)
{
    AnimationComposer* composer = AnimationComposer::GetSingleton();
    utility::vectorRemoveValue(composer->m_composed_animator,animator);
}

#ifdef __GUI__
void AnimationComposer::RemoveAnimator(unsigned int animator_id)
{
    AnimationComposer* composer = AnimationComposer::GetSingleton();
    for(unsigned int i = 0; i < composer->m_composed_animator.size() ; ++i)
        if(composer->m_composed_animator[i]->getId() == animator_id)
        {
            composer->m_composed_animator.erase(composer->m_composed_animator.begin() + i);
            return;
        }

}

#endif


void AnimationComposer::computeAnimation(float time_stamp)
{
    for(Animator* animator : m_composed_animator)
        animator->computeAnimation(time_stamp);
}

void AnimationComposer::PlayComposer(core::SkyEngine* play_engine)
{
    AnimationComposer* composer = AnimationComposer::GetSingleton();
    composer->playAnimation(play_engine);
}

void AnimationComposer::StopComposer(core::SkyEngine* play_engine)
{
    AnimationComposer* composer = AnimationComposer::GetSingleton();
    composer->stopAnimation(play_engine);
}


void AnimationComposer::playAnimation(core::SkyEngine* play_engine)
{
    play_engine->addTask(core::makeFunctionCallTask(&AnimationComposer::play,this),"Animator");
}

void AnimationComposer::play()
{
    if(m_time_stamp <= m_end_time)
    {
        //TODO update paly speed
        computeAnimation(m_time_stamp);

        float dt = m_play_speed;
        m_time_stamp +=  dt;
    }

    if(m_time_stamp > m_end_time && m_is_looping)
        m_time_stamp = m_start_time;
}

}
}
