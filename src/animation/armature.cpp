#include "armature.h"
#include "geometry/geometryalgorithms.h"
#include "render/defaultMesh.h"
#include "glm/gtx/transform.hpp"
namespace skyengine
{

namespace animation
{
Armature::Armature(const std::string& name, Skeleton* skel, scene::SceneGraph *owner ,
                   const unsigned int type,
                   const glm::mat4& world_matrix) : GameObject3D(name,owner,type,world_matrix), m_skel(skel)
{
    m_mesh =  geometry::SpherePrimitive(10,10);
    setRenderShader(dynamic_cast<ressource::Shader*>(ressource::RessourceManager::Get("instance_shader").get()));

}

void Armature::update(double,double)
{
    //m_skel->updateHierarchicalTransforms();
    render::DefaultMesh* mesh = dynamic_cast<render::DefaultMesh*>(m_mesh.get());
    mesh->clearTransforms();
    for(auto& bone : m_skel->getTargets())
        mesh->addTransform(glm::translate(glm::mat4(1.0f),m_skel->getBonePt(bone)->global_pose.translation()));
    mesh->setInstanceNumber(m_skel->getBones().size());
}

/* Replace by Update task
 * void Armature::setShaderUniforms()
{
    //m_skel->updateHierarchicalTransforms();
    render::DefaultMesh* mesh = dynamic_cast<render::DefaultMesh*>(m_mesh.get());
    mesh->clearTransforms();
    for(auto& bone : m_skel->getBones())
        mesh->addTransform(glm::translate(glm::mat4(1.0f),bone.second->global_pose.translation()));
    mesh->setInstanceNumber(m_skel->getBones().size());
}*/


}
}
