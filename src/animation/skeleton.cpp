#include "skeleton.h"
#include "armature.h"
#include "ressources/ressourcemanager.h"
#include "utils/utilities.h"

#include <iostream>
#include <string>
#include <glm/gtx/matrix_decompose.hpp>

namespace skyengine
{

namespace animation
{

Skeleton::Skeleton(const std::string &name) : ressource::Ressource(name)
  ,root_bone(nullptr)
  ,m_root_mvt_bone(nullptr)
  ,m_core_mvt_bone(nullptr)
  ,m_head_mvt_bone(nullptr)
  ,m_global_inverse_transform(glm::mat4(1.0f))
{
}

Skeleton::~Skeleton()
{

}

bool Skeleton::SameStructure(const Skeleton& skel1 , const Skeleton& skel2)
{
    //Check name
    if(skel1.getRootBone()->bone_name == skel2.getRootBone()->bone_name)
    {
        return Skeleton::SameStructureRec(*(skel1.getRootBone()),*(skel2.getRootBone()));
    }
    else
        return false;


}
bool Skeleton::SameStructureRec(const Bone3D& bone1, const Bone3D& bone2)
{
    //Starting here the bone1 and bone2 names match
    bool same_struct = true;
    //We compare child size
    if(bone1.children.size() != bone2.children.size())
        return false;

    //Starting here we will recursivelly test the structure match on children
    for(std::vector<Bone3D*>::const_iterator it = bone1.children.begin() ;
        it < bone1.children.end(); it++)
    {
        bool contain_child = false;
        for(Bone3D* child : bone2.children)
        {
            if(child->bone_name == (*it)->bone_name)
            {
                contain_child = true;
                same_struct = same_struct && SameStructureRec(**it,*child);
                break;
            }
        }
        if(contain_child == false)
            return false;
        if(same_struct == false)
            return false;
    }
    return true;
}

void Skeleton::addBone(std::shared_ptr<Bone3D> bone, const std::string &parent_name)
{
    if(utility::MapInsert<std::string,std::shared_ptr<Bone3D>>(m_bones,bone->bone_name,bone,"addBone bone: " + bone->bone_name + "already exists."))
    {
        if(parent_name != "" && utility::MapContains(m_bones,parent_name))
        {
            m_bones[parent_name]->children.push_back(bone.get());
            bone->parent = m_bones[parent_name].get();
        }
    }
}

void Skeleton::addBone(const Bone3D& bone, const std::string &parent_name)
{
    addBone(std::make_shared<Bone3D>(bone),parent_name);
}


void Skeleton::printSkeleton() const
{
    printBoneRec(root_bone);
}

void Skeleton::printBoneRec(Bone3D* bone) const
{
    utility::Logger::Debug(bone->bone_name);
    for(Bone3D* boneC : bone->children)
        if(boneC != nullptr)
            printBoneRec(boneC);
}

void Skeleton::setRootBone(Bone3D* root_bone)
{
    this->root_bone = root_bone;
}

bool Skeleton::containsBone(const std::string& bone_name) const
{
    return utility::MapContains<std::string,std::shared_ptr<Bone3D> >(m_bones,bone_name);
}

Bone3D* Skeleton::getRootBone() const
{
    return root_bone;
}


Bone3D* Skeleton::getBonePt(const std::string& bone_name)
{
    std::shared_ptr<Bone3D> out_bone = nullptr;
    utility::MapFind<std::string,std::shared_ptr<Bone3D>>(
                m_bones,
                bone_name,
                out_bone,
                "Skeleton::getBonePt : bone " + bone_name + " not found"
                );
    return out_bone.get();
}

const Bone3D &Skeleton::getBone(const std::string& bone_name) const
{
    std::shared_ptr<Bone3D> out_bone = std::make_shared<Bone3D>(error_bone);
    utility::MapFind<std::string,std::shared_ptr<Bone3D>>(
                m_bones,
                bone_name,
                out_bone,
                "Skeleton::getBone : bone " + bone_name + " not found"
                );
    return *(out_bone.get());
}

void Skeleton::updateHierarchicalTransforms()
{
    updateHierarchicalTransformsRec(root_bone->bone_name, m_global_inverse_transform);
}

void Skeleton::updateHierarchicalTransformsRec(const std::string& bone_name, const glm::mat4& hierarchical_transform)
{
    glm::mat4 global_transform = hierarchical_transform * m_bones[bone_name]->local_transform ;
    m_bones[bone_name]->hierarchical_transform = global_transform * m_bones[bone_name]->bone_offset;
    //update global pose
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(global_transform, scale, rotation, translation, skew, perspective);
    m_bones[bone_name]->global_pose = Pose3D(scale,rotation,translation);
    glm::decompose(m_bones[bone_name]->local_transform, scale, rotation, translation, skew, perspective);
    m_bones[bone_name]->local_pose = Pose3D(scale,rotation,translation);

    for(std::vector<Bone3D*>::const_iterator it= m_bones[bone_name]->children.begin() ;
        it < m_bones[bone_name]->children.end(); it++)
    {
        updateHierarchicalTransformsRec((*it)->bone_name,global_transform);
    }
}


std::shared_ptr<ressource::Ressource> Skeleton::Duplicate()
{
    std::string skel_name = ressource::RessourceManager::getDuplicateName(m_name);
    std::shared_ptr<Skeleton> copy_skeleton = Ressource::Create<Skeleton>(skel_name);
    copy_skeleton->setGloabalInverseTransform(m_global_inverse_transform);
    copySkeletonRec(copy_skeleton.get(),root_bone->bone_name,nullptr);
    copy_skeleton->setRootBone(copy_skeleton->m_bones[root_bone->bone_name].get());
    if( m_root_mvt_bone != nullptr)
        copy_skeleton->setRootMvtBone(m_root_mvt_bone->bone_name);
    if( m_core_mvt_bone != nullptr)
        copy_skeleton->setCoreMvtBone(m_core_mvt_bone->bone_name);
    if( m_head_mvt_bone != nullptr)
        copy_skeleton->setHeadMvtBone(m_head_mvt_bone->bone_name);
    copy_skeleton->m_ik_targets = m_ik_targets;
    copy_skeleton->m_effector_target_map = m_effector_target_map;
    copy_skeleton->m_ik_chains = m_ik_chains;

    return copy_skeleton;
}

void Skeleton::copySkeletonRec(Skeleton *copy_skeleton, const std::string& bone_name, Bone3D* parent)
{
    copy_skeleton->addBone(std::make_shared<Bone3D>(getBone(bone_name)));
    copy_skeleton->m_bones[bone_name]->children.clear();
    copy_skeleton->m_bones[bone_name]->parent = parent;
    if(parent != nullptr)
        parent->children.push_back(copy_skeleton->m_bones[bone_name].get());
    for(std::vector<Bone3D*>::const_iterator it= m_bones[bone_name]->children.begin() ;
        it < m_bones[bone_name]->children.end(); it++)
    {
        copySkeletonRec(copy_skeleton,(*it)->bone_name,copy_skeleton->m_bones[bone_name].get());
    }
}

void Skeleton::setGloabalInverseTransform(const glm::mat4& transform)
{
    m_global_inverse_transform = transform;
}

void Skeleton::setRootMvtBone(const std::string& bone_name)
{
    if(utility::MapContains(m_bones,bone_name))
        m_root_mvt_bone = m_bones[bone_name].get();
}

void Skeleton::setCoreMvtBone(const std::string& bone_name)
{
    if(utility::MapContains(m_bones,bone_name))
        m_core_mvt_bone = m_bones[bone_name].get();
}

void Skeleton::setHeadMvtBone(const std::string& bone_name)
{
    if(utility::MapContains(m_bones,bone_name))
        m_head_mvt_bone = m_bones[bone_name].get();
}


void Skeleton::computeSpineArticulations()
{
    if(m_root_mvt_bone != nullptr && m_core_mvt_bone != nullptr && m_head_mvt_bone != nullptr)
    {
        //Clear old bones
        m_spine_bones.clear();
        m_articulation_bones.clear();
        m_effector_bones.clear();


        //Compute Spine
        //Articulations from root are computed implicitally
        computeSpine(m_head_mvt_bone->bone_name,m_root_mvt_bone->bone_name);
        computeSpine(m_core_mvt_bone->bone_name,m_root_mvt_bone->bone_name);
        assert(utility::MapContains(m_spine_bones,m_root_mvt_bone->bone_name));
        //Compute articulations
        computeArticulations(m_head_mvt_bone->bone_name);
        computeArticulations(m_core_mvt_bone->bone_name);
        computeArticulations(m_root_mvt_bone->bone_name);
        //FinallyCompute end effectors
        for(auto& art : m_articulation_bones)
            computeEndEffectors(art.first);
    }

}


void Skeleton::computeSpine(const std::string& bone,const std::string& root)
{
    Bone3D* curr_bone = m_bones[bone].get();
    m_spine_bones[bone] = curr_bone;
    if(bone != root && curr_bone->parent != nullptr)
    {
        computeSpine(curr_bone->parent->bone_name,root);
    }
}

void Skeleton::computeArticulations(const std::string& bone)
{
    Bone3D* curr_bone = m_bones[bone].get();

    for(Bone3D* children : curr_bone->children)
        if(!utility::MapContains(m_spine_bones,children->bone_name))
            m_articulation_bones[children->bone_name] = children;

}

void Skeleton::computeEndEffectors(const std::string& bone)
{
    Bone3D* curr_bone = m_bones[bone].get();
    if(curr_bone->children.size() == 0)
        m_effector_bones[bone] = curr_bone;
    else
        for(Bone3D* child : curr_bone->children)
            computeEndEffectors(child->bone_name);
}

void Skeleton::addIKChain(const std::string& end_bone, unsigned int chain_length)
{
    if(utility::MapContains(m_bones,end_bone))
        m_ik_chains[end_bone] = chain_length;
}

void Skeleton::removeIKChain(const std::string& end_bone)
{
    utility::MapRemove(m_ik_chains,end_bone);
}

void Skeleton::setIKChainLength(const std::string& end_bone, unsigned int chain_length)
{
    if(utility::MapContains(m_ik_chains,end_bone))
        m_ik_chains[end_bone] = chain_length;
}

void Skeleton::addTargetToIK(const std::string& end_bone, const std::string &target_bone)
{
    if(utility::MapContains(m_ik_chains,end_bone)
            && utility::MapContains(m_bones,end_bone)
            && utility::MapContains(m_bones,target_bone))
        if(!utility::vectorContains(m_effector_target_map[end_bone],target_bone))
        {
            m_effector_target_map[end_bone].push_back(target_bone);
            if(utility::vectorContains(m_ik_targets,target_bone))
                m_ik_targets.push_back(target_bone);
        }
}

void Skeleton::setTargetToIK(const std::string& end_bone, const std::string& target_bone)
{
    if(utility::MapContains(m_ik_chains,end_bone))
    {
        m_effector_target_map[end_bone].clear();
        addTargetToIK(end_bone,target_bone);
    }
}

void Skeleton::removeTargetToIK(const std::string& end_bone, const std::string& target_bone)
{
    if(utility::MapContains(m_ik_chains,end_bone))
        utility::vectorRemoveValue(m_effector_target_map[end_bone],target_bone);
}

void Skeleton::addTargetBoneToEffector(const std::string& new_t_bone, const std::string& end_bone)
{
    if(!utility::MapContains(m_bones,new_t_bone) && utility::MapContains(m_bones,end_bone))
    {
        std::shared_ptr<Bone3D> new_bone = std::make_shared<Bone3D>();
        new_bone->bone_name =  new_t_bone;
        //new_bone->bone_offset = m_bones[end_bone]->bone_offset;
        new_bone->is_modifier = true;
        addBone(new_bone,root_bone->bone_name);
        m_ik_targets.push_back(new_t_bone);

    }
}

void Skeleton::createIKTgEff()
{
    for(auto& eff : m_effector_bones)
    {
        addTargetBoneToEffector(eff.first + "_tg",eff.first);
        addIKChain(eff.first,1);
        setTargetToIK(eff.first,eff.first + "_tg");
    }
}

std::shared_ptr<rapidjson::Value> Skeleton::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Ressource::SerializeProp(al);

    utility::JSONHelper::SaveMat(*value,"InvMat",m_global_inverse_transform,al);
    if (m_root_mvt_bone != nullptr )
        utility::JSONHelper::SaveString(*value, "RootMvt", m_root_mvt_bone->bone_name, al);
    if (m_core_mvt_bone != nullptr )
        utility::JSONHelper::SaveString(*value, "CoreMvt", m_core_mvt_bone->bone_name, al);
    if (m_head_mvt_bone != nullptr )
        utility::JSONHelper::SaveString(*value, "HeadMvt", m_head_mvt_bone->bone_name, al);

    rapidjson::Value spn_array(rapidjson::kArrayType);
    for(auto& spn : m_spine_bones)
        spn_array.PushBack(rapidjson::Value(spn.first.c_str(),al), al);
    value->AddMember(rapidjson::Value("Spine", al),
                     spn_array,
                     al);

    rapidjson::Value art_array(rapidjson::kArrayType);
    for(auto& art : m_articulation_bones)
        art_array.PushBack(rapidjson::Value(art.first.c_str(),al), al);
    value->AddMember(rapidjson::Value("Articulation", al),
                     art_array,
                     al);

    rapidjson::Value eff_array(rapidjson::kArrayType);
    for(auto& eff : m_effector_bones)
        eff_array.PushBack(rapidjson::Value(eff.first.c_str(),al), al);
    value->AddMember(rapidjson::Value("Effector", al),
                     eff_array,
                     al);

    rapidjson::Value ik_array(rapidjson::kArrayType);
    for(auto& ik : m_ik_chains)
    {
        rapidjson::Value chain_array(rapidjson::kArrayType);
        chain_array.PushBack(rapidjson::Value(ik.first.c_str(),al), al);
        chain_array.PushBack(rapidjson::Value(ik.second), al);
        ik_array.PushBack(chain_array, al);
    }
    value->AddMember(rapidjson::Value("IKs", al),
                     ik_array,
                     al);

    rapidjson::Value eff_tag_array(rapidjson::kArrayType);
    for(auto& eff_tg : m_effector_target_map)
    {
        rapidjson::Value target_arr(rapidjson::kArrayType);
        target_arr.PushBack(rapidjson::Value(eff_tg.first.c_str(),al), al);
        for(auto& tag : eff_tg.second)
            target_arr.PushBack(rapidjson::Value(tag.c_str(),al), al);
        eff_tag_array.PushBack(target_arr, al);
    }
    value->AddMember(rapidjson::Value("Eff_Tag_Map", al),
                     eff_tag_array,
                     al);

    if(root_bone != nullptr)
        root_bone->Serialize(*value,al);

    return value;
}

void Skeleton::CreateArmature(scene::SceneGraph* sg)
{
    std::shared_ptr<Armature> arm = std::make_shared<Armature>(m_name,
                                                               this);
    arm->setLocalMatrix(m_global_inverse_transform);
    sg->addNode(arm);
}

}
}
