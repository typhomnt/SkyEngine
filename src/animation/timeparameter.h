#ifndef TIMEPARAMETER_H
#define TIMEPARAMETER_H
#include <memory>
namespace skyengine
{

namespace animation
{
struct BaseTimeParameter
{
    BaseTimeParameter(float time = 0.0f) : time(time){}
    virtual ~BaseTimeParameter(){}
    virtual std::shared_ptr<BaseTimeParameter> copy() {return std::make_shared<BaseTimeParameter>(time);}
    float time;
};

template<typename T>
struct TimedParameter : public BaseTimeParameter
{
    T value;
    typedef T data_t;

    TimedParameter(const T& value, const float& time) : BaseTimeParameter(time),value(value){}
    TimedParameter(): BaseTimeParameter(){}

    virtual std::shared_ptr<BaseTimeParameter> copy() {return std::make_shared<TimedParameter<T>>(value,time);}

    inline TimedParameter operator + (const TimedParameter &param) const
    {
        return TimedParameter(value + param.value, time + param.time);
    }

    inline TimedParameter operator - (const TimedParameter &param) const
    {
        return TimedParameter(value - param.value, time - param.time);
    }

    inline TimedParameter operator*(const float& f) const
    {
        return TimedParameter(f*value, f*time);
    }

};
template<typename T>
inline TimedParameter<T> operator*(const float& f, const  TimedParameter<T>& param)
{
       return TimedParameter<T>(f*param.value, f*param.time);
}

}
}
#endif // TIMEPARAMETER_H
