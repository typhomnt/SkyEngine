#include "commons/sampleskyengine.h"

#include "core/functioncalltask.h"
#include "geometry/geometryalgorithms.h"
#include "gui/guimanager.h"
#include "render/drawtask.h"
#include "ressources/image.h"
#include "ressources/lights.h"
#include "ressources/pointcloud.h"
#include "ressources/shader.h"
#include "ressources/texture.h"
#include "scene/camera.h"
#include "scene/cameramanipulator.h"
#include "scene/cubeobject.h"
#include "scene/scenegraphmanager.h"

#include "gui/widgets/logwidget.h"
#include "gui/widgets/nodelist.h"
#include "gui/widgets/resourcelist.h"
#include "gui/widgets/shadereditor.h"
#include "gui/widgets/skywindow.h"
#include "gui/widgets/viewport.h"
#include "gui/widgets/drawtaskeditor.h"

#include "cvhwidget.h"

using namespace skyengine;


class CVHSample : public samples::SampleSkyEngine
{
public:
    CVHSample();
    ~CVHSample();

    void initScene() override;
    void initUserRessources() override;
    void initTasks() override;

private:
    // Shaders
    std::shared_ptr<ressource::Shader> m_pcShader;
    std::shared_ptr<ressource::Shader> m_meshShader;

    // Lights
    std::shared_ptr<ressource::DirLight> m_dLight;
    std::shared_ptr<ressource::PointLight> m_pLight;

    // Camera
    std::shared_ptr<scene::Camera> m_camera;

    // Models
    std::shared_ptr<scene::GameObject3D> m_cube;
    std::shared_ptr<scene::GameObject3D> m_pointCloud;
    std::shared_ptr<scene::GameObject3D> m_sphere;

    // Draw task
    std::shared_ptr<render::DrawTask> m_drawTask;

    // GUI
    std::shared_ptr<module::gui::GuiManager> m_guiManager;
    std::shared_ptr<module::gui::SkyEditor> m_editor;
};

CVHSample::CVHSample() : samples::SampleSkyEngine()
{}

CVHSample::~CVHSample()
{}

void CVHSample::initScene()
{
    auto sg = std::make_shared<scene::SceneGraph>();
    scene::SceneGraphManager::Add("default_scene", sg);

    sg->addNode(m_cube);
    sg->addNode(m_pointCloud);
    sg->addNode(m_sphere);

    m_drawTask = std::make_shared<render::DrawTask>(
        this,
        /*main_window->getSize(),*/
        main_window->eventManager()
    );
    m_drawTask->addShader("pointcloud_shader");
    m_drawTask->addShader("default_shader");
    m_drawTask->addShader("simple_display_shader");
    m_drawTask->addUniformValue(
        "forward",
        "default_shader",
        new scene::NodeParameter<glm::vec3*>("camPosition", 1, m_camera->getPositionPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "default_shader",
        new scene::NodeParameter<glm::mat4*>("V", 1, m_camera->getViewMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "default_shader",
        new scene::NodeParameter<glm::mat4*>("P", 1, m_camera->getProjectionMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "pointcloud_shader",
        new scene::NodeParameter<glm::mat4*>("V", 1, m_camera->getViewMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "pointcloud_shader",
        new scene::NodeParameter<glm::mat4*>("P", 1, m_camera->getProjectionMatrixPt())
    );
    m_drawTask->assignLightToShader("forward", "default_shader", m_dLight.get());
    m_drawTask->assignLightToShader("forward", "default_shader", m_pLight.get());
    m_drawTask->setSceneGraph(sg.get());

    m_guiManager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());
    m_editor = std::make_shared<module::gui::SkyEditor>(
        this,
        m_guiManager
    );

    module::gui::SkyWindow* window = m_editor->getSkyWindow();

    window->addWidget<module::gui::ResourceList>(
        "Resources",
        module::gui::SkyWindow::DockSlot::Left
    );

    window->addWidget<module::gui::NodeList>(
        "Node List",
        module::gui::SkyWindow::DockSlot::Left,
        sg.get()
    );

    window->addWidget<CVHWidget>(
        "3D Convex Hull",
        module::gui::SkyWindow::DockSlot::Right,
        sg.get()
    );

    auto viewport = window->addWidget<module::gui::Viewport>(
        "Viewport",
        module::gui::SkyWindow::DockSlot::Center
    );
    viewport->addDrawTask(m_drawTask.get());
    viewport->setActiveCamera(m_camera);

    auto shaderEditor = window->addWidget<module::gui::ShaderEditor>(
        "Shader Editor",
        module::gui::SkyWindow::DockSlot::Center
    );
    shaderEditor->hide();

    window->addWidget<module::gui::LogWidget>(
        "Logger",
        module::gui::SkyWindow::DockSlot::Footer
    );

    auto taskEditor = window->addWidget<module::gui::DrawTaskEditor>(
        "task editor",
        module::gui::SkyWindow::DockSlot::Center,
        viewport
    );
    taskEditor->hide();
}

void CVHSample::initUserRessources()
{
    m_pcShader = ressource::Ressource::Create<ressource::Shader>(
        "pointcloud_shader",
        "../shaders/pointcloud.vert",
        "../shaders/pointcloud.frag",
        "../shaders/pointcloud.geom"
    );
    m_meshShader = ressource::Ressource::Create<ressource::Shader>(
        "default_shader",
        "../shaders/default.vert",
        "../shaders/default.frag"
    );


    for (auto& res : ressource::RessourceManager::GetSingleton()->getRessources())
    {
        auto shader = std::dynamic_pointer_cast<ressource::Shader>(res.second);
        if (shader)
            shader->compileAndLink();
    }

    m_dLight = ressource::Ressource::Create<ressource::DirLight>("dir_light", 0);
    m_pLight = ressource::Ressource::Create<ressource::PointLight>("point_light", 0);

    m_camera = std::make_shared<scene::Camera>("camera");
    m_camera->setPosition(glm::vec3(-3,0,0));
    m_camera->updateCameraWithCurrentAttributes();

    m_cube = std::make_shared<scene::GameObject3D>("Cube");
    m_cube->setRenderShader(m_meshShader.get());
    m_cube->setMesh(geometry::CubePrimitive(1.0f));

    auto pc = std::make_shared<ressource::PointCloud>("pc_mesh");
    glm::vec4 color(1.0f, 0.0f, 0.0f, 1.0f);
    for (unsigned int i = 0; i < 10; ++i)
    {
        float x = (float)rand() / RAND_MAX;
        float y = (float)rand() / RAND_MAX;
        float z = (float)rand() / RAND_MAX;
        pc->addVertex(ressource::PCVertex(glm::vec3(x, y, z), 5.0f, color));
    }

    m_pointCloud = std::make_shared<scene::GameObject3D>("PointCloud");
    m_pointCloud->setRenderShader(m_pcShader.get());
    m_pointCloud->setMesh(pc);

    m_sphere = std::make_shared<scene::GameObject3D>("Sphere");
    m_sphere->setRenderShader(m_meshShader.get());
    m_sphere->setMesh(geometry::SpherePrimitive(10, 10));
}

void CVHSample::initTasks()
{

    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame, m_guiManager));
    m_editor->addTasksToEngine();
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender, m_guiManager));

    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}


int main()
{
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);

    CVHSample engineInstance;
    engineInstance.init();
    engineInstance.run();

    return 0;
}
