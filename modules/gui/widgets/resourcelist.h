#ifndef __RESOURCELIST_H__
#define __RESOURCELIST_H__

#include "skywidget.h"


namespace module
{

namespace gui
{

class ResourceList : public SkyWidget
{
public:
    ResourceList(const std::string& name, SkyWindow *sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                 std::shared_ptr<SelectObjectEvent> select_evt);

    void draw() override;
};

} // namespace gui

} // namespace module

#endif // __RESOURCELIST_H__
