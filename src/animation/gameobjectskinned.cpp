#include "gameobjectskinned.h"
#include "utils/utilities.h"
#include "utils/jsonhelper.h"
#include "skeletalanimationmodifier.h"
namespace skyengine
{

namespace animation
{
GameObjectSkinned::GameObjectSkinned(const std::string& name, scene::SceneGraph *owner,
                                     const unsigned int type,
                                     const glm::mat4& world_matrix) : GameObject3D(name,owner,type,world_matrix)
{
    m_type = SceneGraphNode::GameObjectSkinned;
    bone_transforms = new scene::NodeParameter<glm::mat4*>("gBones[0]",MAX_BONES);
    bone_transforms->value = new glm::mat4[MAX_BONES];
    addRenderShaderUniformVal("gBones[0]",bone_transforms);
}

void GameObjectSkinned::init()
{

}

void GameObjectSkinned::BeforeUpdate()
{

}


void GameObjectSkinned::addAnimation(SkeletalAnimation* animation)
{
    animations.insert(std::pair<std::string,SkeletalAnimation*>(animation->getName(),animation));
}

SkeletalAnimation* GameObjectSkinned::getAnimationPt(const std::string& animation_name)
{
    if(animations.find(animation_name) != animations.end())
        return animations.at(animation_name);
    throw new std::invalid_argument("animation: " + animation_name + " was not found");
}

void GameObjectSkinned::setSkeleton(Skeleton *skeleton)
{
    m_skeleton = skeleton;
}

Skeleton* GameObjectSkinned::getPSkeleton()
{
    return m_skeleton;
}

Skeleton& GameObjectSkinned::getRefSkeleton()
{
    return *m_skeleton;
}

const Skeleton& GameObjectSkinned::getConstSkeleton() const
{
    return *m_skeleton;
}

void GameObjectSkinned::setBoneTransforms(glm::mat4* transforms)
{
    bone_transforms->value = transforms;
}

std::shared_ptr<scene::SceneGraphNode> GameObjectSkinned::Duplicate()
{

    std::shared_ptr<GameObjectSkinned> duplicate_node = std::make_shared<GameObjectSkinned>(*this);
    duplicate_node->m_name = getDuplicateName(m_name);
    m_scene_graph->addNode(m_parent->getName(),duplicate_node);
    //TODO duplicate childs ???
    duplicate_node->m_mesh = std::dynamic_pointer_cast<ressource::AbstractMesh>(m_mesh->Duplicate());
    duplicate_node->m_skeleton = dynamic_cast<Skeleton*>(m_skeleton->Duplicate().get());
    duplicate_node->animations.clear();
    for(auto& anim  : animations)
    {
        SkeletalAnimation* skel_anim = dynamic_cast<SkeletalAnimation*>(anim.second->Duplicate().get());
        skel_anim->setAnimationSkeleton(duplicate_node->m_skeleton);
        utility::MapInsert(duplicate_node->animations,skel_anim->getName(),skel_anim);
    }
    duplicate_node->bone_transforms = new scene::NodeParameter<glm::mat4*>("gBones[0]",MAX_BONES);
    duplicate_node->bone_transforms->value = new glm::mat4[MAX_BONES];
    render_shader_uniform_values["gBones[0]"] = duplicate_node->bone_transforms;
    return duplicate_node;
}

std::shared_ptr<rapidjson::Value> GameObjectSkinned::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    std::shared_ptr<rapidjson::Value> obj = scene::GameObject3D::SerializeProp(al);
    utility::JSONHelper::SaveString(*(obj.get()),"Skeleton",m_skeleton->getName(),al);
    rapidjson::Value array(rapidjson::kArrayType);

    for(auto& anim : animations)
    {
        rapidjson::Value v_name;
        v_name.SetString(anim.first.c_str(),anim.first.size());
        array.PushBack(v_name, al);
    }

    obj->AddMember(
        rapidjson::Value("Animations", al),
        array,
        al
    );
    return obj;
}


}
}
