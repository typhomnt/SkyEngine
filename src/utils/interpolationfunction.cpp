#include "interpolationfunction.h"
#include "glm/gtx/matrix_decompose.hpp"
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
namespace skyengine
{

namespace utility
{
template<>
InterpolationFunction<glm::quat>* InterpolationFunction<glm::quat>::getLinearInterpolationFunc()
{
    static InterpolationFunction<glm::quat> linear([](const glm::quat& start, const glm::quat& end, float weight)
    {return glm::slerp(start,end,weight);});
    return &linear;
}


template<>
InterpolationFunction<bool>* InterpolationFunction<bool>::getDefaultInterpolationFunc()
{
    return InterpolationFunction<bool>::getDiscreteInterpolationFunc();
}

template<>
InterpolationFunction<glm::quat>* InterpolationFunction<glm::quat>::getDefaultInterpolationFunc()
{
    return InterpolationFunction<glm::quat>::getLinearInterpolationFunc();
}

template<>
InterpolationFunction<glm::mat4>* InterpolationFunction<glm::mat4>::getDefaultInterpolationFunc()
{
    static InterpolationFunction<glm::mat4> def([](const glm::mat4& start, const glm::mat4& end, float weight)
    {
        //TODO we assume that there are no skew and perspective in the matrices
        glm::vec3 scale_s;
        glm::quat rotation_s;
        glm::vec3 translation_s;
        glm::vec3 skew_s;
        glm::vec4 perspective_s;
        glm::decompose( start, scale_s, rotation_s, translation_s, skew_s, perspective_s);

        glm::vec3 scale_e;
        glm::quat rotation_e;
        glm::vec3 translation_e;
        glm::vec3 skew_e;
        glm::vec4 perspective_e;
        glm::decompose( end, scale_e, rotation_e, translation_e, skew_e, perspective_e);


        glm::mat4 translation_matrix = glm::translate(glm::mat4(1.0f),(1.0f - weight)*translation_s + weight*translation_e);
        glm::mat4 scale_matrix = glm::scale(glm::mat4(1.0f),(1.0f - weight)*scale_s + weight*scale_e );
        glm::mat4 rotation_matrix = glm::toMat4(glm::slerp(rotation_s,rotation_e,weight));

        return translation_matrix*rotation_matrix*scale_matrix;
    });
    return &def;
}

}
}

