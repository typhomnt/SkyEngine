#ifndef UPDATEABLE_H
#define UPDATEABLE_H

#include "utils/skyengine_dll.h"
#include <memory>
#include "task.h"
#include "skyengine.h"
namespace skyengine
{

namespace core
{


class SKYENGINE_API Updatable {

public:
    Updatable() {}
    virtual ~Updatable() {}

    /**
     * @brief update
     */
    virtual void update(double, double){}

};

class UpdateTask : public Task {
public:
    UpdateTask(std::shared_ptr<Updatable> u) : m_update {u} {}

    virtual void run() {m_update->update(engine->getTotalTime(), engine->getDeltaTime());}

private:
    std::shared_ptr<Updatable> m_update;

};
}

}
#endif // UPDATEABLE_H
