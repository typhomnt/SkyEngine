#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "utils/skyengine_dll.h"

#include "singleton.h"

#include <iostream>
#include <string>
#include <vector>


namespace skyengine
{

namespace utility
{

/**
 * @brief Simple Logger class, with different severity levels.
 */
class SKYENGINE_API Logger : public Singleton<Logger>
{
public:
    friend Logger* Singleton<Logger>::GetSingleton();

    Logger(const Logger&) = delete;
    Logger(const Logger&&) = delete;
    Logger& operator=(const Logger&) = delete;

    /**
     * @brief Logger severity levels.
     */
    enum class LogLevel : unsigned char
    {
        DEBUG = 0,
        INFO,
        WARNING,
        ERROR,
        FATAL
    };

    /**
     * @brief Sets the Logger minimal severity level. Logs will only be displayed if the level
     *        is >= than the mininal level.
     * @param minLevel Minmal severity level
     */
    static void SetMinLevel(LogLevel minLevel);

    /**
     * @brief   Logs a message with the DEBUG #LogLevel.
     * @details Message will be displayed to the standard output stream.
     * @param   message Message to log
     */
    static void Debug(const std::string& message);

    /**
     * @brief   Logs a message with the INFO #LogLevel.
     * @details Message will be displayed to the standard output stream.
     * @param   message Message to log
     */
    static void Info(const std::string& message);

    /**
     * @brief   Logs a message with the WARNING #LogLevel.
     * @details Message will be displayed to the standard error stream.
     * @param   message Message to log
     */
    static void Warning(const std::string& message);

    /**
     * @brief   Logs a message with the ERROR #LogLevel.
     * @details Message will be displayed to the standard error stream.
     * @param   message Message to log
     */
    static void Error(const std::string& message);

    /**
     * @brief   Logs a message with the FATAL #LogLevel and <b>exits the program</b>.
     * @details Message will be displayed to the standard error stream.
     * @param   message Message to log
     */
    static void Fatal(const std::string& message);

    /**
     * @brief Returns all messages logged since program start (logging history).
     */
    static const std::vector<std::string>& GetLogs();

    /**
     * @brief Clear the logging history.
     */
    static void ClearLogs();

private:
    LogLevel m_minLevel;
    std::vector<std::string> m_logs;

    Logger();

    static void Log(LogLevel level, const std::string& message);
};

} // namespace utility

} // namespace skyengine

#endif // __LOGGER_H__
