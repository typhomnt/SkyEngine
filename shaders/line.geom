#version 330

uniform mat4 V;
uniform mat4 P;

layout(lines) in;
layout(triangle_strip, max_vertices=4) out;

in VS_OUT
{
    vec4 color;
    vec3 dir;
} gs_in[];

out vec4 varColor;


void main()
{
    const float size = 0.5;
    vec4 dir = normalize(vec4(cross((vec4(1,0,0,0)).xyz,gl_in[0].gl_Position.xyz - gl_in[1].gl_Position.xyz).xy,0 ,0.0));
    varColor = gs_in[0].color;

    gl_Position = (gl_in[0].gl_Position + dir*size);
    EmitVertex();

    gl_Position = (gl_in[0].gl_Position - dir*size);
    EmitVertex();

    gl_Position = (gl_in[1].gl_Position + dir*size);
    EmitVertex();

    gl_Position = (gl_in[1].gl_Position - dir*size);
    EmitVertex();

    EndPrimitive();
}
