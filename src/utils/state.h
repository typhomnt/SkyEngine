#ifndef STATE_H
#define STATE_H
#include "statemachine.h"
#include <vector>

namespace skyengine
{

namespace utility
{
class StateMachine;
class State
{
public:
    State(StateMachine* context, bool is_instable= false);
    void addNextState(State* state);
    void setContext(StateMachine* context);
    void setPrecState(State* prec);
    void setIsInstable(bool is_instable);
    bool getIsInstable() const;
    virtual void handleState() = 0;
    virtual void printState() = 0;
protected:
    std::vector<State*> m_next_states;
    StateMachine* m_context;
    State* m_prec;
    bool m_is_instable;
};

}
}
#endif // STATE_H
