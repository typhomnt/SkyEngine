#ifndef CALLTASKS_H
#define CALLTASKS_H

#include "task.h"

namespace skyengine {
namespace core{


template<class F>
class FunctionCallTask : public Task
{
public:
    FunctionCallTask(F&& f) : func(f) {}
    void run() { func();}

private:
    F func;

};

template<class F, class ...Ts>
auto  makeFunctionCallTask(F&& f, Ts&& ...args) -> std::shared_ptr<FunctionCallTask< decltype(std::bind(f, args...))>>
{
    return std::make_shared<FunctionCallTask<decltype(std::bind(f, args...))>>(std::bind(f, args...));
}


}
}

#endif // CALLTASKS_H
