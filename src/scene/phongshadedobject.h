#ifndef PHONGSHADEDOBJECT_H
#define PHONGSHADEDOBJECT_H

#include "gameobject3d.h"

#include "../ressources/material.h"
#include "../ressources/phongmaterial.h"


namespace skyengine
{

namespace scene
{

class PhongShadedObject : public GameObject3D
{
public:
    PhongShadedObject(const std::string& name, scene::SceneGraph *owner = nullptr,
                      const unsigned int type = 0,
                      const glm::mat4& world_matrix = glm::mat4(1.0f),
                      const std::shared_ptr<ressource::Material>& material = ressource::PhongMaterial::Default);

    void setShaderUniforms();

private:
    std::shared_ptr<ressource::Material> m_material;
};

}
}
#endif // PHONGSHADEDOBJECT_H
