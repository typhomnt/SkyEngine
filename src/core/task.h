#ifndef TASK_H
#define TASK_H

#include "utils/skyengine_dll.h"

#include <memory>

#include "scene/scenegraphnode.h"


namespace skyengine
{

namespace core
{

class SkyEngine;

class SKYENGINE_API Task
{
public:
    Task();


    inline const std::shared_ptr<scene::SceneGraphNode>& getNode() const;

    virtual void run() = 0;
    void setTaskName(const std::string& name);
    inline const std::string& getName() const;
    inline SkyEngine* getEngine();

protected:
    std::shared_ptr<scene::SceneGraphNode> m_node;
    std::string m_task_name;
    SkyEngine* engine;

    friend class SkyEngine;
};


inline const std::shared_ptr<scene::SceneGraphNode>& Task::getNode() const
{
    return m_node;
}

inline const std::string& Task::getName() const
{
    return m_task_name;
}

inline SkyEngine* Task::getEngine()
{
    return engine;
}

} // namespace core

} // namespace skyengine


#endif // TASK_H
