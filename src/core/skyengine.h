#ifndef SKYENGINE_H
#define SKYENGINE_H

#include "utils/skyengine_dll.h"

#include "core/task.h"

#ifdef __GUI__
    #include "gui/guimanager.h"
#else
    #include "GLFW/glfw3.h"
#endif

namespace skyengine {
namespace core {

//! Main Engine Class
/*! Main class for engine initialization and runtime.
 * It is responsible for initialization of GLFW and GLEW,
 * owns and runs the task scheduler.
*/

class SKYENGINE_API SkyEngine
{

#ifdef __GUI__
    friend class module::gui::SkyEditor;
#endif
public:

    //! Constructor
    /*! Initialize GLFW
    */
    SkyEngine();

    //! Destructor
    /*! Terminate GLFW
    */
    virtual ~SkyEngine();

    //! OpenGL initialization
    /*! Needs to be called after the creation of the
     * main window
    */
    void initOpenGL();

    //! Main loop function
    /*! Update framerate and run the task scheduler
    */
    void run();

    //! Terminate function
    /*! When called, the main loop finish then end
    */
    void requestTerminate();

    //! Add task
    /*! Append a task to the task list
     * TODO: Should be improved with a real scheduler
    */
    virtual void addTask(std::shared_ptr<Task>);

    //! Add task with a name
    /*! Append a task to the task list
     * TODO: Should be improved with a real scheduler
    */
    virtual void addTask(std::shared_ptr<Task>,const std::string&);

    //! Remove task
    /*! Remove a task from the task list
     * TODO: Should be improved with a real scheduler
    */
    virtual void removeTask(const std::string&);

    //! Get total time
    /*! Get the whole simulation time in seconds
    */
    double getTotalTime() const {return total_t;}

    //! Get delta time
    /*! Get last frame time in seconds
    */
    double getDeltaTime() const {return delta_t;}

    //! Get glfw time
    /*! Get the whole simulation time in seconds. Useful for multithread requests
    */
    double getGLFWTime() const {return glfwGetTime();}

protected:

    std::vector<std::shared_ptr<Task>> task_list;
    std::vector<std::shared_ptr<Task>> add_list;
    std::vector<std::string> remove_list;

    double delta_t, total_t;
    bool running_;

    void removeTaskInternal(const std::string& task_name);
    void addTaskInternal(std::shared_ptr<Task>);

};

}
}

#endif // SKYENGINE_H
