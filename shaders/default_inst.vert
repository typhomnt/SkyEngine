#version 330 core
// // Input vertex data, different for all executions of this shader.
// layout(location = 0) in vec3 vertexPosition_modelspace;
// layout(location = 1) in vec2 vertexUV;
// layout(location = 2) in vec3 normal;

// out vec2 UV;
// out vec3 n;
// out vec3 l;
// out vec3 EyeDirection_cameraspace;
// out float visibility;
// // Values that stay constant for the whole mesh.
// uniform mat4 M;
// uniform mat4 V;
// uniform mat4 P;
// uniform vec3 LightPosition_worldspace;

// const float density = 0.007;
// const float gradient = 1.5;
// void main(){

//     // Output position of the vertex, in clip space : MVP * position
//     gl_Position =  P*V*M * vec4(vertexPosition_modelspace,1);

//     // Position of the vertex, in worldspace : M * position
//     vec3 Position_worldspace = (M * vec4(vertexPosition_modelspace,1)).xyz;

//     // Vector that goes from the vertex to the camera, in camera space.
//     // In camera space, the camera is at the origin (0,0,0).
//     vec3 vertexPosition_cameraspace = ( V * M * vec4(vertexPosition_modelspace,1)).xyz;
//     EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;

//     //For fog effect
//     float distance = length(vertexPosition_cameraspace);
//     visibility = exp(-pow(distance*density,gradient));
//     visibility = clamp(visibility,0.0,1.0);

//     vec3 LightPosition_cameraspace = ( V * vec4(LightPosition_worldspace,1)).xyz;
//     vec3 LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;
//     // The color of each vertex will be interpolated
//     // to produce the color of each fragment
//     UV = vertexUV;
//     // Normal of the computed fragment, in camera space
//     vec3 Normal_cameraspace = normalize(mat3(V*M)*normal);
//     n = normalize( Normal_cameraspace );
//     l = normalize( LightDirection_cameraspace );
// }

// Uniform variables
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform vec3 camPosition;

// Input variables
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 normal;
layout(location = 3) in mat4 instance_matrix;


// Output variables
out vec3 fragPos;
out vec3 fragNormal;
out vec2 fragUV;
out vec3 eyeVec;


void main()
{

    gl_Position = P * V * M* instance_matrix* vec4(vertexPosition_modelspace, 1.0);

    vec4 vertex_world =  M*instance_matrix*vec4(vertexPosition_modelspace, 1.0);
    fragPos = vec3(vertex_world) / vertex_world.w;

    // TODO : This will work unless we apply non-uniform scaling on the model !
    fragNormal = mat3(M) * normal;

    fragUV = vertexUV;
    eyeVec = camPosition - vertex_world.xyz;
}
