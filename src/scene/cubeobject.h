#ifndef CUBE_OBJECT_H
#define CUBE_OBJECT_H

#include "utils/skyengine_dll.h"
#include "scene/gameobject3d.h"


namespace skyengine
{

namespace scene
{

class SKYENGINE_API CubeObject : public GameObject3D
{
public:
    /**
     * @brief Constructs a origin-centered cube.
     * @param name, owner See GameObject3D constructor
     * @param size        Cube size
     */
    CubeObject(const std::string& name,
               SceneGraph* owner = nullptr,
               float size = 2.0f);

    float getCubeSize() const { return m_cubeSize; }

    virtual std::string getTag() {return "CubeObject";}
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);

private:
    float m_cubeSize;

    void build();

    void LoadProp(const rapidjson::Value& value) override;
};

} // namespace scene

} // namespace skyengine

#endif // CUBE_OBJECT_H
