/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SLATEMANAGERLISTENER_H
#define SLATEMANAGERLISTENER_H

#include "SlateManager.h"
#include "core/listener.h"

namespace ext_dvc
{

namespace slate
{


using StatusListener         = skyengine::core::Listener<StatusEvent>;
using DescriptionListener        = skyengine::core::Listener<DescriptionEvent>;
using SoftwareListener    = skyengine::core::Listener<SoftwareEvent>;
using Pen2DListener = skyengine::core::Listener<Pen2DEvent>;
using Pen3DListener   = skyengine::core::Listener<Pen3DEvent>;
using Object3DListener   = skyengine::core::Listener<Object3DEvent>;
using AccelerometerListener = skyengine::core::Listener<AccelerometerEvent>;
using HardwareListener = skyengine::core::Listener<HardwareEvent>;
using DiskStatusListener    = skyengine::core::Listener<DiskStatusEvent>;
using FileDescriptorListener = skyengine::core::Listener<FileDescriptorEvent>;
using FileContentListener   = skyengine::core::Listener<FileContentEvent>;
using FileContentsListener   = skyengine::core::Listener<FileContentsEvent>;
using FileOperationListener = skyengine::core::Listener<FileOperationEvent>;
using ErrorListener = skyengine::core::Listener<ErrorEvent>;

class SlateManagerListener : public StatusListener, public DescriptionListener, public SoftwareListener,public Pen2DListener,
        public Pen3DListener, public Object3DListener, public AccelerometerListener, public HardwareListener,
        public DiskStatusListener, public FileDescriptorListener, public FileContentListener,public FileContentsListener,
        public FileOperationListener,public ErrorListener
{
public:
    SlateManagerListener(SlateManager *manager = nullptr, bool is_active = true);


    virtual ~SlateManagerListener();

    virtual void listened(const StatusEvent*) {}
    virtual void listened(const DescriptionEvent*) {}
    virtual void listened(const SoftwareEvent*) {}
    virtual void listened(const Pen2DEvent* ) {}
    virtual void listened(const Pen3DEvent* ) {}
    virtual void listened(const Object3DEvent* ) {}
    virtual void listened(const AccelerometerEvent* ) {}
    virtual void listened(const HardwareEvent* ) {}
    virtual void listened(const DiskStatusEvent* ) {}
    virtual void listened(const FileDescriptorEvent* ) {}
    virtual void listened(const FileContentEvent* ) {}
    virtual void listened(const FileContentsEvent* ) {}
    virtual void listened(const FileOperationEvent* ) {}
    virtual void listened(const ErrorEvent* ) {}

    void setTranslationShift(const glm::vec3& t);
    void setRotationShift(const glm::vec3& r);

    inline const glm::vec3& getTranslationShift() const;
    inline const glm::vec3& getRotationShift() const;


protected:
    glm::vec3 m_translation_shift;
    glm::vec3 m_rotation_shift;
};

inline const glm::vec3& SlateManagerListener::getTranslationShift() const
{
    return m_translation_shift;
}

inline const glm::vec3& SlateManagerListener::getRotationShift() const
{
    return m_rotation_shift;
}


}

}

#endif // SLATEMANAGERLISTENER_H
