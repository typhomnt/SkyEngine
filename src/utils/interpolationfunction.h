#ifndef INTERPOLATIONFUNCTION_H
#define INTERPOLATIONFUNCTION_H
#include <functional>
#include <animation/timeparameter.h>
#include <glm/gtx/quaternion.hpp>
#include "math/mathutils.h"
namespace skyengine
{

namespace utility
{
class AbstractInterpolationFunction
{
public:

    virtual std::shared_ptr<animation::BaseTimeParameter> ComputeInterpolation(const animation::BaseTimeParameter&,const animation::BaseTimeParameter&,float) = 0;

protected:
    AbstractInterpolationFunction() {}
};

template<typename K>
class InterpolationFunction : public AbstractInterpolationFunction
{
public:
    InterpolationFunction(std::function<K(const K&,const K&, float)> function): m_function(function){}

    std::shared_ptr<animation::BaseTimeParameter> ComputeInterpolation(const animation::BaseTimeParameter&start,const animation::BaseTimeParameter& end,float weight) override
    {
        return ComputeInterpolation(dynamic_cast<const animation::TimedParameter<K>&>(start),dynamic_cast<const animation::TimedParameter<K>&>(end),weight);
    }

    virtual std::shared_ptr<animation::TimedParameter<K>> ComputeInterpolation(const animation::TimedParameter<K>& start, const animation::TimedParameter<K>& end, float weight)
    {return std::make_shared<animation::TimedParameter<K>>(m_function(start.value,end.value,weight),(1+weight)*start.time + weight*end.time);}

    inline std::function<K(const K&,const K&, float)> getFunction() const
    {return m_function;}

    static InterpolationFunction<K>* getLinearInterpolationFunc()
    {
        static InterpolationFunction<K> linear([](const K& start, const K& end, float weight)
        {return (1.0f - weight)*start + weight*end ;});
        return &linear;
    }

    static InterpolationFunction<K>* getDiscreteInterpolationFunc()
    {
        static InterpolationFunction<K> disc([](const K& start, const K& end, float weight)
        {return (weight <= 0.5) ? start : end;});
        return &disc;
    }

    static InterpolationFunction<K>* getDefaultInterpolationFunc()
    {
        static InterpolationFunction<K> def([](const K& start, const K& end, float weight)
        {return (1.0f - weight)*start + weight*end ;});
        return &def;
    }

    static InterpolationFunction<K>* getSmoothInterpolationFunc(float tension = DEFAULT_INTERPOLATION_TENSION
            ,float tangent_mult = DEFAULT_TANGENT_MULTIPLIER)
    {
        static InterpolationFunction<K> smth([tension,tangent_mult](const K& start, const K& end, float weight)
        {return math::CatmullInterpolation(start,end,weight,tension,tangent_mult);});
        return &smth;
    }


protected:
    std::function<K(const K&,const K&, float)> m_function;
};

template<>
InterpolationFunction<glm::quat>* InterpolationFunction<glm::quat>::getLinearInterpolationFunc();

template<>
InterpolationFunction<bool>* InterpolationFunction<bool>::getDefaultInterpolationFunc();

template<>
InterpolationFunction<glm::quat>* InterpolationFunction<glm::quat>::getDefaultInterpolationFunc();

template<>
InterpolationFunction<glm::mat4>* InterpolationFunction<glm::mat4>::getDefaultInterpolationFunc();


}
}
#endif // INTERPOLATIONFUNCTION_H
