#ifndef WINDOWEVENTLISTENER_H
#define WINDOWEVENTLISTENER_H

#include "../listener.h"
#include "glm/glm.hpp"

namespace skyengine
{

namespace core
{

class CloseEvent : public Listenable<CloseEvent>
{
public:
    CloseEvent() {}
    virtual ~CloseEvent() {}

    virtual void send() { notifyListeners(); }
};

class WindowResizeEvent : public Listenable<WindowResizeEvent>
{
public:
    WindowResizeEvent() {}
    virtual ~WindowResizeEvent() {}

    virtual void send(glm::ivec2 s ) { size_ = s;  notifyListeners(); }

    glm::ivec2 size() const {return size_;}

protected:
    glm::ivec2 size_;
};

class WindowMoveEvent : public Listenable<WindowMoveEvent>
{
public:
    WindowMoveEvent() {}
    virtual ~WindowMoveEvent() {}

    virtual void send(glm::ivec2 p ) { pos_ = p;  notifyListeners(); }

    glm::ivec2 pos() const {return pos_;}

protected:
    glm::ivec2 pos_;
};

class WindowIconifyEvent : public Listenable<WindowIconifyEvent>
{
public:
    WindowIconifyEvent() {}
    virtual ~WindowIconifyEvent() {}

    virtual void send(bool ico ) { iconified_ = ico;  notifyListeners(); }

    bool iconified() const {return iconified_;}

protected:
    bool iconified_;
};

class WindowFocusEvent : public Listenable<WindowFocusEvent>
{
public:
    WindowFocusEvent() {}
    virtual ~WindowFocusEvent() {}

    virtual void send(bool foc ) { focus_ = foc;  notifyListeners(); }

    bool focus() const {return focus_;}

protected:
    bool focus_;
};

using CloseEventListener = Listener<CloseEvent>;
using WindowResizeEventListener = Listener<WindowResizeEvent>;
using WindowMoveEventListener = Listener<WindowMoveEvent>;
using WindowIconifyEventListener = Listener<WindowIconifyEvent>;
using WindowFocusEventListener = Listener<WindowFocusEvent>;


class WindowEventManager
{
public:
    WindowEventManager() :
        closeEvent{std::make_shared<CloseEvent>()},
        windowResizeEvent{std::make_shared<WindowResizeEvent>()},
        windowMoveEvent{std::make_shared<WindowMoveEvent>()},
        windowIconifyEvent{std::make_shared<WindowIconifyEvent>()},
        windowFocusEvent{std::make_shared<WindowFocusEvent>()}

    {}
    virtual ~WindowEventManager(){}

    std::shared_ptr<CloseEvent> closeEvent;
    std::shared_ptr<WindowResizeEvent> windowResizeEvent;
    std::shared_ptr<WindowMoveEvent> windowMoveEvent;
    std::shared_ptr<WindowIconifyEvent> windowIconifyEvent;
    std::shared_ptr<WindowFocusEvent> windowFocusEvent;
};

class WindowEventListener :
        public CloseEventListener, public WindowResizeEventListener,
        public WindowMoveEventListener, public WindowIconifyEventListener,
        public WindowFocusEventListener
{
public:
    WindowEventListener(std::shared_ptr<WindowEventManager> wem):
        CloseEventListener(wem->closeEvent),WindowResizeEventListener(wem->windowResizeEvent),
        WindowMoveEventListener(wem->windowMoveEvent), WindowIconifyEventListener(wem->windowIconifyEvent),
        WindowFocusEventListener(wem->windowFocusEvent)
    {}
    virtual ~WindowEventListener(){}
    void setListenerActive(bool b){CloseEventListener::setListenerActive(b);WindowResizeEventListener::setListenerActive(b);
                WindowMoveEventListener::setListenerActive(b);WindowIconifyEventListener::setListenerActive(b);
                WindowFocusEventListener::setListenerActive(b);}

};
}
}

#endif // WINDOWEVENTLISTENER_H
