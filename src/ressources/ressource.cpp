#include "ressource.h"

#include "lights.h"
#include "pbrmaterial.h"
#include "phongmaterial.h"
#include "shader.h"

namespace skyengine
{

namespace ressource
{

Ressource::Ressource(const std::string& name)
    : m_name { name }
    , m_lock { std::make_shared<std::mutex>() }
{}

std::shared_ptr<Ressource> Ressource::Duplicate()
{
    std::string res_name = RessourceManager::getDuplicateName(m_name);
    return Ressource::Create<Ressource>(res_name);
}

void Ressource::Serialize(rapidjson::Value& curr_obj, rapidjson::MemoryPoolAllocator<>& al)
{
    std::shared_ptr<rapidjson::Value> obj = SerializeProp(al);
    curr_obj.AddMember(
        rapidjson::Value(m_name.c_str(), al),
        *(obj.get()),
        al
    );

}

std::shared_ptr<Ressource> Ressource::Load(const rapidjson::Value& value,
                                           const std::string& name,
                                           std::vector<Ressource*>& secondPass)
{
    std::string resName = value["Tag"].GetString();
    std::shared_ptr<Ressource> resource = nullptr;

    // TODO : better way to to this ???
    if (resName == "Shader")
        resource = std::make_shared<Shader>(name);
    else if (resName == "DirLight")
        resource = std::make_shared<DirLight>(name);
    else if (resName == "PointLight")
        resource = std::make_shared<PointLight>(name);
    else if (resName == "SpotLight")
        resource = std::make_shared<SpotLight>(name);
    else if (resName == "PBRMaterial")
        resource = std::make_shared<PBRMaterial>(name);
    else if (resName == "PhongMaterial")
        resource = std::make_shared<PhongMaterial>(name);

    if (resource)
    {
        if (resource->LoadProp(value))
            secondPass.push_back(resource.get());
    }

    return resource;
}

} // namespace ressource

} // namespace skyengine
