#ifndef __VIEWPORT_H__
#define __VIEWPORT_H__

#include "core/rendersurface.h"
#include "skywidget.h"

#include "render/drawtask.h"
#include "scene/cameramanipulator.h"


namespace module
{

namespace gui
{
class DrawTaskEditor;
class Viewport : public SkyWidget, public skyengine::core::RenderSurface
{
    friend class DrawTaskEditor;
public:
    Viewport(const std::string& name, SkyWindow* skyWindow
             ,std::shared_ptr<ActiveObjectEvent> act_evt
             ,std::shared_ptr<SelectObjectEvent> select_evt);

    glm::ivec2 getSize() const override;
    void resize(glm::ivec2) override;

    glm::ivec2 getPos() const override;
    void move(glm::ivec2) override;

    void bind() override {}

    void setActiveCamera(std::shared_ptr<skyengine::scene::Camera> camera);
    void addDrawTask(skyengine::render::DrawTask* drawTask);

    void draw() override;

private:
    skyengine::render::DrawTask* m_drawTask;
    skyengine::scene::CameraManipulator* m_active_camera_manip;
    skyengine::scene::Camera* m_active_camera;
};

} // namespavce gui

} // namespace module

#endif // __VIEWPORT_H__

