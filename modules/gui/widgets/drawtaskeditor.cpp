#include "drawtaskeditor.h"

namespace module
{

namespace gui
{

DrawTaskEditor::DrawTaskEditor(const std::string& name, SkyWindow* window,std::shared_ptr<ActiveObjectEvent> act_evt,
                               std::shared_ptr<SelectObjectEvent> select_evt, Viewport* viewport):
    SkyWidget(name,window,act_evt,select_evt), m_viewport(viewport)
{
    m_graph_node.registerNodeTypes(DrawNodeNames,2,DrawNodeFactory,NULL,-1);
    update();
}

void DrawTaskEditor::draw()
{
    m_graph_node.render();
}

void DrawTaskEditor::update()
{
    if(!m_graph_node.isEmpty())
        m_graph_node.clear();
    std::vector<ImGui::Node*> nodes;
    //insert clear color node
    ColorNode* node = dynamic_cast<ColorNode*>(m_graph_node.addNode(COLOR_NODE,ImVec2()));
    node->setColor(static_cast<ImVec4*>(m_sky_win->getManager()->getGColor()));
    nodes.push_back(node);

    for(unsigned int i = 0 ; i <  m_viewport->m_drawTask->m_task_fbo_order.size(); ++i)
    {
       FrameBufferNode* node = dynamic_cast<FrameBufferNode*>(m_graph_node.addNode(FBO_NODE,ImVec2()));
       node->AddFrameBufferInfo("FBO" + std::to_string(i),FrameBufferInfo(m_viewport->m_drawTask->m_task_fbo_order[i].get()));
       nodes.push_back(node);
    }
    for(unsigned int i = 2 ; i < nodes.size() ; ++i)
        m_graph_node.addLink(nodes[i-1],0,nodes[i],0);

}

}
}
