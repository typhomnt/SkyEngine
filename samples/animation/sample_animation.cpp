
#include "core/skyengine.h"
#include "scene/scenegraphnode.h"
#include "commons/sampleskyengine.h"
#include "core/functioncalltask.h"
#include "core/modelimportermanager.h"
#include "ressources/lights.h"
#include "scene/cameramanipulator.h"

#include "scene/scenegraphmanager.h"
#include "scene/quadobject.h"
#include "geometry/meshmodifier.h"

#include "utils/utilities.h"
#include "animation/skeletalanimator.h"
#include "geometry/geometryalgorithms.h"

#include "render/drawtask.h"
#include "ressources/ressourcemanager.h"

#include "gui/widgets/animator_gui.h"
#include "gui/widgets/nodelist.h"
#include "gui/widgets/resourcelist.h"
#include "gui/widgets/skywindow.h"
#include "gui/widgets/viewport.h"
#include "gui/widgets/shadereditor.h"


using namespace skyengine;

// temporay task for scenemanager draw

class SampleDefault : public samples::SampleSkyEngine
{
public:
    SampleDefault()  : SampleSkyEngine() {}
    ~SampleDefault();
    virtual void initScene() override;
    void initUserRessources() override;
    virtual void initTasks() override;

protected:

    std::shared_ptr<scene::Camera> sceneCamera;

    std::shared_ptr<ressource::Light> dirLight;
    std::shared_ptr<ressource::Light> pointLight;
    std::shared_ptr<animation::SkeletalAnimator> skel_anim;


    std::shared_ptr<render::DrawTask> draw_task;
    std::shared_ptr<module::gui::SkyEditor> m_editor;


    std::shared_ptr<module::gui::GuiManager> gui_manager;

};

void SampleDefault::initScene()
{

    // Write scene exemple
    scene::SceneGraphManager::Add("default_scene",std::make_shared<scene::SceneGraph>());
    core::ModelImporterManager* model_manager = core::ModelImporterManager::GetSingleton();

    //InitImporter
    model_manager->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    //-----------

    //Stage
    model_manager->GetSingleton()->loadModel("../meshes/basic_patform.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    geometry::MeshModifier::static_scale(glm::vec3(),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh-basic_patform").get())->getMesh()).get()),0.03f);
    geometry::MeshModifier::static_translate(glm::vec3(0,0,-0.1),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh-basic_patform").get())->getMesh()).get()));
    //Bob
    model_manager->GetSingleton()->loadModel("../meshes/bob.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    //Animator
    skel_anim = std::make_shared<animation::SkeletalAnimator>();
    skel_anim->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    skel_anim->setAnimatedSkeleton(dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get())->getPSkeleton());
    skel_anim->setIsLooping(true);
    animation::SkeletalAnimation* punch = dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get())->getAnimationPt("Armature|Punch");
    skel_anim->addAnimation("Punch",0,punch->getLastKeyFrame()->getTimeStamp(),punch,0);
    std::vector<std::string> run_bones;
    run_bones.push_back("Foot_R");
    run_bones.push_back("Foot_L");
    run_bones.push_back("Lower_leg_R");
    run_bones.push_back("Lower_leg_L");
    run_bones.push_back("Upper_Leg_R");
    run_bones.push_back("Upper_Leg_L");
    run_bones.push_back("Pelvis");
    animation::SkeletalAnimation* walk = dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get())->getAnimationPt("Armature|Walk_final");
    skel_anim->addAnimation("Walk",0,punch->getLastKeyFrame()->getTimeStamp(),walk,1,run_bones);
    skel_anim->playAnimation(this);

    //DrawTask
    draw_task = std::make_shared<render::DrawTask>(this/*, main_window->getSize()*/,main_window->eventManager(),true);
    draw_task->addShader("default_shader");
    draw_task->addShader("default_skin_shader");
    draw_task->addShader("default_pbr_shader");
    draw_task->addShader("default_skin_pbr_shader");
    draw_task->addShader("instance_shader");
    draw_task->addShader("toon_shader");
    draw_task->addShader("line_shader");
    draw_task->addShader("checkboard_shader");
    draw_task->addShader("deferredl_shader");
    draw_task->addShader("deferredg_shader");
    draw_task->addShader("deferredg_skin_shader");
    draw_task->addShader("simple_display_shader");
    //foward-----------------------------

    draw_task->assignLightToShader("forward","default_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_shader",pointLight.get());
    draw_task->assignLightToShader("forward","default_pbr_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_pbr_shader",pointLight.get());
    draw_task->assignLightToShader("forward","default_skin_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_skin_shader",pointLight.get());
    draw_task->assignLightToShader("forward","default_skin_pbr_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_skin_pbr_shader",pointLight.get());
    draw_task->assignLightToShader("forward","instance_shader",dirLight.get());
    draw_task->assignLightToShader("forward","instance_shader",pointLight.get());
    draw_task->assignLightToShader("forward","toon_shader",dirLight.get());
    draw_task->assignLightToShader("forward","toon_shader",pointLight.get());

    //deferred-------------------------------   
    draw_task->setFramebufferCamera(sceneCamera.get());
    draw_task->assignLightToShader("deferred_light","deferredl_shader",dirLight.get());
    draw_task->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());

    //Gui tasks
    gui_manager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());
    m_editor = std::make_shared<module::gui::SkyEditor>(
        this,
        gui_manager
    );
    module::gui::SkyWindow* window = m_editor->getSkyWindow();
    window->addWidget<module::gui::NodeList>(
        "Node List",
        module::gui::SkyWindow::DockSlot::Left,
        scene::SceneGraphManager::Get("default_scene").get()
    );
    window->addWidget<module::gui::ResourceList>(
        "Resources",
        module::gui::SkyWindow::DockSlot::Left
    );
    window->addWidget<module::gui::ActionPlayer>(
        "Action Player",
        module::gui::SkyWindow::DockSlot::Center,
        skel_anim.get(),
        scene::SceneGraphManager::Get("default_scene").get(),
        dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get())
    );
    window->addWidget<module::gui::ActionAnimator>(
        "Action Animator",
        module::gui::SkyWindow::DockSlot::Center,
        skel_anim.get()
    );
    window->addWidget<module::gui::ArmatureDisplay>(
        "Armature Display",
        module::gui::SkyWindow::DockSlot::Center,
        dynamic_cast<animation::Armature*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob_skeleton").get())
    );
    window->addWidget<module::gui::AnimationEditor>(
        "Animation Editor",
        module::gui::SkyWindow::DockSlot::Center,
        std::dynamic_pointer_cast<module::gui::ActionPlayer>(window->getWidget("Action Player"))->getAnimationActiveEvent()
    );

    auto viewport = window->addWidget<module::gui::Viewport>(
        "Viewport",
        module::gui::SkyWindow::DockSlot::Center
    );
    viewport->addDrawTask(draw_task.get());
    viewport->setActiveCamera(sceneCamera);

    window->addWidget<module::gui::ShaderEditor>(
        "Shader Editor",
        module::gui::SkyWindow::DockSlot::Center
    );
}

void SampleDefault::initUserRessources()
{

    //Init Camera, Lights and Shaders
    sceneCamera = std::make_shared<scene::Camera>("camera");
    sceneCamera->setPosition(glm::vec3(-10,3,0));
    sceneCamera->setDirection(glm::vec3(2.0,-1.0,1.0));


    dirLight = ressource::Ressource::Create<ressource::DirLight>("dirLight", 0);
    pointLight = ressource::Ressource::Create<ressource::PointLight>("pointLight", 0);

    //---------------------------
    sceneCamera->updateCameraWithCurrentAttributes();

}

void SampleDefault::initTasks()
{
    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    addTask(draw_task);
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame,gui_manager));    
    m_editor->addTasksToEngine();
    //addTask(core::makeFunctionCallTask(&module::gui::GuiManager::ApplicationInfoWindow,gui_manager));
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender,gui_manager));
    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}



SampleDefault::~SampleDefault()
{
}


int main()
{
    SampleDefault engineInstance;
    //For random use
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);
    engineInstance.init();
    engineInstance.run();

    return 0;
}

