/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*!
   \file: AABBoxT.h

   Language: C++

   License: Expressive license

   \author: Maxime Quiblier
   E-Mail:  maxime.quiblier@inrialpes.fr

   Description: Header for an axis aligned bounding box classes.

   Platform Dependencies: None
*/

#pragma once
#ifndef AXIS_BOUNDING_BOX_H_
#define AXIS_BOUNDING_BOX_H_
#include <assert.h>
#include <string>
#include <glm/glm.hpp>
///////////////////////////////////////////////////////////////////////////////////////////////
/// TODO:@todo : make this class safe with regards to space dimension (Point in 2D, 3D, 4D ...)
/// TODO:@todo : check what is the more efficient (min-max or center-halfsize, what of avx ???)
/// TODO:@todo : in both case all function could be simplified using eigen functionnality
///////////////////////////////////////////////////////////////////////////////////////////////

namespace ext_dvc {

namespace slate {

/*!
 * \brief AxisAligned bounding box
 * A simple axis aligned bounding box class, templated with a point type that defines the dimension of the AABbox (Point2, Point3D...).
 */
template<typename PointT>
class AABBoxT
{
public:
    typedef PointT VectorT;

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////

    /**
     * @brief AABBoxT Constructs a new AABBox, which is a copy of the given one.
     * @param bbox The bounding box to copy.
     */
    AABBoxT(const AABBoxT& bbox) : min_(bbox.min_), max_(bbox.max_) {}
    /**
     * @brief AABBoxT Constructs a new AABBox.
     * @param min A vector containing the minimum values along each axis.
     * @param max A vector containing the maximum values along each axis.
     * @param reorder if true, this constructor will check if min and max are consistent, and reorder them if necessary.
     */
    AABBoxT(const VectorT& min, const VectorT& max, bool reorder = false);


    /**
     * @brief AABBoxT Default constructor that inizializes the AABBox to a null box (it's minimum value is +infinity, and max is -infinity (or almost).
     */
    AABBoxT() : min_(FLT_MAX*0.25f), max_(FLT_MIN*0.25f) {}    // divide by 4 to be sure that the size of the bounding box is usable.
    ~AABBoxT() {}

    /**
     * @brief to_string returns this AABBox as a std::string.
     * @return the AABBox as a string.
     */
    std::string to_string() const;

    /**
     * @brief Equals checks if box is equal to another.
     * @param other the box to be compared with.
     */
    bool Equals(const AABBoxT& other);

    /**
     * @brief ComputeSizeMax Compares the size of the box along each axis and returns the largest length.
     * @return the largest dimension.
     */
    float ComputeSizeMax() const;

    /**
     * @brief ComputeSizeMax Compares the size of the box along each axis and returns the smallest length.
     * @return the smallest dimension.
     */
    float ComputeSizeMin() const;

    /**
     * @brief ComputeCenter
     * @return the center point of the box.
     */
    inline PointT ComputeCenter() const
    {
        return 0.5f*(min_+max_);
    }

    /**
     * @brief IsEmpty
     * @return true if the box is empty (each axis' length is lower or equal to 0.0)
     */
    bool IsEmpty() const;

    /**
     * @brief Contains
     * @param point an arbitrary point
     * @return true if the given point is inside the box.
     */
    inline bool Contains(const PointT& point) const
    {
//        return (min_.array() <= point.array()).all() && (point.array() <= max_.array()).all();
        //TODO:@todo : not "safe" : check if this is really faster than a loop ...
        //             or if this can be writen directly with eigen function ...
        for(unsigned int i = 0 ; i < point.length(); i++)
            if (point[i] < min_[i] || point[i] > max_[i])
                return false;


        return true;
//        return point[0] > min_[0] && point[0] < max_[0] &&
//               point[1] > min_[1] && point[1] < max_[1] &&
//               point[2] > min_[2] && point[2] < max_[2];
        //        return (min_.array() < point.array()).all() && (max_.array() > point.array()).all();
    }

    /**
     * @brief Contains
     * @param other an arbitrary AABBox with the same dimension as this one.
     * @return true if this contains _completely_ %other.
     */
    bool Contains(const AABBoxT& other) const;

    /**
     * @brief Contains
     * @param other an arbitrary AABBox with the same dimension as this one.
     * @return true if this contains _completely_ or equals to %other.
     */
    bool ContainsOrEquals(const AABBoxT& other) const;

    /**
     * @brief Contains
     * @param other an arbitrary AABBox with the same dimension as this one.
     * @return true if this intersects with %other.
     */
    bool Intersect(const AABBoxT& other) const;

    /**
     * @brief Scale  Scale the bounding box from the center
     * @param factor the factor to scale the box with.
     */
    void Scale(float factor);

    /**
     * @brief Scale  Scale the bounding box from the center
     * @param factor the factor to scale the box with.
     */
    void Scale(const PointT& factor);

    /**
     * @brief Intersection
     * @param abb1 an arbitrary box.
     * @param abb2 another arbitrary box of same dimension.
     * @return the intersection between those 2 boxes : another box of same dimension.
     */
    static AABBoxT Intersection(const AABBoxT& abb1,const AABBoxT& abb2);

    /**
     * @brief Intersection
     * @param other an arbitrary box.
     * @return the intersection between those 2 boxes : another box of same dimension.
     */
    AABBoxT Intersection(const AABBoxT& other) const;

    /**
     * @brief Union
     * @param abb1 an arbitrary box.
     * @param abb2 another arbitrary box of same dimension.
     * @return the union between those 2 boxes : another box of same dimension.
     */
    static AABBoxT Union(const AABBoxT& abb1,const AABBoxT& abb2);

    /**
     * @brief Union
     * @param other an arbitrary box.
     * @return the union between those 2 boxes : another box of same dimension.
     */
    AABBoxT Union(const AABBoxT& other) const;

    /**
     * @brief HierarchyDistance Very special distance given as the max of the distance of each coordinate pair
     * This is used to know if it's good to regroup 2 boxes in boxes tree.
     * @param other another bounding box.
     * @return
     */
    float HierarchyDistance(const AABBoxT& other) const;

    /**
     * @brief FindIntersection Computes an intersection between this and a given ray.
     * @param ray a ray to compute intersection with/
     * @param dist_max the maximum range of the search
     * @return the distance if found, dist_max otherwise.
     */
    float FindIntersection(const PointT& ray_start, const PointT& ray_dir, float dist_max) const;

    /**
     * @brief IsBehind is a fast way to make sure that a bounding box is behind a ray
     * @param ray a ray to compute intersection with/
     * @return true if the bb is behind the ray
     */

    bool isBehind(const PointT& ray_start, const PointT& ray_dir);


    /**
     * @brief Volume
     * @returns the volume of this bounding box.
     */
    float Volume() const;

    ///////////////
    // Accessors //
    ///////////////

    /**
     * @brief min
     * @return A vector containing the minimum values along each axis.
     */
    inline const VectorT& min() const { return min_; }
    /**
     * @brief max
     * @return A vector containing the maximum values along each axis.
     */
    inline const VectorT& max() const { return max_; }

    inline  float &xmin() { return min_[0]; }
    inline float &xmax() { return max_[0]; }
    inline float &ymin() { return min_[1]; }
    inline float &ymax() { return max_[1]; }
    inline float &zmin() { assert(min_.length() > 2); return min_[2]; }
    inline float &zmax() { assert(max_.length() > 2); return max_[2]; }
    inline float &wmin() { assert(min_.length() > 3); return min_[3]; }
    inline float &wmax() { assert(max_.length() > 3); return max_[3]; }

    inline const float &xmin() const { return min_[0]; }
    inline const float &xmax() const { return max_[0]; }
    inline const float &ymin() const { return min_[1]; }
    inline const float &ymax() const { return max_[1]; }
    inline const float &zmin() const { assert(min_.length()> 2); return min_[2]; }
    inline const float &zmax() const { assert(max_.length() > 2); return max_[2]; }
    inline const float &wmin() const { assert(min_.length() > 3); return min_[3]; }
    inline const float &wmax() const { assert(max_.length() > 3); return max_[3]; }

    /**
      * Adds #point to this AABBoxT. Optional argument #radius defines the point radius.
      */
    AABBoxT &Enlarge(const VectorT &point, float radius = 0.0);

    /**
      * Returns a copy of this AABBoxT's union with #point.
      */
    AABBoxT Enlarged(const VectorT &point, float radius = 0.0) const;

    /**
      * Enlarge a bounding box in place by doing an union with another building box
      */
    AABBoxT &Enlarge(const AABBoxT& other);

    /**
      * Enlarge a copy of this bounding box by doing an union with another building box
      */
    AABBoxT Enlarged(const AABBoxT& other) const;

//    /**
//     * Applies a transformation to this AABBox.
//     */
//    template<typename T = PointT,
//    typename std::enable_if<std::is_same<T, Point>::value, bool>::type >
//    AABBoxT &Transform(const Transform &transform);

//    /**
//     * Applies a transformation to a copy of this AABBox.
//     */
//    template<typename T = PointT,
//    typename std::enable_if<std::is_same<T, Point>::value, bool>::type >
//    AABBoxT Transformed(const Transform &transform) const;

    /**
     * @brief width
     * @param i the axis along which the length is returned.
     * @return the width of a boundingbox. If no parameter is given, it's by default on x axis. otherwise, it can be any one of PointT's axis.
     */
    float width(unsigned int i = 0) const;

    /**
     * @brief height
     * @return the height of a boundingbox (y axis).
     */
    float height() const;

    /**
     * @brief height
     * @return the depth of a boundingbox (z axis).
     */
    float depth() const;

    /**
     * @brief GetLargestDimension Same as ComputeSizeMax, but returns the index of the axis instead.
     */
    unsigned int GetLargestDimension() const;

    /**
     * @brief GetLargestDimensionValue Same as #ComputeSizeMax. TODO : Why twice ?
     */
    float GetLargestDimensionValue() const;


    ///////////////
    // Attributs //
    ///////////////

    VectorT min_, max_;
};

template<typename PointT>
AABBoxT<PointT>::AABBoxT(const VectorT& min, const VectorT& max, bool reorder) : min_(min), max_(max)
{
    if (reorder) {
        for(unsigned int i = 0; i < min.length(); i++)
        {
            min_[i] = std::min(min[i], max[i]);
            max_[i] = std::max(min[i], max[i]);
        }
    }
}


template<typename PointT>
std::string AABBoxT<PointT>::to_string() const
{
    std::string res;
    res += "AABBox [" + std::to_string(min_[0]);
    for(unsigned int i = 1; i < min_.length(); i++)
    {
        res += ":" + std::to_string(min_[i]);
    }
    res += "]:[" + std::to_string(max_[0]);
    for(unsigned int i = 1; i < max_.length(); i++)
    {
        res += ":" + std::to_string(max_[i]);
    }
    res += "]";
    return res;
}

template<typename PointT>
bool AABBoxT<PointT>::Equals(const AABBoxT<PointT>& other)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    for(unsigned int i = 0; i < min_.length(); i++)
    {
        if(min_[i] != other.min_[i] || max_[i] != other.max_[i])
        {
            return false;
        }
    }
    return true;
}

template<typename PointT>
float AABBoxT<PointT>::ComputeSizeMax() const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    float max_max = max_[0] - min_[0];
    for(unsigned int i = 1; i < min_.length(); i++)
    {
        float aux = max_[i] - min_[i];
        if(aux>max_max) { max_max = aux; }
    }
    return max_max;
}

template<typename PointT>
float AABBoxT<PointT>::ComputeSizeMin() const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    float min_min = max_[0] - min_[0];
    for(unsigned int i = 1; i < min_.length(); i++)
    {
        float aux = max_[i] - min_[i];
        if(aux<min_min) { min_min = aux; }
    }
    return min_min;
}

template<typename PointT>
bool AABBoxT<PointT>::IsEmpty() const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    //TODO:@todo : not "safe" : check if this is really faster than a loop ...
    return(min_[0]>max_[0] || min_[1]>max_[1] || min_[2]>max_[2]);
}

template<typename PointT>
bool AABBoxT<PointT>::Contains(const AABBoxT& other) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    //TODO:@todo : not "safe" : check if this is really faster than a loop ...
    return other.min_[0] > min_[0] && other.max_[0] < max_[0] &&
        other.min_[1] > min_[1] && other.max_[1] < max_[1] &&
        other.min_[2] > min_[2] && other.max_[2] < max_[2];
}

template<typename PointT>
bool AABBoxT<PointT>::ContainsOrEquals(const AABBoxT& other) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    //TODO:@todo : not "safe" : check if this is really faster than a loop ...
    return other.min_[0] >= min_[0] && other.max_[0] <= max_[0] &&
        other.min_[1] >= min_[1] && other.max_[1] <= max_[1] &&
        other.min_[2] >= min_[2] && other.max_[2] <= max_[2];
}

template<typename PointT>
bool AABBoxT<PointT>::Intersect(const AABBoxT& other) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    //TODO:@todo : not "safe" : check if this is really faster than a loop ...
    return !((other.min_[0] > max_[0] || other.max_[0] < min_[0]) ||
             (other.min_[1] > max_[1] || other.max_[1] < min_[1]) ||
             (other.min_[2] > max_[2] || other.max_[2] < min_[2]));
}

// Scale the bounding box from the center
template<typename PointT>
void AABBoxT<PointT>::Scale(float factor)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    PointT center = ComputeCenter();
    PointT dif = center - min_;
    min_ = center - factor * dif;
    max_ = center + factor * dif;
}

// Scale the bounding box from the center
template<typename PointT>
void AABBoxT<PointT>::Scale(const PointT& factor)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    PointT center = ComputeCenter();
    PointT dif = center - min_;
    for (unsigned int i = 0; i < factor.length(); ++i) {
        min_[i] = center[i] - factor[i] * dif[i];
        max_[i] = center[i] + factor[i] * dif[i];
    }
}

template<typename PointT>
AABBoxT<PointT> AABBoxT<PointT>::Intersection(const AABBoxT& abb1,const AABBoxT& abb2)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    AABBoxT<PointT> res = AABBoxT<PointT>();

    for(unsigned int i = 0; i < abb1.max_.length(); i++)
    {
        res.max_[i] = std::min(abb1.max_[i],abb2.max_[i]);
        res.min_[i] = std::max(abb1.min_[i],abb2.min_[i]);
        if(res.min_[i] > res.max_[i])
        {
            return AABBoxT();
        }
    }
    return res;
}

template<typename PointT>
AABBoxT<PointT> AABBoxT<PointT>::Intersection(const AABBoxT& other) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    AABBoxT<PointT> res = AABBoxT<PointT>();

    for(unsigned int i = 0; i < other.max_.length(); i++)
    {
        res.max_[i] = std::min(max_[i],other.max_[i]);
        res.min_[i] = std::max(min_[i],other.min_[i]);
        if(res.min_[i] > res.max_[i])
        {
            return AABBoxT();
        }
    }
    return res;
}

template<typename PointT>
AABBoxT<PointT> AABBoxT<PointT>::Union(const AABBoxT& abb1,const AABBoxT& abb2)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    AABBoxT<PointT> res = AABBoxT<PointT>();

    for(unsigned int i = 0; i < abb1.max_.length(); i++)
    {
        res.max_[i] = std::max(abb1.max_[i],abb2.max_[i]);
        res.min_[i] = std::min(abb1.min_[i],abb2.min_[i]);
    }
    return res;
}

template<typename PointT>
AABBoxT<PointT> AABBoxT<PointT>::Union(const AABBoxT<PointT>& other) const
{
    return Union(*this,other);
}

// Very special distance given as the max of the distance of each coordinate pair
// This is used to know if it's good to regroup 2 boxes in boxes tree.
template<typename PointT>
float AABBoxT<PointT>::HierarchyDistance(const AABBoxT<PointT>& other) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    float dist = 0.0;

    for(unsigned int i = 0; i < other.max_.length(); i++)
    {
        float coord_dist = other.min_[i] - max_[i];
        if(coord_dist<0.0)
        {
            float aux = min_[i] - other.max_[i];
            if(aux>0.0)
            {
                coord_dist = aux;
            }
            else
            {
                coord_dist = 0.0;
            }
        }

        if(coord_dist>dist)
        {
            dist = coord_dist;
        }
    }

    return dist;
}

// dist_max gives the maximum range of the search.
// If nothing is found, dist_max is returned
template<typename PointT>
float AABBoxT<PointT>::FindIntersection(const PointT& ray_start, const PointT& ray_dir, float dist_max) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    float dist_res = dist_max;
    for(unsigned int i = 0; i < ray_start.length(); i++)
    {
        float aux_res = 0.0;
        // Try intersection with plane Xi = min_[i]
        if(ray_dir[i] != 0.0)
        {
            aux_res = (min_[i] -ray_start[i])/ray_dir[i];
        }

        if(aux_res>0.0 && aux_res < dist_res)
        {
            PointT point_aux_res = ray_start + aux_res*ray_dir;

            bool is_inside = true;
            for(unsigned int j = 0; j < ray_start.length(); j++)
            {
                if(j!=i && (point_aux_res[j]>max_[j] || point_aux_res[j]<min_[j]))
                {
                    is_inside = false;
                    break;
                }
            }
            if(is_inside)
            {
                dist_res = aux_res;
            }
        }

        // Try intersection with plane Xi = max_[i]
        if(ray_dir[i] != 0.0)
        {
            aux_res = (max_[i] -ray_start[i])/ray_dir[i];
        }

        if(aux_res>0.0 && aux_res < dist_res)
        {
            PointT point_aux_res = ray_start + aux_res*ray_dir;

            bool is_inside = true;
            for(unsigned int j = 0; j < ray_start.length(); j++)
            {
                if(j!=i && (point_aux_res[j]>max_[j] || point_aux_res[j]<min_[j]))
                {
                    is_inside = false;
                    break;
                }
            }
            if(is_inside)
            {
                dist_res = aux_res;
            }
        }
    }

    return dist_res;
}

template<typename PointT>
bool AABBoxT<PointT>::isBehind(const PointT& ray_start, const PointT& ray_dir)
{
//    PointT max_in_dir = (ray_dir.array() < 0).select(min_, max_);
    PointT max_in_dir;
    for (unsigned int i = 0; i < ray_start.length(); ++i) {
        max_in_dir[i] = ray_dir[i] < 0 ? min_[0] : max_[0];
    }
    return glm::dot(ray_dir,(max_in_dir-ray_start)) < 0.0;
}

template<typename PointT>
float AABBoxT<PointT>::Volume() const
{
    PointT diff = max_ - min_;
    float vol = 1.0f;
    for(unsigned int i = 0 ; i < diff.length() ; i++)
        vol *= diff[i];
    return  vol;
}


template<typename PointT>
AABBoxT<PointT> &AABBoxT<PointT>::Enlarge(const VectorT &point, float radius)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    for (unsigned int i = 0; i < max_.length(); ++i) {
        min_[i] = std::min(min_[i], point[i] - radius);
        max_[i] = std::max(max_[i], point[i] + radius);
    }
    return *this;
}

template<typename PointT>
AABBoxT<PointT> AABBoxT<PointT>::Enlarged(const VectorT &point, float radius) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    AABBoxT<PointT> res(min_, max_);
    for (unsigned int i = 0; i < max_.length(); ++i) {
        res.min_[i] = std::min(res.min_[i], point[i] - radius);
        res.max_[i] = std::max(res.max_[i], point[i] + radius);
    }
    return res;
}

template<typename PointT>
AABBoxT<PointT> AABBoxT<PointT>::Enlarged(const AABBoxT<PointT>& other) const
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    AABBoxT<PointT> res(min_, max_);
    for(unsigned int i = 0; i < max_.length(); i++)
    {
        res.max_[i] = std::max(res.max_[i],other.max_[i]);
        res.min_[i] = std::min(res.min_[i],other.min_[i]);
    }
    return res;
}

template<typename PointT>
AABBoxT<PointT> &AABBoxT<PointT>::Enlarge(const AABBoxT<PointT>& other)
{
    //TODO:@todo : should be re-writen using directly eigen function ...
    for(unsigned int i = 0; i < max_.length(); i++)
    {
        max_[i] = std::max(max_[i],other.max_[i]);
        min_[i] = std::min(min_[i],other.min_[i]);
    }

    return *this;
}

//template<typename T = PointT,
//typename std::enable_if<std::is_same<T, Point>::value, bool>::type >
//AABBoxT<PointT> &AABBoxT<PointT>::Transform(const Transform &transform)
//{
//    min_ = transform * min_;
//    max_ = transform * max_;
//    return *this;
//}

//template<typename T = PointT,
//typename std::enable_if<std::is_same<T, Point>::value, bool>::type >
//AABBoxT<PointT> AABBoxT<PointT>::Transform(const Transform &transform)
//{
//    AABBoxT<PointT> res = *this;
//    res.min_ = transform * min_;
//    res.max_ = transform * max_;
//    return res;
//}

template<typename PointT>
float AABBoxT<PointT>::width(unsigned int i) const
{
    return max_[i] - min_[i];
}

template<typename PointT>
float AABBoxT<PointT>::height() const
{
    return max_[1] - min_[1];
}

template<typename PointT>
float AABBoxT<PointT>::depth() const
{
    return max_[2] - min_[2];
}

template<typename PointT>
float AABBoxT<PointT>::GetLargestDimensionValue() const
{
    float res = width(0);
    for (unsigned int i = 1; i < max_.length(); ++i) {
        res = std::max(res, width(i));
    }

    return res;
}

template<typename PointT>
unsigned int AABBoxT<PointT>::GetLargestDimension() const
{
    float res = width(0);
    unsigned int dim = 0;
    for (unsigned int i = 1; i < max_.length(); ++i) {
        if (width(i) > res) {
            dim = i;
            res = width(i);
        }
    }

    return dim;
}


typedef AABBoxT<glm::vec2> AABBox2D;
typedef AABBoxT<glm::vec3> AABBox;

} // Close namespace slate

} // Close namespace ext_dvc



#endif // AXIS_BOUNDING_BOX_H_
