#ifndef INSTANCETRANSFORMS_H
#define INSTANCETRANSFORMS_H
#include <glm/glm.hpp>
#include <vector>
#include "ressources/bufferattrib.h"
#include <iostream>
namespace skyengine
{
namespace utility
{

struct Tmat4 : public glm::mat4
{
    Tmat4(): glm::mat4(){}
    Tmat4(const glm::mat4& mat): glm::mat4(mat) {}
    Tmat4(float const & x0, float const & y0, float const & z0, float const & w0,
          float const & x1, float const & y1, float const & z1, float const & w1,
          float const & x2, float const & y2, float const & z2, float const & w2,
          float const & x3, float const & y3, float const & z3, float const & w3):
        glm::mat4(x0,y0,z0,w0,
                  x1,y1,z1,w1,
                  x2,y2,z2,w2,
                  x3,y3,z3,w3){}

    static std::vector<ressource::BufferAttrib> getBufferAttribs(GLuint first_index);
};

struct Tvec3 : public glm::vec3
{
    Tvec3(): glm::vec3(){}
    Tvec3(float const & x, float const & y, float const & z):
        glm::vec3(x,y,z){}

    static std::vector<ressource::BufferAttrib> getBufferAttribs(GLuint first_index);
};
} // namespace utility
} // namespace skyengine

#endif // INSTANCETRANSFORMS_H
