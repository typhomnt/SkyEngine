#include "Vertex3D.h"
#include "math/mathutils.h"
#include "utils/utilities.h"
namespace skyengine
{

namespace render
{
vertex3D::vertex3D()
{

}

vertex3D::vertex3D(const glm::vec3 &position):
    pos(position)
{

}

vertex3D::vertex3D(const glm::vec3 &position, const glm::vec2 &texcoords):
    pos(position), uvs(texcoords)
{

}

vertex3D::vertex3D(const glm::vec3 &position, const glm::vec2 &texcoords,
     const glm::vec3 &normal):
    pos(position), uvs(texcoords), normal(normal)
{

}

std::vector<ressource::BufferAttrib> vertex3D::getBufferAttribs()
{
    vertex3D tmp;
    char* tmp_p = (char*) &tmp;
#define ind(elem) ((char*)(&tmp.elem) - tmp_p)
    std::vector<ressource::BufferAttrib> attribs;
    attribs.push_back(ressource::BufferAttrib(0,3,GL_FLOAT,ind(pos)));
    attribs.push_back(ressource::BufferAttrib(1,2,GL_FLOAT,ind(uvs)));
    attribs.push_back(ressource::BufferAttrib(2,3,GL_FLOAT,ind(normal)));
    return attribs;
}

size_t vertex3D::BufferSize()
{
    return sizeof(pos) + sizeof(uvs) + sizeof(normal);
}

unsigned int vertex3D::MagicNumber()
{
    return 0;
}
vertex3D vertex3D::readFromStream(std::ifstream& in_s)
{
    vertex3D new_v;
    utility::binary_read(in_s,new_v.pos);
    utility::binary_read(in_s,new_v.uvs);
    utility::binary_read(in_s,new_v.normal);
    return new_v;
}

void vertex3D::writeStream(std::ostream& in_s)
{
    utility::binary_write(in_s,pos);
    utility::binary_write(in_s,uvs);
    utility::binary_write(in_s,normal);
}

}

}
