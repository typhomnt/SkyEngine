#ifndef __SKYWINDOW_H__
#define __SKYWINDOW_H__

#include "gui/guimanager.h"

#include <memory>
#include <unordered_map>

#include <imgui/addons/imguidock/imguidock.h>
#include <imgui/addons/imguifilesystem/imguifilesystem.h>


namespace module
{
namespace gui
{

class ActiveObjectEvent : public  skyengine::core::Listenable<ActiveObjectEvent>
{
public:
    ActiveObjectEvent() {}
    virtual ~ActiveObjectEvent() {}

    virtual void send(skyengine::scene::SceneGraphNode* node)
    { node_ = node; notifyListeners(); }

    skyengine::scene::SceneGraphNode* node() const {return node_;}

protected:
   skyengine::scene::SceneGraphNode* node_;
};


class SelectObjectEvent : public  skyengine::core::Listenable<SelectObjectEvent>
{
public:
    SelectObjectEvent() {}
    virtual ~SelectObjectEvent() {}

    virtual void send(skyengine::scene::SceneGraphNode* node)
    { node_ = node; notifyListeners(); }

    skyengine::scene::SceneGraphNode* node() const {return node_;}

protected:
   skyengine::scene::SceneGraphNode* node_;
};

using ActiveObjectListener = skyengine::core::Listener<ActiveObjectEvent>;
using SelectObjectListener = skyengine::core::Listener<SelectObjectEvent>;

} // namespace gui

} // namespace module


#include "skywidget.h"


namespace module
{

namespace gui
{


class SkyWidget;
class SkyWindow : public GuiTask
{
public:
    enum class DockSlot
    {
        Left = 0,
        Right,
        Center,
        Footer,
        Header,
        Float
    };

    SkyWindow(skyengine::core::SkyEngine* engine,
        gui::GuiManager* manager
    );

    template<typename T, typename... Args>
    T* addWidget(const std::string& name, DockSlot slot, Args&&... args)
    {
        auto widget = std::make_shared<T>(
            name,
            this,
            m_active_obj_evt,
            m_select_obj_evt,
            args...
        );

        m_widgets.insert({ widget->getName(), widget });
        dockWidget(widget.get(), slot);

        return widget.get();
    }

    inline std::shared_ptr<SkyWidget> getWidget(const std::string& widget_name);

    void sendActiveObject(skyengine::scene::SceneGraphNode* node);

    void run() override;

private:
    std::map<std::string, std::shared_ptr<SkyWidget>> m_widgets;
    std::map<DockSlot, std::vector<SkyWidget*>> m_dockedWidgets;

    std::shared_ptr<ActiveObjectEvent> m_active_obj_evt;
    std::shared_ptr<SelectObjectEvent> m_select_obj_evt;

    ImGuiFs::Dialog m_loadModelDialog;
    ImGuiFs::Dialog m_openDialog;
    ImGuiFs::Dialog m_saveDialog;

    bool m_themesMenu;
    int m_themeIdx;
    char m_saveTheme[50];

    bool m_aboutMenu;

    int drawMenuBar();
    void drawStatusBar(int menuHeight);
    void drawThemesMenu();

    void dockWidget(SkyWidget* widget, DockSlot slot);
};

inline std::shared_ptr<SkyWidget> SkyWindow::getWidget(const std::string& widget_name)
{
    if(skyengine::utility::MapContains(m_widgets,widget_name))
        return m_widgets[widget_name];
    else
        return nullptr;
}

} // namespace gui

} // namespace module

#endif // __SKYWINDOW_H__
