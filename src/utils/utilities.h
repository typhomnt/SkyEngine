#ifndef UTILITIES_H
#define UTILITIES_H

#include "skyengine_dll.h"

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <functional>
#include <iostream>
#include <memory>
#include <map>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <iterator>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <dirent.h>

#include "logger.h"


namespace skyengine
{

namespace utility
{

// namespace to hide helper functions
namespace str_convert_namespace_helper
{

inline void list_to_sstram(std::stringstream& /*ss*/)
{
}

template<typename T, typename... Targs>
inline void list_to_sstram(std::stringstream& ss, T value, Targs... Fargs)
{
    ss << value;
    list_to_sstram(ss, Fargs...);
}

}

template<typename... Targs>
inline std::string mkstring(Targs... Fargs)
{
    std::stringstream ss;
    str_convert_namespace_helper::list_to_sstram(ss, Fargs...);
    return ss.str();
}

template <class T>
inline bool bool_cast(const T& v)
{
    return v != T(0);
}


bool getSimilarVertexIndex(
        glm::vec3 & in_vertex,
        glm::vec2 & in_uv,
        glm::vec3 & in_normal,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals,
        unsigned short & result
        );


void indexVBO(
        std::vector<glm::vec3> & in_vertices,
        std::vector<glm::vec2> & in_uvs,
        std::vector<glm::vec3> & in_normals,
        std::vector<unsigned short> & out_indices,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals
        );

void computeTangentBasis(
        // inputs
        std::vector<glm::vec3> & vertices,
        std::vector<glm::vec2> & uvs,
        std::vector<glm::vec3> & normals,
        // outputs
        std::vector<glm::vec3> & tangents,
        std::vector<glm::vec3> & bitangents
        );

void indexVBO_NM(
        std::vector<glm::vec3> & in_vertices,
        std::vector<glm::vec2> & in_uvs,
        std::vector<glm::vec3> & in_normals,
        std::vector<glm::vec3> & in_tangents,
        std::vector<glm::vec3> & in_bitangents,
        std::vector<unsigned short> & out_indices,
        std::vector<glm::vec3> & out_vertices,
        std::vector<glm::vec2> & out_uvs,
        std::vector<glm::vec3> & out_normals,
        std::vector<glm::vec3> & out_tangents,
        std::vector<glm::vec3> & out_bitangents
        );

template <typename T>
T clamp(const T& val, const T& lower_bound, const T& upper_bound) {
    return std::max(lower_bound, std::min(val, upper_bound));
}


template<typename S, typename T>
bool MapInsert(std::map<S,T>& map, const S& key_insert, const T& value_insert, const std::string& error_message = "Map insertion failed. Key already exist")
{
    auto res = map.insert({key_insert,value_insert});
    if (!res.second)
        Logger::Error(error_message);

    return res.second;
}

template<typename S, typename T>
bool MapInsert(std::unordered_map<S,T>& map, const S& key_insert, const T& value_insert, const std::string& error_message = "Map insertion failed. Key already exist")
{
    auto res = map.insert({key_insert,value_insert});
    if (!res.second)
        Logger::Error(error_message);

    return res.second;
}



template<class M>
bool MapContains(const M& map, const typename M::key_type& key)
{
    auto it = map.find(key);
    return !(it == map.end());
}


template<typename K, typename V>
bool MapContains(const std::map<K, V>& map, const K& key)
{
    return MapContains<std::map<K,V> >(map,key);
}

template<typename K, typename V>
bool MapContains(const std::unordered_map<K, V>& map, const K& key)
{

    return MapContains<std::unordered_map<K,V> >(map,key);
}


template<class M>
bool MapFind(const M& map,
             const typename M::key_type& key,
             typename M::mapped_type& outValue,
             const std::string& errMessage = "MapFind : key not found")
{
    auto it = map.find(key);
    if (it == map.end())
    {
        Logger::Error(errMessage);
        return false;
    }

    outValue = it->second;
    return true;
}


template<typename K, typename V>
bool MapFind(const std::map<K, V>& map,
             const K& key,
             V& outValue,
             const std::string& errMessage = "MapFind : key not found")
{
    return MapFind<std::map<K, V>>(map, key, outValue, errMessage);
}

template<typename K, typename V>
bool MapFind(const std::unordered_map<K, V>& map,
             const K& key,
             V& outValue,
             const std::string& errMessage = "MapFind : key not found")
{
    return MapFind<std::unordered_map<K, V>>(map, key, outValue, errMessage);
}


template<class M>
void MapRemove(M& map, const typename M::key_type& key_remove, const std::string& error_message = "Map remove failed. Key does not exist in map")
{
    auto map_it = map.find(key_remove);
    if(map_it == map.end())
        Logger::Error(error_message);
    else
        map.erase(map_it);
}


template<typename S, typename T>
void MapRemove(std::unordered_map<S,T>& map, const S& key_remove, const std::string& error_message = "Map remove failed. Key does not exist in map")
{
    MapRemove<std::unordered_map<S,T> >(map,key_remove,error_message);
}

template<typename S, typename T>
void MapRemove(std::map<S,T>& map, const S& key_remove, const std::string& error_message = "Map remove failed. Key does not exist in map")
{
    MapRemove<std::map<S,T> >(map,key_remove,error_message);
}

template<typename S, typename T>
std::pair<T,S> PairFlip(const std::pair<S,T>& pair)
{
    return std::pair<T,S>(pair.second,pair.first);
}


template<typename S, typename T>
std::multimap<T,S> MapFlip(const std::map<S,T>& map)
{
    std::multimap<T,S> multi_map;
    std::transform(map.begin(), map.end(), std::inserter(multi_map, multi_map.begin()),PairFlip<S,T>);
    return multi_map;
}

template<typename S, typename T>
std::multimap<T,S> MapFlip(const std::unordered_map<S,T>& map)
{
    std::multimap<T,S> multi_map;
    std::transform(map.begin(), map.end(), std::inserter(multi_map, multi_map.begin()),PairFlip<S,T>);
    return multi_map;
}


template<typename S, typename T>
std::unordered_map<S,T> MapDeRef(const std::unordered_map<S,std::shared_ptr<T>>& map)
{
    std::unordered_map<S,T> un_ref_map;
    for(auto& pair : map)
        un_ref_map[pair.first] = *(pair.second.get());

    return un_ref_map;
}

void SKYENGINE_API InitRandomSeed();

//0 ≤ H < 360, 0 ≤ S ≤ 1 and 0 ≤ L ≤ 1:
glm::vec3 HSLToRGB(const glm::vec3& hsl_col);
glm::vec3 getRandomColor();
unsigned int RGBToInt(const glm::vec3& rbg_col, bool alpha_chanel =  false);


template<typename K>
K InterpolateValues(const K& start_val,const K& end_val, float interpolation_fac, std::function<K(const K&,const K&, float weigtht)> interpolation_func)
{
    return interpolation_func(start_val,end_val,interpolation_fac);
}

template<typename T>
T LinearInterpolation(const T& start_val,const T& end_val, float interpolation_fac)
{
    return InterpolateValues(start_val,end_val,interpolation_fac,[](const T& start, const T& end, float interpolation_factor) -> T{return (1.0f - interpolation_factor)*start + interpolation_factor*end ;});
}

std::string stringTrim(const std::string& s, char char_trim);

std::vector<std::string> stringSplit(const std::string &s, char delim, bool keep_delim = false);

void stringUpperCase(std::string& input);

template<typename T>
void vectorRemoveValue(std::vector<T>& vec, const T& val)
{
    vec.erase(std::remove(vec.begin(), vec.end(), val), vec.end());
}

template<typename T>
bool vectorContainsIf(std::vector<T>& vec,std::function<bool(const T&)> predicate)
{
    return std::find_if(vec.begin(), vec.end(), predicate) != vec.end();
}

template<typename T>
bool vectorContains(std::vector<T>& vec, const T& val)
{
    return vectorContainsIf<T>(vec,[val](const T& keyf){return keyf == val;});
}



std::string extractFileNameFromPath(const std::string& path);
std::string extractFileExtensionFromPath(const std::string& path);

/**
 * There is a better version in c++ 2017 but for compatibility reason we keep a C version
 */
std::vector<std::string> getFilesInDir(const std::string& dir_path, const std::initializer_list<std::string>& filters);

template<typename T>
std::ostream& binary_write(std::ostream& stream, const T& value)
{
    return stream.write(reinterpret_cast<const char*>(&value), sizeof(T));
}


template<typename T>
std::istream & binary_read(std::istream& stream, T& value)
{
    return stream.read(reinterpret_cast<char*>(&value), sizeof(T));
}

}

namespace render {struct vertex3D;}
namespace utility {struct Tmat4;}
namespace utility {struct Tvec3;}
namespace ressource {struct PCVertex;}
namespace animation {struct Vertex3DSkin;}

std::ostream& operator<< (std::ostream& stream, const render::vertex3D& v);
std::ostream& operator<< (std::ostream& stream, const utility::Tmat4& m);
std::ostream& operator<< (std::ostream& stream, const utility::Tvec3& v);
std::ostream& operator<< (std::ostream& stream, const ressource::PCVertex& v);
std::ostream& operator<< (std::ostream& stream, const animation::Vertex3DSkin& v);
std::ostream& operator<< (std::ostream& stream, const glm::mat4& m);
std::ostream& operator<< (std::ostream& stream, const glm::vec4& v);
std::ostream& operator<< (std::ostream& stream, const glm::vec3& v);
std::ostream& operator<< (std::ostream& stream, const glm::vec2& v);

std::istream& operator>> (std::istream& stream, const render::vertex3D& v);
std::istream& operator>> (std::istream& stream, const utility::Tmat4& m);
std::istream& operator>> (std::istream& stream, const utility::Tvec3& v);
std::istream& operator>> (std::istream& stream, const ressource::PCVertex& v);
std::istream& operator>> (std::istream& stream, const animation::Vertex3DSkin& v);
std::istream& operator>> (std::istream& stream, const glm::mat4& m);
std::istream& operator>> (std::istream& stream, const glm::vec4& v);
std::istream& operator>> (std::istream& stream, const glm::vec3& v);
std::istream& operator>> (std::istream& stream, const glm::vec2& v);

}

#endif // UTILITIES_H
