#ifndef SKYLISTENER_H
#define SKYLISTENER_H

#include <memory>
#include <vector>
#include <map>
#include <assert.h>


namespace skyengine
{

namespace core
{

template<class T_Event>
class Listener;

template<class T_Event>
class Listenable
{
public:
    Listenable(){}
    virtual ~Listenable();

    void notifyListeners() const;

protected:
    void addListener(Listener<T_Event>* l);
    void removeListener(Listener<T_Event>* l);

    // the use of a raw pointer here is not dangerous as this list is managed by the constructors / destructors of the listeners
    std::vector<Listener<T_Event>*> listeners;

    friend class Listener<T_Event>;

};

template<class T_Event>
class Listener
{
public:
    Listener(std::shared_ptr<T_Event>);
    virtual ~Listener();

    void setListenerActive(bool);
    void setListenable(std::shared_ptr<T_Event>);

    virtual void listened(const T_Event*){}

protected:
    bool isListenerActive;
    bool hasListenable;

private:
    std::weak_ptr<T_Event> listenable;

    // managed by listenable if listener is active
    unsigned id;

    friend class Listenable<T_Event>;

};

/// --------------- Listenable -----------------------

template <class T_Event>
Listenable<T_Event>::~Listenable()
{
    assert(listeners.empty() && "Error: listener set is not empty at destruction.");
}

template <class T_Event>
void Listenable<T_Event>::notifyListeners() const
{
    for(auto pl : listeners)
        pl->listened((const T_Event*) this);
}

template <class T_Event>
void Listenable<T_Event>::addListener(Listener<T_Event>* l)
{
    l->id = (unsigned)listeners.size();
    listeners.push_back(l);
}

template <class T_Event>
void Listenable<T_Event>::removeListener(Listener<T_Event> *l)
{
    assert(l->id < listeners.size() && "Error, trying to remove an out of bound listener");

    assert(l->id == listeners[l->id]->id && "Error: invalid matching listener id in list.");

    listeners[l->id] = listeners.back();
    listeners.back()->id = l->id;
    listeners.resize(listeners.size()-1);
}

/// --------------- Listener -----------------------

template<class T_Event>
Listener<T_Event>::Listener(std::shared_ptr<T_Event> l) : isListenerActive(true), listenable(l)
{
    if(l)
        l->addListener(this);
    hasListenable = (l)?true:false;
}

template<class T_Event>
Listener<T_Event>::~Listener()
{
    if(hasListenable && isListenerActive)
    {
        auto sl = listenable.lock();
        assert(sl && "Error: listenable destroyed before listener.");
        sl->removeListener(this);
    }
}

template<class T_Event>
void Listener<T_Event>::setListenerActive(bool b)
{
    if(hasListenable)
    {
        auto sl = listenable.lock();
        assert(sl && "Error: listenable destroyed.");

        if (b && !isListenerActive)
            sl->addListener(this);
        if (!b && isListenerActive)
            sl->removeListener(this);

        isListenerActive = b;
    }
}

template<class T_Event>
void Listener<T_Event>::setListenable(std::shared_ptr<T_Event> sl)
{
    listenable = sl;
}

}
}
#endif // SKYLISTENER_H
