#ifndef RENDERABLE_H
#define RENDERABLE_H

#include "utils/skyengine_dll.h"

#include "scene/scenegraphnode.h"
#include "ressources/texture.h"
#include "ressources/mesh.h"
#include "ressources/shader.h"
#include <vector>
#include "GL/glew.h"

namespace skyengine
{

namespace render
{

class SKYENGINE_API Renderable: public scene::SceneGraphNode
{
public:
    virtual ~Renderable();
    virtual void init(){}
    void draw();
    virtual void setShaderUniforms();
    virtual void setRenderShader(/*std::shared_ptr<ressource::Shader>*/ressource::Shader* render_shader);
    inline ressource::Shader* getRenderShader();
    inline std::shared_ptr<ressource::AbstractMesh> getMesh();
    void setMesh(std::shared_ptr<ressource::AbstractMesh> mesh);

    void show() { m_show = true; }
    void hide() { m_show = false; }    
    bool isRendered() const { return m_show; }

    void showWire() { m_wired = true; }
    void hideWire() { m_wired = false; }
    bool isWireRendered() const { return m_wired; }
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);
protected:

    std::shared_ptr<ressource::AbstractMesh> m_mesh;
    ressource::Shader* m_render_shader;
    std::unordered_map<std::string,scene::baseNodeParameter*> render_shader_uniform_values;
    Renderable(const std::string& name, scene::SceneGraph *owner = nullptr,
              const unsigned int type = Type::Renderable,
              const glm::mat4& world_matrix = glm::mat4(1.0f));
    virtual void LoadProp(const rapidjson::Value& value);

private:
    bool m_show;
    bool m_wired;

    virtual void doDraw();
};

inline std::shared_ptr<ressource::AbstractMesh> Renderable::getMesh()
{
    return m_mesh;
}

inline ressource::Shader* Renderable::getRenderShader()
{
    return m_render_shader;
}


}
}
#endif // RENDERABLE_H
