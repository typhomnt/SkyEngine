#include "pbrmaterial.h"
#include "phongmaterial.h"

#include "ressourcemanager.h"


namespace skyengine
{

namespace ressource
{

PBRMaterial::PBRMaterial(const std::string& name)
    : Material(name)
    , m_albedo { 1.0f }
    , m_metallic (1.0f)
    , m_roughness (1.0f)
    , m_ao (1.0f)
{}

PBRMaterial::PBRMaterial(
    const std::string& name,
        const glm::vec3& albedo,
        float metallic,
        float roughness,
        float ao
)
    : Material(name)
    , m_albedo { albedo }
    , m_metallic { metallic }
    , m_roughness { roughness }
    , m_ao { ao }
{}

void PBRMaterial::sendToShader(const Shader &shader) const
{
    shader.setUniformValue3f("material.albedo", m_albedo);
    shader.setUniformValue1f("material.metallic", m_metallic);
    shader.setUniformValue1f("material.roughness", m_roughness);
    shader.setUniformValue1f("material.ao", m_ao);
    std::multimap<GLuint,std::string> invert_sampler_map =  utility::MapFlip(shader.getSamplers());
    for(auto& sampler : invert_sampler_map)
    {
        Texture* tex;
        if(sampler.first > m_textures.size() - 1 || m_textures.size() == 0)
            tex = std::dynamic_pointer_cast<Texture>(RessourceManager::Get("void_texture")).get();
        else
        {
            tex = m_textures[sampler.first];
        }
        if(tex == nullptr)
        {
            tex = std::dynamic_pointer_cast<Texture>(RessourceManager::Get("void_texture")).get();
        }
        tex->bind(GL_TEXTURE0 + sampler.first);
        shader.setUniformValue1i(sampler.second.c_str(), sampler.first);
    }

}

void PBRMaterial::addTexture(Texture* tex, unsigned int index)
{
    assert(index <= m_textures.size());
    m_textures.insert(m_textures.begin() + index,tex);
}


std::shared_ptr<rapidjson::Value> PBRMaterial::SerializeProp(rapidjson::MemoryPoolAllocator<> &al)
{
    std::shared_ptr<rapidjson::Value> obj = Ressource::SerializeProp(al);
    utility::JSONHelper::SaveVec(*obj.get(),"Albedo",m_albedo,al);
    obj->AddMember(rapidjson::Value("Metallic", al),
                   rapidjson::Value(m_metallic),
                   al);
    obj->AddMember(rapidjson::Value("Roughness", al),
                   rapidjson::Value(m_roughness),
                   al);
    obj->AddMember(rapidjson::Value("AOc", al),
                   rapidjson::Value(m_ao),
                   al);

    rapidjson::Value texArray(rapidjson::kArrayType);
    for (Texture* tex : m_textures)
        texArray.PushBack(rapidjson::Value(tex->getName().c_str(), al), al);
    obj->AddMember(rapidjson::Value("Textures", al), texArray, al);

    return obj;
}

bool PBRMaterial::LoadProp(const rapidjson::Value& value)
{
    bool needSecondPass = false;

    utility::JSONHelper::ReadVec(value["Albedo"], m_albedo);
    m_metallic = value["Metallic"].GetFloat();
    m_roughness = value["Roughness"].GetFloat();
    m_ao = value["AOc"].GetFloat();

    unsigned int i = 0;
    for (const rapidjson::Value& it : value["Textures"].GetArray())
    {
        std::string texName = it.GetString();
        m_texNames.push_back(texName);
        if (RessourceManager::Exists(texName))
        {
            auto tex = RessourceManager::Get(texName);
            addTexture(dynamic_cast<Texture*>(tex.get()), i++);
        }
        else
        {
            needSecondPass = true;
            addTexture(nullptr, i++);
        }
    }

    return needSecondPass;
}

void PBRMaterial::FinishLoading()
{
    for (size_t i = 0; i < m_textures.size(); ++i)
    {
        if (m_textures[i] == nullptr)
        {
            auto tex = RessourceManager::Get(m_texNames[i]);
            assert(tex);

            m_textures[i] = dynamic_cast<Texture*>(tex.get());
        }
    }
}


std::shared_ptr<PBRMaterial> PBRMaterial::None = Ressource::Create<PBRMaterial>("NoneMaterialPBR");
std::shared_ptr<PBRMaterial> PBRMaterial::Default = Ressource::Create<PBRMaterial>(
    "DefaultMaterialPBR",
    glm::vec3(0.8f,0.1f,0.3f),
    0.5f,
    0.5f,
    0.2f
);

std::shared_ptr<PhongMaterial> PBRMaterial::ToPhongMat()
{
    std::shared_ptr<PhongMaterial> phong_mat = ressource::Ressource::Create<PhongMaterial>(m_name+"Phong"
                                                                                           ,glm::vec4()
                                                                                           ,glm::vec4(m_albedo.x,m_albedo.y,m_albedo.z,1.0f)
                                                                                           ,glm::vec4(m_albedo.x,m_albedo.y,m_albedo.z,1.0f)
                                                                                           ,glm::vec4(1.0f)
                                                                                           ,m_roughness*128.f);
    for(Texture* tex : m_textures)
        phong_mat ->addTexture(tex);
    return phong_mat;
}


} // namespace ressource

} // namespace skyengine
