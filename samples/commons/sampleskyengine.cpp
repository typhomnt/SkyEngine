#include "sampleskyengine.h"
#include "utils/utilities.h"
#include "utils/glcheck.h"
#include "ressources/shader.h"
#include "ressources/image.h"
#include "ressources/texture.h"
#include "scene/scenegraphmanager.h"
#include "animation/animationcomposer.h"
namespace skyengine {
namespace samples {


SampleSkyEngine::SampleSkyEngine() :
    SkyEngine()
{

}

void SampleSkyEngine::init()
{
    // create main window and opengl context
    core::OpenGLWindow::setGFLWHint(GLFW_SAMPLES,4);
    core::OpenGLWindow::setGFLWHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    core::OpenGLWindow::setGFLWHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    core::OpenGLWindow::setGFLWHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    core::OpenGLWindow::setGFLWHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    main_window = std::make_shared<core::OpenGLWindow>(1024,920,"Sky Engine");
    main_window->bind();

    // now that the window is created, initialize glew
    initOpenGL();

    // set opengl default values
    glfwSetInputMode(main_window->getGLFWindow(), GLFW_STICKY_KEYS, GL_TRUE);
    GLCHECK(glClearColor(79.0f/255.0f, 66.0f/255.0f, 48.0f/255, 1.0f));

    //We activate depth test
    GLCHECK(glEnable(GL_DEPTH_TEST));

    // Accept fragment if it closer to the camera than the former one
    GLCHECK(glDepthFunc(GL_LESS));
    GLCHECK(glEnable(GL_BLEND));
    //GLCHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
    GLCHECK(glBlendFuncSeparate(GL_ONE, GL_ZERO, GL_ONE, GL_ZERO));
    // GLCHECK(glBlendEquationSeparate( GL_FUNC_ADD, GL_MAX));
    // Activate the use of the GLSL variable gl_PointSize
    GLCHECK(glEnable(GL_PROGRAM_POINT_SIZE));

    // init quit listener
    quit_listener = std::make_shared<QuitListener>(this, main_window->eventManager(), main_window->userInputManager());

    //init scene graph manager
    scene::SceneGraphManager::setSkyEngineTasks(this);
    // init scene and tasks
    initRessources();
    initScene();
    initTasks();
}

void SampleSkyEngine::initRessources()
{
    ressource::Ressource::Create<ressource::Shader>(
                "default_shader",
                "../shaders/default.vert",
                "../shaders/default.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "default_pbr_shader",
                "../shaders/default.vert",
                "../shaders/pbr1.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "default_skin_shader",
                "../shaders/default_skin.vert",
                "../shaders/default.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "default_skin_pbr_shader",
                "../shaders/default_skin.vert",
                "../shaders/pbr1.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "line_shader",
                "../shaders/line.vert",
                "../shaders/line.frag",
                "../shaders/line.geom"
                );

    ressource::Ressource::Create<ressource::Shader>(
                "colored_shader",
                "../shaders/colored.vert",
                "../shaders/colored.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "instance_shader",
                "../shaders/default_inst.vert",
                "../shaders/default_inst.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "simple_display_shader",
                "../shaders/simpleDisplay.vert",
                "../shaders/simpleDisplay.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "gaussian_display_shader",
                "../shaders/gaussian.vert",
                "../shaders/gaussian.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "bloom_display_shader",
                "../shaders/bloom.vert",
                "../shaders/bloom.frag"
                );

    ressource::Ressource::Create<ressource::Shader>(
                "checkboard_shader",
                "../shaders/default.vert",
                "../shaders/checkboard.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "deferredg_skin_shader",
                "../shaders/default_skin.vert",
                "../shaders/deferredg.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "deferredg_shader",
                "../shaders/default.vert",
                "../shaders/deferredg.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "deferredl_shader",
                "../shaders/simpleDisplay.vert",
                "../shaders/deferredl.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
                "toon_shader",
                "../shaders/toon.vert",
                "../shaders/toon.frag"
                );
    ressource::Ressource::Create<ressource::Shader>(
        "pointcloud_shader",
        "../shaders/pointcloud.vert",
        "../shaders/pointcloud.frag",
        "../shaders/pointcloud.geom"
    );
    //---------------------------

    //
    //Compile shaders
    for(auto& res : ressource::RessourceManager::GetSingleton()->getRessources())
    {
        std::shared_ptr<ressource::Shader> shader = std::dynamic_pointer_cast<ressource::Shader>(res.second);
        if(shader != nullptr)
            shader->compileAndLink();
    }

    //Add blank texture
    std::shared_ptr<ressource::Image> blank_img = ressource::Ressource::Create<ressource::Image>("void_sampler");
    unsigned int wht_col = utility::RGBToInt(glm::vec3(255,255,255),true);
    unsigned char white[4];
    white[0] = (wht_col & 255<<(3)*8) >> ((3)*8);
    white[1] = (wht_col & 255<<(2)*8) >> ((2)*8);
    white[2] = (wht_col & 255<<(1)*8) >> ((1)*8);
    white[3] = (wht_col & 255);


    blank_img->setData(1,1,4,white);
    ressource::Ressource::Create<ressource::Texture>("void_texture",blank_img);
    //--------------------------------

    //Animation Initialization--------
    animation::AnimationComposer::PlayComposer(this);
    //--------------------------------
    initUserRessources();
}

}
}
