#ifndef TRANSFORMMODIFIER_H
#define TRANSFORMMODIFIER_H

#include "abstracttransformmodifier.h"

namespace skyengine
{
namespace geometry
{

template<typename Data_T, typename Point_T, typename Axis_T>
class TransformModifier : public AbstractTransformModifier
{
public:

    virtual void scale(const Point_T& pivot_point,  Data_T& data_scale) = 0;
    virtual void translate(const Axis_T& translate_vector,  Data_T& data_translate) = 0;
    virtual void rotate(const Point_T& pivot_point,  Data_T& data_rotate) = 0;

    virtual void scale_axis(const Point_T& pivot_point, const Axis_T& pivot_axis,  Data_T& data_scale) = 0;
    virtual void rotate_axis(const Point_T& pivot_point, const Axis_T& pivot_axis,  Data_T& data_rotate) = 0 ;

    virtual void apply_scale()= 0;
    virtual void apply_translate() = 0;
    virtual void apply_rotate() = 0;

    virtual void apply() = 0;
protected:
       TransformModifier();
};

template<typename Data_T, typename Point_T, typename Axis_T>
TransformModifier<Data_T,Point_T, Axis_T>::TransformModifier()
{

}

}
}

#endif // TRANSFORMMODIFIER_H
