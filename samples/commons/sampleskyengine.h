#ifndef SAMPLESKYENGINE_H
#define SAMPLESKYENGINE_H

#include "core/skyengine.h"
#include "core/openglwindow.h"

#include "quitlistener.h"
//#include "gui/widgets/skywindow.h"


namespace skyengine {
namespace samples {



class SampleSkyEngine : public core::SkyEngine
{
public:
    SampleSkyEngine();
    virtual ~SampleSkyEngine(){}
    inline core::OpenGLWindow* getMainWindow();
    void init();
    virtual void initScene() = 0;
    virtual void initTasks() = 0;
    virtual void initUserRessources(){}
private:
    void initRessources();
protected:
    std::shared_ptr<core::OpenGLWindow> main_window;
    std::shared_ptr<QuitListener> quit_listener;
};

inline core::OpenGLWindow* SampleSkyEngine::getMainWindow()
{
    return main_window.get();
}


}
}

#endif // SAMPLESKYENGINE_H
