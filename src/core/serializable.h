#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H
#include <utils/jsonhelper.h>
#include <rapidjson/document.h>
#include <memory>
namespace skyengine
{

namespace core
{
class Serializable
{
public:

    virtual void Serialize(rapidjson::Value&, rapidjson::MemoryPoolAllocator<>&) = 0;


    // TODO : protected ?
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
    {
        std::shared_ptr<rapidjson::Value> obj = std::make_shared<rapidjson::Value>(rapidjson::kObjectType);
        obj->SetObject();
        utility::JSONHelper::SaveString(*(obj.get()),"Tag",getTag(),al);
        return obj;
    }
    void setSerializable(bool ser) {m_serializable = ser;}
    inline bool getSerializable() {return m_serializable;}

    virtual std::string getTag() =0;

protected:

    Serializable(bool serializable = true):m_serializable(serializable) {}
    virtual ~Serializable() {}

    bool m_serializable;

    virtual void FinishLoading() {}
};

}
}

#include <scene/scenegraph.h>
namespace skyengine
{
namespace scene
{
class SceneGraph;
}

namespace core
{

bool SaveScene(const std::string& file_name,scene::SceneGraph& sg);
bool LoadScene(const std::string& file_name);

}
}

#endif // SERIALIZABLE_H
