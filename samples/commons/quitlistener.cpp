#include "quitlistener.h"

#include "core/skyengine.h"

#include "GLFW/glfw3.h"


namespace skyengine {
namespace samples {

void QuitListener::listened(const core::CloseEvent *event)
{
    onClose();
}

void QuitListener::listened(const core::KeyEvent *event)
{
    utility::Logger::Debug("Key Pressed");
    if(event->key() == GLFW_KEY_ESCAPE && event->action() == GLFW_PRESS)
        onClose();
}

void QuitListener::onClose()
{
    engine_->requestTerminate();
}

}
}
