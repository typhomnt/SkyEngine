#ifndef BSKELETON_H
#define BSKELETON_H
#include "bsphere.h"
#include <utils/graph.h>
namespace module
{

namespace bmeshes
{

using BSkeleton = skyengine::utility::Graph<BSphere>;


}
}
#endif // BSKELETON_H
