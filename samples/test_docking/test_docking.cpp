#include "commons/sampleskyengine.h"

#include "core/functioncalltask.h"
#include "core/manipulator.h"
#include "geometry/geometryalgorithms.h"
#include "render/drawtask.h"
#include "ressources/image.h"
#include "ressources/lights.h"
#include "ressources/texture.h"
#include "ressources/pbrmaterial.h"
#include "scene/camera.h"
#include "scene/cameramanipulator.h"
#include "scene/gameobject3d.h"
#include "scene/scenegraphmanager.h"

#include "gui/widgets/logwidget.h"
#include "gui/widgets/nodelist.h"
#include "gui/widgets/resourcelist.h"
#include "gui/widgets/shadereditor.h"
#include "gui/widgets/skywindow.h"
#include "gui/widgets/viewport.h"
#include "gui/widgets/drawtaskeditor.h"

#include "utils/jsonhelper.h"

using namespace skyengine;


class TestDocking : public samples::SampleSkyEngine
{
public:
    TestDocking();
    ~TestDocking();

    void initScene() override;
    void initUserRessources() override;
    void initTasks() override;

private:
    // Lights
    std::shared_ptr<ressource::DirLight> m_dLight;
    std::shared_ptr<ressource::PointLight> m_pLight;

    // Camera
    std::shared_ptr<scene::Camera> m_camera;

    std::shared_ptr<scene::GameObject3D> m_cube;
    std::shared_ptr<scene::GameObject3D> m_sphere;

    // Draw task
    std::shared_ptr<render::DrawTask> m_drawTask;

    std::shared_ptr<module::gui::GuiManager> m_guiManager;
    std::shared_ptr<module::gui::SkyEditor> m_editor;
};

TestDocking::TestDocking() : samples::SampleSkyEngine()
{}

TestDocking::~TestDocking()
{}

void TestDocking::initScene()
{
    auto sg = std::make_shared<scene::SceneGraph>();
    scene::SceneGraphManager::Add("default_scene", sg);

    sg->addNode(m_cube);
    sg->addNode(m_sphere);
    sg->addNode(m_camera);

    m_drawTask = std::make_shared<render::DrawTask>(
        this,
        /*main_window->getSize(),*/
        main_window->eventManager()
    );
    m_drawTask->addShader("simple_display_shader");
    m_drawTask->addShader("default_pbr_shader");
    m_drawTask->addUniformValue(
        "forward",
        "default_pbr_shader",
        new scene::NodeParameter<glm::vec3*>("camPosition", 1, m_camera->getPositionPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "default_pbr_shader",
        new scene::NodeParameter<glm::mat4*>("V", 1, m_camera->getViewMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "default_pbr_shader",
        new scene::NodeParameter<glm::mat4*>("P", 1, m_camera->getProjectionMatrixPt())
    );
    m_drawTask->assignLightToShader("forward", "default_pbr_shader", m_dLight.get());
    m_drawTask->assignLightToShader("forward", "default_pbr_shader", m_pLight.get());
    m_drawTask->setSceneGraph(sg.get());

    m_guiManager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());
    m_editor = std::make_shared<module::gui::SkyEditor>(
        this,        
        m_guiManager
    );

    module::gui::SkyWindow* window = m_editor->getSkyWindow();
    window->addWidget<module::gui::NodeList>(
        "Node List",
        module::gui::SkyWindow::DockSlot::Left,
        sg.get()
    );
    window->addWidget<module::gui::ResourceList>(
        "Resources",
        module::gui::SkyWindow::DockSlot::Left
    );
    window->addWidget<module::gui::ShaderEditor>(
        "Shader Editor",
        module::gui::SkyWindow::DockSlot::Center
    );
    window->addWidget<module::gui::LogWidget>(
        "Logger",
        module::gui::SkyWindow::DockSlot::Footer
    );

    auto viewport = window->addWidget<module::gui::Viewport>(
        "Viewport",
        module::gui::SkyWindow::DockSlot::Center
    );
    viewport->addDrawTask(m_drawTask.get());
    viewport->setActiveCamera(m_camera);
    window->addWidget<module::gui::DrawTaskEditor>(
        "task editor",
        module::gui::SkyWindow::DockSlot::Center,
        viewport
    );
}

void TestDocking::initUserRessources()
{

    m_dLight = ressource::Ressource::Create<ressource::DirLight>("dir_light", 0);
    m_pLight = ressource::Ressource::Create<ressource::PointLight>("point_light", 0);

    m_camera = std::make_shared<scene::Camera>("camera");
    m_camera->updateCameraWithCurrentAttributes();

    m_cube = std::make_shared<scene::GameObject3D>("Cube");
    m_cube->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("default_pbr_shader")).get());
    m_cube->setMesh(geometry::CubePrimitive(1.0f));
    m_cube->setMaterial(ressource::PBRMaterial::Default);

    m_sphere = std::make_shared<scene::GameObject3D>("Sphere");
    m_sphere->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("default_pbr_shader")).get());
    m_sphere->setMesh(geometry::SpherePrimitive(100, 100));
    m_sphere->setMaterial(ressource::PBRMaterial::Default);

}

void TestDocking::initTasks()
{

    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame, m_guiManager));
    m_editor->addTasksToEngine();
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender, m_guiManager));

    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}


int main()
{
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);

    TestDocking engineInstance;
    engineInstance.init();
    engineInstance.run();

    return 0;
}
