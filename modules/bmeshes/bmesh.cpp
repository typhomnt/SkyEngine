#include "bmesh.h"
#include "math/mathutils.h"
#include "glm/gtx/norm.hpp"
#include "geometry/geometryalgorithms.h"
namespace module
{

namespace bmeshes
{
BMesh::BMesh(const std::string &mesh_name) : m_mesh(std::make_shared<skyengine::geometry::TopologicalMesh<skyengine::animation::Vertex3DSkin>>(mesh_name))
{

}

void BMesh::setRootIndex(size_t index)
{
    m_root_sphere_id = index;
}

void BMesh::AddSphere(float radius, const glm::vec3 &center)
{
    m_skeleton.addNode(std::make_shared<skyengine::utility::GraphNode<BSphere>>(BSphere(radius,center)));
}

void BMesh::AddConnection(size_t id_0, size_t id_1)
{
   m_skeleton.getNode(id_0)->addConnection(m_skeleton.getNode(id_1));
}

void BMesh::computeInitMesh(unsigned int inbetween_step)
{
    m_mesh->clear();

    skyengine::utility::GraphNode<BSphere>* root_node = m_skeleton.getNode(m_root_sphere_id);
    std::unordered_map<skyengine::utility::GraphNode<BSphere>*, skyengine::utility::GraphNode<BSphere>*> deal_nodes;
    computeInitMeshRec(root_node, deal_nodes, inbetween_step);

    m_mesh->ComputeMeshFromTopo();
    skyengine::geometry::ComputeNormalsForMesh(*m_mesh);
}

void BMesh::computeInitMeshRec(skyengine::utility::GraphNode<BSphere>* sphere, std::unordered_map<skyengine::utility::GraphNode<BSphere>* ,skyengine::utility::GraphNode<BSphere>* >& dealt_nodes, unsigned int inbetween_step)
{
    if((sphere->neighbors.size() == 1) || (sphere->neighbors.size() == 2))
    {
        skyengine::utility::GraphNode<BSphere>* neighbor = nullptr;
        bool must_break = false;
        if(!skyengine::utility::MapContains(dealt_nodes,sphere->neighbors[0]))
            neighbor = sphere->neighbors[0];
        else if ((sphere->neighbors.size() == 2) && !skyengine::utility::MapContains(dealt_nodes,sphere->neighbors[1]))
            neighbor = sphere->neighbors[1];
        else
        {
            neighbor = sphere->neighbors[0];
            must_break = true;
        }

        glm::vec3 curr = sphere->data.center;
        glm::vec3 joint = neighbor->data.center;
        glm::vec3 bone_dir = glm::normalize(joint - curr);
        float cur_jt_dist = glm::l1Norm(joint - curr);
        float curr_rad = sphere->data.radius;
        float joint_rad = neighbor->data.radius;
        glm::vec3 y_local_axis;
        glm::vec3 z_local_axis;
        if(skyengine::math::ApproxEqual(fabs(glm::dot(glm::vec3(0,1,0),bone_dir)),1.0,0.01))
        {
            y_local_axis = glm::vec3(1,0,0);
            z_local_axis = glm::vec3(0,0,1);
        }
        else
        {
            y_local_axis = glm::cross(glm::vec3(0,1,0),bone_dir);
            z_local_axis = glm::cross(bone_dir,y_local_axis);
        }


        unsigned int extr_v_1 = m_mesh->addTVertex(curr + glm::normalize(y_local_axis + z_local_axis)*curr_rad);
        unsigned int extr_v_2 = m_mesh->addTVertex(curr + glm::normalize(-y_local_axis + z_local_axis)*curr_rad);
        unsigned int extr_v_3 = m_mesh->addTVertex(curr + glm::normalize(-y_local_axis - z_local_axis)*curr_rad);
        unsigned int extr_v_4 = m_mesh->addTVertex(curr + glm::normalize(y_local_axis - z_local_axis)*curr_rad);

        unsigned int extr_e_1;
        unsigned int extr_e_2;
        unsigned int extr_e_3;
        unsigned int extr_e_4;
        if (must_break)
        {
            extr_e_1 = m_mesh->addHalfedge(extr_v_1);
            extr_e_2 = m_mesh->addHalfedge(extr_v_2);
            extr_e_3 = m_mesh->addHalfedge(extr_v_3);
            extr_e_4 = m_mesh->addHalfedge(extr_v_4);
            std::vector<unsigned int> end_face;
            end_face.push_back(extr_e_1);
            end_face.push_back(extr_e_2);
            end_face.push_back(extr_e_3);
            end_face.push_back(extr_e_4);
            m_mesh->addFaceHE(end_face);
            goto deal;
        }

        for(unsigned int i = 1; i <= inbetween_step; ++i)
        {
            float inbetween_i = float(i) / float(inbetween_step);

            glm::vec3 center = curr + bone_dir * (cur_jt_dist * inbetween_i);
            float radius = (1.0f - inbetween_i) * curr_rad + inbetween_i * joint_rad;

            glm::vec3 pos_1 = center + glm::normalize(y_local_axis + z_local_axis) * radius;
            glm::vec3 pos_2 = center + glm::normalize(-y_local_axis + z_local_axis) * radius;
            glm::vec3 pos_3 = center + glm::normalize(-y_local_axis - z_local_axis) * radius;
            glm::vec3 pos_4 = center + glm::normalize(y_local_axis - z_local_axis) * radius;

            unsigned int t_v_1 = m_mesh->addTVertex(pos_1);
            unsigned int t_v_2 = m_mesh->addTVertex(pos_2);
            unsigned int t_v_3 = m_mesh->addTVertex(pos_3);
            unsigned int t_v_4 = m_mesh->addTVertex(pos_4);

            /*unsigned int t_e_1 = m_mesh->addHalfedge(t_v_1);
            unsigned int t_e_2 = m_mesh->addHalfedge(t_v_2);
            unsigned int t_e_3 = m_mesh->addHalfedge(t_v_3);
            unsigned int t_e_4 = m_mesh->addHalfedge(t_v_4);*/

            //U edges
            //Up
            std::vector<unsigned int> u_e_1;
            extr_e_1 = m_mesh->addHalfedge(extr_v_1);
            u_e_1.push_back(extr_e_1);
            unsigned int h_u_2 = m_mesh->addHalfedge(extr_v_2);
            u_e_1.push_back(h_u_2);
            unsigned int h_u_3 = m_mesh->addHalfedge(t_v_2);
            u_e_1.push_back(h_u_3);
            unsigned int h_u_4 = m_mesh->addHalfedge(t_v_1);
            u_e_1.push_back(h_u_4);
            m_mesh->addFaceHE(u_e_1);

            //Right
            std::vector<unsigned int> u_e_2;
            extr_e_4 = m_mesh->addHalfedge(extr_v_4);
            u_e_2.push_back(extr_e_4);
            unsigned int h_r_2 = m_mesh->addHalfedge(extr_v_1);
            u_e_2.push_back(h_r_2);
            unsigned int h_r_3 = m_mesh->addHalfedge(t_v_1);
            u_e_2.push_back(h_r_3);
            unsigned int h_r_4 = m_mesh->addHalfedge(t_v_4);
            u_e_2.push_back(h_r_4);
            m_mesh->addFaceHE(u_e_2);

            //Left
            std::vector<unsigned int> u_e_3;
            extr_e_2 = m_mesh->addHalfedge(extr_v_2);
            u_e_3.push_back(extr_e_2);
            unsigned int h_l_2 = m_mesh->addHalfedge(extr_v_3);
            u_e_3.push_back(h_l_2);
            unsigned int h_l_3 = m_mesh->addHalfedge(t_v_3);
            u_e_3.push_back(h_l_3);
            unsigned int h_l_4 = m_mesh->addHalfedge(t_v_2);
            u_e_3.push_back(h_l_4);
            m_mesh->addFaceHE(u_e_3);

            //Bottom
            std::vector<unsigned int> u_e_4;
            extr_e_3 = m_mesh->addHalfedge(extr_v_3);
            u_e_4.push_back(extr_e_3);
            unsigned int h_b_2 = m_mesh->addHalfedge(extr_v_4);
            u_e_4.push_back(h_b_2);
            unsigned int h_b_3 = m_mesh->addHalfedge(t_v_4);
            u_e_4.push_back(h_b_3);
            unsigned int h_b_4 = m_mesh->addHalfedge(t_v_3);
            u_e_4.push_back(h_b_4);
            m_mesh->addFaceHE(u_e_4);

            // Close mesh if one of the nodes is an end node
            if(sphere->neighbors.size() == 1 && (i == 1 || i == inbetween_step))
                m_mesh->addFace({t_v_1, t_v_2, t_v_3, t_v_4});

            extr_v_1 = t_v_1;
            extr_v_2 = t_v_2;
            extr_v_3 = t_v_3;
            extr_v_4 = t_v_4;

            /*extr_e_1 = t_e_1;
            extr_e_2 = t_e_2;
            extr_e_3 = t_e_3;
            extr_e_4 = t_e_4;*/
        }
    }


deal:
    dealt_nodes[sphere] = sphere;
    for(skyengine::utility::GraphNode<BSphere>* node : sphere->neighbors)
    {
        if(!skyengine::utility::MapContains(dealt_nodes,node))
            computeInitMeshRec(node,dealt_nodes,inbetween_step);
    }
}


}
}
