#include "statemachine.h"
namespace skyengine
{

namespace utility
{

StateMachine::StateMachine()
{
}

StateMachine::~StateMachine()
{

}

void StateMachine::setCurrentState(std::shared_ptr<State> state)
{
    m_current_state = state;
}

void StateMachine::handleCurrentState()
{
    m_current_state->handleState();
}


}
}
