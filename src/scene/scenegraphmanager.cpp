#include "scenegraphmanager.h"

#include <iostream>

#include "utils/utilities.h"
#include "core/functioncalltask.h"
#include "core/updateable.h"
#include "core/skyengine.h"

namespace skyengine
{

namespace scene
{

void SceneGraphManager::Add(const std::string &name, const std::shared_ptr<SceneGraph> &scene)
{
    SceneGraphManager* manager = GetSingleton();
    utility::MapInsert<std::string, std::shared_ptr<SceneGraph>>(
        manager->m_scenes,
        name,
        scene,
        "Scene " + name + " already exists"
    );
}

std::shared_ptr<SceneGraph> SceneGraphManager::Get(const std::string &name)
{
    SceneGraphManager* manager = GetSingleton();

    std::shared_ptr<SceneGraph> out = nullptr;
    utility::MapFind<std::string, std::shared_ptr<SceneGraph>>(
        manager->m_scenes,
        name,
        out,
        "SceneGraphManager::Get : scenegraph " + name + " not found"
    );

    return out;
}

void SceneGraphManager::Remove(const std::string &name)
{
    SceneGraphManager* manager = GetSingleton();
    utility::MapRemove<std::string, std::shared_ptr<SceneGraph>>(
        manager->m_scenes,
        name,
        "Resource " + name + " does not exist"
    );
}

void SceneGraphManager::setSkyEngineTasks(core::SkyEngine* engine)
{
    engine->addTask(core::makeFunctionCallTask([engine]()
    {
        SceneGraphManager* manager = GetSingleton();
        for(auto& scene : manager->m_scenes)
        {
            for(auto& node :scene.second->m_scene_nodes)
            {
                auto updatable = std::dynamic_pointer_cast<core::Updatable>(node.second);
                if(updatable)
                    updatable->update(engine->getTotalTime(), engine->getDeltaTime());
            }
        }
    }));
}

SceneGraphManager::SceneGraphManager() {}

} // namespace scene

} // namespace skyengine
