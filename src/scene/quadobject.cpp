#include "quadobject.h"
#include "../render/Vertex3D.h"
namespace skyengine
{

namespace scene
{

QuadObject::QuadObject(const std::string& name, scene::SceneGraph *owner,
                       const unsigned int type,
                       const glm::mat4& world_matrix,
                       const glm::vec3 &center_position,
                       float width,
                       float height) :
    GameObject3D(name, owner, type, world_matrix),
    m_center_position(center_position),
    m_width(width),
    m_height(height)
{
    build();
}


void QuadObject::build()
{
    float hWidth = 0.5f * m_width;
    float hHeight = 0.5f * m_height;
    glm::vec3 N(0.0f, 0.0f, 1.0f);

    m_mesh = std::make_shared<ressource::Mesh<render::vertex3D>>("Quad_" + m_name);
    auto pMesh = std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D>>(m_mesh);

    pMesh->addVertex(render::vertex3D(
        m_center_position + glm::vec3(-hWidth, -hHeight, 0.0f),
        glm::vec2(),
        N
    ));
    pMesh->addVertex(render::vertex3D(
        m_center_position + glm::vec3(-hWidth,  hHeight, 0.0f),
        glm::vec2(0.0f, 1.0f),
        N
    ));
    pMesh->addVertex(render::vertex3D(
        m_center_position + glm::vec3( hWidth,  hHeight, 0.0f),
        glm::vec2(1.0f, 1.0f),
        N
    ));
    pMesh->addVertex(render::vertex3D(
        m_center_position + glm::vec3( hWidth, -hHeight, 0.0f),
        glm::vec2(1.0f, 0.0f),
        N
    ));

    pMesh->addIndex(0);
    pMesh->addIndex(2);
    pMesh->addIndex(1);
    pMesh->addIndex(0);
    pMesh->addIndex(3);
    pMesh->addIndex(2);
}

std::shared_ptr<rapidjson::Value> QuadObject::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = GameObject3D::SerializeProp(al);

    utility::JSONHelper::SaveVec(*value, "Center", m_center_position, al);
    value->AddMember(rapidjson::Value("Width", al), rapidjson::Value(m_width), al);
    value->AddMember(rapidjson::Value("Height", al), rapidjson::Value(m_height), al);

    return value;
}

void QuadObject::LoadProp(const rapidjson::Value& value)
{
    GameObject3D::LoadProp(value);

    utility::JSONHelper::ReadVec(value["Center"], m_center_position);
    m_width = value["Width"].GetFloat();
    m_height = value["Height"].GetFloat();
}

}

}
