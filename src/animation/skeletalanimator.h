#ifndef SKELETALANIMATOR_H
#define SKELETALANIMATOR_H

#include "utils/skyengine_dll.h"

#include "animator.h"
#include "gameobjectskinned.h"
#include "skeletalanimationmodifier.h"
#include "geometry/trajectorynode.h"

namespace skyengine
{

namespace animation
{
class SKYENGINE_API  SkeletalAnimator : public Animator
{
public:
    SkeletalAnimator();

    void setAnimatedSkeleton(Skeleton* skeleton);
    inline const Skeleton& getAnimatedSkeleton();
    virtual void addAnimation(const std::string& element_name, float start_time, float end_time, Animation* animation, unsigned int layered_order, std::shared_ptr<geometry::AbstractTransformModifier> modifier  = nullptr,bool add_transition = false);
    virtual void addAnimation(const std::string& element_name, float start_time, float end_time, Animation* animation, unsigned int layered_order, const std::vector<std::string>& computed_bones, std::shared_ptr<geometry::AbstractTransformModifier> modifier = nullptr, bool add_transition = false);

    virtual void removeAnimation(const std::string& animation_name);
    virtual void computeAnimation(float time_stamp);
    virtual void playAnimation(core::SkyEngine* play_engine);    
    virtual void clear();
    virtual void clearAnimatedCompBones();
    virtual void clearMotionPaths();
    virtual void updateMotionPaths();
    virtual bool setElementModifier(const std::string& element_name, std::shared_ptr<geometry::AbstractTransformModifier> modifier);
    virtual void generateNeutralIddle(const std::string& element_name, float start_time, float end_time,const glm::vec3& laban_effort);
    virtual void saveCurrentCompoAsAnim(const std::string& anim_name);

protected:
    std::unordered_map<std::string,std::vector<std::string> > m_animation_computed_bones;
    Skeleton* m_animated_skeleton;
    std::shared_ptr<Skeleton> m_composed_skeleton;
    std::vector<geometry::TrajectoryNode<KeyPose3D>*> m_motion_paths;
    bool m_display_animation;

    virtual void play();
    //Associated draw task or skyengine
};

inline const Skeleton& SkeletalAnimator::getAnimatedSkeleton()
{
    return *m_animated_skeleton;
}

}
}
#endif // SKELETALANIMATOR_H
