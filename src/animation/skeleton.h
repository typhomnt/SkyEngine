#ifndef SKELETON_H
#define SKELETON_H
#include "bone3d.h"
#include "animationmacros.h"
#include "ressources/ressource.h"
#include <unordered_map>
#include <memory>
#include "scene/scenegraph.h"
namespace skyengine
{

namespace animation
{
class Skeleton : public ressource::Ressource
{

public:
    //Constructors & Destructors---------------
    Skeleton(const std::string& name);
    ~Skeleton();
    //-----------------------------------------

    //Getters----------------------------------
    Bone3D* getRootBone() const;
    Bone3D* getBonePt(const std::string& bone_name);
    const Bone3D& getBone(const std::string& bone_name) const;
    bool containsBone(const std::string& bone_name) const;
    inline std::unordered_map<std::string,std::shared_ptr<Bone3D> >& getBones();
    inline const glm::mat4& getGlobalInverseTransform() const;
    inline std::string getDefaultSpineRootBone() const;
    inline std::string getDefaultSpineCoreBone() const;
    inline std::string getDefaultSpineHeadBone() const;
    inline Bone3D* getRootMvtBone();
    inline Bone3D* getCoreMvtBone();
    inline Bone3D* getHeadMvtBone();
    inline std::unordered_map<std::string,unsigned int>& getIKChains();
    inline std::unordered_map<std::string,std::vector<std::string>>& getEffTargetMap();
    inline std::vector<std::string>& getTargets();
    inline const std::vector<std::string>& getIKEndTargets(const std::string& end_bone);
    inline bool containsIKChain(const std::string& end_bone);
    inline bool containsTargets(const std::string& end_bone);    
    inline bool isSpineBone(const std::string& bone);
    inline bool isArtBone(const std::string& bone);
    inline bool isEffBone(const std::string& bone);
    //Setters & adders-------------------------
    void setRootBone(Bone3D *root_bone);
    void addBone(std::shared_ptr<Bone3D> bone,const std::string& parent_name = "");
    void addBone(const Bone3D& bone, const std::string& parent_name = "");
    void setGloabalInverseTransform(const glm::mat4& transform);
    void setRootMvtBone(const std::string& bone_name);
    void setCoreMvtBone(const std::string& bone_name);
    void setHeadMvtBone(const std::string& bone_name);
    void addIKChain(const std::string& end_bone, unsigned int chain_length);
    void removeIKChain(const std::string& end_bone);
    void setIKChainLength(const std::string& end_bone, unsigned int chain_length);
    void addTargetToIK(const std::string& end_bone, const std::string& target_bone);
    void setTargetToIK(const std::string& end_bone, const std::string& target_bone);
    void removeTargetToIK(const std::string& end_bone, const std::string& target_bone);    
    void addTargetBoneToEffector(const std::string& new_t_bone, const std::string& end_bone);    
    //-----------------------------------------

    //Compute Hierarchycal transform using local and offset transform
    void updateHierarchicalTransforms();

    //IK Target Adder for each end Effector
    void createIKTgEff();



    void printSkeleton() const;
    void printBoneRec(Bone3D* bone) const;
    std::shared_ptr<Ressource> Duplicate();

    static bool SameStructure(const Skeleton& skel1 , const Skeleton& skel2);

    /**
     * @brief computeSpineArticulations
     * Two steps function:
     * - first we compute the spine bones which are all the bones located bewteen the root and the core and
     * the root and the head in the hierarchy
     * - second we compute the articulation bones which are the children of the root,core and head and which are not spine bones
     * @param root
     * @param core
     * @param head
     */

    void computeSpineArticulations();
    inline std::unordered_map<std::string,Bone3D*>& getSpineBones();
    inline std::unordered_map<std::string,Bone3D*>& getArticulationBones();
    inline std::unordered_map<std::string,Bone3D*>& getEffectorBones();

    void CreateArmature(scene::SceneGraph*);
    std::string getTag() override { return "Skel"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);
private:

    Bone3D* root_bone;
    Bone3D* m_root_mvt_bone;
    Bone3D* m_core_mvt_bone;
    Bone3D* m_head_mvt_bone;
    std::unordered_map<std::string,Bone3D*> m_spine_bones;
    std::unordered_map<std::string,Bone3D*> m_articulation_bones;
    std::unordered_map<std::string,Bone3D*> m_effector_bones;
    std::unordered_map<std::string,std::shared_ptr<Bone3D> > m_bones;
    glm::mat4 m_global_inverse_transform;
    std::vector<std::string> m_ik_targets;
    std::unordered_map<std::string,std::vector<std::string>> m_effector_target_map;
    std::unordered_map<std::string,unsigned int> m_ik_chains; // string represents end effector and unsigned int chain legnth

    // TODO: this is to solve the failure case of "getBone". Some more elegant solutions exists
    Bone3D error_bone;

    //Used for skeleton copy
    void copySkeletonRec(Skeleton* copy_skeleton, const std::string& bone_name, Bone3D* parent);

    //Used for hierarchical transform updates
    void updateHierarchicalTransformsRec(const std::string& bone_name, const glm::mat4& hierarchical_transform);

    static bool SameStructureRec(const Bone3D& bone1, const Bone3D& bone2);

    void computeSpine(const std::string& bone, const std::string& root);
    void computeArticulations(const std::string& bone);
    void computeEndEffectors(const std::string& bone);

};

inline std::unordered_map<std::string,std::shared_ptr<Bone3D> >& Skeleton::getBones()
{
    return m_bones;
}

inline const glm::mat4& Skeleton::getGlobalInverseTransform() const
{
    return m_global_inverse_transform;
}


inline std::string Skeleton::getDefaultSpineRootBone() const
{
    for(auto& bone : m_bones)
    {
        std::string bone_name = bone.first;
        skyengine::utility::stringUpperCase(bone_name);
        if(bone_name.find("PELVIS") != std::string::npos)
            return bone.first;
    }

    return "";
}

inline std::string Skeleton::getDefaultSpineCoreBone() const
{
    for(auto& bone : m_bones)
    {
        std::string bone_name = bone.first;
        skyengine::utility::stringUpperCase(bone_name);
        if(bone_name.find("CHEST") != std::string::npos)
            return bone.first;
    }

    return "";
}

inline std::string Skeleton::getDefaultSpineHeadBone() const
{
    for(auto& bone : m_bones)
    {
        std::string bone_name = bone.first;
        skyengine::utility::stringUpperCase(bone_name);
        if(bone_name.find("HEAD") != std::string::npos)
            return bone.first;
    }

    return "";
}

inline Bone3D* Skeleton::getRootMvtBone()
{
    return m_root_mvt_bone;
}

inline Bone3D* Skeleton::getCoreMvtBone()
{
    return m_core_mvt_bone;
}

inline Bone3D*Skeleton:: getHeadMvtBone()
{
    return m_head_mvt_bone;
}

inline std::unordered_map<std::string,Bone3D*>& Skeleton::getSpineBones()
{
    return m_spine_bones;
}

inline std::unordered_map<std::string,Bone3D*>& Skeleton::getArticulationBones()
{
    return m_articulation_bones;
}

inline std::unordered_map<std::string,Bone3D*>& Skeleton::getEffectorBones()
{
    return m_effector_bones;
}

inline std::unordered_map<std::string,unsigned int>& Skeleton::getIKChains()
{
    return m_ik_chains;
}

inline std::unordered_map<std::string,std::vector<std::string>>& Skeleton::getEffTargetMap()
{
    return m_effector_target_map;
}


inline std::vector<std::string>& Skeleton::getTargets()
{
    return m_ik_targets;
}

inline const std::vector<std::string>& Skeleton::getIKEndTargets(const std::string& end_bone)
{
    return m_effector_target_map[end_bone];
}

inline bool Skeleton::containsIKChain(const std::string& end_bone)
{
    return utility::MapContains(m_ik_chains,end_bone);
}

inline bool Skeleton::containsTargets(const std::string& end_bone)
{
   return (utility::MapContains(m_effector_target_map,end_bone))&& (m_effector_target_map[end_bone].size() != 0);
}

inline bool Skeleton::isSpineBone(const std::string& bone)
{
    return utility::MapContains(m_spine_bones,bone);
}

inline bool  Skeleton::isArtBone(const std::string& bone)
{
    return utility::MapContains(m_articulation_bones,bone);
}

inline bool  Skeleton::isEffBone(const std::string& bone)
{
    return utility::MapContains(m_effector_bones,bone);
}


}
}
#endif // SKELETON_H
