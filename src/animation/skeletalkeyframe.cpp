#include "skeletalkeyframe.h"
#include <iostream>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
namespace skyengine
{

namespace animation
{
SkeletalKeyFrame::SkeletalKeyFrame(SkeletalAnimation *animation, float time_stamp):
    KeyFrame(time_stamp)
{
    setAnimation(animation);
}

SkeletalKeyFrame::~SkeletalKeyFrame()
{

}

float SkeletalKeyFrame::keyframeDistance(SkeletalKeyFrame &keyframe_1, SkeletalKeyFrame &keyframe_2)
{
    SkeletalAnimation* anim1 = dynamic_cast<SkeletalAnimation*>(keyframe_1.m_animation);
    SkeletalAnimation* anim2 = dynamic_cast<SkeletalAnimation*>(keyframe_2.m_animation);
    std::shared_ptr<Skeleton> skel_1 = std::dynamic_pointer_cast<Skeleton>(anim1->getAnimationSkeleton()->Duplicate());
    std::shared_ptr<Skeleton> skel_2 = std::dynamic_pointer_cast<Skeleton>(anim2->getAnimationSkeleton()->Duplicate());
    float time_in_ticks_1 = keyframe_1.m_time_stamp * anim1->getTickPerSecond();
    float animation_time_1 = fmod(time_in_ticks_1, anim1->getDuration()*anim1->getTickPerSecond());
    float time_in_ticks_2 = keyframe_2.m_time_stamp * anim2->getTickPerSecond();
    float animation_time_2 = fmod(time_in_ticks_2, anim2->getDuration()*anim2->getTickPerSecond());
    anim1->ComputeBoneTransform(animation_time_1);
    anim2->ComputeBoneTransform(animation_time_2);
    float total_dist = 0.0f;
    for(auto bone : skel_1->getBones())
    {
        glm::mat4 mat1 =  bone.second->hierarchical_transform;
        glm::mat4 mat2 =  skel_2->getBone(bone.first).hierarchical_transform;
        glm::mat4 mat3 = mat1 - mat2;
        for(unsigned int i = 0 ; i < 4 ; i++)
            for (unsigned int j = 0; j < 4; ++j)
                    total_dist +=  mat3[i][j]*mat3[i][j];
    }
    ressource::RessourceManager::Remove(skel_1->getName());
    ressource::RessourceManager::Remove(skel_2->getName());
    return total_dist;
}



}
}
