#ifndef ANIMATION_H
#define ANIMATION_H

#include "utils/skyengine_dll.h"

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <assert.h>
#include "keyframe.h"
#include "timeparameter.h"
#include <glm/glm.hpp>
#include <math/mathutils.h>
#include "utils/utilities.h"
#include "ressources/ressource.h"
#include "utils/interpolationfunction.h"

#ifdef __GUI__
namespace module
{

namespace gui
{
class ActionAnimator;
}
}
#endif

namespace skyengine
{

namespace animation
{
class KeyFrame;
class SKYENGINE_API Animation : public ressource::Ressource
{

#ifdef __GUI__
    friend class module::gui::ActionAnimator;
#endif

public:
    //Constructors & Destructors---------------------
    Animation(const std::string& animation_name);
    virtual ~Animation();
    //-----------------------------------------------


    //Getters---------------------------------------
    float getDuration() const;
    float getTickPerSecond() const;

    virtual inline KeyFrame* getKeyFrame(unsigned int index);
    virtual KeyFrame* getKeyFrameAtTime(float time_stamp);
    virtual inline KeyFrame* getFirstKeyFrame();
    virtual inline KeyFrame *getLastKeyFrame();
    unsigned int getKeyframeNbr() const;
    unsigned int getNearestKeyFrameId(float time);
    inline const std::vector<std::shared_ptr<BaseTimeParameter>>& getKey(const std::string& key_name);
    inline std::shared_ptr<BaseTimeParameter> getKey(const std::string& key_name, float time_stamp);
    inline unsigned int getNearestKey(const std::string& key_name, float time_stamp);
    inline std::unordered_map<std::string,std::vector<std::shared_ptr<BaseTimeParameter>> >& getKeys();
    inline std::shared_ptr<BaseTimeParameter> getLastComputedKey(const std::string& key_name);
    inline std::unordered_map<std::string, std::vector<std::string> > &getKeyGroups();
    inline bool containsKeyGroup(const std::string& key_group_name) const;
    inline bool containsKey(const std::string& key_name) const;
    bool HasKeyFrameAtTime(float time) const;
    inline bool isAnalyzed() const;
    inline glm::vec3 getLabanEfforts() const;
    //Setters & Adders-------------------------------
    void setDuration(const float& duration);
    void setTickPerSecond(const float& tick_per_second);
    void addKeyFrame(const KeyFrame& keyframe);
    void addKeyFrame(std::shared_ptr<KeyFrame> keyframe);
    void addKeyFrame(float time_stamp);
    void addKeyToGroup(const std::string& group_name, const std::string& key_name);
    virtual void addKey(const std::string& key_name, std::shared_ptr<BaseTimeParameter> key_value);

    template<typename T>
    void addKey(const std::string& key_name, std::shared_ptr<TimedParameter<T>> key_value)
    {
        addKey(key_name,std::static_pointer_cast<BaseTimeParameter>(key_value));
        setInterpolationFunction(key_name,utility::InterpolationFunction<T>::getDefaultInterpolationFunc());
    }

    virtual void addKeypoint(KeyFrame* keyframe);
    virtual void addBreakdown(KeyFrame* keyframe);
    virtual void removeKey(const std::string& key_name, float time_stamp);
    virtual void removeKey(const std::string& key_name);
    virtual void setInterpolationFunction(const std::string& key, utility::AbstractInterpolationFunction* fct, unsigned int st_fr = 0, unsigned int ed_frame = 0);
    virtual void ClampInterpolationToEnd(const std::string& key);


    virtual void ComputeAnimation(float time_stamp);
    virtual void Simplify();

    /**
     * @brief getFrameKeys
     * @param key_name
     * @param time_stamp
     * @return Nearest keys at time_stamp
     */
    std::pair<BaseTimeParameter*,BaseTimeParameter*> getFrameKeys(const std::string& key_name, float time_stamp);

    template<typename T>
    T interpolateKey(const std::string& key_name, float time_stamp, std::function<T(const T&,const T&, float weigtht)> interpolation_func)
    {
        if(m_time_keys[key_name].size() == 1)
            return std::dynamic_pointer_cast<TimedParameter<T>>(m_time_keys[key_name][0])->value;

        unsigned int key_index = getNearestKey(key_name,time_stamp);

        if(key_index == m_time_keys[key_name].size() - 1)
        {
            return std::dynamic_pointer_cast<TimedParameter<T>>(m_time_keys[key_name][m_time_keys[key_name].size() - 1])->value;
        }

        unsigned int next_key_index = key_index + 1;
        assert(next_key_index < m_time_keys[key_name].size());

        float delta_time = m_time_keys[key_name][next_key_index]->time - m_time_keys[key_name][key_index]->time;
        float interpolation_factor = utility::clamp((time_stamp - m_time_keys[key_name][key_index]->time) / delta_time,0.0f,1.0f);
        //assert(interpolation_factor >= 0.0f && interpolation_factor <= 1.0f);
        const T& start_key = std::dynamic_pointer_cast<TimedParameter<T>>(m_time_keys[key_name][key_index])->value;
        const T& end_key = std::dynamic_pointer_cast<TimedParameter<T>>(m_time_keys[key_name][next_key_index])->value;
        return utility::InterpolateValues<T>(start_key,end_key,interpolation_factor,interpolation_func);
    }

    /**
      * Each animation can be decomposed up to 5 stages:
      * - Preparation: object accumulates a little effort for the Anticipation stage
      * - Anticipation: object accumulates necessary effort to execute action
      * - Action: main part of the animation <=> purpose
      * - Follow Throught: part of the animated object led by its moving part receiving a passive effect from it
      * - Transition: End of the follow throught where the object come back to a stable position
      */
    //Animation properties---------------
        //Getters--------------------
    inline float getPreparationTime() const;
    inline float getAnticipationTime() const;
    inline float getActionTime() const;
    inline float getFollowThroughTime() const;
    inline float getTransitionTime() const;

    inline std::pair<float,float> getPreparationStartEnd() const;
    inline std::pair<float,float> getAnticipationStartEnd() const;
    inline std::pair<float,float> getActionStartEnd() const;
    inline std::pair<float,float> getFollowThroughStartEnd() const;
    inline std::pair<float,float> getTransitionStartEnd() const;

        //setters
    void setPreparationTime(float time);
    void setAnticipationTime(float time);
    void setActionTime(float time);
    void setFollowThroughTime(float time);
    void setTransitionTime(float time);

    //---------------------------------

    std::string getTag() override { return "Anim"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);
protected:

    float duration;
    float tick_per_second;

    //Time ordered keyframes
    std::vector<std::shared_ptr<KeyFrame>> keyframes;
    std::unordered_map<std::string,std::vector<std::string>> m_key_groups;
    std::unordered_map<std::string,std::vector<std::shared_ptr<BaseTimeParameter>> > m_time_keys;
    std::unordered_map<std::string,std::shared_ptr<BaseTimeParameter>> m_last_computed_keys;
    std::vector<KeyFrame*> m_keypoints;
    std::vector<KeyFrame*> m_breakdowns;
    std::unordered_map<std::string,std::map<unsigned int,std::pair<unsigned int,utility::AbstractInterpolationFunction*>>> m_keys_interpolation_func;

    //Test Compute animation cache
    std::map<float,std::unordered_map<std::string,std::shared_ptr<BaseTimeParameter> > > m_keys_cache;

    //animation properties
    float m_preparation_time;
    float m_anticipation_time;
    float m_action_time;
    float m_follow_throught_time;
    float m_transition_time;

    glm::vec3 m_laban_efforts;

    void copyAnimationProperties(Animation& output_anim);
};

inline float Animation::getPreparationTime() const
{
    return m_preparation_time;
}

inline float Animation::getAnticipationTime() const
{
    return m_anticipation_time;
}

inline float Animation::getActionTime() const
{
    return m_action_time;
}

inline float Animation::getFollowThroughTime() const
{
    return m_follow_throught_time;
}

inline float Animation::getTransitionTime() const
{
    return m_transition_time;
}

inline KeyFrame* Animation::getKeyFrame(unsigned int index)
{
    assert(index < keyframes.size());
    return keyframes[index].get();
}

inline KeyFrame* Animation::getFirstKeyFrame()
{
    return keyframes[0].get();
}

inline KeyFrame* Animation::getLastKeyFrame()
{
    return keyframes[keyframes.size() - 1].get();
}

inline const std::vector<std::shared_ptr<BaseTimeParameter> > &Animation::getKey(const std::string& key_name)
{
    return m_time_keys[key_name];
}

inline std::shared_ptr<BaseTimeParameter> Animation::getKey(const std::string& key_name, float time_stamp)
{

    for(unsigned int i = 0 ; i < m_time_keys.at(key_name).size() ; i++)
        if(math::ApproxEqual<float>(m_time_keys.at(key_name).at(i)->time,time_stamp,(float)ANIM_EPS1))
            return m_time_keys.at(key_name).at(i);

    return nullptr;
}

inline unsigned int Animation::getNearestKey(const std::string& key_name, float time_stamp)
{
    if(time_stamp <= m_time_keys[key_name][0]->time + ANIM_EPS1)
        return 0;
    else if(time_stamp >= m_time_keys[key_name][m_time_keys[key_name].size() - 1]->time + ANIM_EPS1)
        return m_time_keys[key_name].size() - 1;
    for(unsigned int i = 0 ; i < m_time_keys[key_name].size() ; i++)
    {
        if (time_stamp <= m_time_keys[key_name][i]->time + ANIM_EPS1)
        {
            if(math::ApproxEqual<float>(time_stamp, m_time_keys[key_name][i]->time,ANIM_EPS1))
                return i;
            else
                return i - 1;
        }
    }
    //default case
    return 0;
}

inline std::unordered_map<std::string,std::vector<std::shared_ptr<BaseTimeParameter>> >& Animation::getKeys()
{
    return m_time_keys;
}

inline std::shared_ptr<BaseTimeParameter> Animation::getLastComputedKey(const std::string& key_name)
{
    return m_last_computed_keys[key_name];
}

inline std::unordered_map<std::string,std::vector<std::string>>& Animation::getKeyGroups()
{
    return m_key_groups;
}

inline bool Animation::containsKeyGroup(const std::string& key_group_name) const
{
    return utility::MapContains<std::string,std::vector<std::string>>(m_key_groups,key_group_name);
}

inline bool Animation::containsKey(const std::string& key_name) const
{
    return utility::MapContains<std::string,std::vector<std::shared_ptr<BaseTimeParameter>> >(m_time_keys,key_name);
}

inline std::pair<float,float> Animation::getPreparationStartEnd() const
{
    if(m_preparation_time == -1.0f)
        return std::pair<float,float>(-1.0f,-1.0f);
    else
        return std::pair<float,float>(0.0f,m_preparation_time);
}

inline std::pair<float,float> Animation::getAnticipationStartEnd() const
{
    if(m_anticipation_time == -1.0f)
        return std::pair<float,float>(-1.0f,-1.0f);
    else
        return std::pair<float,float>(std::max(0.0f,m_preparation_time),m_anticipation_time);
}

inline std::pair<float,float> Animation::getActionStartEnd() const
{
    if(m_action_time == -1.0f)
        return std::pair<float,float>(-1.0f,-1.0f);
    else
        return std::pair<float,float>(std::max(0.0f,m_anticipation_time),m_action_time);
}

inline std::pair<float,float> Animation::getFollowThroughStartEnd() const
{
    if(m_follow_throught_time == -1.0f)
        return std::pair<float,float>(-1.0f,-1.0f);
    else
        return std::pair<float,float>(std::max(0.0f,m_action_time),m_follow_throught_time);
}

inline std::pair<float,float> Animation::getTransitionStartEnd() const
{
    if(m_transition_time == -1.0f)
        return std::pair<float,float>(-1.0f,-1.0f);
    else
        return std::pair<float,float>(std::max(0.0f,m_follow_throught_time),m_transition_time);
}

inline bool Animation::isAnalyzed() const
{
    //TODO add invalidate
    return (m_breakdowns.size() != 0 || m_keypoints.size() != 0)/*&& m_invalidate*/;
}

inline glm::vec3 Animation::getLabanEfforts() const
{
    return m_laban_efforts;
}

}
}
#endif // ANIMATION_H
