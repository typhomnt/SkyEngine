#ifndef STATEMACHINE_H
#define STATEMACHINE_H
#include "state.h"
#include <memory>
namespace skyengine
{

namespace utility
{
class State;
class StateMachine
{
public:
    StateMachine();
    virtual ~StateMachine();
    void setCurrentState(std::shared_ptr<State> state);
    void handleCurrentState();
protected:
    std::shared_ptr<State> m_current_state;
};

}
}
#endif // STATEMACHINE_H
