#ifndef BONE3D_H
#define BONE3D_H
#include "pose3d.h"
#include "animationmacros.h"
#include <string>
#include <vector>
#include "core/serializable.h"
namespace skyengine
{

namespace animation
{
struct Bone3D : public core::Serializable
{
    glm::mat4 bone_offset;
    glm::mat4 hierarchical_transform;
    glm::mat4 local_transform;

    Pose3D local_pose;
    Pose3D global_pose;

    bool is_modifier;
    std::string bone_name;
    unsigned int bone_ID;
    Bone3D* parent;
    std::vector<Bone3D*> children;    
    Bone3D(): bone_offset(glm::mat4(1.0f)), hierarchical_transform(glm::mat4(1.0f)), local_transform(glm::mat4(1.0f)),local_pose(Pose3D()),global_pose(Pose3D()),is_modifier(false) ,bone_name(""), bone_ID(0), parent(nullptr)
    {
    }

    void Serialize(rapidjson::Value& curr_obj,rapidjson::MemoryPoolAllocator<>& al)
    {
        std::shared_ptr<rapidjson::Value> obj = SerializeProp(al);

        for(Bone3D* bone : children)
            bone->Serialize(*obj,al);

        curr_obj.AddMember(
            rapidjson::Value(bone_name.c_str(), al),
            *(obj.get()),
            al
        );
    }

    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<> &al)
    {
        std::shared_ptr<rapidjson::Value> obj = Serializable::SerializeProp(al);
        utility::JSONHelper::SaveMat(*obj,"offset",bone_offset,al);
        utility::JSONHelper::SaveMat(*obj,"local",local_transform,al);
        obj->AddMember(
            rapidjson::Value("Modifier", al),
            rapidjson::Value(is_modifier),
            al
        );
        obj->AddMember(
            rapidjson::Value("ID", al),
            rapidjson::Value(bone_ID),
            al
        );
        return obj;
    }

    std::string getTag() override { return "Bone3D"; }

};
}
}
#endif // BONE3D_H
