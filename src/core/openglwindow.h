#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include "utils/skyengine_dll.h"

#include "rendersurface.h"

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <unordered_map>

namespace skyengine
{

namespace core
{

class UserInputManager;
class WindowEventManager;

//! OpenGL Window Class
/*! Provides a wrapper to GLFW windows
 *
 * TODO: Use image ressources for icon and cursor
*/

class SKYENGINE_API OpenGLWindow : public RenderSurface
{
    //cursor
    class Cursor
    {
    public:
        Cursor (const GLFWimage*, glm::ivec2);
        Cursor(int);
        ~Cursor();
    protected:
        GLFWcursor* cursor;
        friend class OpenGLWindow;
    };

    // static functions
public:

    //! OpenGLWindow to GLFWWindow converter
    static OpenGLWindow* getOpenGLWindow(GLFWwindow*);

     //! get currently bound window
    static OpenGLWindow* getCurrent();

    //! GLFW hint
    static void setGFLWHint(int first_value, int second_value);

    //! restore GLFW default hints
    static void setGFLWDefaultsHint();

public:

    //! Constructor: create the window
    OpenGLWindow(int width_, int height_, std::string glf_title_ , GLFWmonitor* glf_wmonitor_ = nullptr, std::shared_ptr<OpenGLWindow> shared = nullptr);

    //! Destructor: kill the window
    ~OpenGLWindow();

    //! Bind the window
    void bind();

    // close flag
    // set by setCloseFlag or by the system
    // calls the close callback.
    int closeFlag();
    void setCloseFlag(int);

    // window geometry, in screen coordinates (relative to the monitor, different from framebuffer pixels)
    void setWindowSize(glm::ivec2);
    glm::ivec2 getWindowSize();
    glm::ivec4 getWindowFrameSize(); // distance to the borders of the window (left, top, right, bottom)
    void setWindowSizeLimits(glm::ivec2, glm::ivec2); //min, max. can be GLFW_DONT_CARE
    void setWindowAspectRatio(glm::ivec2);
    void setWindowPosition(glm::ivec2);
    glm::ivec2 getWindowPosition();

    // utf-8 encoded
    void setTitle(std::string);

    // Todo change type to skyengine::image
    void setIcon(std::vector<GLFWimage>);

    // monitor specific functions. Monitor = fullscreen
    GLFWmonitor* getMonitor();
    bool isFullScreen();
    void setMonitor(GLFWmonitor*, glm::ivec2, glm::ivec2, int);
    void setFullScreen(GLFWmonitor *, glm::ivec2 = glm::ivec2(-1), int=GLFW_DONT_CARE);
    void setWindowed();

    // iconification
    bool isIconified();
    void iconify();
    void restore();

    //visibility
    bool isVisible();
    void hide();
    void show();

    // focus
    bool hasFocus();
    void giveFocus();
    bool hasMainFocus();
    void setMainFocus(bool focus);


    // attriutes
    int getGLFWAttrib(int);


    // framebuffer (or rendersurface) geometry, in pixel cooridnates
    glm::ivec2 getSize() const;
    void resize(glm::ivec2);

    glm::ivec2 getPos() const override;
    void move(glm::ivec2 pos) override;

    // events
    void setInputMode(int, int);
    int getKeyState(int);
    glm::vec2 getCursorPos();

    void setCursor(std::shared_ptr<Cursor>);

    std::string getClipboardString();
    void setClipboadString(std::string);


    void doneRendering();

    std::shared_ptr<core::UserInputManager> userInputManager();
    std::shared_ptr<core::WindowEventManager> eventManager();
    void initCallbacks();

    GLFWwindow* getGLFWindow();


private:

    GLFWwindow* glf_window;

    std::shared_ptr<core::UserInputManager> ui_manager;
    std::shared_ptr<core::WindowEventManager> we_manager;

    std::string title;
    std::shared_ptr<Cursor> cursor;

    bool is_focused;
};
}
}
#endif // OPENGLWINDOW_H
