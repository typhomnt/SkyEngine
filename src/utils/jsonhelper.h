#ifndef __JSON_HELPER_H__
#define __JSON_HELPER_H__

#include "utils/skyengine_dll.h"

#include <rapidjson/document.h>
#include <glm/glm.hpp>


namespace skyengine
{

namespace utility
{

/**
 * @brief Utility class defining methods to interact with JSON data.
 *        The JSON data is handled using the rapidjson (https://github.com/Tencent/rapidjson) library.
 */
class SKYENGINE_API JSONHelper
{
public:

    /**
     * @brief  Reads a JSON file and returns the corresponding JSON document.
     * @param  fileName    JSON file name
     * @param  outDocument Output JSON document
     * @return @a true if load succeeded, @a false otherwise (non-existing file, parse error ...)
     */
    static bool Load(const std::string& fileName, rapidjson::Document& outDocument);

    /**
     * @brief  Saves a JSON document to a JSON file.
     * @param  fileName File name
     * @param  document JSON document to save
     * @return @a true if save succeeded, @a false otherwise (failed to open file stream)
     */
    static bool Save(const std::string& fileName, const rapidjson::Document& document);


    /**
     * @brief Adds a string value to a JSON object.
     * @param obj   JSON object
     * @param name  Key name
     * @param value Value name
     * @param al    Document allocatod (needed by rapidjson)
     */
    static void SaveString(
        rapidjson::Value& obj,
        const std::string& name,
        const std::string& value,
        rapidjson::MemoryPoolAllocator<>& al
    );

    /**
     * @brief Adds a GLM matrix to a JSON object.
     * @param obj   JSON object
     * @param name  Key name
     * @param value GLM matrix
     * @param al    Document allocator (needed by rapidjson)
     */
    template<typename glmMat>
    static void SaveMat(rapidjson::Value& obj, const std::string& name, glmMat value, rapidjson::MemoryPoolAllocator<>& al)
    {
        assert(obj.IsObject());

        rapidjson::Value array(rapidjson::kArrayType);
        for(int j = 0 ; j < value[0].length(); ++j)
            for(int i = 0 ; i < value.length(); ++i)
            {
                array.PushBack(value[i][j], al);
            }

        obj.AddMember(rapidjson::Value(name.c_str(), al), array, al);
    }

    /**
     * @brief Adds a GLM vector to a JSON object.
     * @param obj   JSON object
     * @param name  Key name
     * @param value GLM vector
     * @param al    Document allocator (needed by rapidjson)
     */
    template<typename glmVec>
    static void SaveVec(rapidjson::Value& obj, const std::string& name, glmVec value, rapidjson::MemoryPoolAllocator<>& al)
    {
        assert(obj.IsObject());

        rapidjson::Value array(rapidjson::kArrayType);
        for(unsigned int i = 0 ; i < value.length(); ++i)
        {
            array.PushBack(value[i], al);
        }

        obj.AddMember(rapidjson::Value(name.c_str(), al), array, al);
    }

    /**
     * @brief Adds an array to a JSON object.
     * @param obj   JSON object
     * @param name  Key name
     * @param value Array to save
     * @param size  Array size
     * @param al    Document allocator (needed by rapidjson)
     */
    template<typename T>
    static void SaveArray(rapidjson::Value& obj, const std::string& name, T* value, size_t size, rapidjson::MemoryPoolAllocator<>& al)
    {
        assert(obj.IsObject());

        rapidjson::Value array(rapidjson::kArrayType);
        for(unsigned int i = 0 ; i < size; ++i)
        {
            array.PushBack(value[i], al);
        }

        obj.AddMember(rapidjson::Value(name.c_str(), al), array, al);
    }

    /**
     * @brief Reads a GLM vector from a JSON array.
     * @param array  JSON array
     * @param outVec Output GLM vector
     */
    template<typename glmVec>
    static void ReadVec(const rapidjson::Value& array, glmVec& outVec)
    {
        assert(array.IsArray());

        for (rapidjson::SizeType i = 0; i < array.Size(); ++i)
            outVec[i] = array[i].GetFloat();
    }

    /**
     * @brief Reads a GLM matrix from a JSON array.
     * @param array  JSON array
     * @param outVec Output GLM matrix
     */
    template<typename glmMat>
    static void ReadMat(const rapidjson::Value& array, glmMat& outMat)
    {
        assert(array.IsArray());
        int dim = outMat.length();

        // TODO : only workw with NxN matrices for now
        for (int j = 0; j < dim; ++j)
        {
            for (int i = 0; i < dim; ++i)
                outMat[i][j] = array[i * dim + j].GetFloat();
        }
    }

};

} // namespace utility

} // namespace skyengine

#endif // __JSON_HELPER_H__
