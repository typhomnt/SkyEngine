#ifndef __NODELIST_H__
#define __NODELIST_H__

#include "skywidget.h"

#include "scene/scenegraph.h"


namespace module
{

namespace gui
{

class NodeList : public SkyWidget
{
public:
    NodeList(const std::string& name, SkyWindow *sky_win, std::shared_ptr<ActiveObjectEvent> act_evt,
             std::shared_ptr<SelectObjectEvent> select_evt, skyengine::scene::SceneGraph* sceneGraph);

    void setSceneGraph(skyengine::scene::SceneGraph* sceneGraph) { m_sceneGraph = sceneGraph; }

    void draw() override;

private:
    skyengine::scene::SceneGraph* m_sceneGraph;

    std::vector<std::string> m_shaders;
    std::vector<const char*> m_guiShaders;

    void drawNode(skyengine::scene::SceneGraphNode* node);
};

} // namespace gui

} // namespace module

#endif // __NODELIST_H__
