#version 330

// Input
in vec4 varColor;

// Output
out vec4 fragColor;

void main()
{
    fragColor = varColor;
}
