#define TEMPLATE_INSTANTIATION_AABBOX

#include "AABBox.h"

namespace ext_dvc
{

namespace slate
{

// explicit instantiation
template class AABBoxT<glm::vec3>;
template class AABBoxT<glm::vec2>;

}

}
