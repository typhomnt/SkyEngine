#ifndef __SHADER_EDITOR_H__
#define __SHADER_EDITOR_H__

#include "skywidget.h"

#include <imgui/addons/imguitabwindow/imguitabwindow.h>

#include "ressources/shader.h"


namespace module
{

namespace gui
{

class ShaderEditor : public SkyWidget
{
public:
    class ShaderTabLabel : public ImGui::TabWindow::TabLabel
    {
    public:
        enum class ShaderType : unsigned char
        {
            VERTEX = 0,
            GEOMETRY,
            FRAGMENT
        };

        ShaderTabLabel(const char* label, const char* tooltip = nullptr);
        ~ShaderTabLabel();

        void setShader(skyengine::ressource::Shader* shader, ShaderType type);

        void askToSave() { m_saveAsked = true; }

        void render() override;

    private:
        skyengine::ressource::Shader* m_shader;
        ShaderType m_type;

        char* m_buffer;
        size_t m_bufferSize;

        bool m_saveAsked;
    };


    ShaderEditor(const std::string& name, SkyWindow* window, std::shared_ptr<ActiveObjectEvent> act_evt,
                 std::shared_ptr<SelectObjectEvent> select_evt, ImGuiWindowFlags flags = 0);

    void draw() override;

private:
    int m_shaderIndex;
    ImGui::TabWindow m_tabShaders;
};

} // namespace gui

} // namespace module

#endif //__SHADER_EDITOR_H__
