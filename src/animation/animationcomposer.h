#ifndef ANIMATIONCOMPOSER_H
#define ANIMATIONCOMPOSER_H
#include "animator.h"
#include "utils/singleton.h"

namespace skyengine
{

namespace animation
{
class AnimationComposer : public utility::Singleton<AnimationComposer>, AnimatorInterface
{
public:
    friend AnimationComposer* Singleton<AnimationComposer>::GetSingleton();

    static void AddAnimator(Animator* animator);
    static void RemoveAnimator(Animator* animator);
    #ifdef __GUI__
    static void RemoveAnimator(unsigned int animator_id);
    #endif
    static void PlayComposer(core::SkyEngine* play_engine);
    static void StopComposer(core::SkyEngine* play_engine);

    virtual void computeAnimation(float time_stamp);
    virtual void playAnimation(core::SkyEngine* play_engine);

    AnimationComposer(const AnimationComposer&) = delete;
    AnimationComposer& operator=(const AnimationComposer&) = delete;
    void play();


protected:
    AnimationComposer();
    std::vector<Animator*> m_composed_animator;
};
}
}
#endif // ANIMATIONCOMPOSER_H
