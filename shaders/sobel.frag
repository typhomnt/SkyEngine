#version 330 core
uniform sampler2D renderedTexture;
in vec2 UV;

// Output variable
out vec4 fragColor;
void make_kernel(inout vec4 n[9], sampler2D tex, vec2 coord)
{
        ivec2 tex_size = textureSize(tex,0);
        float w = 1.0 / tex_size.x;
        float h = 1.0 / tex_size.y;

        n[0] = texture(tex, coord + vec2( -w, -h));
        n[1] = texture(tex, coord + vec2(0.0, -h));
        n[2] = texture(tex, coord + vec2(  w, -h));
        n[3] = texture(tex, coord + vec2( -w, 0.0));
        n[4] = texture(tex, coord);
        n[5] = texture(tex, coord + vec2(  w, 0.0));
        n[6] = texture(tex, coord + vec2( -w, h));
        n[7] = texture(tex, coord + vec2(0.0, h));
        n[8] = texture(tex, coord + vec2(  w, h));
}

void main(void)
{
        vec4 n[9];
        make_kernel( n, renderedTexture, UV );

        float threshold = 1.0f;
        vec4 sobel_edge_h = n[2] + (2.0*n[5]) + n[8] - (n[0] + (2.0*n[3]) + n[6]);
        vec4 sobel_edge_v = n[0] + (2.0*n[1]) + n[2] - (n[6] + (2.0*n[7]) + n[8]);
        vec4 sobel = sqrt(threshold*(sobel_edge_h * sobel_edge_h) + threshold*(sobel_edge_v * sobel_edge_v));

        fragColor = texture(renderedTexture,UV)*vec4( 1.0 - sobel.rgb, 1.0 );
}
