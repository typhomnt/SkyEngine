#ifndef MESHMODIFIER_H
#define MESHMODIFIER_H

#include "utils/skyengine_dll.h"

#include "transformmodifier.h"
#include "../ressources/mesh.h"
#include "../render/Vertex3D.h"
namespace skyengine
{

namespace geometry
{


class SKYENGINE_API MeshModifier : public TransformModifier<ressource::Mesh<render::vertex3D>,glm::vec3,glm::vec3>
{
public:
    MeshModifier();

    void setScaleFactor(float factor);
    void setRotateFactor(float factor);
    void setTranslateFactor(float factor);

    virtual void scale(const glm::vec3& pivot_point,  ressource::Mesh<render::vertex3D>& data_scale);
    virtual void translate(const glm::vec3& translate_vector,  ressource::Mesh<render::vertex3D>& data_translate);
    virtual void rotate(const glm::vec3& pivot_point,  ressource::Mesh<render::vertex3D>& data_rotate);

    virtual void scale_axis(const glm::vec3& pivot_point, const glm::vec3& pivot_axis,ressource::Mesh<render::vertex3D>& data_scale);
    virtual void rotate_axis(const glm::vec3& pivot_point,const glm::vec3& pivot_axis,  ressource::Mesh<render::vertex3D>& data_rotate);

    virtual void apply_scale();
    virtual void apply_translate();
    virtual void apply_rotate();

    virtual void apply();



    //static versions
    static void static_scale(const glm::vec3& pivot_point,  ressource::Mesh<render::vertex3D>& data_scale,float scale_factor);
    static void static_scale_axis(const glm::vec3& pivot_point,const glm::vec3& pivot_axis, ressource::Mesh<render::vertex3D>& data_scale,float scale_factor);
    static void static_translate(const glm::vec3& translate_vector,  ressource::Mesh<render::vertex3D>& data_translate);
private:
    float m_scale_factor;
    float m_rotate_factor;
    float m_translate_factor;

};

}
}

#endif // MESHMODIFIER_H
