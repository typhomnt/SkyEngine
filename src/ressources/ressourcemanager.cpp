#include "ressourcemanager.h"

#include <iostream>

#include "shader.h"
#include "image.h"
#include "texture.h"
#include "utils/logger.h"


namespace skyengine
{

namespace ressource
{

void RessourceManager::Add(const std::string& name, const std::shared_ptr<Ressource>& ressource)
{
    RessourceManager* manager = GetSingleton();
    //TODO is it the good behaviour
    if(!utility::MapInsert<std::string, std::shared_ptr<Ressource>>(
                manager->m_ressources,
                name,
                ressource,
                "Resource " + name + " already exists"
                ))
    {
        utility::MapInsert<std::string, std::shared_ptr<Ressource>>(
                        manager->m_ressources,
                        getDuplicateName(name),
                        ressource,
                        "Resource " + name + " already exists"
                        );
    }
}

bool RessourceManager::Exists(const std::string &name)
{
    RessourceManager* manager = GetSingleton();
    return utility::MapContains<std::string, std::shared_ptr<Ressource>>(
                                                                            manager->m_ressources,
                                                                            name
                                                                            );
}

std::shared_ptr<Ressource> RessourceManager::Get(const std::string &name)
{
    RessourceManager* manager = GetSingleton();

    std::shared_ptr<Ressource> out = nullptr;
    utility::MapFind<std::string, std::shared_ptr<Ressource>>(
                manager->m_ressources,
                name,
                out,
                "RessourceManager::Get : ressource " + name + " not found"
                );

    return out;
}

void RessourceManager::Remove(const std::string &name)
{
    RessourceManager* manager = GetSingleton();
    utility::MapRemove<std::string, std::shared_ptr<Ressource>>(
                manager->m_ressources,
                name,
                "Resource " + name + " does not exist"
                );
}

void RessourceManager::Load(const rapidjson::Value& resources)
{
    std::vector<Ressource*> secondPass;

    for (auto& resIt : resources.GetObject())
    {
        if (std::string(resIt.name.GetString()) != "Tag")
        {
            auto resource = Ressource::Load(resIt.value, resIt.name.GetString(), secondPass);
            if (resource)
                Add(resIt.name.GetString(), resource);
        }
    }

    // Finish load of resources needing a second pass
    for (Ressource* resource : secondPass)
        resource->FinishLoading();
}


RessourceManager::RessourceManager()
{

}

void RessourceManager::Serialize(rapidjson::Value& curr_obj, rapidjson::MemoryPoolAllocator<> &al)
{
    std::shared_ptr<rapidjson::Value> obj = Serializable::SerializeProp(al);
    for(auto& res : GetSingleton()->m_ressources)
        res.second->Serialize(*(obj.get()),al);

    curr_obj.AddMember(
                rapidjson::Value("Resources", al),
                *(obj.get()),
                al
                );

}

} // namespace ressource

} // namespace skyengine
