#include "commons/sampleskyengine.h"

#include "bmesh/bmesh_gui.h"
#include "core/functioncalltask.h"
#include "geometry/geometryalgorithms.h"
#include "gui/guimanager.h"
#include "render/drawtask.h"
#include "ressources/image.h"
#include "ressources/lights.h"
#include "ressources/pointcloud.h"
#include "ressources/shader.h"
#include "ressources/texture.h"
#include "scene/camera.h"
#include "scene/cameramanipulator.h"
#include "scene/cubeobject.h"
#include "scene/scenegraphmanager.h"
#include "bmesh.h"
using namespace skyengine;


class CVHSample : public samples::SampleSkyEngine
{
public:
    CVHSample();
    ~CVHSample();

    void initScene() override;
    void initUserRessources() override;
    void initTasks() override;

private:
    // Shaders
    std::shared_ptr<ressource::Shader> m_pcShader;
    std::shared_ptr<ressource::Shader> m_meshShader;

    // Lights
    std::shared_ptr<ressource::DirLight> m_dLight;
    std::shared_ptr<ressource::PointLight> m_pLight;

    // Camera
    std::shared_ptr<scene::Camera> m_camera;

    // Models
    std::shared_ptr<scene::GameObject3D> m_cube;
    std::shared_ptr<scene::GameObject3D> m_pointCloud;
    std::shared_ptr<scene::GameObject3D> m_sphere;
    std::shared_ptr<scene::GameObject3D> m_bnode;
    std::shared_ptr<module::bmeshes::BMesh> m_bmesh;
    // Draw task
    std::shared_ptr<render::DrawTask> m_drawTask;

    // GUI
    std::shared_ptr<module::gui::GuiManager> m_guiManager;
    std::shared_ptr<module::gui::CVHGui> m_cvhGui;
};

CVHSample::CVHSample() : samples::SampleSkyEngine()
{}

CVHSample::~CVHSample()
{}

void CVHSample::initScene()
{
    auto sg = std::make_shared<scene::SceneGraph>();
    scene::SceneGraphManager::Add("default_scene", sg);

    sg->addNode(m_cube);
    sg->addNode(m_pointCloud);
    sg->addNode(m_sphere);
    sg->addNode(m_bnode);

    m_drawTask = std::make_shared<render::DrawTask>(
        this,
        /*main_window->getSize(),*/
        main_window->eventManager()
    );
    m_drawTask->addShader("pointcloud_shader");
    m_drawTask->addShader("default_shader");
    m_drawTask->addUniformValue(
        "forward",
        "default_shader",
        new scene::NodeParameter<glm::vec3*>("camPosition", 1, m_camera->getPositionPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "default_shader",
        new scene::NodeParameter<glm::mat4*>("V", 1, m_camera->getViewMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "default_shader",
        new scene::NodeParameter<glm::mat4*>("P", 1, m_camera->getProjectionMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "pointcloud_shader",
        new scene::NodeParameter<glm::mat4*>("V", 1, m_camera->getViewMatrixPt())
    );
    m_drawTask->addUniformValue(
        "forward",
        "pointcloud_shader",
        new scene::NodeParameter<glm::mat4*>("P", 1, m_camera->getProjectionMatrixPt())
    );
    m_drawTask->assignLightToShader("forward", "default_shader", m_dLight.get());
    m_drawTask->assignLightToShader("forward", "default_shader", m_pLight.get());
    m_drawTask->setSceneGraph(sg.get());

    m_guiManager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());
    m_cvhGui = std::make_shared<module::gui::CVHGui>(
        this,
        main_window.get()->getGLFWindow(),
        sg.get(),
        m_guiManager.get()
    );
}

void CVHSample::initUserRessources()
{
    m_pcShader = ressource::Ressource::Create<ressource::Shader>(
        "pointcloud_shader",
        "../shaders/pointcloud.vert",
        "../shaders/pointcloud.frag",
        "../shaders/pointcloud.geom"
    );
    m_meshShader = ressource::Ressource::Create<ressource::Shader>(
        "default_shader",
        "../shaders/default.vert",
        "../shaders/default.frag"
    );

    for (auto& res : ressource::RessourceManager::GetSingleton()->getRessources())
    {
        auto shader = std::dynamic_pointer_cast<ressource::Shader>(res.second);
        if (shader)
            shader->compileAndLink();
    }

    m_dLight = ressource::Ressource::Create<ressource::DirLight>("dir_light", 0);
    m_pLight = ressource::Ressource::Create<ressource::PointLight>("point_light", 0);

    m_camera = std::make_shared<scene::Camera>("camera");
    m_camera->updateCameraWithCurrentAttributes();

    m_cube = std::make_shared<scene::GameObject3D>("Cube");
    m_cube->setRenderShader(m_meshShader.get());
    m_cube->setMesh(geometry::CubePrimitive(1.0f));

    m_bnode = std::make_shared<scene::GameObject3D>("bnode");
    m_bmesh = std::make_shared<module::bmeshes::BMesh>("bmesh");
    m_bmesh->AddSphere(3,glm::vec3(0,1,0));
    m_bmesh->AddSphere(1,glm::vec3(5,1,0));
    m_bmesh->AddConnection(0,1);
    m_bmesh->setRootIndex(0);
    m_bmesh->computeInitMesh(8);
    m_bnode->setRenderShader(m_meshShader.get());
    m_bnode->setMesh(m_bmesh->getMesh());

    auto pc = std::make_shared<ressource::PointCloud>("pc_mesh");
    glm::vec4 color(1.0f, 0.0f, 0.0f, 1.0f);
    for (unsigned int i = 0; i < 10; ++i)
    {
        float x = (float)rand() / RAND_MAX;
        float y = (float)rand() / RAND_MAX;
        float z = (float)rand() / RAND_MAX;
        pc->addVertex(ressource::PCVertex(glm::vec3(x, y, z), 5.0f, color));
    }

    m_pointCloud = std::make_shared<scene::GameObject3D>("PointCloud");
    m_pointCloud->setRenderShader(m_pcShader.get());
    m_pointCloud->setMesh(pc);

    m_sphere = std::make_shared<scene::GameObject3D>("Sphere");
    m_sphere->setRenderShader(m_meshShader.get());
    m_sphere->setMesh(geometry::SpherePrimitive(10, 10));

}

void CVHSample::initTasks()
{

    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
    addTask(m_drawTask);

    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame, m_guiManager));
    addTask(m_cvhGui);
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender, m_guiManager));

    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}


int main()
{
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);

    CVHSample engineInstance;
    engineInstance.init();
    engineInstance.run();

    return 0;
}
