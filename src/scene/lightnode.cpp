#include "lightnode.h"

#include <cassert>


namespace skyengine
{

namespace scene
{

LightNode::LightNode(const std::string& name,
                     SceneGraph* owner,
                     unsigned int type,
                     const glm::mat4& worldMatrix,
                     const std::shared_ptr<ressource::Light>& light)
    : GameObject3D(name, owner, type, worldMatrix)
    , m_light(light)
{}

std::shared_ptr<rapidjson::Value> LightNode::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = SceneGraphNode::SerializeProp(al);
    utility::JSONHelper::SaveString(*value, "Light", m_light->getName(), al);
    return value;
}

void LightNode::LoadProp(const rapidjson::Value& value)
{
    GameObject3D::LoadProp(value);

    auto lightRes = ressource::RessourceManager::Get(value["Light"].GetString());
    m_light = std::dynamic_pointer_cast<ressource::Light>(lightRes);
    assert(m_light);
}

} // namespace scene

} // namespace skyengine
