#include "modelimportermanager.h"

#include <cassert>
#include <iostream>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/quaternion.hpp>

#include "animation/vertex3dskin.h"
#include "animation/bone3d.h"
#include "animation/skeletalanimation.h"
#include "ressources/ressourcemanager.h"
#include "ressources/texture.h"
#include "utils/utilities.h"
#include "utils/logger.h"
#include "ressources/phongmaterial.h"
#include "animation/skeletalanimator.h"
#include "animation/armature.h"

#include <thread>

namespace skyengine
{

namespace core
{


ModelImporterManager::ModelImporterManager()
{

}

ModelImporterManager::ModelImporterManager(scene::SceneGraph *scene_graph):
    m_scene_graph(scene_graph)
{

}

ModelImporterManager::~ModelImporterManager()
{

}


void ModelImporterManager::setSceneGraph(scene::SceneGraph* scene_graph)
{
    m_scene_graph = scene_graph;
}

void ModelImporterManager::loadModel(const std::string& model_path, scene::SceneGraph* scene_graph, scene::SceneGraphNode* parent_node)
{
    if(scene_graph != nullptr)
        setSceneGraph(scene_graph);
    ai_scene = const_cast<aiScene*>(importer.ReadFile(model_path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs));
    if(ai_scene == nullptr || ai_scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || ai_scene->mRootNode == nullptr)
    {
        std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
        return;
    }
    processNode(ai_scene->mRootNode, ai_scene, model_path, parent_node);
    //TODO skeleton merging cleanning step
    skeletonCleanProcess();
    processAnimations(ai_scene);


    if (ai_scene->HasTextures())
        processTextures(ai_scene);
}

void ModelImporterManager::processNode(aiNode* node, aiScene* scene, const std::string& model_path, scene::SceneGraphNode *parent_node)
{
    // Process all the node's meshes (if any)

    //We assume that all processed node are game object
    // Materials and textures are treated separately
    std::shared_ptr<scene::GameObject3D> new_object;
    //We process each mesh of the node;
    for(GLuint i = 0; i < node->mNumMeshes; i++)
    {
        //Process of mesh i and extracting his name
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        std::string object_name = std::string(mesh->mName.C_Str()) + "-" + utility::extractFileNameFromPath(model_path);
        utility::Logger::Info("Inserting new object" + object_name);
        //We determined what kind of object we are importing
        //If the node posses an armature it is considered as a skinned object
        LoadMode mesh_load_mode = NORMAL_3D;
        if(mesh->mNumBones > 0)
        {
            utility::Logger::Info("will load armature of " + object_name);
            mesh_load_mode = SKINNED_3D;
        }


        //Object allocation with respect to its type
        if(mesh_load_mode == SKINNED_3D)
        {
            new_object = std::make_shared<animation::GameObjectSkinned>(object_name);
            new_object->setMesh(std::make_shared<ressource::Mesh<animation::Vertex3DSkin>>(object_name + "mesh"));

        }
        else
        {
            new_object = std::make_shared<scene::GameObject3D>(object_name);
            new_object->setMesh(std::make_shared<ressource::Mesh<render::vertex3D>>(object_name + "mesh"));
        }

        //Object insertion into the scene graph
        if(parent_node == nullptr)
            m_scene_graph->addNode(new_object);
        else
            m_scene_graph->addNode(parent_node->getName(),new_object);

        processMesh(mesh, scene,object_name,node,mesh_load_mode,new_object);
    }

    // Then do the same for each of the node's children
    for(GLuint i = 0; i < node->mNumChildren; i++)
    {
        this->processNode(node->mChildren[i], scene, model_path,new_object.get());
    }
}

void ModelImporterManager::BuildSkeleton(const aiScene* scene,const std::vector<animation::Bone3D> & bones, std::shared_ptr<animation::Skeleton> skel_to_build)
{
    //Get m_InverglobalTransform
    //We store the recovered bones into their skeleton
    for(unsigned int i = 0 ; i < bones.size() ; i++)
        skel_to_build->addBone(bones[i]);
    //skel_to_build.setBones(bones);
    //std::vector<Bone3D> & skel_bones = skel_to_build.getRefBones();

    //Unfortunatelly the bone list we recover is deprived of its hierarchy
    //It is stored in the ai_scene as node
    //Thus We search go through the scene hierarchy and recover the skeleton hierarchy
    fillBoneHierarchy(ai_scene->mRootNode, skel_to_build->getBones(), skel_to_build.get());

    //Only one bone as root check.
    //TODO It can be changed in order to recover controllers but we don't want them for now
    bool has_more_than_one_root = false;
    for(auto  bone : skel_to_build->getBones())
    {
        if(bone.second->parent == nullptr)
        {
            assert(has_more_than_one_root == false);
            skel_to_build->setRootBone(bone.second.get());
            has_more_than_one_root = true;
        }
    }

    //Set skeleton properties
    //skel_to_build->setGloabalInverseTransform(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation)));
    std::string root_mvt = skel_to_build->getDefaultSpineRootBone();
    std::string core_mvt = skel_to_build->getDefaultSpineCoreBone();
    std::string head_mvt =skel_to_build->getDefaultSpineHeadBone();
    if(root_mvt != "")
        skel_to_build->setRootMvtBone(root_mvt);
    if(core_mvt != "")
        skel_to_build->setCoreMvtBone(core_mvt);
    if(head_mvt != "")
        skel_to_build->setHeadMvtBone(head_mvt);
    skel_to_build->computeSpineArticulations();
    skel_to_build->createIKTgEff();


}

void ModelImporterManager::fillBoneHierarchy(aiNode* node, std::unordered_map<std::string,std::shared_ptr<animation::Bone3D>>& bone_mapping,animation::Skeleton* skel_to_build)
{

    //We check if this node is a bone of our skeleton
    if(bone_mapping.find(std::string(node->mName.C_Str())) != bone_mapping.end())
    {

        //We check if the bone we insert is a child of another one
        if(bone_mapping.find(std::string(node->mParent->mName.C_Str())) != bone_mapping.end())
        {
            bone_mapping[std::string(node->mName.C_Str())]->parent = bone_mapping[std::string(node->mParent->mName.C_Str())].get();
            bone_mapping[std::string(node->mParent->mName.C_Str())]->children.push_back(bone_mapping[std::string(node->mName.C_Str())].get());

        }
        else /*we add either a modifier chain or the root bone*/
        {
            utility::Logger::Debug("will add modifier of " + std::string(node->mName.C_Str()));
            aiNode* tmp_node_parent = node->mParent;
            aiNode* tmp_node_curr = node;

            //We recover all the modifier chain in order to be able to compute the right transform for animatinos
            while(tmp_node_parent != nullptr && bone_mapping.find(std::string(tmp_node_parent->mName.C_Str())) == bone_mapping.end())
            {
                //We create a modifier bone
                animation::Bone3D new_modifier;
                new_modifier.bone_name = std::string(tmp_node_parent->mName.C_Str());
                new_modifier.bone_offset = convertAiMat4ToGLMMat4(tmp_node_parent->mTransformation);
                new_modifier.is_modifier = true;
                skel_to_build->addBone(new_modifier);
                animation::Bone3D* new_bone = skel_to_build->getBonePt(new_modifier.bone_name);
                //bone_mapping.insert(std::pair<std::string,animation::Bone3D*>(new_bone->bone_name, new_bone));
                bone_mapping[std::string(tmp_node_curr->mName.C_Str())]->parent = new_bone;
                bone_mapping[std::string(tmp_node_parent->mName.C_Str())]->children.push_back(bone_mapping[std::string(tmp_node_curr->mName.C_Str())].get());

                //We iterate over the ai scene node
                tmp_node_curr = tmp_node_parent;
                tmp_node_parent = tmp_node_parent->mParent;


            }
            //We add the final child-parent link for this chain
            if(tmp_node_parent != nullptr)
            {
                bone_mapping[std::string(tmp_node_curr->mName.C_Str())]->parent = bone_mapping[std::string(tmp_node_parent->mName.C_Str())].get();
                bone_mapping[std::string(tmp_node_parent->mName.C_Str())]->children.push_back(bone_mapping[std::string(tmp_node_curr->mName.C_Str())].get());
            }
        }
    }

    for(unsigned int i = 0; i < node->mNumChildren; i++)
    {
        fillBoneHierarchy(node->mChildren[i], bone_mapping,skel_to_build);
    }
}

animation::GameObjectSkinned* ModelImporterManager::FindBoneInSkeletons(const std::string& bone_name)
{
    for(const auto& node : m_scene_graph->getNodes())
        if(node.second->getType() == scene::SceneGraphNode::GameObjectSkinned)
        {
            if(dynamic_cast<animation::GameObjectSkinned*>(node.second.get())->getConstSkeleton().containsBone(bone_name))
                return dynamic_cast<animation::GameObjectSkinned*>(node.second.get());
        }

    return nullptr;
}

bool ModelImporterManager::FindBoneInSkeleton(const std::string bone_name, const animation::GameObjectSkinned& object)
{
    return object.getConstSkeleton().containsBone(bone_name);
}

void ModelImporterManager::skeletonCleanProcess()
{
    //Check if skeleton are equal and merge them
}

void ModelImporterManager::processAnimations(aiScene* scene)
{

    std::vector<std::thread> anim_threads;
    for(GLuint i = 0 ; i < scene->mNumAnimations ; i ++)
    {
        anim_threads.push_back(std::thread(&ModelImporterManager::processAnimation,this,(scene->mAnimations[i])));
    }
    for(unsigned int i = 0; i < anim_threads.size() ; ++i)
        anim_threads[i].join();
}

void ModelImporterManager::processAnimation(aiAnimation* animation)
{
    //Animation initialisation
    std::shared_ptr<animation::SkeletalAnimation> skel_anim = std::make_shared<animation::SkeletalAnimation>(std::string(animation->mName.C_Str()));
    //By default all animation are set to be 25fps
    float ticks_per_second = (float)animation->mTicksPerSecond != 0.0f ?
                (float)animation->mTicksPerSecond : 24.0f;
    skel_anim->setTickPerSecond(ticks_per_second);
    float animation_duration = (float)animation->mDuration/ticks_per_second;
    skel_anim->setDuration(animation_duration);


    //TODO search directly inside skeletons
    animation::GameObjectSkinned* animated_object = nullptr;
    for(unsigned int i = 0 ; i < animation->mNumChannels ; i++)
    {
        animation::GameObjectSkinned* tmp_animated_obj = FindBoneInSkeletons(std::string(animation->mChannels[i]->mNodeName.C_Str()));
        if(tmp_animated_obj != nullptr)
        {
            animated_object = tmp_animated_obj;
            break;
        }
    }

    if(animated_object != nullptr)
    {
        std::cout << "inserting animation: " <<  std::string(animation->mName.C_Str()) << std::endl;
        //bool value is meaningless. we only use a map for automatic sorting
        std::map<float,bool> keyFrame_time;
        for(unsigned int i = 0 ; i < animation->mNumChannels ; i++)
        {
            if(FindBoneInSkeleton(std::string(animation->mChannels[i]->mNodeName.C_Str()), *animated_object))
            {
                aiNodeAnim* anim = animation->mChannels[i];



                for(unsigned int j = 0 ; j < anim->mNumPositionKeys ; j++)
                {

                    skel_anim->addKey(std::string(anim->mNodeName.C_Str()) + "_position",std::make_shared<animation::TimedParameter<glm::vec3>>(
                                          convertAiVector3DToGLMVec3(anim->mPositionKeys[j].mValue),
                                          (float)anim->mPositionKeys[j].mTime/ticks_per_second));                    
                    if(keyFrame_time.find((float)anim->mPositionKeys[j].mTime/ticks_per_second) == keyFrame_time.end())
                        keyFrame_time.insert(std::pair<float,bool>((float)anim->mPositionKeys[j].mTime/ticks_per_second,true));
                }

                for(unsigned int j = 0 ; j < anim->mNumRotationKeys ; j++)
                {

                    skel_anim->addKey(std::string(anim->mNodeName.C_Str()) + "_rotation",std::make_shared<animation::TimedParameter<glm::quat>>(
                                          convertAiQuaternionToGLMQuat(anim->mRotationKeys[j].mValue),
                                          (float)anim->mRotationKeys[j].mTime/ticks_per_second));
                    if(keyFrame_time.find((float)anim->mRotationKeys[j].mTime/ticks_per_second) == keyFrame_time.end())
                        keyFrame_time.insert(std::pair<float,bool>((float)anim->mRotationKeys[j].mTime/ticks_per_second,true));
                }

                for(unsigned int j = 0 ; j < anim->mNumScalingKeys ; j++)
                {


                    skel_anim->addKey(std::string(anim->mNodeName.C_Str()) + "_scale",std::make_shared<animation::TimedParameter<glm::vec3>>(
                                          convertAiVector3DToGLMVec3(anim->mScalingKeys[j].mValue),
                                          (float) anim->mScalingKeys[j].mTime/ticks_per_second));
                    if(keyFrame_time.find((float)anim->mScalingKeys[j].mTime/ticks_per_second) == keyFrame_time.end())
                        keyFrame_time.insert(std::pair<float,bool>((float)anim->mScalingKeys[j].mTime/ticks_per_second,true));
                }

                if(anim->mNumPositionKeys > 0)
                {
                    skel_anim->addKeyToGroup(std::string(anim->mNodeName.C_Str()),std::string(anim->mNodeName.C_Str()) + "_position");
                    //skel_anim->setInterpolationFunction(std::string(anim->mNodeName.C_Str()) + "_position",utility::InterpolationFunction<glm::vec3>::getLinearInterpolationFunc());
                }
                if(anim->mNumRotationKeys > 0)
                {
                    skel_anim->addKeyToGroup(std::string(anim->mNodeName.C_Str()),std::string(anim->mNodeName.C_Str()) + "_rotation");
                    //skel_anim->setInterpolationFunction(std::string(anim->mNodeName.C_Str()) + "_rotation",utility::InterpolationFunction<glm::quat>::getLinearInterpolationFunc());
                }
                if(anim->mNumScalingKeys > 0)
                {
                    skel_anim->addKeyToGroup(std::string(anim->mNodeName.C_Str()),std::string(anim->mNodeName.C_Str()) + "_scale");
                    //skel_anim->setInterpolationFunction(std::string(anim->mNodeName.C_Str()) + "_scale",utility::InterpolationFunction<glm::vec3>::getLinearInterpolationFunc());
                }

            }
        }


        ressource::RessourceManager::Add(skel_anim->getName(),skel_anim);


        for(auto const& keyframe_time : keyFrame_time)
        {
            std::shared_ptr<animation::SkeletalKeyFrame> new_frame = std::make_shared<animation::SkeletalKeyFrame>(nullptr,keyframe_time.first);
            skel_anim->addKeyFrame(new_frame);

            //foreach key at time keyframe_time add a key pointer to the corresponding keyframe
            for(auto& animated_bone: skel_anim->getKeys())
            {
                animation::BaseTimeParameter* key = skel_anim->getKey(animated_bone.first,keyframe_time.first).get();
                if(key != nullptr)
                    new_frame->addKeyParameter(animated_bone.first,key);
            }
        }
        skel_anim->setAnimationSkeleton(animated_object->getPSkeleton());
        skel_anim->analyzeAnimation();
        animated_object->addAnimation(skel_anim.get());
    }
    else
    {
        utility::Logger::Info("Animation " + std::string(animation->mName.C_Str())  + " has no attached object");
    }


}

void ModelImporterManager::processMesh(aiMesh* mesh, const aiScene* scene, const std::string& mesh_name, aiNode *node, const LoadMode &loadMode, std::shared_ptr<scene::GameObject3D> object)
{
    //vector<GLuint> indices;
    //Here we process the vertices
    std::vector<std::shared_ptr<render::vertex3D>> vertices;
    //object->getMesh()->resizeVertices(mesh->mNumVertices);
    vertices.resize(mesh->mNumVertices);
    //objetc reserve verticies
    for(GLuint i = 0; i < mesh->mNumVertices; i++)
    {
        if(loadMode == SKINNED_3D)
            vertices[i] = std::make_shared<animation::Vertex3DSkin>();
        else
            vertices[i] = std::make_shared<render::vertex3D>();
        //Here we process the position
        vertices[i]->pos.x = mesh->mVertices[i].x;
        vertices[i]->pos.y = mesh->mVertices[i].y;
        vertices[i]->pos.z = mesh->mVertices[i].z;
        //Here we process the normal
        if(mesh->mNormals != nullptr)
        {
            vertices[i]->normal.x = mesh->mNormals[i].x;
            vertices[i]->normal.y = mesh->mNormals[i].y;
            vertices[i]->normal.z = mesh->mNormals[i].z;
        }
        else
        {
            vertices[i]->normal.x = 0;
            vertices[i]->normal.y = 0;
            vertices[i]->normal.z = 0;
        }
        //Here we process the texture
        //TODO multitexture import
        if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
        {
            vertices[i]->uvs.x = mesh->mTextureCoords[0][i].x;
            vertices[i]->uvs.y = mesh->mTextureCoords[0][i].y;
        }
        else
            vertices[i]->uvs = glm::vec2(0.0f, 0.0f);

    }
    //objetc reserve indicies
    // Process indices
    for(GLuint i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        for(GLuint j = 0; j < face.mNumIndices; j++)
        {
            if(loadMode == SKINNED_3D)
                std::static_pointer_cast<ressource::Mesh<animation::Vertex3DSkin>>(object->getMesh())->addIndex(face.mIndices[j]);
            else
                std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D>>(object->getMesh())->addIndex(face.mIndices[j]);
        }
    }

    // Process material
    bool has_texture = false;
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

    // Get material components
    aiString matName;
    aiColor3D emissive, ambient, diffuse, specular;
    float shininess;
    material->Get(AI_MATKEY_NAME, matName);
    material->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);
    material->Get(AI_MATKEY_COLOR_AMBIENT, ambient);
    material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
    material->Get(AI_MATKEY_COLOR_SPECULAR, specular);
    material->Get(AI_MATKEY_SHININESS, shininess);

    auto phongMat = ressource::Ressource::Create<ressource::PhongMaterial>(
        std::string(matName.C_Str()),
        glm::vec4(emissive.r, emissive.g, emissive.b, 1.0f),
        glm::vec4(ambient.r, ambient.g, ambient.b, 1.0f),
        glm::vec4(diffuse.r, diffuse.g, diffuse.b, 1.0f),
        glm::vec4(specular.r, specular.g, specular.b, 1.0f),
        std::max(shininess,1.0f)
    );
    object->setMaterial(phongMat);

    loadMaterialTextures(material, aiTextureType_DIFFUSE,has_texture,object);
    loadMaterialTextures(material, aiTextureType_SPECULAR,has_texture,object);

    // For skeleted animated objects
    //TODO Do better
    std::vector<animation::Bone3D> bones;
    if(loadMode == SKINNED_3D)
    {
        //Store bone name to id and object Bones
        std::map<std::string,unsigned int> bone_mapping;

        unsigned int num_bones = 0;

        //We process each bone and assign to each vertex its influence bones
        for (unsigned int  i = 0 ; i < mesh->mNumBones ; i++)
        {
            //For each bone, its id is assgined to i (storing order)
            unsigned bone_index = 0;
            std::string BoneName(mesh->mBones[i]->mName.data);
            //Add bone to the list of bones
            if (bone_mapping.find(BoneName) == bone_mapping.end())
            {
                bone_index = num_bones;
                num_bones++;
                animation::Bone3D bi;
                bones.push_back(bi);
            }
            else
            {
                //TODO not sure that it is useful but depend on import format
                bone_index = bone_mapping[BoneName];
            }


            bone_mapping[BoneName] = bone_index;
            bones[bone_index].bone_offset = convertAiMat4ToGLMMat4(mesh->mBones[i]->mOffsetMatrix);
            bones[bone_index].bone_name = BoneName;
            bones[bone_index].bone_ID = bone_index;
            for (unsigned int j = 0 ; j < mesh->mBones[i]->mNumWeights ; j++)
            {
                unsigned int VertexID = mesh->mBones[i]->mWeights[j].mVertexId;
                float Weight = mesh->mBones[i]->mWeights[j].mWeight;
                std::static_pointer_cast<animation::Vertex3DSkin>(vertices[VertexID])->SetBoneData(bone_index, Weight);
                std::static_pointer_cast<animation::Vertex3DSkin>(vertices[VertexID])->vertex_ID = VertexID;
            }
        }

    }

    //Adding all vertices
    for(GLuint i = 0; i < mesh->mNumVertices; i++)
    {
        if(loadMode == SKINNED_3D)
            std::dynamic_pointer_cast<ressource::Mesh<animation::Vertex3DSkin>>(object->getMesh())->addVertex(*(std::static_pointer_cast<animation::Vertex3DSkin>(vertices[i]).get()));
        else
            std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D>>(object->getMesh())->addVertex(*(vertices[i].get()));
    }
    //Now that we got all the vertices used by the mesh we have to build bone hierarchy
    if(loadMode == SKINNED_3D)
    {
        std::shared_ptr<animation::Skeleton> skel;

        if(ressource::RessourceManager::Exists(mesh_name + "_skeleton"))
            skel = std::dynamic_pointer_cast<animation::Skeleton>(ressource::RessourceManager::Get(mesh_name + "_skeleton"));
        else
        {
            skel = ressource::Ressource::Create<animation::Skeleton>(mesh_name + "_skeleton");            
            BuildSkeleton(scene,bones,skel);
            skel->setGloabalInverseTransform(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation))*convertAiMat4ToGLMMat4(node->mParent->mTransformation)
                                             *glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation))*convertAiMat4ToGLMMat4(node->mTransformation));
        }
        skel->updateHierarchicalTransforms();
        std::shared_ptr<animation::Armature> arm = std::make_shared<animation::Armature>(skel->getName(),skel.get());
        arm->setLocalMatrix(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation))*convertAiMat4ToGLMMat4(node->mTransformation));
        if(node->mParent != nullptr)
            arm->setParentMatrix(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation))*convertAiMat4ToGLMMat4(node->mParent->mTransformation));
        m_scene_graph->addNode(arm);
        std::static_pointer_cast<animation::GameObjectSkinned>(object)->setSkeleton(skel.get());
    }


    utility::Logger::Info("Loaded: "  + mesh_name);

    object->setLocalMatrix(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation))*convertAiMat4ToGLMMat4(node->mTransformation));

    if(loadMode == SKINNED_3D)
    {
        std::static_pointer_cast<animation::GameObjectSkinned>(object)->getPSkeleton()->setGloabalInverseTransform(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation)*convertAiMat4ToGLMMat4(node->mTransformation)));
    }

    if(node->mParent != nullptr)
        object->setParentMatrix(glm::inverse(convertAiMat4ToGLMMat4(scene->mRootNode->mTransformation))*convertAiMat4ToGLMMat4(node->mParent->mTransformation));
    switch (loadMode)
    {
    case NORMAL_3D:
            object->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::GetSingleton()->Get("default_shader")).get());
        break;
    case SKINNED_3D:
        object->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::GetSingleton()->Get("default_skin_shader")).get());
        break;
    default:
        break;
    }

}

void ModelImporterManager::processTextures(const aiScene *scene)
{
    std::shared_ptr<ressource::Image> image = nullptr;
    for (unsigned int texId = 0; texId < scene->mNumTextures; ++texId)
    {
        std::string strTexId = std::to_string(texId);

        aiTexture* texture = scene->mTextures[texId];
        if (texture->mHeight == 0)
        {
            // Compressed texture.
            // Check format and create image from bytes
            image = ressource::Ressource::Create<ressource::Image>(
                        std::string("image") + strTexId,
                        texture->achFormatHint,
                        texture->mWidth,
                        texture->pcData);
        }
        else
        {
            // Uncompressed texture
            // The data format is ARGB. We must convert it to RGBA format.
            // TODO : is there a better solution ?
            unsigned int width = texture->mWidth;
            unsigned int height = texture->mHeight;

            aiTexel* argbData = texture->pcData;
            unsigned char* rgbaData = new unsigned char[4 * width * height];
            assert(rgbaData);

            for (size_t i = 0; i < width * height; ++i)
            {
                rgbaData[4 * i] = argbData[i].r;
                rgbaData[4 * i + 1] = argbData[i].g;
                rgbaData[4 * i + 2] = argbData[i].b;
                rgbaData[4 * i + 3] = argbData[i].a;
            }

            image = ressource::Ressource::Create<ressource::Image>(
                        std::string("image") + strTexId,
                        width, height,
                        4,
                        rgbaData);

            delete[] rgbaData;
        }

        ressource::Ressource::Create<ressource::Texture>(
                    std::string("texture") + strTexId,
                    image,
                    GL_TEXTURE_2D
                    );
    }
}

void ModelImporterManager::loadMaterialTextures(aiMaterial* mat, aiTextureType type, bool& has_texture,std::shared_ptr<scene::GameObject3D> object)
{
    for (unsigned int i = 0; i < mat->GetTextureCount(type); ++i)
    {
        aiString path;
        mat->GetTexture(type, i, &path);

        std::string name = utility::extractFileNameFromPath(path.data);

        if(name == std::string())
        {
            utility::Logger::Warning("Invalid texture path: " + std::string(path.data));
            continue;
        }

        // Check if texture already exists or not
        if (!ressource::RessourceManager::Exists(name)){
            ressource::Ressource::Create<ressource::Texture>(name, path.data);
        }
        //std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::GetSingleton()->Get("phong_shader"))->setTextureToSampler(name,"myTextureSampler" /* + std::to_string(i)*/);
        std::dynamic_pointer_cast<ressource::PhongMaterial>(object->getMaterial())->addTexture(
                    std::dynamic_pointer_cast<ressource::Texture>(ressource::RessourceManager::Get(name)).get(),
                    i);/*TODO i*/
        has_texture = true;
    }
}

glm::mat4 ModelImporterManager::convertAiMat4ToGLMMat4(const aiMatrix4x4& mat4)
{

    return glm::transpose(glm::mat4(mat4.a1,mat4.a2,mat4.a3,mat4.a4,
                                    mat4.b1,mat4.b2,mat4.b3,mat4.b4,
                                    mat4.c1,mat4.c2,mat4.c3,mat4.c4,
                                    mat4.d1,mat4.d2,mat4.d3,mat4.d4));
}

glm::vec3 ModelImporterManager::convertAiVector3DToGLMVec3(const aiVector3D& vec3)
{

    return glm::vec3(vec3.x,vec3.y,vec3.z);
}


glm::quat ModelImporterManager::convertAiQuaternionToGLMQuat(const aiQuaternion& quat)
{

    return glm::quat(quat.w,quat.x,quat.y,quat.z);
}


}

}
