#include "core/skyengine.h"
#include "core/functioncalltask.h"
#include "core/openglwindow.h"

#include "utils/logger.h"

#include "commons/quitlistener.h"

#include <thread>
#include <chrono>

using namespace skyengine;

class SampleWindow : public core::SkyEngine
{
public:
    SampleWindow()  : core::SkyEngine() {}
    ~SampleWindow(){}

    void init();
    void initTasks();

protected:

    std::shared_ptr<core::OpenGLWindow> main_window;
    std::shared_ptr<samples::QuitListener> quit_listener;

};


void SampleWindow::init()
{
    utility::Logger::Info("Init Sample Window");

    // create main window and opengl context
    core::OpenGLWindow::setGFLWHint(GLFW_SAMPLES,4);
    core::OpenGLWindow::setGFLWHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    core::OpenGLWindow::setGFLWHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    core::OpenGLWindow::setGFLWHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    core::OpenGLWindow::setGFLWHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    main_window = std::make_shared<core::OpenGLWindow>(1024,920,"Sky Engine");
    main_window->bind();

    // now that the window is created, initialize glew
    initOpenGL();

    quit_listener = std::make_shared<samples::QuitListener>(this, main_window->eventManager(), main_window->userInputManager());

    initTasks();
}

class SleepTask : public core::Task
{
public:
    SleepTask(long long ms) : time_(ms) {}
    void run() {std::this_thread::sleep_for(std::chrono::milliseconds(time_));}
private:
    long long time_;
};

void SampleWindow::initTasks()
{
    addTask(std::make_shared<SleepTask>(40));
    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}


int main()
{
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);

    SampleWindow engineInstance;
    engineInstance.init();
    engineInstance.run();

    return 0;
}



