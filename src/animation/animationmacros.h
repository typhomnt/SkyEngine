#ifndef ANIMATIONMACROS_H
#define ANIMATIONMACROS_H
namespace skyengine
{

namespace animation
{
#define MAX_BONES 100
#define NUM_BONES_PER_VEREX 4
#define ANIM_EPS1 0.0001

}
}
#endif // ANIMATIONMACROS_H
