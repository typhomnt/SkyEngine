#ifndef POSE3D_H
#define POSE3D_H
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
namespace skyengine
{

namespace animation
{
struct Pose3D
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 position;

    Pose3D(){}
    Pose3D(const glm::vec3& scale, const glm::quat& rotation, const glm::vec3& position) : scale(scale), rotation(rotation), position(position) {}

    float norm() const
    {
        return sqrt(glm::l1Norm(scale) + glm::l1Norm(glm::eulerAngles(rotation)) + glm::l1Norm(position));
    }

    float dist(const Pose3D& pose) const
    {
        return (*this - pose).norm();
    }

    static float dist(const Pose3D& pose_1, const Pose3D& pose_2)
    {
        return (pose_1 - pose_2).norm();
    }

    glm::vec3 translation() const
    {
        return position;
    }

    inline Pose3D operator+(const Pose3D &pose) const
    {
        return Pose3D(scale + pose.scale, rotation + pose.rotation, position + pose.position);
    }


    inline Pose3D operator-(const Pose3D &pose) const
    {
        return Pose3D(scale - pose.scale, glm::inverse(pose.rotation)*rotation/*glm::quat(glm::eulerAngles(rotation) - glm::eulerAngles(pose.rotation))*/, position - pose.position);
    }

   inline Pose3D operator*(const float& f) const
   {
       return Pose3D(f*scale,glm::normalize(f*rotation), f*position);
   }

   inline Pose3D operator/(const float& f) const
   {
       return Pose3D(scale/f,glm::normalize(rotation/f), position/f);
   }

   inline Pose3D& operator+=(const Pose3D &pose)
   {
       scale += pose.scale;
       rotation += pose.rotation;
       position += pose.position;
       return *this;
   }

   inline Pose3D& operator/=(const float& f)
   {
       scale = scale/f;
       rotation = glm::normalize(rotation/f);
       position = position/f;
       return *this;
   }

   inline bool operator==(const Pose3D &pose)
   {
       return scale == pose.scale && position == pose.position && rotation == pose.rotation;
   }
};

inline float dot(const Pose3D& pose_1, const Pose3D& pose_2)
{
    return glm::dot(pose_1.scale,pose_2.scale)
            + glm::dot(pose_1.position,pose_2.position)
            + glm::dot(pose_1.rotation,pose_2.rotation);
}

}
}
#endif // POSE3D_H
