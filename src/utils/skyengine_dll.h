#ifndef DLL_EXPORTS_H
#define DLL_EXPORTS_H

#ifdef _MSC_VER

// this warning indicate that a member of the class need to be dll to be accessed by dll user.
#pragma warning( disable : 4251 )
// truncaded name. May cause issues for debug
#pragma warning( disable : 4503 )

#ifdef SKYENGINEDLL_EXPORTS
#define SKYENGINE_API __declspec(dllexport)
#else
#define SKYENGINE_API __declspec(dllimport)
#endif

#else

#define SKYENGINE_API

#endif

#endif // DLL_EXPORTS_H
