#include "openglwindow.h"

#include "core/eventlisteners/userinputlistener.h"
#include "core/eventlisteners/windoweventlistener.h"
#include "utils/glcheck.h"
#include <iostream>

#include "utils/utilities.h"


namespace skyengine
{

namespace core
{

OpenGLWindow* OpenGLWindow::getOpenGLWindow(GLFWwindow* window)
{
    return (OpenGLWindow*)glfwGetWindowUserPointer(window);
}

OpenGLWindow* OpenGLWindow::getCurrent()
{
    return getOpenGLWindow(glfwGetCurrentContext());
}


void OpenGLWindow::setGFLWHint(int first_value, int second_value)
{
    glfwWindowHint(first_value, second_value);
}

void OpenGLWindow::setGFLWDefaultsHint()
{
    glfwDefaultWindowHints();
}

// cursor
OpenGLWindow::Cursor::Cursor (const GLFWimage* image, glm::ivec2 hotpos)
{
    cursor = glfwCreateCursor(image, hotpos.x, hotpos.y);
}

OpenGLWindow::Cursor::Cursor(int shape)
{
    cursor = glfwCreateStandardCursor(shape);
}

OpenGLWindow::Cursor::~Cursor()
{
    glfwDestroyCursor(cursor);
}


GLFWwindow* OpenGLWindow::getGLFWindow()
{
    return glf_window;
}



OpenGLWindow::OpenGLWindow(int width_, int height_, std::string glf_title_, GLFWmonitor* glf_wmonitor_, std::shared_ptr<OpenGLWindow> shared):
    ui_manager(std::make_shared<core::UserInputManager>()),
    we_manager(std::make_shared<core::WindowEventManager>()),
    title(glf_title_),
    is_focused(true)
{
    GLFWwindow* scw = shared? shared->getGLFWindow() : nullptr;

    utility::Logger::Info(utility::mkstring("Creating window \"", glf_title_, "\" of size ", width_, "x", height_));

    glf_window = glfwCreateWindow( width_, height_, glf_title_.c_str(), glf_wmonitor_, scw);

    if( glf_window == NULL ){
        utility::Logger::Fatal("Failed to open GLFW window.\n") ;
        glfwTerminate();
        exit(-1);
    }

    glfwSetWindowUserPointer(glf_window, (void*) this);

    initCallbacks();

    /**
     * This is necessary otherwise the windowResizeEvent->getSize returns (0, 0)
     * as long as we do not resize the window
     */
    we_manager->windowResizeEvent->send(glm::ivec2(width_, height_));
}


OpenGLWindow::~OpenGLWindow()
{
    // Close OpenGL window and terminate GLFW
    glfwDestroyWindow(glf_window);
}

void OpenGLWindow::bind()
{
    // before making the context current
    glfwMakeContextCurrent(glf_window);
}

int OpenGLWindow::closeFlag()
{
    return glfwWindowShouldClose(glf_window);
}

void OpenGLWindow::setCloseFlag(int flag)
{
    glfwSetWindowShouldClose(glf_window, flag);
}

void OpenGLWindow::setWindowSize(glm::ivec2 s)
{
    glfwSetWindowSize(glf_window, s.x, s.y);
}

glm::ivec2 OpenGLWindow::getWindowSize()
{
    glm::ivec2 r;
    glfwGetWindowSize(glf_window, &r.x, &r.y);
    return r;
}

glm::ivec4 OpenGLWindow::getWindowFrameSize()
{
    glm::ivec4 r;
    glfwGetWindowFrameSize(glf_window, &r.x, &r.y, &r.z, &r.w);
    return r;
}

void OpenGLWindow::setWindowSizeLimits(glm::ivec2 slmin, glm::ivec2 slmax)
{
    glfwSetWindowSizeLimits(glf_window, slmin.x, slmin.y, slmax.x, slmax.y);
}

void OpenGLWindow::setWindowAspectRatio(glm::ivec2 ratio)
{
    glfwSetWindowAspectRatio(glf_window, ratio.x, ratio.y);
}

void OpenGLWindow::setWindowPosition(glm::ivec2 p)
{
    glfwSetWindowPos(glf_window, p.x, p.y);
}

glm::ivec2 OpenGLWindow::getWindowPosition()
{
    glm::ivec2 r;
    glfwGetWindowPos(glf_window, &r.x, &r.y);
    return r;
}

void OpenGLWindow::setTitle(std::string title_)
{
    glfwSetWindowTitle(glf_window, title_.c_str());
    title = title_;
}

void OpenGLWindow::setIcon(std::vector<GLFWimage> images)
{
    if(images.empty())
        glfwSetWindowIcon(glf_window, 0, nullptr);
    else
        glfwSetWindowIcon(glf_window, (int)images.size(), images.data());
}

GLFWmonitor* OpenGLWindow::getMonitor()
{
    return glfwGetWindowMonitor(glf_window);
}

bool OpenGLWindow::isFullScreen()
{
    return !glfwGetWindowMonitor(glf_window);
}

void OpenGLWindow::setMonitor(GLFWmonitor* monitor, glm::ivec2 pos, glm::ivec2 size, int refreshRate)
{
    glfwSetWindowMonitor(glf_window, monitor, pos.x, pos.y, size.x, size.y, refreshRate);
}

void OpenGLWindow::setFullScreen(GLFWmonitor* monitor, glm::ivec2 size, int refreshRate)
{
    if(size==glm::ivec2(-1))
        size = getWindowSize();
    setMonitor(monitor, glm::ivec2(0), size, refreshRate);
}

void OpenGLWindow::setWindowed()
{
    setMonitor(nullptr, getWindowPosition(), getWindowSize(), 0);
}

bool OpenGLWindow::isIconified()
{
    return utility::bool_cast(glfwGetWindowAttrib(glf_window, GLFW_ICONIFIED));
}

void OpenGLWindow::iconify()
{
    glfwIconifyWindow(glf_window);
}

void OpenGLWindow::restore()
{
    glfwRestoreWindow(glf_window);
}

bool OpenGLWindow::isVisible()
{
    return utility::bool_cast(glfwGetWindowAttrib(glf_window, GLFW_VISIBLE));
}

void OpenGLWindow::hide()
{
    glfwHideWindow(glf_window);
}

void OpenGLWindow::show()
{
    glfwShowWindow(glf_window);
}

bool OpenGLWindow::hasFocus()
{
    return utility::bool_cast(glfwGetWindowAttrib(glf_window, GLFW_FOCUSED));
}

void OpenGLWindow::giveFocus()
{
    glfwFocusWindow(glf_window);
}

bool OpenGLWindow::hasMainFocus()
{
    return is_focused;
}

void OpenGLWindow::setMainFocus(bool focus)
{
    is_focused = focus;
}

glm::ivec2 OpenGLWindow::getSize() const
{
    glm::ivec2 s;
    glfwGetFramebufferSize(glf_window, &s.x, &s.y);
    return s;
}

int OpenGLWindow::getGLFWAttrib(int attrib)
{
    return glfwGetWindowAttrib(glf_window, attrib);
}

void OpenGLWindow::resize(glm::ivec2 size)
{
    // there is no way to directly set glfw framebuffer size
    // This can be approximated with a proportionality relatioship between
    //    the window size and the framebuffer size
    setWindowSize(size * getWindowSize() / getSize());
}

glm::ivec2 OpenGLWindow::getPos() const
{
    return moveEvent_->pos();
}

void OpenGLWindow::move(glm::ivec2 pos)
{
    moveEvent_->send(pos);
}

void OpenGLWindow::setInputMode(int mode, int value)
{
    glfwSetInputMode(glf_window, mode, value);
}

int OpenGLWindow::getKeyState(int key)
{
    return glfwGetKey(glf_window, key);
}

glm::vec2 OpenGLWindow::getCursorPos()
{
    glm::dvec2 r;
    glfwGetCursorPos(glf_window, &r.x, &r.y);
    return r;
}

void OpenGLWindow::setCursor(std::shared_ptr<Cursor> c)
{
    if(c)
        glfwSetCursor(glf_window, c->cursor);
    else
        glfwSetCursor(glf_window, nullptr);

    cursor = c;
}

std::string OpenGLWindow::getClipboardString()
{
    const char* text = glfwGetClipboardString(glf_window);
    std::string ret;
    if(text)
        ret = text;
    return ret;
}

void OpenGLWindow::setClipboadString(std::string s)
{
    glfwSetClipboardString(glf_window, s.c_str());
}

std::shared_ptr<core::UserInputManager> OpenGLWindow::userInputManager()
{
    return ui_manager;
}

std::shared_ptr<core::WindowEventManager> OpenGLWindow::eventManager()
{
    return we_manager;
}

void OpenGLWindow::initCallbacks()
{
    // surface size
    auto frameBufferSizeCallback = [](GLFWwindow* window, int w, int h)
    {
        OpenGLWindow::getOpenGLWindow(window)->resizeEvent()->send(glm::ivec2(w, h));
    };

    glfwSetFramebufferSizeCallback(glf_window, frameBufferSizeCallback);


    // window events
    auto closeCallback = [](GLFWwindow* window)
    {
        OpenGLWindow::getOpenGLWindow(window)->eventManager()->closeEvent->send();
    };

    auto windowResizeCallback = [](GLFWwindow* window, int w, int h)
    {
        OpenGLWindow::getOpenGLWindow(window)->eventManager()->windowResizeEvent->send(glm::ivec2(w, h));
    };

    auto windowPosCallback = [](GLFWwindow* window, int x, int y)
    {
        OpenGLWindow::getOpenGLWindow(window)->eventManager()->windowMoveEvent->send(glm::ivec2(x, y));
    };

    auto windowIconifyCallback = [](GLFWwindow* window, int ico)
    {
        OpenGLWindow::getOpenGLWindow(window)->eventManager()->windowIconifyEvent->send(utility::bool_cast(ico));
    };

    auto windowFocusCallback = [](GLFWwindow* window, int focus)
    {
        OpenGLWindow::getOpenGLWindow(window)->eventManager()->windowFocusEvent->send(utility::bool_cast(focus));
    };

    glfwSetWindowCloseCallback(glf_window, closeCallback);
    glfwSetWindowSizeCallback(glf_window, windowResizeCallback);
    glfwSetWindowPosCallback(glf_window, windowPosCallback);
    glfwSetWindowIconifyCallback(glf_window, windowIconifyCallback);
    glfwSetWindowFocusCallback(glf_window,windowFocusCallback);


    // user input
    auto keyCallback = [](GLFWwindow* window, int key, int scancode, int action, int mods)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->keyEvent->send(key, scancode, action, mods);
    };

    auto charCallback = [](GLFWwindow* window, unsigned int codepoint)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->charEvent->send(codepoint);
    };

    auto charmodsCallback = [](GLFWwindow* window, unsigned int codepoint, int mods)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->charModsEvent->send(codepoint, mods);
    };

    auto mouseMoveCallback = [](GLFWwindow* window, double xpos, double ypos)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->mouseMoveEvent->send(glm::vec2(xpos, ypos));
    };

    auto mouseEnterCallback = [](GLFWwindow* window, int enter)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->mouseEnterEvent->send(utility::bool_cast(enter));
    };

    auto mouseButtonCallback = [](GLFWwindow* window, int button, int action, int mods)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->mouseButtonEvent->send(button, action, mods);
    };

    auto scrollCallback = [](GLFWwindow* window, double xoffset, double yoffset)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->mouseScrollEvent->send(glm::vec2(xoffset, yoffset));
    };

    auto pathCallback = [](GLFWwindow* window, int count, const char** paths)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
        {
            std::vector<std::string> tmp;
            for(int i=0; i<count; ++i)
                tmp.push_back(std::string(paths[i]));
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->pathDropEvent->send(tmp);
        }
    };

    /*auto joystickCallback = [](int joy, int event)
    {
        if(OpenGLWindow::getOpenGLWindow(window)->is_focused)
            OpenGLWindow::getOpenGLWindow(window)->userInputManager()->joystickConfigEvent->send(joy,event);
    };*/

    glfwSetKeyCallback         (glf_window, keyCallback);
    glfwSetCharCallback        (glf_window, charCallback);
    glfwSetCharModsCallback    (glf_window, charmodsCallback);
    glfwSetCursorPosCallback   (glf_window, mouseMoveCallback);
    glfwSetMouseButtonCallback (glf_window, mouseButtonCallback);
    glfwSetCursorEnterCallback (glf_window, mouseEnterCallback);
    glfwSetScrollCallback      (glf_window, scrollCallback);
    glfwSetDropCallback        (glf_window, pathCallback);
    //glfwSetJoystickCallback    (joystickCallback);
}

void OpenGLWindow::doneRendering()
{

    glfwSwapBuffers(glf_window);
    glfwPollEvents();
    //Joystickevents---------
    int count;
    for(unsigned int i = 0; i < 16 ; ++i)
    {
        const float* axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1 + i, &count);
        if(axes != nullptr)
        {
            std::vector<float> axes_v(axes,axes + count);
            ui_manager->joystickAxisEvent->send(GLFW_JOYSTICK_1 + i,axes_v);
        }
        const unsigned char* buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1 + i, &count);
        if(buttons != nullptr)
        {
            std::string buttonsStr(reinterpret_cast<const char*>(buttons));
            if (buttonsStr != "")
            {
                std::vector<char> buttons_v(buttons,buttons + count);
                ui_manager->joystickButtonEvent->send(GLFW_JOYSTICK_1 + i,buttons_v);
            }
        }
    }
    //-----------------------
    int width, height;
    glfwGetFramebufferSize(glf_window,&width,&height);
    GLCHECK(glViewport(0,0,width,height));

}





}
}
