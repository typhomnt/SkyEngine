#include "logwidget.h"

#include "utils/logger.h"


namespace module
{

namespace gui
{

LogWidget::LogWidget(const std::string& name, SkyWindow* window,std::shared_ptr<ActiveObjectEvent> act_evt,
                     std::shared_ptr<SelectObjectEvent> select_evt, ImGuiWindowFlags flags)
    : SkyWidget { name, window, act_evt, select_evt, flags }
    , m_filter {}
{}

void LogWidget::draw()
{
    if (ImGui::Button("Clear"))
        clear();

    ImGui::SameLine();
    m_filter.Draw();

    ImGui::Separator();
    ImGui::BeginChild("Scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

    const std::vector<std::string>& logs = skyengine::utility::Logger::GetLogs();
    if (m_filter.IsActive())
    {
        for (const std::string& log : logs)
        {
            const char* logStr = log.c_str();
            if (m_filter.PassFilter(logStr))
                ImGui::TextUnformatted(logStr);
        }
    }
    else
    {
        for (const std::string& log : logs)
            ImGui::TextUnformatted(log.c_str());
    }

    ImGui::SetScrollHere(1.0f);
    ImGui::EndChild();
}


void LogWidget::clear()
{
    skyengine::utility::Logger::ClearLogs();
}

} // namespace gui

} // namespace module
