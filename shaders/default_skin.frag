#version 330 core

// // Interpolated values from the vertex shaders
// in vec2 UV;
// in vec3 n;
// in vec3 l;
// in vec3 EyeDirection_cameraspace;
// in float visibility;
// // Values that stay constant for the whole mesh.
// uniform sampler2D myTextureSampler;
// uniform float LightPower;
// uniform vec4 LightColor;
// uniform vec3 material_ka;
// uniform vec3 material_kd;
// uniform vec3 material_ks;
// uniform float material_shininess;
// // Ouput data
// layout (location = 0) out vec4 color;
// layout (location = 1) out vec4 BrightColor;
// const float cartoonLevels = 40;

// void main(){

//         // Output color = color specified in the vertex shader,
//         // interpolated between all 3 surrounding vertices
//         //n = normalize(n);
//         //l = normalize(l);
//         vec3 N = normalize(n);
//         vec3 L = normalize(l);
//         float nlDot = dot( N,L );
//         vec3 E = normalize(EyeDirection_cameraspace);
//         vec3 R = reflect(-L,N);
//         vec3 H = normalize(L + E);
//         nlDot = dot(H,N);
//         float cosTheta = clamp( nlDot, 0,1 );

//         float brightness = max(nlDot,0.0);
//         float level = floor(cartoonLevels * brightness);
//         float cosAlpha = clamp( dot( E,normalize(R) ), 0,1 );
//         float dampFactor = pow(cosAlpha,material_shininess);
//         float levelSpec = floor(cartoonLevels * dampFactor);
//         //For cartoon shading
//         //dampFactor = levelSpec / cartoonLevels;
//         //brightness = level / cartoonLevels ;
//         // Cosine of the angle between the Eye vector and the Reflect vector,
//         // clamped to 0
//         //  - Looking into the reflection -> 1
//         //  - Looking elsewhere -> < 1

// 	vec4 tex_color = texture(myTextureSampler, UV);
//         vec4 MaterialDiffuseColor = vec4(material_kd,1.0f)*vec4(tex_color.rgb*brightness,1.0f);//*brightness;
//         // MaterialDiffuseColor.w = 1.0f;
//         vec4 MaterialAmbientColor = vec4(material_ka,1.0f) * MaterialDiffuseColor*LightColor*LightPower;
//         vec4 MaterialSpecularColor = vec4(material_ks,1.0f)*LightColor*LightPower*dampFactor;
//         color = MaterialDiffuseColor*cosTheta*LightColor*LightPower + MaterialAmbientColor + MaterialSpecularColor;
//         color.w = 1.0f;
//         //color = mix(vec4(0.5f,0.5f,0.5f,1.0f),color, visibility);
//         float edgeDetection = (dot(E, n) > 0.2) ? 1 : 0;
//        // color = edgeDetection*color;
//         color.w = 1.0f;
//         //color = vec4(0.9,0,0,1) + color*0.01;
        
//         //brightness
//         float bright = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));
//         if(bright > 0.8)
//             BrightColor = vec4(color.rgb, 1.0);
//         else
//             BrightColor = vec4(0,0,0,1);

// }


// Struct definitions
struct DirLight
{
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct PointLight
{
    vec3 position;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
};

struct SpotLight
{
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
    float cutoff;
};

struct Material
{
    vec4 emissive;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

// Uniform variables
uniform Material material;
uniform vec3 camPosition;
uniform sampler2D myTextureSampler;
uniform sampler2D myTextureSampler1;
uniform sampler2D myTextureSampler2;

// Uniform lights.
// TODO : for now, only 1 DirLight and 1 PointLight
#define NB_LIGHTS 1
uniform DirLight dirLight[NB_LIGHTS];
uniform PointLight pointLight[NB_LIGHTS];

// Input variables
in vec3 fragPos;
in vec3 fragNormal;
in vec2 fragUV;

// Output variable
out vec4 fragColor;


vec4 computeDirLighting(DirLight dirLight)
{
    // Ambient
    vec4 color = material.emissive + material.ambient * dirLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(-dirLight.direction);

    float NdotL = dot(N, L);
    if (NdotL > 0.0)
    {
        color += material.diffuse * dirLight.diffuse * NdotL;

        // Specular
        vec3 V = normalize(camPosition - fragPos);
        vec3 R = normalize(reflect(-L, N));

        float RdotV = max(dot(R, V), 0.0);
        color += material.specular * dirLight.specular * pow(RdotV, material.shininess);
    }

    return color;
}


vec4 computePointLighting(PointLight pointLight)
{
    // Ambient
    vec4 color = material.emissive + material.ambient * pointLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(pointLight.position - fragPos);

    float NdotL = dot(N, L);
    if (NdotL > 0.0)
    {
        color += material.diffuse * pointLight.diffuse * NdotL;

        // Specular
        vec3 V = normalize(camPosition - fragPos);
        vec3 R = normalize(reflect(-L, N));

        float RdotV = max(dot(R, V), 0.0);
        color += material.specular * pointLight.specular * pow(RdotV, material.shininess);
    }

    // Attenuation factor
    float d = length(pointLight.position - fragPos);
    float att =   pointLight.constant
                + pointLight.linear * d
                + pointLight.quadratic * d * d;

    return att * color;
}


vec4 computeSpotLighting(SpotLight spotLight)
{
    // Ambient
    vec4 color = material.emissive + material.ambient * spotLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(spotLight.position - fragPos);

    float spotFactor = dot(-L, normalize(spotLight.direction));
    if (spotFactor > spotLight.cutoff)
    {
        float NdotL = dot(N, L);
        if (NdotL > 0.0)
        {
            color += material.diffuse * spotLight.diffuse * NdotL;

            // Specular
            vec3 V = normalize(camPosition - fragPos);
            vec3 R = normalize(reflect(-L, N));

            float RdotV = max(dot(R, V), 0.0);
            color += material.specular * spotLight.specular * pow(RdotV, material.shininess);
        }

        // Attenuate color with spot factor
        color *= spotFactor;
    }

    // Attenuation factor
    float d = length(spotLight.position - fragPos);
    float att =   spotLight.constant
                + spotLight.linear * d
                + spotLight.quadratic * d * d;

    return att * color;
}


void main()
{
    fragColor = vec4(0.0);

    for (int i = 0; i < NB_LIGHTS; ++i)
        fragColor += computeDirLighting(dirLight[i]);
    for (int i = 0; i < NB_LIGHTS; ++i)
        fragColor += computePointLighting(pointLight[i]);

    fragColor *= vec4(texture(myTextureSampler, fragUV).xyz,1.0f);
    fragColor *= vec4(texture(myTextureSampler1, fragUV).xyz,1.0f);
    fragColor *= vec4(texture(myTextureSampler2, fragUV).xyz,1.0f);

    //TEST rim light
    float rim = smoothstep(0.0,1.0,1.0 - max(dot(normalize(camPosition - fragPos),normalize(fragNormal)),0.0));
    fragColor *= (1.0 + rim);
    fragColor = clamp(fragColor, 0.0, 1.0);
}
