#include "shadereditor.h"

#include <fstream>

#include "ressources/ressourcemanager.h"
#include "utils/logger.h"


static char* _ReadShaderSource(const std::string& fileName, size_t& outBufferSize)
{
    std::ifstream stream(fileName);
    if (!stream.is_open())
    {
        skyengine::utility::Logger::Error("Failed to open file " + fileName);
        return nullptr;
    }

    // Get file size
    stream.seekg(0, stream.end);
    size_t fileSize = stream.tellg();
    stream.seekg(0, stream.beg);

    // FIXME : ImGui does not handle dynamic buffers.
    outBufferSize = 5 * fileSize;
    char* buffer = new char[outBufferSize]();
    stream.read(buffer, fileSize);

    stream.close();
    return buffer;
}

static bool _SaveShaderSource(const std::string& fileName, const char* const buffer)
{
    std::ofstream stream(fileName);
    if (!stream.is_open())
    {
        skyengine::utility::Logger::Error("Failed to open file " + fileName);
        return false;
    }

    stream << buffer;
    stream.close();

    return true;
}


namespace module
{

namespace gui
{

ImGui::TabWindow::TabLabel* CreateShaderTab(
    ImGui::TabWindow& /*parent*/,
    const char* label,
    const char* tooltip,
    bool /*closable*/,
    bool /*draggable*/,
    void* userPtr,
    const char* userText,
    int userInt,
    int ImGuiWindowFlagsForContent
)
{
    auto tab = new ShaderEditor::ShaderTabLabel(label, tooltip);
    tab->userPtr = userPtr;
    tab->setUserText(userText);
    tab->userInt =userInt;
    tab->wndFlags = ImGuiWindowFlagsForContent;

    return (ImGui::TabWindow::TabLabel*)tab;
}


ShaderEditor::ShaderEditor(const std::string& name, SkyWindow* window
                           ,std::shared_ptr<ActiveObjectEvent> act_evt,
                           std::shared_ptr<SelectObjectEvent> select_evt, ImGuiWindowFlags flags)
    : SkyWidget { name, window, act_evt, select_evt, flags }
    , m_shaderIndex { -1 }
    , m_tabShaders()
{
    m_tabShaders.TabLabelFactoryCb = CreateShaderTab;
}

void ShaderEditor::draw()
{
    std::vector<skyengine::ressource::Shader*> shaders;
    std::vector<const char*> guiShaders;

    for (auto& R : skyengine::ressource::RessourceManager::GetSingleton()->getRessources())
    {
        auto shader = std::dynamic_pointer_cast<skyengine::ressource::Shader>(R.second);
        if (shader)
        {
            shaders.push_back(shader.get());
            guiShaders.push_back(R.first.c_str());
        }
    }

    int newShaderIndex = m_shaderIndex;
    ImGui::Combo("", &newShaderIndex, guiShaders.data(), guiShaders.size());

    ImGui::SameLine();
    bool saveAsked = ImGui::Button("Save");
    if (saveAsked)
    {
        ImVector<ImGui::TabWindow::TabLabel*> tabs;
        m_tabShaders.getAllTabLabels(tabs);

        for (int i = 0; i < tabs.size(); ++i)
        {
            auto shaderTab = (ShaderTabLabel*)tabs[i];
            shaderTab->askToSave();
        }
    }

    ImGui::Separator();

    if (newShaderIndex != m_shaderIndex)
    {
        m_shaderIndex = newShaderIndex;

        skyengine::ressource::Shader* shader = shaders[m_shaderIndex];
        if (!m_tabShaders.isInited())
        {
            auto vTab = (ShaderTabLabel*)m_tabShaders.addTabLabel("Vertex", "Vertex Shader", false, false);
            auto fTab = (ShaderTabLabel*)m_tabShaders.addTabLabel("Fragment", "Fragment Shader", false, false);

            vTab->setShader(shader, ShaderTabLabel::ShaderType::VERTEX);
            fTab->setShader(shader, ShaderTabLabel::ShaderType::FRAGMENT);

            if (!shader->m_geomFile.empty())
            {
                auto gTab = (ShaderTabLabel*)m_tabShaders.addTabLabel("Geometry", "Geometry Shader", false, false);
                gTab->setShader(shader, ShaderTabLabel::ShaderType::GEOMETRY);
            }
        }
        else
        {
            auto vTab = (ShaderTabLabel*)m_tabShaders.findTabLabelFromLabel("Vertex");
            auto fTab = (ShaderTabLabel*)m_tabShaders.findTabLabelFromLabel("Fragment");

            vTab->setShader(shader, ShaderTabLabel::ShaderType::VERTEX);
            fTab->setShader(shader, ShaderTabLabel::ShaderType::FRAGMENT);

            auto gTab = m_tabShaders.findTabLabelFromLabel("Geometry");
            if (gTab)
            {
                if (!shader->m_geomFile.empty())
                    ((ShaderTabLabel*)gTab)->setShader(shader, ShaderTabLabel::ShaderType::GEOMETRY);
                else
                    m_tabShaders.removeTabLabel(gTab);
            }
            else if (!shader->m_geomFile.empty())
            {
                auto geomTab = (ShaderTabLabel*)m_tabShaders.addTabLabel("Geometry", "Geometry Shader", false, false);
                geomTab->setShader(shader, ShaderTabLabel::ShaderType::GEOMETRY);
            }
        }
    }

    if (m_shaderIndex != -1)
        m_tabShaders.render();
}


ShaderEditor::ShaderTabLabel::ShaderTabLabel(const char* label, const char* tooltip)
    : ImGui::TabWindow::TabLabel { label, tooltip, false, false }
    , m_type { ShaderType::VERTEX }
    , m_buffer { nullptr }
    , m_bufferSize { 0 }
{}

ShaderEditor::ShaderTabLabel::~ShaderTabLabel()
{
    if (m_buffer)
    {
        delete[] m_buffer;
        m_buffer = nullptr;
    }
}

void ShaderEditor::ShaderTabLabel::setShader(skyengine::ressource::Shader* shader, ShaderType type)
{
    assert(shader);
    m_shader = shader;
    m_type = type;

    // First delete old buffer if necessary
    if (m_buffer)
    {
        delete[] m_buffer;
        m_buffer = nullptr;
        m_bufferSize = 0;
    }

    switch (m_type)
    {
        case ShaderType::VERTEX:
            m_buffer = _ReadShaderSource(shader->m_vertexFile, m_bufferSize);
        break;
        case ShaderType::GEOMETRY:
            m_buffer = _ReadShaderSource(shader->m_geomFile, m_bufferSize);
        break;
        default: // ShaderType::FRAGMENT
            m_buffer = _ReadShaderSource(shader->m_fragmentFile, m_bufferSize);
        break;
    }
    assert(m_buffer);
}

void ShaderEditor::ShaderTabLabel::render()
{
    if (!m_shader)
        return;

    ImGui::InputTextMultiline(
        "##HiddenID",
        m_buffer,
        m_bufferSize,
        ImVec2(0.95f * ImGui::GetWindowWidth(), 0.95f * ImGui::GetWindowHeight())
    );

    if (m_saveAsked)
    {
        m_saveAsked = false;

        switch (m_type)
        {
            case ShaderType::VERTEX:
                _SaveShaderSource(m_shader->m_vertexFile, m_buffer);
            break;
            case ShaderType::GEOMETRY:
                _SaveShaderSource(m_shader->m_geomFile, m_buffer);
            break;
            default: // ShaderType::FRAGMENT
                _SaveShaderSource(m_shader->m_fragmentFile, m_buffer);
            break;
        }

        // Recompile shader
        m_shader->compileAndLink();
    }
}

} // namespace gui

} // namespace module
