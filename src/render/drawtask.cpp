#include "drawtask.h"
#include "../core/skyengine.h"
#include "ressources/ressourcemanager.h"
#include "utils/utilities.h"
namespace skyengine
{

namespace render
{
DrawTask::DrawTask(core::SkyEngine* engine/*, glm::ivec2 win_size TO BE REMOVED! */,std::shared_ptr<core::WindowEventManager> wem,bool is_forward)
    :m_is_forward(is_forward),m_display(false),m_post_proc_quad(scene::QuadObject("post_quad")),m_wem(wem)
{
    //init-----------------------
    this->engine = engine;
    //
    std::shared_ptr<core::FrameBuffer> forward_fbo = std::make_shared<core::FrameBuffer>(wem);
    std::shared_ptr<core::FrameBuffer> deferred_fbo = std::make_shared<core::FrameBuffer>(wem);
    //std::shared_ptr<core::FrameBuffer> deferred_light_fbo = std::make_shared<core::FrameBuffer>(wem);

    //forward--------------------
    m_task_framebuffers["forward"] = forward_fbo;
    //forward_fbo->resize(win_size);
    forward_fbo->bind();
    forward_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::COLORATTACHMENT0,GL_RGBA16F,GL_RGBA,GL_FLOAT,forward_fbo->getSize().x,
                                forward_fbo->getSize().y);
    forward_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::DEPTHATTACHMENT);
    forward_fbo->setFrameBuffer();
    forward_fbo->checkStatus();
    forward_fbo->unBind();

    //-------------------------

    //deferred-----------------
    m_task_framebuffers["deferred"] = deferred_fbo;
    //m_task_framebuffers["deferred_light"] = deferred_light_fbo;
    //deferred_fbo->resize(win_size);
    deferred_fbo->bind();
    //Pos
    deferred_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::COLORATTACHMENT0,GL_RGBA16F,GL_RGBA,GL_FLOAT,deferred_fbo->getSize().x,
                                 deferred_fbo->getSize().y);
    //Normal
    deferred_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::COLORATTACHMENT1,GL_RGBA16F,GL_RGBA,GL_FLOAT,deferred_fbo->getSize().x,
                                 deferred_fbo->getSize().y);
    //AlbedoSpec
    deferred_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::COLORATTACHMENT2,GL_RGBA16F,GL_RGBA,GL_FLOAT,deferred_fbo->getSize().x,
                                 deferred_fbo->getSize().y);
    deferred_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::DEPTHATTACHMENT);
    deferred_fbo->setFrameBuffer();
    deferred_fbo->checkStatus();
    deferred_fbo->unBind();

    //deferred_light_fbo->resize(win_size);
    /*deferred_light_fbo->bind();
    deferred_light_fbo->bindAttachment(core::FrameBuffer::AttachmentMode::COLORATTACHMENT0,GL_RGBA,GL_RGBA,GL_UNSIGNED_BYTE,deferred_light_fbo->getSize().x,
                                       deferred_light_fbo->getSize().y);
    deferred_light_fbo->setFrameBuffer();
    deferred_light_fbo->checkStatus();
    deferred_light_fbo->unBind();*/

    //deferred light initialization
    m_post_proc_quad.setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("deferredl_shader")).get());
    m_post_proc_quad.setMaterial(nullptr);
    std::unordered_map<core::FrameBuffer::AttachmentMode, std::vector<unsigned int>, std::hash<int> > attach;
    std::unordered_map<core::FrameBuffer::AttachmentMode,std::string,std::hash<int>> input;
    std::vector<unsigned int> arg{(unsigned int)GL_RGBA16F,(unsigned int)GL_RGBA,(unsigned int)GL_FLOAT,(unsigned int)(deferred_fbo->getSize().x),
                (unsigned int)(deferred_fbo->getSize().y)};
    attach[core::FrameBuffer::AttachmentMode::COLORATTACHMENT0] = arg;
    input[core::FrameBuffer::AttachmentMode::COLORATTACHMENT0] = "gPosition";
    input[core::FrameBuffer::AttachmentMode::COLORATTACHMENT1] = "gNormal";
    input[core::FrameBuffer::AttachmentMode::COLORATTACHMENT2] = "gAlbedoSpec";
    addPostProcFBO("deferred_light",attach,"deferredl_shader","deferred",input);

    //-------------------------
    if(!is_forward)
    {
        m_task_fbo_order.push_back(deferred_fbo);
        m_task_fbo_order.push_back(m_task_framebuffers["deferred_light"]);
    }
    m_task_fbo_order.push_back(forward_fbo);

    input.clear();
    input[core::FrameBuffer::AttachmentMode::COLORATTACHMENT0] = "renderedTexture";
    addPostProcFBO("HDR",attach,"simple_display_shader","forward",input);
    m_task_fbo_order.push_back(m_task_framebuffers["HDR"]);



}

DrawTask::~DrawTask()
{

}

void DrawTask::setFramebufferCamera(scene::Camera* cam, const std::string& fbo_name)
{
    //TODO Factor using a vector of string.
    std::vector<std::string> fbo_names;
    if(fbo_name == "")
        for(auto& fbo : m_task_framebuffers)
            fbo_names.push_back(fbo.first);
    else
        fbo_names.push_back(fbo_name);

    for(auto& fbo : fbo_names)
    {
        for(auto& shader: m_task_shader)
        {
            std::shared_ptr<ressource::Shader> sh_res = std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get(shader));
            if(sh_res->containsUniform("camPosition"))
                addUniformValue(fbo,shader,new scene::NodeParameter<glm::vec3*>("camPosition",1,cam->getPositionPt()));
            if(sh_res->containsUniform("V"))
                addUniformValue(fbo,shader,new scene::NodeParameter<glm::mat4*>("V",1,cam->getViewMatrixPt()));
            if(sh_res->containsUniform("P"))
                addUniformValue(fbo,shader,new scene::NodeParameter<glm::mat4*>("P",1,cam->getProjectionMatrixPt()));
        }
    }

}

void DrawTask::setSceneGraph(scene::SceneGraph* scene_graph)
{
    m_scene_graph = scene_graph;
}

void DrawTask::assignTextureToSampler(const std::string& fbo_name, const std::string& shader_name, const std::string& sampler_uniform, ressource::Texture* texture)
{
    m_fbo_shader_texture_unit[m_task_framebuffers[fbo_name]->getId()][shader_name][sampler_uniform] = texture;
}

void DrawTask::addUniformValue(const std::string& fbo_name, const std::string& shader_name, scene::baseNodeParameter* uniform)
{
    m_fbo_shaders_uniform_map[m_task_framebuffers[fbo_name]->getId()][shader_name][uniform->name] = uniform;
}

void DrawTask::assignLightToShader(const std::string& fbo_name, const std::string& shader_name, ressource::Light* light)
{
    m_fbo_shader_light[m_task_framebuffers[fbo_name]->getId()][shader_name][light->getName()] = light;
}

void DrawTask::addShader(const std::string& shader_name)
{
    m_task_shader.push_back(shader_name);
}

void DrawTask::addPostProcFBO(const std::string& fbo_name, const std::unordered_map<core::FrameBuffer::AttachmentMode,std::vector<unsigned int>,std::hash<int>>& attachents
                              , const std::string &shader_name, const std::string& input_fbo_name
                              , const std::unordered_map<core::FrameBuffer::AttachmentMode, std::string, std::hash<int> > &input_attachents)
{
    if(!utility::MapContains(m_task_framebuffers,fbo_name))
    {
        std::shared_ptr<core::FrameBuffer> post_proc_fbo = std::make_shared<core::FrameBuffer>(m_wem);
        m_task_framebuffers[fbo_name] = post_proc_fbo;
        //post_proc_fbo->resize(win_size);
        post_proc_fbo->bind();
        for(auto & attach : attachents)
        {
            if(attach.second.size() != 0)
            {
                post_proc_fbo->bindAttachment(attach.first,attach.second[0],attach.second[1],attach.second[2],attach.second[3],
                        attach.second[4]);
            }
            else
            {
                post_proc_fbo->bindAttachment(attach.first);
            }
        }
        post_proc_fbo->setFrameBuffer();
        post_proc_fbo->checkStatus();
        post_proc_fbo->unBind();

        m_fbo_shaders_renderables_map[m_task_framebuffers[fbo_name]->getId()][shader_name].push_back(&m_post_proc_quad);
        if(input_fbo_name != "")
        {
            for(auto& attach : input_attachents)
            {
                assignTextureToSampler(fbo_name,shader_name,attach.second,m_task_framebuffers[input_fbo_name]->getAttachmentTex(attach.first));
            }
        }
    }
}

void DrawTask::run()
{
    // TODO remove and do smarter than cleaning at each frame
    m_fbo_shaders_renderables_map[m_task_framebuffers["forward"]->getId()].clear();
    m_fbo_shaders_renderables_map[m_task_framebuffers["deferred"]->getId()].clear();
    /* if(!m_is_forward)
        m_fbo_shaders_renderables_map[m_task_framebuffers["deferred_light"]->getId()]["deferredl_shader"].push_back(&m_post_proc_quad);*/
    for(auto& node: m_scene_graph->getNodes())
    {
        if(node.second->isRenderable())
        {
            render::Renderable* rend = std::dynamic_pointer_cast<render::Renderable>(node.second).get();
            if(!m_is_forward)
            {
                if(rend->getRenderShader()->getName() == "default_shader")
                {
                    rend->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("deferredg_shader")).get());
                }
                else if(rend->getRenderShader()->getName() == "default_skin_shader")
                {
                    rend->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("deferredg_skin_shader")).get());
                }

                if(rend->getRenderShader()->getName() == "deferredg_shader" || rend->getRenderShader()->getName() == "deferredg_skin_shader")
                    m_fbo_shaders_renderables_map[m_task_framebuffers["deferred"]->getId()][rend->getRenderShader()->getName()].push_back(rend);
                else
                    m_fbo_shaders_renderables_map[m_task_framebuffers["forward"]->getId()][rend->getRenderShader()->getName()].push_back(rend);
            }
            else
            {
                m_fbo_shaders_renderables_map[m_task_framebuffers["forward"]->getId()][rend->getRenderShader()->getName()].push_back(rend);
            }
        }
    }

    // Texture
    for(auto& fbo : m_task_fbo_order)
    {
        if(fbo->getId() == m_task_framebuffers["forward"]->getId()  && !m_is_forward)
        {
            core::FrameBuffer::Copy(*(m_task_framebuffers["deferred"].get()),
                    core::FrameBuffer::DEPTHATTACHMENT,
                    *(m_task_framebuffers["forward"].get()),
                    GL_DEPTH_BUFFER_BIT);

            core::FrameBuffer::Copy(*(m_task_framebuffers["deferred_light"].get()),
                    core::FrameBuffer::COLORATTACHMENT0,
                    *(m_task_framebuffers["forward"].get()),
                    GL_COLOR_BUFFER_BIT);
        }

        if(fbo->getId() == m_task_framebuffers["forward"]->getId() && m_is_forward)
        {
            GLCHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
        }
        else if(fbo->getId() == m_task_framebuffers["deferred"]->getId()
                ||fbo->getId() == m_task_framebuffers["deferred_light"]->getId())
        {
            GLCHECK(glBlendFuncSeparate(GL_ONE, GL_ZERO, GL_ONE, GL_ZERO));
        }




        fbo->bind();

        /**
         * OpenGL viewport coordinate system is different than ImGui screen space.
         * ImGui : origin at top-left corner, X axis to the left and Y axis downwards
         * OpenGL : origin at bottom-left corner, X axis to the left and Y axis upwards
         *
         * When calling glViewport, we must give the position of the bottom-left point in the OpenGL system.
         * Since FBO position is stored in ImGui space, we must make some conversions.
         */
        glm::ivec2 fboPos = fbo->getPos();
        glm::ivec2 fboSize = fbo->getSize();
        glm::ivec2 winSize = m_wem->windowResizeEvent->size();
        GLCHECK(glViewport(0/*fboPos.x*/,0/* winSize.y - (fboPos.y + fboSize.y)*/, fboSize.x, fboSize.y));

        if((fbo->getId() != m_task_framebuffers["forward"]->getId() && !m_is_forward) || m_is_forward)
            GLCHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
        //if(fbo->getId() != m_task_framebuffers["deferred_light"]->getId() )

        for(const std::string& shader_name : m_task_shader)
        {
            auto resource = std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get(shader_name));
            ressource::Shader* shader =  resource.get();

            // bind shader
            shader->use();

            // Lights
            for(auto& lights : m_fbo_shader_light[fbo->getId()][shader_name])
                lights.second->sendToShader(*shader);

            // Textures
            for(auto& tex : m_fbo_shader_texture_unit[fbo->getId()][shader_name])
                shader->setTextureToSampler(tex.second->getName(),tex.first);
            shader->bindTextures();

            for(auto& parameter: m_fbo_shaders_uniform_map[fbo->getId()][shader_name])
                shader->setUniformValue(parameter.second);

            for(Renderable* renderable: m_fbo_shaders_renderables_map[fbo->getId()][shader_name])
            {
                if (renderable->isRendered())
                {
                    if(renderable->getRenderShader() == nullptr || renderable->getRenderShader()->getName() != shader_name)
                        renderable->setRenderShader(shader);
                    renderable->setShaderUniforms();
                    if(renderable->isWireRendered())
                        GLCHECK(glPolygonMode(GL_FRONT_AND_BACK,GL_LINE));
                    renderable->draw();
                    if(renderable->isWireRendered())
                        GLCHECK(glPolygonMode(GL_FRONT_AND_BACK,GL_FILL));

                }
            }

            // unbind
            ressource::Shader::clearUse();
        }

        fbo->unBind();
        if(m_display)
            m_task_fbo_order.back()->displayAttachment(core::FrameBuffer::COLORATTACHMENT0);

    }
}

void DrawTask::show()
{
    m_display = true;
}
void DrawTask::hide()
{
    m_display = false;
}

}
}
