#include "guiutils.h"

#include <vector>

#include "utils/jsonhelper.h"


namespace module
{

namespace gui
{

static std::vector<const char*> ColorTags {
    "Text",
    "TextDisabled",
    "WindowBg",
    "ChildWindowBg",
    "PopupBg",
    "Border",
    "BorderShadow",
    "FrameBg",
    "FrameBgHovered",
    "FrameBgActive",
    "TitleBg",
    "TitleBgActive",
    "TitleBgCollapsed",
    "MenuBarBg",
    "ScrollbarBg",
    "ScrollbarGrab",
    "ScrollbarGrabHovered",
    "ScrollbarGrabActive",
    "ComboBg",
    "CheckMark",
    "SliderGrab",
    "SliderGrabActive",
    "Button",
    "ButtonHovered",
    "ButtonActive",
    "Header",
    "HeaderHovered",
    "HeaderActive",
    "Separator",
    "SeparatorHovered",
    "SeparatorActive",
    "ResizeGrip",
    "ResizeGripHovered",
    "ResizeGripActive",
    "CloseButton",
    "CloseButtonHovered",
    "CloseButtonActive",
    "PlotLines",
    "PlotLinesHovered",
    "PlotHistogram",
    "PlotHistogramHovered",
    "TextSelectedBg",
    "ModalWindowDarkening",
};

static ImVec2 ReadVec2(const rapidjson::Value& value)
{
    return ImVec2(value[0].GetFloat(), value[1].GetFloat());
}

static ImVec4 ReadVec4(const rapidjson::Value& value)
{
    return ImVec4(
        value[0].GetFloat(),
        value[1].GetFloat(),
        value[2].GetFloat(),
        value[3].GetFloat()
    );
}

template<typename T>
static void SaveElement(rapidjson::Document& doc, const std::string& name, T value)
{
    doc.AddMember(
        rapidjson::Value(name.c_str(), doc.GetAllocator()),
        rapidjson::Value(value),
        doc.GetAllocator()
    );
}

static void SaveElement(rapidjson::Document& doc, const std::string& name, ImVec2 value)
{
    rapidjson::Value array(rapidjson::kArrayType);
    array.PushBack(value.x, doc.GetAllocator());
    array.PushBack(value.y, doc.GetAllocator());

    doc.AddMember(
        rapidjson::Value(name.c_str(), doc.GetAllocator()),
        array,
        doc.GetAllocator()
    );
}

static void SaveElement(rapidjson::Document& doc, const std::string& name, ImVec4 value)
{
    rapidjson::Value array(rapidjson::kArrayType);
    array.PushBack(value.x, doc.GetAllocator());
    array.PushBack(value.y, doc.GetAllocator());
    array.PushBack(value.z, doc.GetAllocator());
    array.PushBack(value.w, doc.GetAllocator());

    doc.AddMember(
        rapidjson::Value(name.c_str(), doc.GetAllocator()),
        array,
        doc.GetAllocator()
    );
}


void GuiUtils::ApplyTheme(const std::string& themeName)
{
    rapidjson::Document doc;

    std::string themeFile = "../themes/" + themeName + ".json";
    if (skyengine::utility::JSONHelper::Load(themeFile, doc))
    {
        ImGuiStyle& style = ImGui::GetStyle();
        ImGuiStyle defaultStyle;

        // Non-color parameters
        style.AntiAliasedLines = doc.HasMember("AntiAliasedLines")
            ? doc["AntiAliasedLines"].GetBool()
            : defaultStyle.AntiAliasedLines;
        style.AntiAliasedShapes = doc.HasMember("AntiAliasedShapes")
            ? doc["AntiAliasedShapes"].GetBool()
            : defaultStyle.AntiAliasedShapes;

        style.Alpha = doc.HasMember("Alpha") ? doc["Alpha"].GetFloat() : defaultStyle.Alpha;
        style.WindowRounding = doc.HasMember("WindowRounding")
            ? doc["WindowRounding"].GetFloat()
            : defaultStyle.WindowRounding;
        style.FrameRounding = doc.HasMember("FrameRounding")
            ? doc["FrameRounding"].GetFloat()
            : defaultStyle.FrameRounding;
        style.IndentSpacing = doc.HasMember("IndentSpacing")
            ? doc["IndentSpacing"].GetFloat()
            : defaultStyle.IndentSpacing;
        style.ColumnsMinSpacing = doc.HasMember("ColumnsMinSpacing")
            ? doc["ColumnsMinSpacing"].GetFloat()
            : defaultStyle.ColumnsMinSpacing;
        style.ScrollbarSize = doc.HasMember("ScrollbarSize")
            ? doc["ScrollbarSize"].GetFloat()
            : defaultStyle.ScrollbarSize;
        style.ScrollbarRounding = doc.HasMember("ScrollbarRounding")
            ? doc["ScrollbarRounding"].GetFloat()
            : defaultStyle.ScrollbarRounding;
        style.GrabMinSize = doc.HasMember("GrabMinSize")
            ? doc["GrabMinSize"].GetFloat()
            : defaultStyle.GrabMinSize;
        style.GrabRounding = doc.HasMember("GrabRounding")
            ? doc["GrabRounding"].GetFloat()
            : defaultStyle.GrabRounding;
        style.CurveTessellationTol = doc.HasMember("CurveTessellationTol")
            ? doc["CurveTessellationTol"].GetFloat()
            : defaultStyle.CurveTessellationTol;

        style.WindowPadding = doc.HasMember("WindowPadding")
            ? ReadVec2(doc["WindowPadding"])
            : defaultStyle.WindowPadding;
        style.WindowMinSize = doc.HasMember("WindowMinSize")
            ? ReadVec2(doc["WindowMinSize"])
            : defaultStyle.WindowMinSize;
        style.WindowTitleAlign = doc.HasMember("WindowTitleAlign")
            ? ReadVec2(doc["WindowTitleAlign"])
            : defaultStyle.WindowTitleAlign;
        style.FramePadding = doc.HasMember("FramePadding")
            ? ReadVec2(doc["FramePadding"])
            : defaultStyle.FramePadding;
        style.ItemSpacing = doc.HasMember("ItemSpacing")
            ? ReadVec2(doc["ItemSpacing"])
            : defaultStyle.ItemSpacing;
        style.ItemInnerSpacing = doc.HasMember("ItemInnerSpacing")
            ? ReadVec2(doc["ItemInnerSpacing"])
            : defaultStyle.ItemInnerSpacing;
        style.TouchExtraPadding = doc.HasMember("TouchExtraPadding")
            ? ReadVec2(doc["TouchExtraPadding"])
            : defaultStyle.TouchExtraPadding;
        style.ButtonTextAlign = doc.HasMember("ButtonTextAlign")
            ? ReadVec2(doc["ButtonTextAlign"])
            : defaultStyle.ButtonTextAlign;
        style.DisplayWindowPadding = doc.HasMember("DisplayWindowPadding")
            ? ReadVec2(doc["DisplayWindowPadding"])
            : defaultStyle.DisplayWindowPadding;
        style.DisplaySafeAreaPadding = doc.HasMember("DisplaySafeAreaPadding")
            ? ReadVec2(doc["DisplaySafeAreaPadding"])
            : defaultStyle.DisplaySafeAreaPadding;

        // Colors
        for (int i = 0; i < ImGuiCol_COUNT; ++i)
        {
            style.Colors[(ImGuiCol)i] = doc.HasMember(ColorTags[i])
                ? ReadVec4(doc[ColorTags[i]])
                : defaultStyle.Colors[(ImGuiCol)i];
        }
    }
}

void GuiUtils::SaveTheme(const std::string& themeName)
{
    ImGuiStyle& style = ImGui::GetStyle();

    rapidjson::Document doc;
    doc.SetObject();

    SaveElement(doc, "Alpha", style.Alpha);
    SaveElement(doc, "WindowPadding", style.WindowPadding);
    SaveElement(doc, "WindowMinSize", style.WindowMinSize);
    SaveElement(doc, "WindowRounding", style.WindowRounding);
    SaveElement(doc, "WindowTitleAlign", style.WindowTitleAlign);
    SaveElement(doc, "FramePadding", style.FramePadding);
    SaveElement(doc, "FrameRounding", style.FrameRounding);
    SaveElement(doc, "ItemSpacing", style.ItemSpacing);
    SaveElement(doc, "ItemInnerSpacing", style.ItemInnerSpacing);
    SaveElement(doc, "TouchExtraPadding", style.TouchExtraPadding);
    SaveElement(doc, "IndentSpacing", style.IndentSpacing);
    SaveElement(doc, "ColumnsMinSpacing", style.ColumnsMinSpacing);
    SaveElement(doc, "ScrollbarSize", style.ScrollbarSize);
    SaveElement(doc, "ScrollbarRounding", style.ScrollbarRounding);
    SaveElement(doc, "GrabMinSize", style.GrabMinSize);
    SaveElement(doc, "GrabRounding", style.GrabRounding);
    SaveElement(doc, "ButtonTextAlign", style.ButtonTextAlign);
    SaveElement(doc, "DisplayWindowPadding", style.DisplayWindowPadding);
    SaveElement(doc, "DisplaySafeAreaPadding", style.DisplaySafeAreaPadding);
    SaveElement(doc, "AntiAliasedLines", style.AntiAliasedLines);
    SaveElement(doc, "CurveTessellationTol", style.CurveTessellationTol);
    SaveElement(doc, "Alpha", style.Alpha);


    // Colors
    for (int i = 0; i < ImGuiCol_COUNT; ++i)
        SaveElement(doc, ColorTags[i], style.Colors[(ImGuiCol)i]);

    std::string themeFile = "../themes/" + themeName + ".json";
    skyengine::utility::JSONHelper::Save(themeFile, doc);
}

} // namespace gui

} // namespace module
