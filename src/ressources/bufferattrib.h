#ifndef BUFFERATTRIB_H
#define BUFFERATTRIB_H
#include <GL/glew.h>
namespace skyengine {
namespace ressource {

struct BufferAttrib
{
    GLuint index;
    GLint size;
    GLenum type;
    size_t offset;
    size_t stride;
    GLboolean normalized;
    enum class SpecialAttrib {None, Int, Double} special_attrib;
    GLuint instance_update;
    // give type size as offset during construction ;
    // the offset will be computed during construction of the mesh
    BufferAttrib(GLuint i, GLint s, GLenum t, size_t typesize, size_t st = 0, GLboolean n=GL_FALSE, GLuint iu = 0):
        index(i), size(s), type(t), offset(typesize), stride(st), normalized(n), special_attrib(SpecialAttrib::None), instance_update(iu) {}
    BufferAttrib(GLuint i, GLint s, GLenum t, size_t typesize, SpecialAttrib sa, GLuint iu = 0):
        index(i), size(s), type(t), offset(typesize), stride(0), normalized(GL_FALSE), special_attrib(sa), instance_update(iu) {}


};
}
}
#endif // BUFFERATTRIB_H
