#ifndef DOODLEUTILS_H
#define DOODLEUTILS_H
#include "Utils/Vector7D.h"
#include "animation/animatedbone3d.h"
namespace module
{

namespace doodle
{


inline motionDoodle::Vector7D KeyPoseToVec7(const skyengine::animation::KeyPose3D& pose)
{
    glm::vec3 euler = glm::eulerAngles(pose.value.rotation);
    return motionDoodle::Vector7D(pose.value.position.x,pose.value.position.y,pose.value.position.z,euler.y,euler.x,euler.z,pose.time);
}

std::vector<motionDoodle::Vector7D> AnimationTrajToDoodleTraj(const skyengine::animation::AnimatedPoseTrajectory& curve);
skyengine::animation::AnimatedPoseTrajectory DoodleTrajToAnimationTraj(const std::vector<motionDoodle::Vector7D>& path, const glm::vec3 pos_shift = glm::vec3());

bool LoadLMATrainingFile(const std::string& path);

}
}

#endif // DOODLEUTILS_H
