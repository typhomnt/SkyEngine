#include "phongmaterial.h"
#include "pbrmaterial.h"

#include "ressourcemanager.h"


namespace skyengine
{

namespace ressource
{

PhongMaterial::PhongMaterial(const std::string& name)
    : Material(name)
    , m_emissive {}
    , m_ambient { 1.0f }
    , m_diffuse { 1.0f }
    , m_specular { 1.0f }
    , m_shininess { 1.0f }
{}

PhongMaterial::PhongMaterial(
    const std::string& name,
    const glm::vec4& emissive,
    const glm::vec4& ambient,
    const glm::vec4& diffuse,
    const glm::vec4& specular,
    float shininess
)
    : Material(name)
    , m_emissive { emissive }
    , m_ambient { ambient }
    , m_diffuse { diffuse }
    , m_specular { specular }
    , m_shininess { shininess }
{}

void PhongMaterial::sendToShader(const Shader &shader) const
{
    shader.setUniformValue4f("material.emissive", m_emissive);
    shader.setUniformValue4f("material.ambient", m_ambient);
    shader.setUniformValue4f("material.diffuse", m_diffuse);
    shader.setUniformValue4f("material.specular", m_specular);
    shader.setUniformValue1f("material.shininess", m_shininess);
    std::multimap<GLuint,std::string> invert_sampler_map =  utility::MapFlip(shader.getSamplers());
    for(auto& sampler : invert_sampler_map)
    {
        Texture* tex;
        if(sampler.first > m_textures.size() - 1 || m_textures.size() == 0)
            tex = std::dynamic_pointer_cast<Texture>(RessourceManager::Get("void_texture")).get();
        else
        {
            tex = m_textures[sampler.first];
        }
        if(tex == nullptr)
        {
            tex = std::dynamic_pointer_cast<Texture>(RessourceManager::Get("void_texture")).get();
        }
        tex->bind(GL_TEXTURE0 + sampler.first);
        shader.setUniformValue1i(sampler.second.c_str(), sampler.first);
    }

}

void PhongMaterial::addTexture(Texture* tex, unsigned int index)
{
    assert(index <= m_textures.size());
    m_textures.insert(m_textures.begin() + index, tex);
}

std::shared_ptr<rapidjson::Value> PhongMaterial::SerializeProp(rapidjson::MemoryPoolAllocator<> &al)
{
    std::shared_ptr<rapidjson::Value> obj = Ressource::SerializeProp(al);
    utility::JSONHelper::SaveVec(*obj.get(),"Emissive",m_emissive,al);
    utility::JSONHelper::SaveVec(*obj.get(),"Ambient",m_ambient,al);
    utility::JSONHelper::SaveVec(*obj.get(),"Diffuse",m_diffuse,al);
    utility::JSONHelper::SaveVec(*obj.get(),"Specular",m_specular,al);
    obj->AddMember(rapidjson::Value("Shininess", al),
                   rapidjson::Value(m_shininess),
                   al);

    rapidjson::Value texArray(rapidjson::kArrayType);
    for (Texture* tex : m_textures)
        texArray.PushBack(rapidjson::Value(tex->getName().c_str(), al), al);
    obj->AddMember(rapidjson::Value("Textures", al), texArray, al);

    return obj;
}

bool PhongMaterial::LoadProp(const rapidjson::Value& value)
{
    bool needSecondPass = false;

    utility::JSONHelper::ReadVec(value["Emissive"], m_emissive);
    utility::JSONHelper::ReadVec(value["Ambient"], m_ambient);
    utility::JSONHelper::ReadVec(value["Diffuse"], m_diffuse);
    utility::JSONHelper::ReadVec(value["Specular"], m_specular);
    m_shininess = value["Shininess"].GetFloat();

    unsigned int i = 0;
    for (const rapidjson::Value& it : value["Textures"].GetArray())
    {
        std::string texName = it.GetString();
        m_texNames.push_back(texName);
        if (RessourceManager::Exists(texName))
        {
            auto tex = RessourceManager::Get(texName);
            addTexture(dynamic_cast<Texture*>(tex.get()), i++);
        }
        else
        {
            needSecondPass = true;
            addTexture(nullptr, i++);
        }
    }

    return needSecondPass;
}

void PhongMaterial::FinishLoading()
{
    for (size_t i = 0; i < m_textures.size(); ++i)
    {
        if (m_textures[i] == nullptr)
        {
            auto tex = RessourceManager::Get(m_texNames[i]);
            assert(tex);

            m_textures[i] = dynamic_cast<Texture*>(tex.get());
        }
    }
}


std::shared_ptr<PhongMaterial> PhongMaterial::None = Ressource::Create<PhongMaterial>("NoneMaterial");
std::shared_ptr<PhongMaterial> PhongMaterial::Default = Ressource::Create<PhongMaterial>(
    "DefaultMaterial",
    glm::vec4(),
    glm::vec4(0.19225f, 0.19225f, 0.19225f, 1.0f),
    glm::vec4(0.50754f, 0.50754f, 0.50754f, 1.0f),
    glm::vec4(0.508273f, 0.508273f, 0.508273f, 1.0f),
    0.4f * 128.0f
);
std::shared_ptr<PhongMaterial> PhongMaterial::Turquoise = Ressource::Create<PhongMaterial>(
    "TurquoiseMaterial",
    glm::vec4(),
    glm::vec4(0.1f, 0.18725f, 0.1745f, 1.0f),
    glm::vec4(0.396f, 0.74151f, 0.69102f, 1.0f),
    glm::vec4(0.297254f, 0.30829f, 0.306678f, 1.0f),
    0.1f * 128.0f
);

std::shared_ptr<PBRMaterial> PhongMaterial::ToPBRMat()
{
    std::shared_ptr<PBRMaterial> pbr_mat = ressource::Ressource::Create<PBRMaterial>(m_name+"PBR",glm::vec3(m_diffuse.x,m_diffuse.y,m_diffuse.z),0.5,m_shininess/128.0f,0.5);
    for(Texture* tex : m_textures)
        pbr_mat->addTexture(tex);
    return pbr_mat;
}


} // namespace ressource

} // namespace skyengine
