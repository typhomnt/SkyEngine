#include "renderable.h"
#include "utils/logger.h"
#include "utils/jsonhelper.h"
#include "render/Vertex3D.h"
#include "animation/vertex3dskin.h"
#include "ressources/pointcloud.h"
namespace skyengine
{

namespace render
{


Renderable::Renderable(const std::string& name, scene::SceneGraph *owner,
                       const unsigned int type,
                       const glm::mat4& world_matrix)
    : SceneGraphNode(name,owner,type,world_matrix)
    , m_mesh(nullptr)
    , m_show(true)
    , m_wired(false)
{
}

Renderable::~Renderable()
{

}

void Renderable::setShaderUniforms()
{
    for (const std::pair<std::string,scene::baseNodeParameter*>& parameter: render_shader_uniform_values)
    {
        if(m_render_shader->containsUniform(parameter.first))
            m_render_shader->setUniformValue(parameter.second);
    }
}

void Renderable::setRenderShader(ressource::Shader *render_shader)
{
    m_render_shader = render_shader;
}


void Renderable::draw()
{
    if (m_show)
        doDraw();
}

void Renderable::setMesh(std::shared_ptr<ressource::AbstractMesh> mesh)
{
    m_mesh = mesh;
}


void Renderable::doDraw()
{
    if(m_mesh != nullptr)
        m_mesh->drawBuffers();
}


std::shared_ptr<rapidjson::Value> Renderable::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    std::shared_ptr<rapidjson::Value> obj = scene::SceneGraphNode::SerializeProp(al);
    obj->AddMember(rapidjson::Value("Show", al),
                   rapidjson::Value(m_show),
                   al);
    obj->AddMember(rapidjson::Value("Wired", al),
                   rapidjson::Value(m_wired),
                   al);


    utility::JSONHelper::SaveString(*(obj.get()),"Shader",m_render_shader->getName(),al);
    if(m_mesh != nullptr)
        obj->AddMember(rapidjson::Value("Mesh", al),*m_mesh->SerializeProp(al).get(),al);

    return obj;
}

void Renderable::LoadProp(const rapidjson::Value& value)
{
    SceneGraphNode::LoadProp(value);
    m_show = value["Show"].GetBool();
    m_wired = value["Wired"].GetBool();
    m_render_shader = dynamic_cast<ressource::Shader*>(ressource::RessourceManager::Get(value["Shader"].GetString()).get());
    //TODO Mesh
    if(value.HasMember("Mesh"))
    {
        std::string name = value["Mesh"]["File"].GetString();
        std::ifstream file(name, std::ios::binary);
        if (file.is_open())
        {
            //file.seekg(0, std::ios::beg);
            unsigned int  mg_nbr = 0;
            utility::binary_read(file,mg_nbr);
            //file >> mg_nbr;
            //file >> mg_nbr;
            switch (mg_nbr)
            {
            case 0:
                m_mesh = ressource::Mesh<render::vertex3D>::Load(name);
                break;
            case 1:
                m_mesh = ressource::Mesh<animation::Vertex3DSkin>::Load(name);
                break;
            case 2:
                m_mesh = ressource::Mesh<ressource::PCVertex>::Load(name);
                break;
            default:
                break;
            }
            file.close();
        }
    }
}

}
}
