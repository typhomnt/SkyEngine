#ifndef COLORSHADEDOBJECT_H
#define COLORSHADEDOBJECT_H

#include "gameobject3d.h"

#include <glm/glm.hpp>

#include "../ressources/material.h"
#include "../ressources/phongmaterial.h"


namespace skyengine
{

namespace scene
{

class ColorShadedObject: public GameObject3D
{
public:
    ColorShadedObject(const std::string& name, scene::SceneGraph *owner = nullptr,
                      const unsigned int type = 0,
                      const glm::mat4& world_matrix = glm::mat4(1.0f),
                      const std::shared_ptr<ressource::Material>& material = ressource::PhongMaterial::Default,
                      const glm::vec4& color = glm::vec4(1.0,0,0,1.0));

    void setShaderUniforms();

private:
    std::shared_ptr<ressource::Material> m_material;
    glm::vec4 m_color;

};

}
}
#endif // COLORSHADEDOBJECT_H
