/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef ABSTRACTSLATEMANAGER_H
#define ABSTRACTSLATEMANAGER_H

#include "core/listener.h"
#include "utils/singleton.h"
#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <memory>
#include "AABBox.h"
#include "ISKN_API.h"


namespace ext_dvc
{

namespace slate
{


#define SLATE_MEAN_FREQ 1.0f/144.0f

class StatusEvent : public  skyengine::core::Listenable<StatusEvent>
{
public:


    StatusEvent() {}
    virtual ~StatusEvent() {}

    virtual void send(int battery, int inCharge) { battery_ = battery; inCharge_ = inCharge;  notifyListeners(); }

    int battery() const {return battery_;}
    int inCharge() const {return inCharge_;}

protected:
    int battery_;
    int inCharge_;
};

class DescriptionEvent : public  skyengine::core::Listenable<DescriptionEvent>
{
public:
    DescriptionEvent() {}
    virtual ~DescriptionEvent() {}

    virtual void send(const std::string &deviceName, const std::string &firmwareVersion, const glm::vec2 &slateSize, const AABBox &activeZone)
    { deviceName_ = deviceName; firmwareVersion_ = firmwareVersion; slateSize_ = slateSize; activeZone_ = activeZone;   notifyListeners(); }

    const std::string& deviceName() const {return deviceName_;}
    const std::string& firmwareVersion() const {return firmwareVersion_;}
    const glm::vec2& slateSize() const {return slateSize_;}
    const AABBox& activeZone() const {return activeZone_;}

protected:
    std::string deviceName_;
    std::string firmwareVersion_;
    glm::vec2 slateSize_;
    AABBox activeZone_;
};

class SoftwareEvent : public  skyengine::core::Listenable<SoftwareEvent>
{
public:
    SoftwareEvent() {}
    virtual ~SoftwareEvent() {}

    virtual void send(int objectId, unsigned char softwareEventType) { objectId_ = objectId; softwareEventType_ = softwareEventType;  notifyListeners(); }

    int objectId() const {return objectId_;}
    int softwareEventType() const {return softwareEventType_;}

protected:
    int objectId_;
    int softwareEventType_;
};

class Pen2DEvent : public  skyengine::core::Listenable<Pen2DEvent>
{
public:
    Pen2DEvent() {}
    virtual ~Pen2DEvent() {}

    virtual void send(const glm::vec2 &position, const glm::vec2 &rotation, bool touch) { position_ = position; rotation_ = rotation; touch_ = touch ;  notifyListeners(); }

    const glm::vec2 & position() const {return position_;}
    const glm::vec2 & rotation() const {return rotation_;}
    bool touch() const {return touch_;}

protected:
    glm::vec2  position_;
    glm::vec2  rotation_;
    bool touch_;
};

class Pen3DEvent : public  skyengine::core::Listenable<Pen3DEvent>
{
public:
    Pen3DEvent() {}
    virtual ~Pen3DEvent() {}

    virtual void send(const glm::vec3 &position, const glm::vec2 &rotation, float zPaper, bool touch) { position_ = position; rotation_ = rotation; zPaper_ = zPaper; touch_ = touch; notifyListeners(); }

    const glm::vec3  &position() const {return position_;}
    const glm::vec2  &rotation() const {return rotation_;}
    float zPaper() const {return zPaper_;}
    bool touch() const {return touch_;}

protected:
    glm::vec3 position_;
    glm::vec2 rotation_;
    float zPaper_;
    bool touch_;
};

class Object3DEvent : public  skyengine::core::Listenable<Object3DEvent>
{
public:
    Object3DEvent() {}
    virtual ~Object3DEvent() {}

    virtual void send(const glm::vec3 &position, const glm::vec2 &rotation) { position_ = position; rotation_ = rotation;  notifyListeners(); }


    const glm::vec3  &position() const {return position_;}
    const glm::vec2  &rotation() const {return rotation_;}

protected:
    glm::vec3 position_;
    glm::vec2 rotation_;
};

class AccelerometerEvent : public  skyengine::core::Listenable<AccelerometerEvent>
{
public:
    AccelerometerEvent() {}
    virtual ~AccelerometerEvent() {}

    virtual void send(const glm::vec3 &accelerometer) { accelerometer_ = accelerometer; notifyListeners(); }

    const glm::vec3 &accelerometer() const {return accelerometer_;}

protected:
    glm::vec3 accelerometer_;
};

class HardwareEvent : public  skyengine::core::Listenable<HardwareEvent>
{
public:
    HardwareEvent() {}
    virtual ~HardwareEvent() {}

    virtual void send(unsigned char hardwareEventType) { hardwareEventType_ = hardwareEventType;  notifyListeners(); }

    unsigned char hardwareEventType() const {return hardwareEventType_;}

protected:
    unsigned char hardwareEventType_;

};

class DiskStatusEvent : public  skyengine::core::Listenable<DiskStatusEvent>
{
public:
    DiskStatusEvent() {}
    virtual ~DiskStatusEvent() {}

    virtual void send(unsigned int freeDiskSpace, int filesNumber, bool diskOnline, bool diskReadable, bool diskWritable,
                      bool diskChanged, bool sdCardInserted)
    {freeDiskSpace_ = freeDiskSpace; filesNumber_ = filesNumber;  diskOnline_ = diskOnline; diskReadable_ = diskReadable;
        diskWritable_ = diskWritable; diskChanged_ = diskChanged; sdCardInserted_ = sdCardInserted; notifyListeners(); }

    unsigned int freeDiskSpace()const {return freeDiskSpace_;}
    int filesNumber()const {return filesNumber_;}
    bool diskOnline()const {return diskOnline_;}
    bool diskReadable()const {return diskReadable_;}
    bool diskWritable()const {return diskWritable_;}
    bool diskChanged()const {return diskChanged_;}
    bool sdCardInserted()const {return sdCardInserted_;}

protected:

    unsigned int freeDiskSpace_;
    int filesNumber_;
    bool diskOnline_;
    bool diskReadable_;
    bool diskWritable_;
    bool diskChanged_;
    bool sdCardInserted_;
};

class FileDescriptorEvent : public  skyengine::core::Listenable<FileDescriptorEvent>
{
public:
    FileDescriptorEvent() {}
    virtual ~FileDescriptorEvent() {}

    virtual void send(unsigned int size, int id, const time_t &creationDate) { size_ = size; id_ = id; creationDate_ = creationDate;  notifyListeners(); }

    unsigned int size() const {return size_;}
    int id() const {return id_;}
    const time_t &creationDate() const {return creationDate_;}

protected:
    unsigned int size_;
    int id_;
    time_t creationDate_;
};

class FileContentEvent : public  skyengine::core::Listenable<FileContentEvent>
{
public:
    FileContentEvent() {}
    virtual ~FileContentEvent() {}

    virtual void send(unsigned short int id, unsigned char status, unsigned int numPart, unsigned int nbParts, unsigned short int size, char* buffer, int bufferSize)
    { id_ = id; status_ = status; numPart_ = numPart; nbParts_ =  nbParts; size_ = size;  buffer_ = buffer; bufferSize_ = bufferSize;  notifyListeners(); }


    unsigned short int id() const {return id_;}
    unsigned char status() const {return status_;}
    unsigned int numPart() const {return numPart_;}
    unsigned int nbParts() const {return nbParts_;}
    unsigned short int size() const {return size_;}
    char* buffer() const {return buffer_;}
    int bufferSize() const {return bufferSize_;}
protected:
    unsigned short int id_;
    unsigned char status_;
    unsigned int numPart_;
    unsigned int nbParts_;
    unsigned short int size_;
    char* buffer_;
    int bufferSize_;
};

class FileContentsEvent : public  skyengine::core::Listenable<FileContentsEvent>
{
public:
    FileContentsEvent() {}
    virtual ~FileContentsEvent() {}

    virtual void send(const std::vector<unsigned short int> &ids, const std::vector<unsigned char> &status, const std::vector<unsigned int>& numPart, const std::vector<unsigned int>& nbParts, const std::vector<unsigned short int>& size, const std::vector<char*>& buffer, const std::vector<int>& bufferSize)
    { ids_ = ids; status_ = status; numPart_ = numPart; nbParts_ = nbParts; size_ = size; buffer_= buffer; bufferSize_ = bufferSize;  notifyListeners(); }

    const std::vector<unsigned short int> &ids()const {return ids_;}
    const std::vector<unsigned char> &status()const {return status_;}
    const std::vector<unsigned int>& numPart()const {return numPart_;}
    const std::vector<unsigned int>& nbParts()const {return nbParts_;}
    const std::vector<unsigned short int>& size()const {return size_;}
    const std::vector<char*>& buffer()const {return buffer_;}
    const std::vector<int>& bufferSize()const {return bufferSize_;}

protected:
    std::vector<unsigned short int> ids_;
    std::vector<unsigned char> status_;
    std::vector<unsigned int> numPart_;
    std::vector<unsigned int> nbParts_;
    std::vector<unsigned short int> size_;
    std::vector<char*> buffer_;
    std::vector<int> bufferSize_;
};

class FileOperationEvent : public  skyengine::core::Listenable<FileOperationEvent>
{
public:
    FileOperationEvent() {}
    virtual ~FileOperationEvent() {}

    virtual void send(unsigned long long operation, unsigned char status) { operation_ = operation; status_ = status;  notifyListeners(); }

    unsigned long long operation() const {return operation_;}
    unsigned char status() const {return status_;}

protected:
    unsigned long long operation_;
    unsigned char status_;
};

class ErrorEvent : public  skyengine::core::Listenable<ErrorEvent>
{
public:
    ErrorEvent() {}
    virtual ~ErrorEvent() {}

    virtual void send(unsigned char errorCode, int parameter) { errorCode_ = errorCode; parameter_= parameter;  notifyListeners(); }

    unsigned char errorCode() const {return errorCode_;}
    int parameter() const {return parameter_;}

protected:
    unsigned char errorCode_;
    int parameter_;
};

class SlateManager : public skyengine::utility::Singleton<SlateManager>, skyengine::core::Listenable<SlateManager>, ISKN_API::Listener
{
public:
    static void initSingletonSlate();
    static bool isConnected();

    unsigned short int client_width() { return m_active_box.width(); }
    unsigned short int client_height() { return m_active_box.height(); }
    unsigned short int client_left() { return m_active_box.xmin(); }
    unsigned short int client_top() { return m_active_box.ymin(); }

    const AABBox &active_bounding_box() { return m_active_box; }
    const glm::vec2 &slate_size() { return m_slate_size; }

protected:
    SlateManager();

    virtual ~SlateManager() {}

    virtual void initSlate();

    void processEvent(ISKN_API::Event &e, unsigned int timecode);
    void connectionStatusChanged(bool connected);


public:

    std::shared_ptr<StatusEvent> m_status_event;
    std::shared_ptr<DescriptionEvent> m_description_event;
    std::shared_ptr<SoftwareEvent> m_software_event;
    std::shared_ptr<Pen2DEvent> m_pen2d_event;
    std::shared_ptr<Pen3DEvent> m_pen3d_event;
    std::shared_ptr<Object3DEvent> m_object3d_event;
    std::shared_ptr<AccelerometerEvent> m_accelerometer_event;
    std::shared_ptr<HardwareEvent> m_hardware_event;
    std::shared_ptr<DiskStatusEvent> m_diskstatus_event;
    std::shared_ptr<FileDescriptorEvent> m_filedescriptor_event;
    std::shared_ptr<FileContentEvent> m_filecontent_event;
    std::shared_ptr<FileContentsEvent> m_filecontents_event;
    std::shared_ptr<FileOperationEvent> m_fileoperation_event;
    std::shared_ptr<ErrorEvent> m_error_event;

protected:

    // ==================================
    // Slate info
    // ==================================
    AABBox m_active_box;
    glm::vec2 m_slate_size;

    ISKN_API::SlateManager  *m_manager;
    ISKN_API::Device        *m_slate;

    friend class skyengine::utility::Singleton<SlateManager>;
};

}

}

#endif // ABSTRACTSLATEMANAGER_H
