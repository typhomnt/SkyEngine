#ifndef GAMEOBJECTSKINNED_H
#define GAMEOBJECTSKINNED_H

#include "utils/skyengine_dll.h"


#include <map>
#include "scene/gameobject3d.h"
#include "vertex3dskin.h"
#include "skeletalanimation.h"
#include "bone3d.h"
#include "skeleton.h"

#ifdef __GUI__
namespace module
{

namespace gui
{
class ActionPlayer;
}
}
#endif

namespace skyengine
{

namespace animation
{
class SKYENGINE_API GameObjectSkinned :public scene::GameObject3D
{
#ifdef __GUI__
    friend class module::gui::ActionPlayer;
#endif

public:

    GameObjectSkinned(const std::string& name, scene::SceneGraph *owner = nullptr,
                      const unsigned int type = Type::GameObjectSkinned,
                      const glm::mat4& world_matrix = glm::mat4(1.0f));
    void init();
    void BeforeUpdate();
    SkeletalAnimation* getAnimationPt(const std::string& animation_name);
    void addAnimation(SkeletalAnimation *animation);
    void setSkeleton(Skeleton* skeleton);
    const Skeleton& getConstSkeleton() const;
    Skeleton& getRefSkeleton();
    Skeleton* getPSkeleton();
    inline glm::mat4 getBoneTransform(unsigned int bone_id) const;
    void setBoneTransforms(glm::mat4* transforms);
    virtual std::shared_ptr<scene::SceneGraphNode> Duplicate();
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);

    virtual std::string getTag() {return "GmObjSk";}

private:

    Skeleton* m_skeleton;
    scene::NodeParameter<glm::mat4*>* bone_transforms;
    std::map<std::string,SkeletalAnimation*> animations;
};


inline glm::mat4 GameObjectSkinned::getBoneTransform(unsigned int bone_id) const
{
    return bone_transforms->value[bone_id];
}

}
}
#endif // GAMEOBJECTSKINNED_H
