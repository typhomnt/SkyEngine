/**
Copyright (c) 2017, INPG
Main authors : Antoine Begault, Cédric Zanni, Ulysse Vimont, Guillaume Cordonnier, Maxime Garcia
All rights reserved.

Redistribution and use in source and binary forms of the CORE part (see bellow for a complete list of what is not included in this license), with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the INPG nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TRAJECTORYMANIPULATOR_H
#define TRAJECTORYMANIPULATOR_H

// Dependencies: core

#include "animation/animatedbone3d.h"
#include "animation/skeletalanimator.h"
#include "animation/gameobjectskinned.h"
#include "geometry/trajectorynode.h"
#include "SlateManagerListener.h"
#include "core/skyengine.h"
#include "ActionDetector/motionmanager.h"
#include "StoryTelling/motionscenario.h"
#include "animation/gameobjectanimation.h"
#include "physics/physicsmanager.h"
#include "utils/state.h"


namespace module {
namespace doodle {

class TrajectoryManipulator :  public ext_dvc::slate::SlateManagerListener
{
public:
    TrajectoryManipulator(skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D> *node, ext_dvc::slate::SlateManager *manager);

    virtual ~TrajectoryManipulator();


    virtual void listened(const ext_dvc::slate::HardwareEvent* e) ;
    virtual void listened(const ext_dvc::slate::Object3DEvent* e) ;

protected:
    skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D> *m_node;


    unsigned int m_start_timestamp;
};

class DoodleManipulator : public ext_dvc::slate::SlateManagerListener
{
protected:

    class DoodleMachine : public skyengine::utility::StateMachine
    {
        friend class DoodleManipulator;
    public:
        DoodleMachine(DoodleManipulator* manipulator) : m_manipulator(manipulator)
        {

        }
    protected:
        DoodleManipulator* m_manipulator;
    };

    class InitState : public skyengine::utility::State
    {
    public:
        InitState(skyengine::utility::StateMachine* context):
            skyengine::utility::State(context,true)
        {

        }

        void handleState();
        void printState()
        {
            skyengine::utility::Logger::Debug("State : Init");
        }
    };

    class ComputeTransfer : public skyengine::utility::State
    {
    public:
        ComputeTransfer(skyengine::utility::StateMachine* context):
            skyengine::utility::State(context)
        {

        }

        void handleState();
        void printState()
        {
            skyengine::utility::Logger::Debug("State : Compute Transfer");
        }
    };

    class Drawing : public skyengine::utility::State
    {
    public:
        Drawing(skyengine::utility::StateMachine* context):
            skyengine::utility::State(context)
        {

        }

        void handleState();
        void printState()
        {
            skyengine::utility::Logger::Debug("State : Drawing");
        }
    };

    class PushAnimation : public skyengine::utility::State
    {
    public:
        PushAnimation(skyengine::utility::StateMachine* context):
            skyengine::utility::State(context,true)
        {

        }

        void handleState();
        void printState()
        {
            skyengine::utility::Logger::Debug("State : Push animation");
        }
    };

    class InCycle : public skyengine::utility::State
    {
    public:
        InCycle(skyengine::utility::StateMachine* context):
            skyengine::utility::State(context,true)
        {

        }

        void handleState();
        void printState()
        {
            skyengine::utility::Logger::Debug("State : In Cycle");
        }
    };

    class NotInCycle : public skyengine::utility::State
    {
    public:
        NotInCycle(skyengine::utility::StateMachine* context):
            skyengine::utility::State(context,true)
        {

        }

        void handleState();
        void printState()
        {
            skyengine::utility::Logger::Debug("State : Not In Cycle");
        }
    };

public:
    DoodleManipulator(skyengine::animation::GameObjectSkinned *node,skyengine::core::SkyEngine * engine,skyengine::animation::SkeletalAnimator* animator ,ext_dvc::slate::SlateManager *manager);

    virtual ~DoodleManipulator(){}
    //Listen SlateButton and Pen----------------
    virtual void listened(const ext_dvc::slate::HardwareEvent* e);
    virtual void listened(const ext_dvc::slate::Object3DEvent* e);
    //------------------------------------------

    //External Functions------------------------
    void setAnimator(skyengine::animation::SkeletalAnimator* animator);
    void ImportActions(const std::string& action_path);
    void LoadCurveFile(const char* file_name);
    void SaveCurve(const char* file_name);
    //------------------------------------------

    //Getters-----------------------------------
    inline std::vector<motionDoodle::Vector7D> getLastCurve() const;
    inline const std::vector<motionDoodle::Vector7D>& getLastDoodleVec() const;
    inline std::shared_ptr<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>> getDrawingNode();
    inline const std::string& getLastAction() const;
    inline const std::string& getLastPushedToken() const;
    inline float getLastClockTime() const;
    inline motionDoodle::MotionScenario* getScenario();
    inline motionDoodle::MotionManager* getManager();
    inline bool* getDrawingMode();
    inline bool* getNoClamp();
    inline float getCurrentFreq() const;
    //------------------------------------------

    void UpdateFromDrawing();

protected:
    skyengine::animation::GameObjectSkinned *m_node;
    std::shared_ptr<motionDoodle::MotionManager> m_doodle_manager;
    skyengine::core::SkyEngine* m_engine;
    skyengine::animation::SkeletalAnimator* m_animator;
    std::shared_ptr<skyengine::animation::GameObjectAnimation> m_path_animation;
    std::shared_ptr<motionDoodle::MotionScenario> m_doodle_scenario;
    skyengine::scene::GameObject3D* m_ground_cube;
    bool m_no_clamp;
    bool m_drawing_mode;
    std::shared_ptr<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>> m_drawing_node;
    std::shared_ptr<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>> m_frame_node;
    DoodleMachine m_doodle_machine;
    //time changing properties-----------------------
    float m_register_time;
    std::string m_last_played_action;
    std::string m_lasted_pushed_token;
    float m_last_event_time;
    float m_last_accepted_time;
    float m_current_freq;
    unsigned int m_action_count;
    std::vector<skyengine::animation::KeyPose3D> m_last_curve;
    std::vector<motionDoodle::Vector7D> m_doodle_curve;
    unsigned int m_last_pushed_nbr;
    motionDoodle::Vector7D m_last_accepted_point;
    bool m_is_in_cycle;
    //-----------------------------------------------

    skyengine::geometry::GeometryCurve<skyengine::animation::TimedParameter<glm::vec3> > DoodleTrajToCurve(const std::vector<motionDoodle::Vector7D>& path, const glm::vec3 pos_shift = glm::vec3());
    //returns true if pushed a new action
    bool UpdateDoodle(const skyengine::animation::KeyPose3D& new_pose);
    std::shared_ptr<skyengine::animation::SkeletalAnimationModifier> createSkelModifier(const motionDoodle::MotionAction& action,float duration);
    void AdapatUniformKeyFramCurve(skyengine::animation::AnimatedPoseTrajectory& input_c, float adapted_duration)
    {
        assert(input_c .GetSize() > 1);
        float total_time = input_c.GetEnd().time- input_c.GetStart().time;
        float time_sc = (adapted_duration/total_time);

         for(unsigned int i = 1 ; i < input_c.GetSize(); ++i)
             input_c.GetPoint(i).time =  input_c.GetPoint(0).time + (input_c.GetPoint(i).time - input_c.GetPoint(0).time)*time_sc;

    }

    void ManageMachine();

};

inline std::vector<motionDoodle::Vector7D> DoodleManipulator::getLastCurve() const
{
    return m_doodle_curve;
    /*std::vector<skyengine::animation::TimedParameter<glm::vec3>> traj;
    if(!m_drawing_mode)
        for(const skyengine::animation::KeyPose3D& pose : m_last_curve)
            traj.push_back(skyengine::animation::TimedParameter<glm::vec3>(pose.value.position,pose.time));
    else
        for(const skyengine::animation::KeyPose3D& pose : m_drawing_node->GetPoints())
            traj.push_back(skyengine::animation::TimedParameter<glm::vec3>(pose.value.position,pose.time));
    return traj;*/
}

inline const std::vector<motionDoodle::Vector7D>& DoodleManipulator::getLastDoodleVec() const
{
    return m_doodle_curve;
}

inline const std::string& DoodleManipulator::getLastAction() const
{
    return m_last_played_action;
}
inline float DoodleManipulator::getLastClockTime() const
{
    return m_last_event_time;
}

inline const std::string& DoodleManipulator::getLastPushedToken() const
{
    return m_lasted_pushed_token;
}

inline motionDoodle::MotionScenario* DoodleManipulator::getScenario()
{
    return m_doodle_scenario.get();
}

inline motionDoodle::MotionManager* DoodleManipulator::getManager()
{
    return m_doodle_manager.get();
}

inline bool* DoodleManipulator::getDrawingMode()
{
    return &m_drawing_mode;
}

inline bool* DoodleManipulator::getNoClamp()
{
    return &m_no_clamp;
}

inline float DoodleManipulator::getCurrentFreq() const
{
    return m_current_freq;
}

inline std::shared_ptr<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>> DoodleManipulator::getDrawingNode()
{
    return m_drawing_node;
}



class NodeManipulator :  public ext_dvc::slate::SlateManagerListener
{
public:
    NodeManipulator(skyengine::scene::SceneGraphNode *node, ext_dvc::slate::SlateManager *manager);

    virtual ~NodeManipulator(){}


    virtual void listened(const ext_dvc::slate::HardwareEvent* e) ;
    virtual void listened(const ext_dvc::slate::Object3DEvent* e) ;

protected:
    skyengine::scene::SceneGraphNode *m_node;

    unsigned int m_start_timestamp;
};

}

}

#endif // TRAJECTORYMANIPULATOR_H
