#ifndef CAMERAMANIPULATOR_H
#define CAMERAMANIPULATOR_H

#include "utils/skyengine_dll.h"

#include "core/eventlisteners/userinputlistener.h"
#include "core/manipulator.h"
#include "core/openglwindow.h"
#include "camera.h"


namespace skyengine
{

namespace scene
{

class SKYENGINE_API CameraManipulator : public core::Manipulator, public core::UserInputListener,public core::Listener<core::RenderSurface::ResizeEvent>
{
public:
    CameraManipulator(core::OpenGLWindow*, const std::shared_ptr<core::UserInputManager>&,
                      const std::shared_ptr<core::RenderSurface::ResizeEvent>&,
                      const std::shared_ptr<Camera>& = nullptr);
    virtual ~CameraManipulator() {}

    void attachCamera(std::shared_ptr<Camera> cam) {m_camera = cam;}

    virtual void update(double t, double dt);

    virtual void listened(const core::KeyEvent*);
    virtual void listened(const core::MouseMoveEvent*);
    virtual void listened(const core::MouseButtonEvent*);
    virtual void listened(const core::MouseScrollEvent*);
    virtual void listened(const core::JoystickAxisEvent*);
    virtual void listened(const core::JoystickButtonEvent*);
    virtual void listened(const core::RenderSurface::ResizeEvent*);

protected:
    std::weak_ptr<Camera> m_camera;

    core::OpenGLWindow* m_window;

    float m_move_speed;
    float m_sensitivity;

    glm::vec3 m_displacement;
    glm::vec2 m_mouse_delta;
    glm::vec2 m_mouse_prev;
    glm::vec2 m_mouse_cur;

    bool m_first_mouse_move;
    bool m_left_button_pressed;
};

} // namespace scene

} // namespace skyengine

#endif // CAMERAMANIPULATOR_H
