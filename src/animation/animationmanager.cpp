#include "animationmanager.h"
#include "skeletalanimation.h"
namespace skyengine
{

namespace animation
{
AnimationManager::AnimationManager():
    m_animator_count(0)
{

}

unsigned int AnimationManager::AddAnimator(std::shared_ptr<Animator> animator)
{
    AnimationManager* manager = GetSingleton();
    return manager->AddAnimator(animator);
}

unsigned int AnimationManager::addAnimator(std::shared_ptr<Animator> animator)
{
    utility::MapInsert(m_animators,m_animator_count,animator);
    #ifdef __GUI__
    animator->setId(m_animator_count);
    #endif
    m_animator_count++;
    return m_animator_count - 1;
}

}
}
