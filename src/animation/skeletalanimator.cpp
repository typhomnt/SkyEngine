#include "skeletalanimator.h"
#include "skeletalanimationmodifier.h"
#include "core/functioncalltask.h"
#include "geometry/geometryalgorithms.h"
#include "ressources/image.h"
#include "ressources/ressourcemanager.h"
#include "utils/interpolationfunction.h"

namespace skyengine
{

namespace animation
{

SkeletalAnimator::SkeletalAnimator() : Animator(), m_display_animation(true)
{
    addAnimationModifier(std::make_shared<SkeletalAnimationModifier>(),"Default");
}


void SkeletalAnimator::setAnimatedSkeleton(Skeleton* skeleton)
{
    m_animated_skeleton = skeleton;
}

//TODO Find the right file to put this.
std::map<float,KeyPose3D> getKeyPoseMap(const Bone3DTrajectory& trajectory)
{
    std::map<float,KeyPose3D> keyPose_map;
    for(auto& pose : trajectory.GetPoints())
        keyPose_map[pose.time] = pose;
    return keyPose_map;
}

void SkeletalAnimator::addAnimation(const std::string& element_name, float start_time, float end_time, Animation* animation, unsigned int layered_order, std::shared_ptr<geometry::AbstractTransformModifier> modifier, bool add_transition)
{
    addAnimation(element_name,start_time,end_time,animation,layered_order,std::vector<std::string>(),modifier,add_transition);
}

bool SkeletalAnimator::setElementModifier(const std::string& element_name, std::shared_ptr<geometry::AbstractTransformModifier> modifier)
{
    std::shared_ptr<SkeletalAnimationModifier> skel_modif = std::dynamic_pointer_cast<SkeletalAnimationModifier>(modifier);
    if(skel_modif != nullptr)
    {
        bool could_set = Animator::setElementModifier(element_name,modifier);
        skel_modif->setModifiedAnimation(dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[element_name]->modified_animation.get()));
        return could_set;
    }
    return false;
}

void SkeletalAnimator::addAnimation(const std::string &element_name, float start_time, float end_time, Animation* animation, unsigned int layered_order, const std::vector<std::string> &computed_bones, std::shared_ptr<geometry::AbstractTransformModifier> modifier,bool add_transition)
{    
    Animator::addAnimation(element_name,start_time,end_time,animation,layered_order,modifier,add_transition);

    if(modifier != nullptr)
    {
        m_composed_anim_els[element_name]->modified_animation = std::dynamic_pointer_cast<SkeletalAnimation>(animation->Duplicate());
        std::dynamic_pointer_cast<SkeletalAnimationModifier>(modifier)->setModifiedAnimation(dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[element_name]->modified_animation.get()));
        modifier->apply();
    }

    SkeletalAnimation* skel_anim = dynamic_cast<SkeletalAnimation*>(animation);
    if(skel_anim != nullptr)
    {
        if(computed_bones.size() == 0)
            for(auto& bone : skel_anim->getKeyGroups())
                m_animation_computed_bones[element_name].push_back(bone.first);
        else
            for(const std::string& bone_name: computed_bones)
                m_animation_computed_bones[element_name].push_back(bone_name);
    }
    else
        for(const std::string& bone_name: computed_bones)
            m_animation_computed_bones[element_name].push_back(bone_name);
}

void SkeletalAnimator::removeAnimation(const std::string& animation_name)
{
    Animator::removeAnimation(animation_name);
    utility::MapRemove<std::string,std::vector<std::string>>(m_animation_computed_bones,animation_name);
}


void SkeletalAnimator::computeAnimation(float time_stamp)
{
    //TODO do not compute animation at time stamp but at time stamp - start
    //Step1 compute read animation
    if(m_composed_anim_els.size() > 0)
    {
        Animator::computeAnimation(time_stamp);

        glm::mat4* final_animation = new glm::mat4[MAX_BONES];
        if(m_composed_skeleton != nullptr)
            ressource::RessourceManager::Remove(m_composed_skeleton->getName());
        m_composed_skeleton = std::dynamic_pointer_cast<Skeleton>(m_animated_skeleton->Duplicate());


        for(unsigned int i = 0 ; i < MAX_BONES ; i++)
            final_animation[i] = glm::mat4(1.0f);

        unsigned int nb_el_t = 0;
        for(auto& anim_el : m_animation_layering_order)
            if(anim_el.second == nullptr || dynamic_cast<SkeletalAnimation*>(anim_el.second->animation) != nullptr)
                nb_el_t++;


        for(auto& anim_l : m_animation_layering_order)
        {
            unsigned int lay_nbr = anim_l.first;

            if(anim_l.second == nullptr)
            {

                assert(dynamic_cast<SkeletalAnimation*>(m_played_trans[lay_nbr]->source_anim->animation) != nullptr
                        && dynamic_cast<SkeletalAnimation*>(m_played_trans[lay_nbr]->target_anim->animation) != nullptr);
                float blend_factor = (time_stamp - m_played_trans[lay_nbr]->start_time)/(m_played_trans[lay_nbr]->end_time - m_played_trans[lay_nbr]->start_time);
                std::string bone_comp_anim;
                if(blend_factor < 0.5)
                    bone_comp_anim = m_played_trans[lay_nbr]->source_anim->name;
                else
                    bone_comp_anim = m_played_trans[lay_nbr]->target_anim->name;
                for(const std::string& computed_bone: m_animation_computed_bones[bone_comp_anim])
                {
                    Bone3D bone_src;
                    Bone3D bone_trg;
                    if(m_composed_anim_els[m_played_trans[lay_nbr]->source_anim->name]->modified_animation != nullptr)
                        bone_src = dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[m_played_trans[lay_nbr]->source_anim->name]->modified_animation.get())->getLastPoseSkel()->getBone(computed_bone);
                    else
                        bone_src = dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[m_played_trans[lay_nbr]->source_anim->name]->animation)->getLastPoseSkel()->getBone(computed_bone);
                    if(m_composed_anim_els[m_played_trans[lay_nbr]->target_anim->name]->modified_animation != nullptr)
                        bone_trg = dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[m_played_trans[lay_nbr]->target_anim->name]->modified_animation.get())->getLastPoseSkel()->getBone(computed_bone);
                    else
                        bone_trg = dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[m_played_trans[lay_nbr]->target_anim->name]->animation)->getLastPoseSkel()->getBone(computed_bone);

                    m_composed_skeleton->getBonePt(computed_bone)->local_transform =
                            utility::InterpolationFunction<glm::mat4>::getLinearInterpolationFunc()->ComputeInterpolation(
                                TimedParameter<glm::mat4>(bone_src.local_transform,0)
                                ,TimedParameter<glm::mat4>(bone_trg.local_transform,0)
                                ,blend_factor)->value;
                    m_composed_skeleton->getBonePt(computed_bone)->bone_offset =
                            utility::InterpolationFunction<glm::mat4>::getLinearInterpolationFunc()->ComputeInterpolation(
                                TimedParameter<glm::mat4>(bone_src.bone_offset,0)
                                ,TimedParameter<glm::mat4>(bone_trg.bone_offset,0)
                                ,blend_factor)->value;
                }

            }
            else
            {
                for(const std::string& computed_bone: m_animation_computed_bones[anim_l.second->name])
                {
                    if(dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[anim_l.second->name]->animation) != nullptr)
                    {

                        Bone3D bone;
                        if(m_composed_anim_els[anim_l.second->name]->modified_animation != nullptr)
                            bone = dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[anim_l.second->name]->modified_animation.get())->getLastPoseSkel()->getBone(computed_bone);
                        else
                            bone = dynamic_cast<SkeletalAnimation*>(m_composed_anim_els[anim_l.second->name]->animation)->getLastPoseSkel()->getBone(computed_bone);
                        m_composed_skeleton->getBonePt(computed_bone)->local_transform = bone.local_transform;
                        m_composed_skeleton->getBonePt(computed_bone)->bone_offset = bone.bone_offset;
                    }
                    else
                    {
                        std::shared_ptr<TimedParameter<glm::mat4>> bone_key = std::dynamic_pointer_cast<TimedParameter<glm::mat4>>(m_composed_anim_els[anim_l.second->name]->animation->getLastComputedKey(computed_bone));
                        if(bone_key != nullptr)
                        {
                            //Propose operator replace or multiply (add)
                            m_composed_skeleton->getBonePt(computed_bone)->local_transform  *= bone_key->value;
                        }
                    }
                }
            }

        }


        m_composed_skeleton->updateHierarchicalTransforms();

        if(m_display_animation)
        {
            //New version
            if(nb_el_t >0)
                for(auto & bone : m_composed_skeleton->getBones())
                    if(!bone.second->is_modifier)
                        final_animation[bone.second->bone_ID] = bone.second->hierarchical_transform;

            //Search For all object linked to this skeleton
            for(auto node : m_scene_graph->getNodes())
            {
                std::shared_ptr<GameObjectSkinned> game_obj = std::dynamic_pointer_cast<GameObjectSkinned>(node.second);
                if(game_obj != nullptr)
                    if(game_obj->getPSkeleton()->getName() == m_animated_skeleton->getName())
                    {
                        game_obj->setBoneTransforms(final_animation);
                    }
            }
        }


    }

}

void SkeletalAnimator::playAnimation(core::SkyEngine* play_engine)
{
    play_engine->addTask(core::makeFunctionCallTask(&SkeletalAnimator::play,this),"Animator");
}

void SkeletalAnimator::play()
{
    if(m_time_stamp <= m_end_time)
    {
        //TODO update paly speed
        computeAnimation(m_time_stamp);

        float dt = m_play_speed;
        m_time_stamp +=  dt;
    }

    if(m_time_stamp > m_end_time && m_is_looping)
        m_time_stamp = m_start_time;
}

void SkeletalAnimator::updateMotionPaths()
{

    //Motion path clear
    clearMotionPaths();

    m_display_animation = false;
    std::unordered_map<std::string,Bone3DTrajectory> bone_traj;


    //First version working
    for(float f = m_start_time; f <= m_end_time ; f+=m_play_speed)
    {
        computeAnimation(f);

        for(auto& bone : m_composed_skeleton->getBones())
        {
            //TODO inverse offset
            bone_traj[bone.first].AddPoint(KeyPose3D(bone.second->global_pose,f));
        }
    }

    //Second version
    //bone_traj =

    for(auto& traj : bone_traj)
    {
        std::shared_ptr<geometry::TrajectoryNode<KeyPose3D>> traj_node = std::make_shared<geometry::TrajectoryNode<KeyPose3D>>(traj.first + "_trajectory",geometry::TrajectoryNode<animation::KeyPose3D>::SPHERE);
        traj_node->setTrajectory(traj.second);
        m_motion_paths.push_back(traj_node.get());
        //setCurve + push into scene
        m_scene_graph->addNode(traj_node);
        traj_node->setWorldMatrix(m_scene_graph->getNode(m_animated_skeleton->getName())->getWorldMatrix()/*m_animated_skeleton->getGlobalInverseTransform()*/);
    }

    m_display_animation = true;
}

void SkeletalAnimator::generateNeutralIddle(const std::string& element_name, float start_time, float end_time,const glm::vec3& laban_effort)
{

}

void SkeletalAnimator::saveCurrentCompoAsAnim(const std::string& anim_name)
{

}

void SkeletalAnimator::clear()
{
    Animator::clear();
    clearAnimatedCompBones();
    clearMotionPaths();
}

void SkeletalAnimator::clearAnimatedCompBones()
{
    m_animation_computed_bones.clear();
}

void SkeletalAnimator::clearMotionPaths()
{
    for(geometry::TrajectoryNode<KeyPose3D>* traj: m_motion_paths)
        m_scene_graph->deleteNode(traj->getName());
    m_motion_paths.clear();

}

}
}
