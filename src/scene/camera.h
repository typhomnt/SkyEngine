#ifndef CAMERA_H
#define CAMERA_H

#include "scenegraphnode.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "utils/skyengine_dll.h"


namespace skyengine
{

namespace scene
{

class SKYENGINE_API Camera : public SceneGraphNode
{
public:
    Camera(const std::string& name, SceneGraph *owner = nullptr,
           const unsigned int type = 0,
           const glm::mat4& world_matrix = glm::mat4(1.0f));

    const glm::vec3& getPosition() const { return m_position; }
    glm::vec3* getPositionPt() { return &m_position; }

    const glm::vec3& getDirection() const { return m_direction; }
    const glm::vec3& getCurrDirection() const { return m_curr_direction; }
    const glm::vec3& getUpVector() const { return m_up_vector; }

    const glm::mat4& getViewMatrix() const { return m_view_matrix; }
    glm::mat4* getViewMatrixPt() { return &m_view_matrix; }

    const glm::mat4& getProjectionMatrix() const { return m_projection_matrix; }
    glm::mat4* getProjectionMatrixPt() { return &m_projection_matrix; }

    float getPitch() const { return m_pitch; }
    float getYaw() const { return m_yaw; }
    float getFov() const { return m_fov; }
    float getRatio() const {return m_ratio;}

    void setPosition(const glm::vec3& position);
    void setDirection(const glm::vec3& direction);
    void setCurrDirection(const glm::vec3& curr_direction);
    void setUpVector(const glm::vec3& up_vector);
    void setPitch(float pitch);
    void setYaw(float yaw);
    void setFov(float fov);
    void setRatio(float ratio);

    void updateCameraWithCurrentAttributes();
    void resetCameraOrientation();

    //TODO move into a new class CameraNode
    virtual void setWorldMatrix(const glm::mat4& world_matrix);

    void setPositionDirectionFromWMatrix();
    void setWorldMatrixFromPositionDirection();

    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);
    virtual std::string getTag() {return std::string("Cam");}

private:
    glm::vec3 m_position;
    glm::vec3 m_direction;
    glm::vec3 m_curr_direction;
    glm::vec3 m_up_vector;

    float m_pitch;
    float m_yaw;

    float m_fov;
    float m_ratio;

    glm::mat4 m_projection_matrix;
    glm::mat4 m_view_matrix;

    void LoadProp(const rapidjson::Value& value) override;
};

} // namespace scene

} // namespace skyengine

#endif // CAMERA_H
