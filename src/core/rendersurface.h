#ifndef RENDERSURFACE_H
#define RENDERSURFACE_H

#include "utils/skyengine_dll.h"

#include <glm/glm.hpp>
#include "listener.h"

namespace skyengine
{

namespace core
{

// base class for each render surface that can be resized and bound
class SKYENGINE_API RenderSurface
{
public:
    class ResizeEvent : public Listenable<ResizeEvent>
    {
    public:
        ResizeEvent(){}
        virtual ~ResizeEvent(){}
        virtual void send(glm::ivec2 s){size_ = s; notifyListeners();}
        glm::ivec2 size() const{return size_;}
    protected:
        glm::ivec2 size_;
    };

    class MoveEvent : public Listenable<MoveEvent>
    {
    public:
        MoveEvent() : m_pos(0, 0) {}
        virtual ~MoveEvent() {}

        glm::ivec2 pos() const { return m_pos; }

        virtual void send(glm::ivec2 pos)
        {
            m_pos = pos;
            notifyListeners();
        }

    protected:
        glm::ivec2 m_pos;
    };

public:
    RenderSurface()
        : resizeEvent_{std::make_shared<ResizeEvent>()}
        , moveEvent_ { std::make_shared<MoveEvent>() }
    {}
    virtual ~RenderSurface(){}

    virtual glm::ivec2 getSize() const = 0;
    virtual void resize(glm::ivec2) = 0;

    virtual glm::ivec2 getPos() const = 0;
    virtual void move(glm::ivec2) = 0;

    virtual void bind() = 0;

    std::shared_ptr<ResizeEvent> resizeEvent() {return resizeEvent_;}
    std::shared_ptr<MoveEvent> moveEvent() { return moveEvent_; }

protected:
    std::shared_ptr<ResizeEvent> resizeEvent_;
    std::shared_ptr<MoveEvent> moveEvent_;
};

}
}

#endif // RENDERSURFACE_H
