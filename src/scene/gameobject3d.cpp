#include "gameobject3d.h"

#include "render/Vertex3D.h"
#include "ressources/phongmaterial.h"
#include "utils/utilities.h"
#include "utils/jsonhelper.h"

namespace skyengine
{

namespace scene
{


GameObject3D::GameObject3D(const std::string &name,
                           SceneGraph *owner,
                           const unsigned int type,
                           const glm::mat4 &world_matrix):
    Renderable::Renderable(name, owner, type, world_matrix),
    Updatable::Updatable(),
    m_material(std::make_shared<ressource::PhongMaterial>(*(ressource::PhongMaterial::Default.get())))
{
    addRenderShaderUniformVal("M",new scene::NodeParameter<glm::mat4*>("M",1,&m_world_matrix));
    setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("default_shader")).get());
}

GameObject3D::~GameObject3D()
{

}

void GameObject3D::addRenderShaderUniformVal(const std::string& param_name,baseNodeParameter* uniform_val)
{
    if(!uniformValueAlreadyExist(uniform_val->name))
        render_shader_uniform_values[param_name] = uniform_val;
    else
        std::cout << "Uniform value: " << uniform_val->name << " already exist, please use update function instead" << std::endl;
}



void GameObject3D::setShaderUniforms()
{
    if(m_material != nullptr)
        m_material->sendToShader(*m_render_shader);
    Renderable::setShaderUniforms();
}



bool GameObject3D::uniformValueAlreadyExist(const std::string& parameter_name) const
{

    auto parameter_it = render_shader_uniform_values.find(parameter_name);
    if(parameter_it == render_shader_uniform_values.end())
        return false;
    else
        return true;
}


void GameObject3D::setMaterial(const std::shared_ptr<ressource::Material>& material)
{
    m_material = material;  
}

void GameObject3D::setWorldMatrix(const glm::mat4& world_matrix)
{
    SceneGraphNode::setWorldMatrix(world_matrix);
     //dynamic_cast<NodeParameter<glm::mat4>*>(render_shader_uniform_values["M"])->value = getHierarchicalWorldMatrix();
}

void GameObject3D::setDefaultShaderUniforms()
{

}

std::shared_ptr<SceneGraphNode> GameObject3D::Duplicate()
{

    std::shared_ptr<GameObject3D> duplicate_node = std::make_shared<GameObject3D>(*this);
    duplicate_node->m_name = getDuplicateName(m_name);
    m_scene_graph->addNode(m_parent->getName(),duplicate_node);
    //TODO duplicate childs ???
    duplicate_node->m_mesh = std::dynamic_pointer_cast<ressource::AbstractMesh>(m_mesh->Duplicate());
    duplicate_node->m_render_shader = m_render_shader;
    return duplicate_node;
}

std::shared_ptr<rapidjson::Value> GameObject3D::SerializeProp(rapidjson::MemoryPoolAllocator<>&al)
{
    std::shared_ptr<rapidjson::Value> obj = render::Renderable::SerializeProp(al);
    if(m_material != nullptr)
        utility::JSONHelper::SaveString(*(obj.get()),"Material",m_material->getName(),al);
    return obj;
}

void GameObject3D::LoadProp(const rapidjson::Value& value)
{
    Renderable::LoadProp(value);

    if (value.HasMember("Material"))
    {
        auto materialRes = ressource::RessourceManager::Get(value["Material"].GetString());
        m_material = std::dynamic_pointer_cast<ressource::Material>(materialRes);
        assert(m_material);
    }
}

}
}
