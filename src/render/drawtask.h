#ifndef DRAWTASK_H
#define DRAWTASK_H

#include "utils/skyengine_dll.h"

#include "core/task.h"
#include "scene/scenegraph.h"
#include "scene/quadobject.h"
#include "scene/camera.h"
#include "render/renderable.h"
#include "core/framebuffer.h"
#include "ressources/shader.h"
#include  "ressources/lights.h"

#ifdef __GUI__
namespace module {
namespace gui {

class Viewport;
class DrawTaskEditor;

}
}
#endif // __GUI__


namespace skyengine
{

namespace render
{

class SKYENGINE_API DrawTask : public core::Task
{

#ifdef __GUI__
    friend class module::gui::Viewport;
    friend class module::gui::DrawTaskEditor;
#endif // __GUI__

public:
    DrawTask(core::SkyEngine* engine/*, glm::ivec2 win_size  TO BE REMOVED! */, std::shared_ptr<core::WindowEventManager> wem, bool is_forward = true);
    virtual ~DrawTask();
    void run();

    void setSceneGraph(scene::SceneGraph* scene_graph);
    void setFramebufferCamera(scene::Camera* cam, const std::string& fbo_name = "");

    void assignTextureToSampler(const std::string& fbo_name, const std::string& shader_name, const std::string& sampler_uniform, ressource::Texture* texture);
    void assignLightToShader(const std::string& fbo_name, const std::string& shader_name, ressource::Light* light);
    void addUniformValue(const std::string& fbo_name, const std::string& shader_name, scene::baseNodeParameter* uniform);
    void addShader(const std::string& shader_name);
    void addPostProcFBO(const std::string& fbo_name, const std::unordered_map<core::FrameBuffer::AttachmentMode, std::vector<unsigned int>, std::hash<int> > &attachents,
                        const std::string& shader_name, const std::string& input_fbo_name = ""
            , const std::unordered_map<core::FrameBuffer::AttachmentMode,std::string,std::hash<int>>& input_attachents  = std::unordered_map<core::FrameBuffer::AttachmentMode,std::string,std::hash<int>>() );

    void show();
    void hide();
private:

    scene::SceneGraph* m_scene_graph;

    std::map<GLuint,std::map<std::string,std::vector<render::Renderable*> > > m_fbo_shaders_renderables_map;
    std::map<GLuint,std::map<std::string,std::map<std::string,scene::baseNodeParameter*> > > m_fbo_shaders_uniform_map;
    std::map<GLuint,std::map<std::string,std::map<std::string,ressource::Texture*> > > m_fbo_shader_texture_unit;

    std::map<GLuint,std::map<std::string,std::map<std::string,ressource::Light*> > > m_fbo_shader_light;


    std::unordered_map<std::string,std::shared_ptr<core::FrameBuffer> > m_task_framebuffers;
    std::vector<std::string> m_task_shader;
    std::vector<std::shared_ptr<core::FrameBuffer>> m_task_fbo_order;
    bool m_is_forward;
    bool m_display;
    //For post-process effects
    scene::QuadObject m_post_proc_quad;

    //wem
    std::shared_ptr<core::WindowEventManager> m_wem;



};
}
}
#endif // DRAWTASK_H
