#ifndef VERTEX3D_H
#define VERTEX3D_H
#include <glm/glm.hpp>
#include <vector>
#include <iostream>
#include "ressources/bufferattrib.h"
namespace skyengine
{

namespace render
{
struct vertex3D
{
    glm::vec3 pos;
    glm::vec2 uvs;
    glm::vec3 normal;
    vertex3D();
    vertex3D(const glm::vec3 &position);
    vertex3D(const glm::vec3 &position, const glm::vec2 &texcoords);
    vertex3D(const glm::vec3 &position, const glm::vec2 &texcoords,
         const glm::vec3 &normal);
    const glm::vec3& position() const {return pos;}
    const glm::vec3& getNormal() const {return normal;}
    void setNormal(const glm::vec3& n) {normal = n;}
    static std::vector<ressource::BufferAttrib> getBufferAttribs();
    static size_t BufferSize();
    static unsigned int MagicNumber();
    static vertex3D readFromStream(std::ifstream& in_s);
    void writeStream(std::ostream& in_s);
};


}
}

#endif // VERTEX3D_H
