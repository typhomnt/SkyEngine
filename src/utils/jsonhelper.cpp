#include "jsonhelper.h"

#include <fstream>

#include <rapidjson/error/en.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/prettywriter.h>

#include "utils/logger.h"


namespace skyengine
{

namespace utility
{

bool JSONHelper::Load(const std::string& fileName, rapidjson::Document& outDocument)
{
    std::ifstream file(fileName);
    if (!file.is_open())
    {
        Logger::Error("JSONHelper::Load : failed to open file " + fileName);
        return false;
    }

    rapidjson::IStreamWrapper stream(file);
    outDocument.ParseStream(stream);

    if (outDocument.HasParseError())
    {
        Logger::Error("JSONHelper::Load : error while parsing file " + fileName);
        Logger::Error(rapidjson::GetParseError_En(outDocument.GetParseError()));
        return false;
    }

    return true;
}

bool JSONHelper::Save(const std::string& fileName, const rapidjson::Document& document)
{
    std::ofstream file(fileName);
    if (!file.is_open())
    {
        Logger::Error("JSONHelper::Load : failed to open file " + fileName);
        return false;
    }

    rapidjson::OStreamWrapper stream(file);
    rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(stream);
    document.Accept(writer);

    return true;
}

void JSONHelper::SaveString(rapidjson::Value& obj, const std::string& name,const std::string& value, rapidjson::MemoryPoolAllocator<>& al)
{
    obj.AddMember(
        rapidjson::Value(name.c_str(), al),
        rapidjson::Value(value.c_str(), al),
        al
    );
}

} // namespace utility

} // namespace skyengine
