#ifndef PBRMATERIAL_H
#define PBRMATERIAL_H

#include "utils/skyengine_dll.h"

#include "material.h"
#include "texture.h"

#include <memory>

#include <glm/glm.hpp>


namespace skyengine
{

namespace ressource
{
class PhongMaterial;

class SKYENGINE_API PBRMaterial : public Material
{
public:
    PBRMaterial(const std::string& name);

    PBRMaterial(const std::string& name,
                  const glm::vec3& albedo,
                  float metallic,
                  float roughness,
                  float ao
                  );

    const glm::vec3& getAlbedo() const { return m_albedo; }
    float getMetallic() const { return m_metallic; }
    float getRoughness() const { return m_roughness; }
    float getAo() const { return m_ao; }

    void setAlbedo(const glm::vec3& albedo) { m_albedo = albedo; }
    void setMetallic(float metallic) { m_metallic = metallic; }
    void setRoughness(float roughness) { m_roughness = roughness; }
    void setAo(float ao) { m_ao = ao; }

    virtual void sendToShader(const Shader& shader) const override;

    void addTexture(Texture* tex, unsigned int index = 0);


    /**
     * Usual materials
     */
    static std::shared_ptr<PBRMaterial> None;
    static std::shared_ptr<PBRMaterial> Default;

#ifdef __GUI__
    glm::vec3* getAlbedoP() { return &m_albedo; }
    float* getMetallicP() { return &m_metallic; }
    float* getRoughnessP() { return &m_roughness; }
    float* getAoP() { return &m_ao; }
#endif // __GUI__

    std::shared_ptr<PhongMaterial> ToPhongMat();

    virtual std::string getTag() {return "PBRMaterial";}
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<> &al);

private:
    glm::vec3 m_albedo;
    float m_metallic;
    float m_roughness;
    float m_ao;

    std::vector<Texture*> m_textures;

    bool LoadProp(const rapidjson::Value& value) override;

    std::vector<std::string> m_texNames;
    void FinishLoading() override;
};

} // namespace ressource

} // namespace skyengine
#endif // PBRMATERIAL_H
