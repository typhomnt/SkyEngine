#ifndef TRAJECTORYNODE_H
#define TRAJECTORYNODE_H

#include "geometrycurve.h"
#include "../scene/gameobject3d.h"
#include "geometryalgorithms.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include "meshmodifier.h"
#include "ressources/phongmaterial.h"
#include "ressources/ressourcemanager.h"
#include "ressources/pointcloud.h"

namespace skyengine
{

namespace geometry
{

template<typename Point_T>
class TrajectoryNode : public scene::GameObject3D
{
public:
    enum display_type{SPHERE, SQUARE, LINES, MESH};
    TrajectoryNode(const std::string& name, display_type d_type=SPHERE, scene::SceneGraph *owner = nullptr,
                   const unsigned int type = scene::SceneGraphNode::GameObject,
                   const glm::mat4& world_matrix = glm::mat4(1.0f)):
        scene::GameObject3D(name,owner,type,world_matrix), m_d_type(d_type), m_curve(std::make_shared<GeometryCurve<Point_T> >())
    {
        setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("default_shader")).get());
        setMaterial(ressource::PhongMaterial::Turquoise);
        switch (m_d_type)
        {
        case SPHERE:
            m_mesh = SpherePrimitive(10,10);
            MeshModifier::static_scale(glm::vec3(),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(m_mesh).get()),0.2f);
            //setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("instance_shader")).get());
            break;
        case SQUARE:
            m_mesh = CubePrimitive(1);
            //setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("instance_shader")).get());
            break;
        case LINES:
            m_mesh = std::make_shared<ressource::Mesh<ressource::PCVertex> >("Curve_mesh",GL_STATIC_DRAW,GL_LINE_STRIP);
            setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("line_shader")).get());            
            setMaterial(nullptr);
            break;
        default:
            break;
        }



    }

    TrajectoryNode(const std::string& name,ressource::Mesh<render::vertex3D>* mesh, scene::SceneGraph *owner = nullptr,
                   const unsigned int type = scene::SceneGraphNode::GameObject,
                   const glm::mat4& world_matrix = glm::mat4(1.0f));



    void setTrajectory(const GeometryCurve<Point_T>& curve);

    inline void AddPoint(const Point_T& point);
    inline void AddPoint(const Point_T &point, unsigned int index);
    void setPoints(const std::vector<Point_T>& points);
    inline unsigned int GetSize() const;
    inline Point_T& GetPoint(unsigned int index);
    inline const Point_T& GetPoint(unsigned int index) const;
    inline std::vector<Point_T>& GetPoints();
    inline const std::vector<Point_T>& GetPoints() const;
    inline Point_T& GetStart();
    inline const Point_T& GetStart() const;
    inline Point_T& GetEnd();
    inline const Point_T& GetEnd() const;
    inline const GeometryCurve<Point_T>& getCurve() const;
    virtual void clearTrajectory();

    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>&);
    virtual std::string getTag() {return "TrajNode";}


    virtual std::shared_ptr<SceneGraphNode> Duplicate();


protected:
    display_type m_d_type;
    std::shared_ptr<GeometryCurve<Point_T>> m_curve;

};

template<typename Point_T>
TrajectoryNode<Point_T>::TrajectoryNode(const std::string& name,ressource::Mesh<render::vertex3D>* mesh, scene::SceneGraph *owner,
               const unsigned int type,
               const glm::mat4& world_matrix):
    scene::GameObject3D(name,owner,type,world_matrix), m_d_type(MESH), m_curve(std::make_shared<GeometryCurve<Point_T> >())
{

    m_mesh = std::dynamic_pointer_cast<ressource::AbstractMesh>(mesh->Duplicate());
    setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("default_shader")).get());
    setMaterial(ressource::PhongMaterial::Turquoise);

}

template<typename Point_T>
void TrajectoryNode<Point_T>::setTrajectory(const GeometryCurve<Point_T>& curve)
{
    clearTrajectory();
    for(unsigned int i = 0; i < curve.GetSize() ; i++)
        AddPoint(curve.GetPoint(i));
}

template<typename Point_T>
void TrajectoryNode<Point_T>::AddPoint(const Point_T& point)
{
    if(m_curve != nullptr && m_curve->GetSize() != 0)
        AddPoint(point,m_curve->GetSize());
    else
        AddPoint(point,0);
}

template<typename Point_T>
void TrajectoryNode<Point_T>::clearTrajectory()
{
    if(m_curve != nullptr)
        m_curve->RemovePoints();
    if(m_mesh != nullptr)
    {
        if(m_d_type != LINES)
            std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(m_mesh)->clearTransforms();
        else
            std::dynamic_pointer_cast<ressource::Mesh<ressource::PCVertex> >(m_mesh)->clear();
    }
}

template<typename Point_T>
inline const GeometryCurve<Point_T>& TrajectoryNode<Point_T>::getCurve() const
{
    return *(m_curve.get());
}

template<typename Point_T>
inline unsigned int TrajectoryNode<Point_T>::GetSize() const
{
    return m_curve->GetSize();
}

template<typename Point_T>
inline Point_T& TrajectoryNode<Point_T>::GetEnd()
{
    return m_curve->GetEnd();
}

template<typename Point_T>
inline const Point_T& TrajectoryNode<Point_T>::GetEnd() const
{
    return m_curve->GetEnd();
}

template<typename Point_T>
inline Point_T& TrajectoryNode<Point_T>::GetStart()
{
    return m_curve->GetStart();
}

template<typename Point_T>
inline const Point_T& TrajectoryNode<Point_T>::GetStart() const
{
    return m_curve->GetStart();
}

template<typename Point_T>
inline const Point_T& TrajectoryNode<Point_T>::GetPoint(unsigned int index) const
{
    return m_curve->GetPoint(index);
}

template<typename Point_T>
inline Point_T& TrajectoryNode<Point_T>::GetPoint(unsigned int index)
{
    return m_curve->GetPoint(index);
}
template<typename Point_T>
inline std::vector<Point_T>& TrajectoryNode<Point_T>::GetPoints()
{
    return m_curve->GetPoints();
}

template<typename Point_T>
inline const std::vector<Point_T>& TrajectoryNode<Point_T>::GetPoints() const
{
    return m_curve->GetPoints();
}

template<typename Point_T>
inline std::shared_ptr<rapidjson::Value> TrajectoryNode<Point_T>::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    std::shared_ptr<rapidjson::Value> obj = scene::GameObject3D::SerializeProp(al);

    return obj;
}

template<typename Point_T>
void TrajectoryNode<Point_T>::AddPoint(const Point_T &point, unsigned int index)
{             
    m_curve->AddPoint(point,index);
    //TODO Correct add vertex at right position for line rendering
    std::shared_ptr<ressource::Mesh<render::vertex3D>> mesh = std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(m_mesh);
    switch (m_d_type)
    {
    case SPHERE:
    {
        setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("instance_shader")).get());
        mesh->setInstanceNumber(m_curve->GetSize());
        mesh->addTransform(utility::Tmat4(glm::translate(glm::mat4(1.0f),point.translation())));
        return;
    }
    case LINES:
    {
        glm::vec3 position = point.translation();
        std::dynamic_pointer_cast<ressource::Mesh<ressource::PCVertex> >(m_mesh)->addVertex(index,ressource::PCVertex(position));
        return;
    }
    case MESH:
    {
        setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("instance_shader")).get());
        mesh->setInstanceNumber(m_curve->GetSize());
        mesh->addTransform(utility::Tmat4(glm::translate(glm::mat4(1.0f),point.translation())*glm::toMat4(point.rotation())));
        return;
    }
    default:
        return;
    }
}

template<typename Point_T>
void TrajectoryNode<Point_T>::setPoints(const std::vector<Point_T>& points)
{
    clearTrajectory();
    for(const Point_T& point : points)
        AddPoint(point);

}

template<typename Point_T>
std::shared_ptr<scene::SceneGraphNode> TrajectoryNode<Point_T>::Duplicate()
{
    std::shared_ptr<TrajectoryNode<Point_T>> duplicate_node = std::make_shared<TrajectoryNode<Point_T>>(*this);
    duplicate_node->m_name = getDuplicateName(m_name);
    if(m_parent != nullptr)
        m_scene_graph->addNode(m_parent->getName(),duplicate_node);
    else
        m_scene_graph->addNode(duplicate_node);
    //TODO duplicate childs ???
    duplicate_node->m_mesh = std::dynamic_pointer_cast<ressource::AbstractMesh>(m_mesh->Duplicate());
    duplicate_node->m_d_type = m_d_type;
    duplicate_node->m_curve = m_curve->clone();
    duplicate_node->m_render_shader = m_render_shader;
    return duplicate_node;
}

}
}

#endif // TRAJECTORYNODE_H
