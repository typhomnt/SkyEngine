#include "image.h"

#include <cassert>
#include <stdexcept>

#include <IL/ilu.h>

#include "utils/logger.h"


namespace skyengine
{
namespace ressource
{

bool Image::ILLoaded = false;

Image::Image(const std::string& name) :
    Ressource(name),
    m_ilID(0),
    m_width(1),
    m_height(1),
    m_dataFormat(GL_RGB)
{
    m_ilID = Image::GenILImage();
}

Image::Image(const std::string& name, const std::string& fileName) :
    Ressource(name),
    m_ilID(0),
    m_width(1),
    m_height(1),
    m_dataFormat(GL_RGB)
{
    m_ilID = Image::GenILImage();
    loadFromFile(fileName);
}

Image::Image(const std::string& name,
             const std::string& format,
             size_t size,
             void *bytes) :
    Ressource(name),
    m_ilID(0),
    m_width(1),
    m_height(1),
    m_dataFormat(GL_RGB)
{
    m_ilID = Image::GenILImage();
    loadFromBytes(format, size, bytes);
}

Image::Image(const std::string& name,
             unsigned int width,
             unsigned int height,
             unsigned int nbChannels,
             const unsigned char* data) :
    Ressource(name),
    m_ilID(0),
    m_width(1),
    m_height(1),
    m_dataFormat(GL_RGB)
{
    m_ilID = Image::GenILImage();
    setData(width, height, nbChannels, data);
}

Image::Image(const std::string& name, const Image& other) :
    Ressource(name),
    m_ilID(0),
    m_width(1),
    m_height(1),
    m_dataFormat(GL_RGB)
{
    m_ilID = Image::GenILImage();

    // Copy image
    ilBindImage(m_ilID);
    ILboolean status = ilCopyImage(other.m_ilID);
    if (!status)
        utility::Logger::Error("DevIL : failed to copy image");
    else
    {
        m_width = ilGetInteger(IL_IMAGE_WIDTH);
        m_height = ilGetInteger(IL_IMAGE_HEIGHT);

        if (ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL) == 4)
            m_dataFormat = GL_RGBA;
    }
}

void Image::loadFromFile(const std::string& fileName)
{
    ilBindImage(m_ilID);

    // Load data
    ILboolean status = ilLoadImage(fileName.c_str());
    if (!status)
        utility::Logger::Error(std::string("DevIL : failed to load image ") + fileName);
    else
    {
        m_width = ilGetInteger(IL_IMAGE_WIDTH);
        m_height = ilGetInteger(IL_IMAGE_HEIGHT);

        if (ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL) == 4)
            m_dataFormat = GL_RGBA;
    }
}

void Image::saveToFile(const std::string& outputFile)
{
    ilBindImage(m_ilID);

    ILboolean status = ilSaveImage(outputFile.c_str());
    if (!status)
        utility::Logger::Error("DevIL : failed to save image");
}

void Image::loadFromBytes(const std::string& format, size_t size, void *bytes)
{
    ilBindImage(m_ilID);

    ILenum ilFormat;
    if (format == "jpg")
        ilFormat = IL_JPG;
    else if (format == "png")
        ilFormat = IL_PNG;
    else if (format == "dds")
        ilFormat = IL_DDS;
    else
        utility::Logger::Error("Format not supported yet");

    ILboolean status = ilLoadL(ilFormat, bytes, (ILuint)size);
    if (!status)
        utility::Logger::Error("DevIL : failed to load image from bytes");
    else
    {
        m_width = ilGetInteger(IL_IMAGE_WIDTH);
        m_height = ilGetInteger(IL_IMAGE_HEIGHT);

        if (ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL) == 4)
            m_dataFormat = GL_RGBA;
    }
}

const unsigned char* Image::getData() const
{
    ilBindImage(m_ilID);
    return ilGetData();
}

void Image::setData(unsigned int width,
                    unsigned int height,
                    unsigned int nbChannels,
                    const unsigned char* data)
{
    ilBindImage(m_ilID);
    ILboolean status = ilTexImage(width,
                                  height,
                                  1,
                                  nbChannels,
                                  nbChannels == 4 ? IL_RGBA : IL_RGB,
                                  IL_UNSIGNED_BYTE,
                                  (void*)data);
    if (!status)
        utility::Logger::Error("DevIL : failed to set data");

    m_width = width;
    m_height = height;
    m_dataFormat = (nbChannels == 4) ? GL_RGBA : GL_RGB;
}

Image::~Image()
{
    if (m_ilID)
    {
        ilDeleteImages(1, &m_ilID);
        m_ilID = 0;
    }
}


void Image::InitIL()
{
    ilInit();

    ILenum status = ilGetError();
    if (status != IL_NO_ERROR)
    {
        utility::Logger::Error("Failed to init DevIL");
        throw std::runtime_error("DevIL init failed");
    }

    Image::ILLoaded = true;
}

ILuint Image::GenILImage()
{
    if (!Image::ILLoaded)
        Image::InitIL();

    ILuint id = 0;
    ilGenImages(1, &id);
    assert(id != 0);

    return id;
}

} // namespace ressource
} // namespace skyengine
