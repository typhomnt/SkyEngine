#ifndef SCENE_GRAPH_H
#define SCENE_GRAPH_H

#include "utils/skyengine_dll.h"

#include "scenegraphnode.h"
#include "scenegraphmanager.h"
#include <unordered_map>
#include <core/serializable.h>
namespace skyengine
{

namespace core
{
    class SkyEngine;
}

namespace scene
{
class SceneGraphNode;
class SceneGraphManager;
class SKYENGINE_API SceneGraph : public core::Serializable
{
public:
    SceneGraph(const std::shared_ptr<SceneGraphNode>& root = nullptr);

    inline const std::shared_ptr<SceneGraphNode> getRoot() const;
    inline std::shared_ptr<SceneGraphNode> getNode(const std::string& node_name) const;
    inline const std::unordered_map<std::string,std::shared_ptr<SceneGraphNode> >& getNodes() const;
    inline bool containsNode(const std::string& node_name) const;
    void setRoot(const std::shared_ptr<SceneGraphNode>& root);

    void addNode(const std::shared_ptr<SceneGraphNode>& node);
    void addNode(const std::string& parent_name, const std::shared_ptr<SceneGraphNode>& node);

    void changeParent(const std::string& node_name, const std::string& new_parent_name);
    void deleteNode(const std::string& node_name);

    virtual void Serialize(rapidjson::Value&,rapidjson::MemoryPoolAllocator<>&);
    std::string getTag() override {return "SceneGraph";}

    void LoadProp(const rapidjson::Value& scene);

    void setUpdateTask(core::SkyEngine*);

private:
    std::shared_ptr<SceneGraphNode> m_root;
    std::unordered_map<std::string,std::shared_ptr<SceneGraphNode> > m_scene_nodes;

    friend class SceneGraphManager;

};


inline const std::shared_ptr<SceneGraphNode> SceneGraph::getRoot() const
{
    return m_root;
}

inline const std::unordered_map<std::string,std::shared_ptr<SceneGraphNode> >& SceneGraph::getNodes() const
{
    return m_scene_nodes;
}

inline bool SceneGraph::containsNode(const std::string& node_name) const
{
    return m_scene_nodes.find(node_name) != m_scene_nodes.end();

}

inline std::shared_ptr<SceneGraphNode> SceneGraph::getNode(const std::string& node_name) const
{
    auto node_it = m_scene_nodes.find(node_name);
    if(node_it == m_scene_nodes.end()) {
        std::cerr << "[Error] trying to access inexistant node: " << node_name << std::endl;
        return nullptr;
    }
    else
        return node_it->second;
}


} // namespace scene

} // namespace skyengine

#endif //SCENE_GRAPH_H
