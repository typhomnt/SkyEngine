#ifndef RESSOURCE_H
#define RESSOURCE_H

#include "utils/skyengine_dll.h"

#include <memory>
#include <string>
#include <mutex>

#include "ressourcemanager.h"


namespace skyengine
{

namespace ressource
{


class SKYENGINE_API Ressource : public core::Serializable
{
    friend class RessourceManager;

public:
    Ressource(const std::string& name);
    virtual ~Ressource() {}

    template<typename T, typename... Args>
    static std::shared_ptr<T> Create(const std::string& name, Args&&... args)
    {         
        auto resource = std::make_shared<T>(name, args...);
        RessourceManager::Add(name, resource);
        return resource;
    }

    virtual std::shared_ptr<Ressource> Duplicate();

    const std::string& getName() const { return m_name; }
    const std::shared_ptr<std::mutex>& getMutex() { return m_lock; }

    template<typename T>
    bool is()
    {
        return (dynamic_cast<T*>(this) != nullptr);
    }

    virtual std::string getTag() { return "Resource"; }
    virtual void Serialize(rapidjson::Value& curr_obj,rapidjson::MemoryPoolAllocator<>& al);

    static std::shared_ptr<Ressource> Load(
        const rapidjson::Value& value,
        const std::string& name,
        std::vector<Ressource*>& secondPass
    );

protected:
    std::string m_name;
    std::shared_ptr<std::mutex> m_lock;

    virtual bool LoadProp(const rapidjson::Value&) { return false; }
};


} // namespace ressource

} // namespace skyengine

#endif // RESSOURCE_H
