#ifndef MESH_H
#define MESH_H

#include "abstractmesh.h"

#include <utils/meta.h>
#include <glm/glm.hpp>
#include <vector>
#include "utils/utilities.h"
#include "utils/instancetransforms.h"
#include "utils/glcheck.h"
#include <iostream>
#include <fstream>



namespace skyengine {
namespace ressource {

//! OpenGL renderable Mesh class
/*! The Mesh class is designed to encapsulate the VAO Opengl stucture
 * for any rendering purpose.
 * it contain a vector to a generic VertexType vertex type, and
 * a vector to a IndexType among unsigned char, unsigned short and
 * unsigned int to complie with OpenGL specification.
 * The vertex type mush constaint the following function, to ensure
 * the generation of the correct attribute buffers:
 *  static std::vector<BufferAttrib> VertexType::getBufferAttribs();
*/

template<class Vertex_T, typename Index_T = unsigned int, typename Transform_T = utility::Tmat4>
class Mesh : public AbstractMesh
{
public:

    using VertexType = Vertex_T;
    using IndexType  = Index_T;
    using TransformType = Transform_T;
    using SwitchType = typename utility::TypeSwitch<IndexType,
    /**/        utility::TypePair<unsigned char,  std::integral_constant<GLenum, GL_UNSIGNED_BYTE>>,
    /**/        utility::TypePair<unsigned short, std::integral_constant<GLenum, GL_UNSIGNED_SHORT>>,
    /**/        utility::TypePair<unsigned int,   std::integral_constant<GLenum, GL_UNSIGNED_INT>>,
    /**/        std::integral_constant<GLenum, 0> >::type;
    static constexpr GLenum gl_index_type =
            SwitchType::type::type::value;
    static_assert(gl_index_type != 0, "Error: Mesh Index_T can only be usnigned char, unsigned short or unsigned int");
    //TODO similar check for transforms

public:
    Mesh(const std::string& name, GLenum usage = GL_STATIC_DRAW, GLenum mode = GL_TRIANGLES, GLuint instance_nbr = 0);
    virtual ~Mesh();

    // opengl upload / draw calls
    void sendBuffers();
    virtual void drawBuffers(GLint first = 0, GLsizei count = std::numeric_limits<GLsizei>::max());
    void setUsage(GLenum usage) {m_usage = usage;}
    void setMode(GLenum mode)   {m_mode = mode;}
    GLenum getUsage() const {return m_usage; }
    GLenum getMode()  const {return m_mode;}

    // helpers
    inline void clear();
    inline void clearVertices();
    inline void clearIndices();
    inline void clearTransforms();

    inline bool empty();
    inline bool hasIndices();
    inline bool hasVertices();
    inline bool hasTransforms();

    inline void reloadVertices();
    inline void reloadIndices();
    inline void reloadTransforms();

    inline void reserveVertices(size_t);
    inline void reserveIndices(size_t);
    inline void reserveTransforms(size_t);

    inline void resizeVertices(size_t);
    inline void resizeIndices(size_t);
    inline void resizeTransforms(size_t);

    inline virtual IndexType addVertex(const VertexType&);
    inline virtual IndexType addVertex(IndexType,const VertexType &);
    inline virtual void setVertex(IndexType, const VertexType&);
    inline virtual void removeVertex(IndexType);

    inline virtual void addIndex(IndexType);
    inline virtual void setIndex(size_t, IndexType);

    inline virtual void addTransform(const TransformType&);
    inline virtual void setTransform(GLuint, const TransformType&);


    inline void setInstanceNumber(GLuint);
    inline GLuint getInstanceNumber() const;

    virtual std::shared_ptr<Ressource> Duplicate();
    inline unsigned int getNbrVertexPerFace() const;

    // vertex and indices diponible for direct lookup and change.
    // remember to request indices/vertices reload after any change
    inline VertexType& vertex(IndexType);
    inline const VertexType& vertex(IndexType) const;
    inline const std::vector<VertexType>& vertices() const;
    inline std::vector<VertexType>& vertices();
    inline IndexType& index(size_t);
    inline const IndexType& index(size_t) const;
    inline const std::vector<IndexType> indices() const;
    inline TransformType& transform(GLuint);
    inline const TransformType& transform(GLuint) const;

    virtual std::string getTag() {return "Mesh";}
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<> &al);
    static std::shared_ptr<Mesh<Vertex_T, Index_T, Transform_T>> Load(const std::string& name);
protected:

    // data
    std::vector<VertexType> m_vertices;
    std::vector<IndexType>  m_indices;
    std::vector<TransformType>  m_transforms;
    bool reload_vertices;
    bool reload_indices;
    bool reload_transforms;

    // opengl data
    GLuint m_vao_id, m_vbuffer_id, m_ibuffer_id, m_tbuffer_id;
    GLenum m_usage, m_mode;
    std::vector<BufferAttrib> buffer_attribs;
    std::vector<BufferAttrib> transform_attribs;
    // instanced
    GLuint m_instance_number;

    //Bind buffer functions
    void attribPointers(std::vector<BufferAttrib>& attribs, size_t default_stride);
    //Shift indices
    void shiftIndices(unsigned int pivot_index, unsigned int shift = 1);
};




//                           Implementation                           //



template<class Vertex_T, typename Index_T, typename Transform_T>
Mesh<Vertex_T, Index_T, Transform_T>::Mesh(const std::string& name, GLenum usage, GLenum mode, GLuint instance_nbr) :
    AbstractMesh(name), reload_vertices{false}, reload_indices{false},reload_transforms{false}, m_usage{usage}, m_mode{mode}, m_instance_number{instance_nbr}
{
    GLCHECK(glGenVertexArrays(1, &m_vao_id));

    // activate vertex attributes for the given VAO
    buffer_attribs = Vertex_T::getBufferAttribs();
    GLuint max_vertex_attrib = 0;
    for(BufferAttrib& attrib : buffer_attribs)
        if(max_vertex_attrib < attrib.index)
            max_vertex_attrib = attrib.index;

    transform_attribs = Transform_T::getBufferAttribs(GLuint(max_vertex_attrib + 1));
    //buffer_attribs.insert(std::end(buffer_attribs),std::begin(transform_attribs),std::end(transform_attribs));

    GLCHECK(glBindVertexArray(m_vao_id));
    //GLuint attribs_offset = 0;
    for(BufferAttrib& attrib : buffer_attribs)
    {
        GLCHECK(glEnableVertexAttribArray(attrib.index));
    }
    for(BufferAttrib& attrib : transform_attribs)
    {
        GLCHECK(glEnableVertexAttribArray(attrib.index));
    }
    //Instancing transform buffer
    GLCHECK(glBindVertexArray(0));


    GLCHECK(glGenBuffers(1, &m_vbuffer_id));
    GLCHECK(glGenBuffers(1, &m_ibuffer_id));
    GLCHECK(glGenBuffers(1, &m_tbuffer_id));

}

template<class Vertex_T, typename Index_T, typename Transform_T>
Mesh<Vertex_T, Index_T, Transform_T>::~Mesh()
{
    GLCHECK(glDeleteBuffers(1, &m_ibuffer_id));
    GLCHECK(glDeleteBuffers(1, &m_vbuffer_id));
    GLCHECK(glDeleteBuffers(1, &m_tbuffer_id));
    GLCHECK(glDeleteVertexArrays(1, &m_vao_id));
}

template<class Vertex_T, typename Index_T, typename Transform_T>
void Mesh<Vertex_T, Index_T, Transform_T>::sendBuffers()
{
    // it is needed to bind the vao before binding the *element* array buffer
    GLCHECK(glBindVertexArray(m_vao_id));

    if(reload_indices && !m_indices.empty())
    {
        GLCHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibuffer_id));
        GLCHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(IndexType), (const GLvoid*) m_indices.data(), m_usage));
        reload_indices = false;
    }

    if(reload_vertices && !m_vertices.empty())
    {
        GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, m_vbuffer_id));
        GLCHECK(glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(VertexType), (const GLvoid*) m_vertices.data(), m_usage));
        reload_vertices = false;
    }


    if(reload_transforms && !m_transforms.empty())
    {
        GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, m_tbuffer_id));
        GLCHECK(glBufferData(GL_ARRAY_BUFFER, m_transforms.size() * sizeof(TransformType), (const GLvoid*) m_transforms.data(), m_usage));
        reload_transforms = false;
    }
}

template<class Vertex_T, typename Index_T, typename Transform_T>
void Mesh<Vertex_T, Index_T, Transform_T>::drawBuffers(GLint first, GLsizei count)
{
    if(!hasVertices())
        return;

    // bind vao and send buffers if data changed
    sendBuffers();
    GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, m_vbuffer_id));
    attribPointers(buffer_attribs,sizeof(Vertex_T));

    if(hasTransforms())
    {
        GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, m_tbuffer_id));
        attribPointers(transform_attribs,sizeof(Transform_T));
    }

    if(hasIndices())
    {
        count = std::min(count, GLint(m_indices.size()));

        GLCHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibuffer_id));


        if( m_instance_number == 0)
        {
            GLCHECK(glDrawElements(
                        m_mode,                // mode
                        count,                 // count
                        gl_index_type,       // type
                        (void*)(intptr_t)first           // element array buffer offset
                        ));
        }
        else
        {
            GLCHECK(glDrawElementsInstanced(m_mode,
                                            count,
                                            gl_index_type,
                                            (void*)(intptr_t)first,
                                            m_instance_number
                                            ));
        }

    }
    else
    {
        count = std::min(count, GLint(m_vertices.size()));
        glDrawArrays(m_mode, first, count);
    }
    for(BufferAttrib& attrib : buffer_attribs)
    {
        GLCHECK(glDisableVertexAttribArray(attrib.index));
    }
    for(BufferAttrib& attrib : transform_attribs)
    {
        GLCHECK(glDisableVertexAttribArray(attrib.index));
    }

}

template<class Vertex_T, typename Index_T, typename Transform_T>
std::shared_ptr<Ressource> Mesh<Vertex_T, Index_T, Transform_T>::Duplicate()
{
    std::shared_ptr<Mesh<Vertex_T, Index_T, Transform_T>> duplicata = std::make_shared<Mesh<Vertex_T, Index_T, Transform_T> >(
                m_name, m_usage, m_mode, m_instance_number);
    duplicata->m_vertices = m_vertices;
    duplicata->m_indices =  m_indices;
    duplicata->m_transforms = m_transforms;
    duplicata->reloadVertices();
    duplicata->reloadIndices();
    duplicata->reloadTransforms();
    return std::dynamic_pointer_cast<Ressource>(duplicata);
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::clear()
{
    clearVertices();
    clearIndices();
    clearTransforms();
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::clearVertices()
{
    m_vertices.clear();
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::clearIndices()
{
    m_indices.clear();
}
template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::clearTransforms()
{
    m_transforms.clear();
    setInstanceNumber(0);
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline bool Mesh<Vertex_T, Index_T, Transform_T>::empty()
{
    return !hasIndices() && !hasVertices() && !hasTransforms();
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline bool Mesh<Vertex_T, Index_T, Transform_T>::hasIndices()
{
    return !m_indices.empty();
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline bool Mesh<Vertex_T, Index_T, Transform_T>::hasVertices()
{
    return !m_vertices.empty();
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline bool Mesh<Vertex_T, Index_T, Transform_T>::hasTransforms()
{
    return !m_transforms.empty();
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T,  Transform_T>::reloadVertices()
{
    reload_vertices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::reloadIndices()
{
    reload_indices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::reloadTransforms()
{
    reload_transforms = false;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::reserveVertices(size_t s)
{
    m_vertices.reserve(s);
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::reserveIndices(size_t s)
{
    m_indices.reserve(s);
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::reserveTransforms(size_t s)
{
    m_transforms.reserve(s);
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::resizeVertices(size_t s)
{
    m_vertices.resize(s);
    reload_vertices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::resizeIndices(size_t s)
{
    m_indices.resize(s);
    reload_indices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::resizeTransforms(size_t s)
{
    m_transforms.resize(s);
    reload_transforms = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline Index_T Mesh<Vertex_T, Index_T, Transform_T>::addVertex(const VertexType& v)
{
    assert(m_vertices.size() < size_t(IndexType(-1)));
    m_vertices.push_back(v);
    reload_vertices = true;
    return IndexType(m_vertices.size()-1);
}
template<class Vertex_T, typename Index_T, typename Transform_T>
inline Index_T Mesh<Vertex_T, Index_T, Transform_T>::addVertex(IndexType id ,const VertexType & v)
{
    assert(m_vertices.size() < size_t(IndexType(-1)));
    assert(id <= m_vertices.size() || m_vertices.size() ==  0);
    m_vertices.insert(m_vertices.begin() + id,v);
    reload_vertices = true;
    //TODO is this line always necessary ?
    if(m_indices.size() > 0)
    {
        addIndex(m_vertices.size() - 1);
        reload_indices = true;
    }
    return id;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::setVertex(IndexType id, const VertexType& v)
{
    assert(id < m_vertices.size());
    m_vertices[id] = v;
    reload_vertices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::setTransform(GLuint id, const TransformType& t)
{
    assert(id < m_transforms.size());
    m_transforms[id] = t;
    reload_transforms= true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::addTransform(const TransformType& t)
{
    m_transforms.push_back(t);
    reload_transforms = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::addIndex(IndexType id)
{
    m_indices.push_back(id);
    reload_indices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::setIndex(size_t i, IndexType id)
{
    assert(id < m_indices.size());
    m_indices[i] = id;
    reload_indices = true;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void  Mesh<Vertex_T, Index_T, Transform_T>::setInstanceNumber(GLuint nbr)
{
    m_instance_number = nbr;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline GLuint Mesh<Vertex_T, Index_T, Transform_T>::getInstanceNumber() const
{
    return m_instance_number;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline Vertex_T& Mesh<Vertex_T, Index_T, Transform_T>::vertex(IndexType id)
{
    return m_vertices[id];
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline const Vertex_T& Mesh<Vertex_T, Index_T, Transform_T>::vertex(IndexType id) const
{
    return m_vertices[id];
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline const std::vector<Vertex_T>& Mesh<Vertex_T, Index_T, Transform_T>::vertices() const
{
    return m_vertices;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline std::vector<Vertex_T>& Mesh<Vertex_T, Index_T, Transform_T>::vertices()
{
    return m_vertices;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline const std::vector<Index_T> Mesh<Vertex_T, Index_T, Transform_T>::indices() const
{
    return m_indices;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline Index_T& Mesh<Vertex_T, Index_T,  Transform_T>::index(size_t i)
{
    return m_indices[i];
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline const Index_T& Mesh<Vertex_T, Index_T, Transform_T>::index(size_t i) const
{
    return m_indices[i];
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline Transform_T& Mesh<Vertex_T, Index_T,  Transform_T>::transform(GLuint i)
{
    return m_transforms[i];
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline const Transform_T& Mesh<Vertex_T, Index_T, Transform_T>::transform(GLuint i) const
{
    return m_transforms[i];
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline void Mesh<Vertex_T, Index_T, Transform_T>::removeVertex(IndexType id)
{
    unsigned int index_per_face = getNbrVertexPerFace();



    for(unsigned int i = 0 ; i < m_indices.size() ; i += index_per_face)
    {
        bool remove_face = false;
        for(unsigned j = i ; j < i + index_per_face; ++j)
            if(j == id)
            {
                remove_face = true;
                break;
            }

        if(remove_face)
        {
            for(unsigned j = 0 ; j < index_per_face; ++j)
                m_indices.erase(m_indices.begin() + i);
        }
    }

    //TODO decrease each indices > id by 1
    reload_vertices = false;
    reload_indices = true;

    if(id < m_indices.size())
        shiftIndices(id);

}


template<class Vertex_T, typename Index_T, typename Transform_T>
void Mesh<Vertex_T, Index_T, Transform_T>::attribPointers(std::vector<BufferAttrib>& attribs, size_t default_stride)
{
    for(BufferAttrib& attrib : attribs)
    {
        GLCHECK(glEnableVertexAttribArray(attrib.index));
    }
    for(BufferAttrib& attrib : attribs)
    {
        size_t stride = attrib.stride;
        if(stride == 0)
            stride = default_stride;
        //glEnableVertexAttribArray(attrib.index);
        switch (attrib.special_attrib) {
        case BufferAttrib::SpecialAttrib::Int:

            GLCHECK(glVertexAttribIPointer(attrib.index,
                                           attrib.size,
                                           attrib.type,
                                           (GLsizei)stride,
                                           (void*)(attrib.offset)));
            break;
        case BufferAttrib::SpecialAttrib::Double:
            GLCHECK(glVertexAttribLPointer(attrib.index,
                                           attrib.size,
                                           GL_DOUBLE,
                                           (GLsizei)stride,
                                           (void*)(attrib.offset)));
            break;
        default:
            GLCHECK(glVertexAttribPointer(attrib.index,
                                          attrib.size,
                                          attrib.type,
                                          attrib.normalized,
                                          (GLsizei)stride,
                                          (void*)(attrib.offset)));
            break;
        }
        if(attrib.instance_update > 0)
            GLCHECK(glVertexAttribDivisor(attrib.index, attrib.instance_update));
    }
}

template<class Vertex_T, typename Index_T, typename Transform_T>
void Mesh<Vertex_T, Index_T, Transform_T>::shiftIndices(unsigned int pivot_index, unsigned int shift)
{
    for(unsigned int i = 0 ; i < m_indices.size() ; ++i)
        if(m_indices[i] > pivot_index)
            m_indices[i] -= shift;
}

template<class Vertex_T, typename Index_T, typename Transform_T>
inline unsigned int Mesh<Vertex_T, Index_T, Transform_T>::getNbrVertexPerFace() const
{
    switch (m_mode)
    {
    case GL_TRIANGLES:
        return 3;
    case GL_LINES:
        return 2;
    case GL_LINE_STRIP:
        return 2;
        //TODO other enums
    default:
        return 0;
    }
}

template<class Vertex_T, typename Index_T, typename Transform_T>
std::shared_ptr<rapidjson::Value> Mesh<Vertex_T, Index_T, Transform_T>::SerializeProp(rapidjson::MemoryPoolAllocator<> &al)
{
    std::shared_ptr<rapidjson::Value> obj = Ressource::SerializeProp(al);
    std::string file_name(m_name + ".bin");
    utility::JSONHelper::SaveString(*obj,"File",file_name,al);
    //TODO Test binary mesh file
    std::ofstream mesh_file;
    mesh_file.open(file_name.c_str(), std::ios::out | std::ios::binary);
    utility::binary_write(mesh_file,Vertex_T::MagicNumber());
    utility::binary_write(mesh_file,m_usage);
    utility::binary_write(mesh_file,m_mode);
    utility::binary_write(mesh_file,m_instance_number);
    utility::binary_write(mesh_file,m_vertices.size());
    utility::binary_write(mesh_file,m_indices.size());
    utility::binary_write(mesh_file,m_transforms.size());
    utility::binary_write(mesh_file,sizeof(Vertex_T));
    utility::binary_write(mesh_file,sizeof(Index_T));
    utility::binary_write(mesh_file,sizeof(Transform_T));

    for(unsigned int i = 0 ; i < m_vertices.size() ; ++i)
    {
         m_vertices[i].writeStream(mesh_file);
    }
    for(unsigned int i = 0 ; i < m_indices.size() ; ++i)
    {
         utility::binary_write(mesh_file,m_indices[i]);
    }
    /*for(unsigned int i = 0 ; i < m_transforms.size() ; ++i)
    {
         mesh_file << m_transforms[0];
    }*/
    mesh_file.close();

    return obj;

}

template<class Vertex_T, typename Index_T, typename Transform_T>
std::shared_ptr<Mesh<Vertex_T, Index_T, Transform_T>> Mesh<Vertex_T, Index_T, Transform_T>::Load(const std::string& name)
{

    std::shared_ptr<Mesh<Vertex_T, Index_T, Transform_T>> mesh = nullptr;
    std::ifstream file(name,std::ios::in|std::ios::binary);
    //std::streampos size;
    if (file.is_open())
    {
       /* size = file.tellg();
        file.seekg (0, std::ios::beg);*/
        unsigned int magic_nbr;
        unsigned int usage;
        unsigned int mode;
        unsigned int inst_nbr;
        unsigned int v_size;
        unsigned int i_size;
        unsigned int t_size;
        unsigned int v_t_size;
        unsigned int i_t_size;
        unsigned int t_t_size;
        utility::binary_read(file,magic_nbr);
        utility::binary_read(file,usage);
        utility::binary_read(file,mode);
        utility::binary_read(file,inst_nbr);
        utility::binary_read(file,v_size);
        utility::binary_read(file,i_size);
        utility::binary_read(file,t_size);
        utility::binary_read(file,v_t_size);
        utility::binary_read(file,i_t_size);
        utility::binary_read(file,t_t_size);

        mesh = std::make_shared<Mesh<Vertex_T, Index_T, Transform_T>>(utility::extractFileNameFromPath(name),usage,mode,inst_nbr);

        for(unsigned int i = 0 ; i < v_size ; ++i)
        {
            mesh->addVertex(Vertex_T::readFromStream(file));
        }
        for(unsigned int i = 0 ; i < i_size ; ++i)
        {
            unsigned int index;
            utility::binary_read(file,index);
            mesh->addIndex(index);
        }

        file.close();
    }

    return mesh;
}



}
}

#endif // MESH_H
