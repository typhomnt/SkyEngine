#include "meshmodifier.h"
namespace skyengine
{

namespace geometry
{


MeshModifier::MeshModifier(): TransformModifier<ressource::Mesh<render::vertex3D>,glm::vec3,glm::vec3>(),
    m_scale_factor(1.0f),m_rotate_factor(1.0f),m_translate_factor(1.0f)
{

}

void MeshModifier::scale(const glm::vec3& pivot_point,  ressource::Mesh<render::vertex3D>& data_scale)
{
    for(unsigned int i = 0 ; i <  data_scale.vertices().size(); i++)
    {
        data_scale.setVertex(i, render::vertex3D(data_scale.vertex(i).pos  + (m_scale_factor - 1.0f)*(data_scale.vertex(i).pos - pivot_point),data_scale.vertex(i).uvs,data_scale.vertex(i).normal));
    }
}

void MeshModifier::translate(const glm::vec3& translate_vector,  ressource::Mesh<render::vertex3D>& data_translate)
{
    for(unsigned int i = 0 ; i <  data_translate.vertices().size(); i++)
    {
        data_translate.setVertex(i, render::vertex3D(data_translate.vertex(i).pos  + m_translate_factor*translate_vector,data_translate.vertex(i).uvs,data_translate.vertex(i).normal));
    }
}

void MeshModifier::rotate(const glm::vec3& /*pivot_point*/,  ressource::Mesh<render::vertex3D>& /*data_rotate*/)
{

}

void MeshModifier::scale_axis(const glm::vec3& /*pivot_point*/, const glm::vec3 &/*pivot_axis*/,  ressource::Mesh<render::vertex3D>& /*data_scale*/){

}

void MeshModifier::rotate_axis(const glm::vec3& /*pivot_point*/, const glm::vec3 &/*pivot_axis*/,  ressource::Mesh<render::vertex3D>& /*data_rotate*/)
{

}

void MeshModifier::apply_scale()
{

}

void MeshModifier::apply_translate()
{

}

void MeshModifier::apply_rotate()
{

}

void MeshModifier::apply()
{

}

void MeshModifier::setScaleFactor(float factor)
{
    m_scale_factor = factor;
}

void MeshModifier::setRotateFactor(float factor)
{
    m_rotate_factor = factor;
}

void MeshModifier::setTranslateFactor(float factor)
{
    m_translate_factor = factor;
}

void MeshModifier::static_scale(const glm::vec3& pivot_point, ressource::Mesh<render::vertex3D>& data_scale,float scale_factor)
{
    /*for(unsigned int i = 0 ; i <  data_scale.vertices().size(); i++)
        data_scale.setVertex(i, render::vertex3D(data_scale.vertex(i).pos  + scale_factor*(data_scale.vertex(i).pos - pivot_point),data_scale.vertex(i).uvs,data_scale.vertex(i).normal));*/
    //test with x axis
    for(unsigned int i = 0 ; i <  data_scale.vertices().size(); i++)
    {
        data_scale.setVertex(i, render::vertex3D(data_scale.vertex(i).pos  + (scale_factor - 1.0f)*(data_scale.vertex(i).pos - pivot_point),data_scale.vertex(i).uvs,data_scale.vertex(i).normal));
    }
//    data_scale.reloadVertices();
}

void MeshModifier::static_scale_axis(const glm::vec3& pivot_point,const glm::vec3& pivot_axis, ressource::Mesh<render::vertex3D>& data_scale,float scale_factor)
{
    float diff_scale = scale_factor - 1.0f;
    for(unsigned int i = 0 ; i <  data_scale.vertices().size(); i++)
    {

        float ax_scale;
        float dot_ax = glm::dot(glm::normalize(data_scale.vertex(i).pos - pivot_point),pivot_axis);
        ax_scale = 1.0f + fabs(dot_ax)*diff_scale;
        float sign = utility::clamp(float(dot_ax/fabs(dot_ax)),-1.0f,1.0f);
        data_scale.setVertex(i, render::vertex3D(data_scale.vertex(i).pos  + (ax_scale- 1.0f)*(sign)*pivot_axis,data_scale.vertex(i).uvs,glm::normalize(data_scale.vertex(i).normal + (ax_scale- 1.0f)*(sign)*pivot_axis)));
    }
}
void MeshModifier::static_translate(const glm::vec3& translate_vector,  ressource::Mesh<render::vertex3D>& data_translate)
{
    for(unsigned int i = 0 ; i <  data_translate.vertices().size(); i++)
    {
        data_translate.setVertex(i, render::vertex3D(data_translate.vertex(i).pos  + translate_vector,data_translate.vertex(i).uvs,data_translate.vertex(i).normal));
    }
}



}
}
