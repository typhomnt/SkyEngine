#ifndef TEXTURE_H
#define TEXTURE_H

#include "utils/skyengine_dll.h"

#include "ressource.h"

#include <memory>

#include <GL/glew.h>

#include "image.h"


#ifdef __GUI__
namespace module {
namespace gui {

class FrameBufferNode;
class Viewport;

}
}
#endif // __GUI__


namespace skyengine
{

// Forward declaration, since fbo.h includes texture.h
namespace render { class FBO; }
namespace core {class FrameBuffer;}

namespace ressource
{

/**
 * @brief Definition of an OpenGL texture.
 */
class SKYENGINE_API Texture: public Ressource
{
    // FBO class can use Texture private elements
    friend class render::FBO;
    friend class core::FrameBuffer;

#ifdef __GUI__
    friend class module::gui::FrameBufferNode;
    friend class module::gui::Viewport;
#endif // __GUI__

public:
    /**
     * @brief Creates an uninitialized texture.
     * @param name   Texture name
     * @param target Texture target (2D, 3D ...). Set to <code>GL_TEXTURE_2D</code> by default.
     */
    Texture(const std::string& name, GLenum target = GL_TEXTURE_2D);

    /**
     * @brief Creates an empty texture.
     * @param name            Texture name
     * @param internal_format Texture internal format (number of color components in the texture)
     * @param format          Texture format (composition of each pixel of the texture data)
     * @param type            Texture type (data type of the pixel data)
     * @param width           Texture width
     * @param height          Texture height
     */
    Texture(
        const std::string& name,
        GLenum internal_format,
        GLenum format,
        GLenum type,
        unsigned int width,
        unsigned int height
    );

    /**
     * @brief Creates a texture from an image file.
     * @param name         Texture name
     * @param texImageName Image file
     * @param target       Texture target (2D, 3D ...). Set to <code>GL_TEXTURE_2D</code> by default.
     */
    Texture(const std::string& name, const std::string& texImageName, GLenum target = GL_TEXTURE_2D);

    /**
     * @brief Creates a texture from an Image resource.
     * @param name   Texture name
     * @param image  Image resource
     * @param target Texture target (2D, 3D ...). Set to <code>GL_TEXTURE_2D</code> by default.
     */
    Texture(const std::string& name, const std::shared_ptr<Image>& image, GLenum target = GL_TEXTURE_2D);

    ~Texture();

    /**
     * @brief Returns the Texture internal format (number of color components in the texture).
     */
    GLenum getInternalFormat() const { return m_internal_format; }

    /**
     * @brief Returns the Texture format (the composition of each pixel of the Texture data).
     */
    GLenum getFormat() const { return m_format; }

    /**
     * @brief Returns the texture type (data type of the pixel data).
     */
    GLenum getType() const { return m_type; }

    /**
     * @brief Returns the texture width.
     */
    unsigned int getWidth() const { return m_width; }

    /**
     * @brief Returns the texture height.
     */
    unsigned int getHeight() const { return m_height; }

    /**
     * @brief Loads a texture from an Image resource name.
     * @param imageName Image resource name
     */
    void loadFromImage(const std::string& imageName);

    /**
     * @brief Loads a texture from an Image resource
     * @param image Image resource
     */
    void loadFromImage(const std::shared_ptr<Image>& image);

    /**
     * @brief Loads a texture from an image file.
     * @param fileName Image file name
     */
    void loadFromFile(const std::string& fileName);

    /**
     * @brief Binds a texture to the given texture unit.
     * @param textureUnit Texture unit
     */
    void bind(GLenum textureUnit = GL_TEXTURE0);

    /**
     * @brief Unbinds the texture.
     */
    void unbind();

    std::string getTag() override { return "Texture"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);

private:
    GLuint m_texID;
    GLenum m_target;
    GLenum m_internal_format;
    GLenum m_format;
    GLenum m_type;
    unsigned int m_width;
    unsigned int m_height;
    std::string m_embed_file;

    GLuint getTextureID() const { return m_texID; }
    GLenum getTarget() const { return m_target; }

    bool LoadProp(const rapidjson::Value& value) override;
};

} // namespace ressource
} // namespace skyengine

#endif // TEXTURE_H
