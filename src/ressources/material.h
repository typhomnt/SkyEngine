#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include "utils/skyengine_dll.h"

#include "shader.h"


namespace skyengine
{

namespace ressource
{

class SKYENGINE_API Material : public Ressource
{
public:
    Material(const std::string& name) :
        Ressource(name)
    {}

    virtual void sendToShader(const Shader& shader) const = 0;
    virtual std::string getTag() {return "Material";}
};

} // namespace ressource

} // namespace skyengine

#endif // __MATERIAL_H__
