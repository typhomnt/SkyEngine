#ifndef ABSTRACTTRANSFORMMODIFIER_H
#define ABSTRACTTRANSFORMMODIFIER_H

#include "utils/skyengine_dll.h"
namespace skyengine
{

namespace geometry
{

class SKYENGINE_API AbstractTransformModifier
{
public:
    AbstractTransformModifier();

    virtual void apply_scale()= 0;
    virtual void apply_translate() = 0;
    virtual void apply_rotate() = 0;
    virtual void apply() = 0;
};

}
}

#endif // ABSTRACTTRANSFORMMODIFIER_H
