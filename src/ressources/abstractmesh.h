#ifndef ABSTRACTMESH_H
#define ABSTRACTMESH_H

#include "utils/skyengine_dll.h"

#include "ressource.h"

#include <GL/glew.h>
#include <limits>
#include <memory>


namespace skyengine
{

namespace ressource
{
class SKYENGINE_API AbstractMesh : public Ressource
{
public:
    AbstractMesh(const std::string& name): Ressource(name){}
    virtual ~AbstractMesh(){}
    virtual void sendBuffers() = 0;
    virtual void drawBuffers(GLint first = 0, GLsizei count = std::numeric_limits<GLsizei>::max()) = 0;
    virtual inline void reloadVertices() = 0;
    virtual inline void reloadIndices() = 0;
    virtual inline void reloadTransforms() = 0;

    virtual inline void reserveVertices(size_t) = 0;
    virtual inline void reserveIndices(size_t) = 0;
    virtual inline void reserveTransforms(size_t) = 0;

    virtual inline void resizeVertices(size_t) = 0;
    virtual inline void resizeIndices(size_t) = 0;
    virtual inline void resizeTransforms(size_t) = 0;

    virtual inline void clear() = 0;


};

}
}
#endif // ABSTRACTMESH_H
