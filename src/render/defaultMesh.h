#ifndef DEFAULTVERTEX3D_H
#define DEFAULTVERTEX3D_H
#include "../ressources/mesh.h"
#include "Vertex3D.h"
namespace skyengine
{

namespace render
{

using  DefaultMesh = ressource::Mesh<vertex3D>;


}
}

#endif // DEFAULTVERTEX3D_H
