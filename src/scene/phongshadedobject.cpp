#include "phongshadedobject.h"
#include "../utils/utilities.h"
namespace skyengine
{

namespace scene
{

PhongShadedObject::PhongShadedObject(const std::string &name,
                                     SceneGraph *owner,
                                     const unsigned int type,
                                     const glm::mat4 &world_matrix,
                                     const std::shared_ptr<ressource::Material>& material):
    GameObject3D(name, owner, type, world_matrix),
    m_material(material)
{}

void PhongShadedObject::setShaderUniforms()
{
    //glUseProgram should be used before be called
    m_render_shader->setUniformValueMatrix4fv("M", &m_world_matrix);
    m_material->sendToShader(*m_render_shader);
}

}
}
