﻿#include "gameobjectanimation.h"
#include <glm/gtc/matrix_transform.hpp>
#include "render/defaultMesh.h"
namespace skyengine
{

namespace animation
{

GameObjectAnimation::GameObjectAnimation(const std::string& animation_name, scene::SceneGraphNode* game_node, scene::SceneGraph *scene_graph)
    :Animation(animation_name), m_game_node(game_node),m_path(std::make_shared<geometry::TrajectoryNode<KeyPose3D>>(animation_name + "_trajectory", skyengine::geometry::TrajectoryNode<KeyPose3D>::LINES /*dynamic_cast<skyengine::render::DefaultMesh*>(std::dynamic_pointer_cast<skyengine::scene::GameObject3D>(
                                                                                                                                                                                                                                            scene_graph->getNode("Cube-frame"))->getMesh().get())*/))
    ,m_scene_graph(scene_graph)
{    
    /* m_path->setMaterial(std::dynamic_pointer_cast<skyengine::scene::GameObject3D>(
                            scene_graph->getNode("Cube-frame"))->getMaterial());*/
    m_scene_graph->addNode(m_path);
    m_traj_node = dynamic_cast<geometry::TrajectoryNode<KeyPose3D>*>(scene_graph->getNode(animation_name + "_trajectory").get());
}

GameObjectAnimation::~GameObjectAnimation()
{

}

void GameObjectAnimation::setPath(const AnimatedPoseTrajectory& path)
{
    for(const KeyPose3D& point : path.GetPoints())
        addPoint(point);
}

void GameObjectAnimation::setPath(const geometry::GeometryCurve<Pose3D>& path)
{
    for(const Pose3D& point : path.GetPoints())
        addPoint(point);
}

void GameObjectAnimation::addPoint(const Pose3D& point)
{
    if(m_path->GetSize() > 0)
        addPoint(KeyPose3D(point,m_path->GetEnd().time + 1.0f));
    else
        addPoint(KeyPose3D(point,0.0f));
}

void GameObjectAnimation::addPoint(const KeyPose3D &point)
{
    std::shared_ptr<KeyFrame> new_frame = std::make_shared<KeyFrame>(point.time);
    if(keyframes.size() != 0)
    {
        if(point.value.position != std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(getKey("position")[getKey("position").size() - 1])->value)
        {
            addKey("position",std::make_shared<TimedParameter<glm::vec3>>(TimedParameter<glm::vec3>(point.value.position,point.time)));
            new_frame->addKeyParameter("position",getKey("position",point.time).get());
        }
        if(point.value.rotation != std::dynamic_pointer_cast<TimedParameter<glm::quat>>(getKey("rotation")[getKey("rotation").size() - 1])->value)
        {
            addKey("rotation",std::make_shared<TimedParameter<glm::quat>>(TimedParameter<glm::quat>(point.value.rotation,point.time)));
            new_frame->addKeyParameter("rotation",getKey("rotation",point.time).get());
        }
        if(point.value.scale != std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(getKey("scale")[getKey("scale").size() - 1])->value)
        {
            addKey("scale",std::make_shared<TimedParameter<glm::vec3>>(TimedParameter<glm::vec3>(point.value.scale,point.time)));
            new_frame->addKeyParameter("scale",getKey("scale",point.time).get());
        }
    }
    else
    {
        addKey("position",std::make_shared<TimedParameter<glm::vec3>>(TimedParameter<glm::vec3>(point.value.position,point.time)));
        addKey("rotation",std::make_shared<TimedParameter<glm::quat>>(TimedParameter<glm::quat>(point.value.rotation,point.time)));
        addKey("scale",std::make_shared<TimedParameter<glm::vec3>>(TimedParameter<glm::vec3>(point.value.scale,point.time)));
        new_frame->addKeyParameter("position",getKey("position",point.time).get());
        new_frame->addKeyParameter("rotation",getKey("rotation",point.time).get());
        new_frame->addKeyParameter("scale",getKey("scale",point.time).get());
    }
    m_path->AddPoint(point);
    addKeyFrame(new_frame);
    setDuration(m_path->GetEnd().time - m_path->GetStart().time);
}

void GameObjectAnimation::clearPath()
{
    m_path->clearTrajectory();
    removeKey("position");
    removeKey("rotation");
    removeKey("scale");
    keyframes.clear();
}

void GameObjectAnimation::ComputeAnimation(float time_stamp)
{
    Animation::ComputeAnimation(time_stamp);
    if(keyframes.size() > 0)
    {

        glm::mat4 translation_matrix = glm::translate(glm::mat4(1.0f),std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(m_last_computed_keys["position"])->value);
        glm::mat4 scale_matrix = glm::scale(glm::mat4(1.0f),std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(m_last_computed_keys["scale"])->value);
        glm::mat4 rotation_matrix = glm::toMat4(std::dynamic_pointer_cast<TimedParameter<glm::quat>>(m_last_computed_keys["rotation"])->value);

        m_game_node->setWorldMatrix(translation_matrix*rotation_matrix*scale_matrix*m_game_node->getLocalMatrix());
    }
}

}
}
