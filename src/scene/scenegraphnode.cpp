#include "scenegraphnode.h"
#include "utils/jsonhelper.h"
#include "camera.h"
#include "cubeobject.h"
#include "quadobject.h"
#include "lightnode.h"
#include "gameobject3d.h"


namespace skyengine
{

namespace scene
{

SceneGraphNode::SceneGraphNode(const std::string& name, SceneGraph* owner,
                               const unsigned int type, const glm::mat4 &world_matrix) :
    m_name(name),
    m_type(type),
    m_parent_matrix(glm::mat4(1.0f)),
    m_local_matrix(glm::mat4(1.0f)),
    m_world_matrix(world_matrix),
    m_parent(nullptr),
    m_scene_graph(owner)
{}

SceneGraphNode::~SceneGraphNode()
{
    //TODO
}

void SceneGraphNode::setType(unsigned int type)
{
    m_type = type;
}

void SceneGraphNode::setParent(SceneGraphNode* parent)
{
    m_parent = parent;
}

void SceneGraphNode::setParentMatrix(const glm::mat4& parent_matrix)
{
    m_parent_matrix = parent_matrix;
    UpdateHierarchicalWorldMatrix();
}

void SceneGraphNode::setLocalMatrix(const glm::mat4& local_matrix)
{
    m_local_matrix = local_matrix;
    UpdateHierarchicalWorldMatrix();
}

void SceneGraphNode::setWorldMatrix(const glm::mat4& world_matrix)
{
    m_world_matrix = world_matrix;
    if(m_parent != nullptr)
        m_parent_matrix = glm::inverse(m_parent->getWorldMatrix())*m_world_matrix;
}

void SceneGraphNode::setCenter(const glm::vec3& center)
{
    m_center = center;
}


void SceneGraphNode::removeChild(const std::string& child_name)
{
    for (auto it = m_children.begin(); it != m_children.end(); ++it)
    {
        if ((*it)->m_name == child_name)
        {
            (*it)->m_parent = nullptr;
            m_children.erase(it);

            break;
        }
    }
}

void SceneGraphNode::removeParameter(const std::string& parameter_name)
{
    auto parameter_it = m_parameters.find(parameter_name);
    if(parameter_it == m_parameters.end())
        std::cerr << "removeParameter : Parameter" << parameter_name<< " does not exist" << std::endl;
    else
        m_parameters.erase(parameter_it);
}

void SceneGraphNode::addChild(const std::shared_ptr<SceneGraphNode>& child)
{
    if(!m_scene_graph->containsNode(child->getName()))
        m_scene_graph->addNode(m_name, child);
    else
        m_scene_graph->changeParent(child->getName(),m_name);
}

std::shared_ptr<SceneGraphNode> SceneGraphNode::Duplicate()
{
    std::shared_ptr<SceneGraphNode> duplicate_node = std::make_shared<SceneGraphNode>(*this);
    duplicate_node->m_name = getDuplicateName(m_name);
    m_scene_graph->addNode(m_parent->m_name,duplicate_node);
    //TODO duplicate childs ???
    return duplicate_node;
}
std::string SceneGraphNode::getDuplicateName(const std::string &input_name)
{
    unsigned int res_nbr = 0;
    while(m_scene_graph->containsNode(input_name + ".0" + std::to_string(res_nbr)))
        res_nbr++;

    return input_name + ".0" + std::to_string(res_nbr);
}

void SceneGraphNode::UpdateHierarchicalWorldMatrix()
{
    glm::mat4 hierarchical_world_matrix = m_local_matrix*m_parent_matrix;
    SceneGraphNode* current_parent = m_parent;
    while(current_parent != nullptr)
    {
        hierarchical_world_matrix = current_parent->getParentMatrix() * hierarchical_world_matrix;
        current_parent = m_parent->getParent();
    }

    m_world_matrix = hierarchical_world_matrix;
}

bool SceneGraphNode::hasParameter(const std::string& p_name)
{
    return utility::MapContains(m_parameters,p_name);
}

void SceneGraphNode::Serialize(rapidjson::Value &curr_obj, rapidjson::MemoryPoolAllocator<> &al)
{
    std::shared_ptr<rapidjson::Value> obj = SerializeProp(al);

    rapidjson::Value children(rapidjson::kObjectType);
    for(SceneGraphNode* child : m_children)
        child->Serialize(children, al);
    obj->AddMember(rapidjson::Value("Children", al), children, al);

    curr_obj.AddMember(
                rapidjson::Value(m_name.c_str(), al),
                *(obj.get()),
                al
                );


}

std::shared_ptr<rapidjson::Value> SceneGraphNode::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    std::shared_ptr<rapidjson::Value> obj = Serializable::SerializeProp(al);
    obj->AddMember(rapidjson::Value("Type", al),
                   rapidjson::Value(m_type),
                   al);

    utility::JSONHelper::SaveMat(*(obj.get()),"World_Mtx",m_world_matrix,al);
    utility::JSONHelper::SaveMat(*(obj.get()),"Local_Mtx",m_local_matrix,al);
    utility::JSONHelper::SaveMat(*(obj.get()),"Parent_Mtx",m_parent_matrix,al);
    utility::JSONHelper::SaveVec(*(obj.get()),"Center",m_center,al);

    return obj;
}

std::shared_ptr<SceneGraphNode> SceneGraphNode::Load(const std::string& name,
                                                     const rapidjson::Value& value,
                                                     SceneGraph* const sceneGraph)
{
    std::shared_ptr<SceneGraphNode> node = nullptr;

    // Check if node already exists
    // We do not use the getNode method since we do not want to log an error if the
    // node does not exist.
    auto nodes = sceneGraph->getNodes();
    auto it = nodes.find(name);

    if (it != nodes.end())
    {
        node = it->second;
    }
    else
    {
        std::string tag = value["Tag"].GetString();
        if (tag == "SceneGraphNode")
            node = std::make_shared<SceneGraphNode>(name, sceneGraph);
        else if (tag == "GmObj")
            node = std::make_shared<GameObject3D>(name, sceneGraph);
        else if (tag == "Cam")
            node = std::make_shared<Camera>(name, sceneGraph);
        else if (tag == "LightNode")
            node = std::make_shared<LightNode>(name, sceneGraph);
        else if (tag == "CubeObject")
            node = std::make_shared<CubeObject>(name, sceneGraph);
        else if (tag == "QuadObject")
            node = std::make_shared<QuadObject>(name, sceneGraph);
    }

    if (node)
    {
        node->LoadProp(value);

        // Load children
        for (auto& child : value["Children"].GetObject())
        {
            auto childNode = SceneGraphNode::Load(
                child.name.GetString(),
                child.value,
                sceneGraph
            );
            node->addChild(childNode);
        }
    }

    return node;
}

void SceneGraphNode::LoadProp(const rapidjson::Value& value)
{
    m_type = value["Type"].GetInt();
    utility::JSONHelper::ReadMat(value["World_Mtx"], m_world_matrix);
    utility::JSONHelper::ReadMat(value["Local_Mtx"], m_local_matrix);
    utility::JSONHelper::ReadMat(value["Parent_Mtx"], m_parent_matrix);
    utility::JSONHelper::ReadVec(value["Center"], m_center);
}

} // namespace scene

} // namespace skyengine

