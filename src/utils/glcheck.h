#ifndef __GLCHECK_H__
#define __GLCHECK_H__

#include <string>


void checkGLErrors(const char* func, const char* file, int line);


#ifndef NDEBUG
    #define GLCHECK(func) \
        func; \
        checkGLErrors(#func, __FILE__, __LINE__);
#else
    #define GLCHECK(func) func;
#endif // NDEBUG

#endif // __GLCHECK_H__
