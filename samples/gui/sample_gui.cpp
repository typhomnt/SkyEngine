
#include "core/skyengine.h"
#include "scene/scenegraphnode.h"
#include "commons/sampleskyengine.h"
#include "core/functioncalltask.h"
#include "core/modelimportermanager.h"
#include "ressources/lights.h"
#include "scene/cameramanipulator.h"

#include "scene/scenegraphmanager.h"
#include "scene/quadobject.h"
#include "geometry/meshmodifier.h"

#include "utils/utilities.h"
#include "animation/skeletalanimator.h"
#include "geometry/geometryalgorithms.h"
#include "geometry/trajectorynode.h"
#include "TrajectoryManipulator.h"
#include "SlateManager.h"
#include "doodle_gui.h"
#include "doodleutils.h"
#include "gui/widgets/animator_gui.h"
#include "render/drawtask.h"
#include "ressources/ressourcemanager.h"
#include "physics/physicsmanager.h"

#include "gui/widgets/nodelist.h"
#include "gui/widgets/resourcelist.h"
#include "gui/widgets/skywindow.h"
#include "gui/widgets/viewport.h"
#include "gui/widgets/shadereditor.h"


using namespace skyengine;

// temporay task for scenemanager draw

class SampleDefault : public samples::SampleSkyEngine
{
public:
    SampleDefault()  : SampleSkyEngine() {}
    ~SampleDefault();
    virtual void initScene() override;
    void initUserRessources() override;
    virtual void initTasks() override;
    void takeIco();
    void doodlePhyTask();
protected:    

    std::shared_ptr<scene::Camera> sceneCamera;

    std::shared_ptr<ressource::Light> dirLight;
    //std::shared_ptr<ressource::Light> pointLight;


    std::shared_ptr<animation::SkeletalAnimator> skel_anim;
    //std::shared_ptr<animation::SkeletalAnimator> skel_anim2;
    std::shared_ptr<animation::SkeletalAnimator> sandbag_anim;
    bool took_ico;


    std::shared_ptr<module::doodle::NodeManipulator> node_manip;
    std::shared_ptr<module::doodle::DoodleManipulator> doodle_manip;

    std::shared_ptr<render::DrawTask> draw_task;


    std::shared_ptr<module::gui::GuiManager> gui_manager;
    std::shared_ptr<module::gui::SkyEditor> m_editor;

};

void SampleDefault::initScene()
{

    took_ico = false;
    // Write scene exemple
    scene::SceneGraphManager::Add("default_scene",std::make_shared<scene::SceneGraph>());
    core::ModelImporterManager* model_manager = core::ModelImporterManager::GetSingleton();

    //InitImporter
    model_manager->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    //-----------

    //Camera
    //scene::SceneGraphManager::Get("default_scene").get()->addNode(sceneCamera);


    //Cube
    model_manager->GetSingleton()->loadModel("../meshes/frame.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    model_manager->GetSingleton()->loadModel("../meshes/bob.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Cube-frame").get())->hide();
    std::shared_ptr<scene::GameObject3D> cube = std::make_shared<scene::GameObject3D>("cube");
    scene::GameObject3D* ico =dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Icosphere-bob").get());
    cube->setMesh(geometry::CubePrimitive(2));
    cube->setMaterial(ressource::PhongMaterial::Turquoise);
    scene::SceneGraphManager::Get("default_scene")->addNode(cube);
    cube->setRenderShader(std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::GetSingleton()->Get("default_shader")).get());
    //physics::PhysicsManager::GetSingleton()->AddBoxCollider(cube.get(),glm::vec3(1.0f,1.0f,1.0f),cube->computeMassCenter<render::vertex3D>(),1.0f);
    physics::PhysicsManager::GetSingleton()->AddBoxCollider(ico,glm::vec3(1.0f,1.0f,1.0f),ico->computeMassCenter<render::vertex3D>(),1.0f);
    std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(ico->getParameter("rigid_body"))->value.setFlags(BT_DISABLE_WORLD_GRAVITY);
    //Stage
    //model_manager->GetSingleton()->loadModel("../meshes/basic_patform.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    //geometry::MeshModifier::static_scale(glm::vec3(),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh-basic_patform").get())->getMesh()).get()),0.03f);
    //geometry::MeshModifier::static_translate(glm::vec3(0,0,-0.1),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh-basic_patform").get())->getMesh()).get()));
    //Bob
    //Tranning Room
    //model_manager->GetSingleton()->loadModel("../meshes/training_stage.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    //Animator
    skel_anim = std::make_shared<animation::SkeletalAnimator>();
    skel_anim->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    skel_anim->setAnimatedSkeleton(dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get())->getPSkeleton());
    skel_anim->setIsLooping(true);
    skel_anim->playAnimation(this);

    /* skel_anim2 = std::make_shared<animation::SkeletalAnimator>();
    skel_anim2->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    skel_anim2->setAnimatedSkeleton(dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get()->Duplicate().get())->getPSkeleton());
    skel_anim2->setIsLooping(true);
    skel_anim2->playAnimation(this);*/

    /*sandbag_anim = std::make_shared<animation::SkeletalAnimator>();
    sandbag_anim->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    sandbag_anim->setAnimatedSkeleton(dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Sandbag-Mesh-training_stage").get())->getPSkeleton());
    animation::SkeletalAnimation* punched = dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Sandbag-Mesh-training_stage").get())->getAnimationPt("Armature|PunshedBag");
    sandbag_anim->addAnimation("punched",0.0f,punched->getDuration(),punched,0);
    sandbag_anim->setTimeStamp(punched->getDuration());
    sandbag_anim->setIsLooping(false);
    sandbag_anim->playAnimation(this);*/
    //DrawTask
    draw_task = std::make_shared<render::DrawTask>(this/*, main_window->getSize()*/,main_window->eventManager());
    draw_task->addShader("default_shader");
    draw_task->addShader("default_skin_shader");
    draw_task->addShader("default_pbr_shader");
    draw_task->addShader("default_skin_pbr_shader");
    draw_task->addShader("instance_shader");
    draw_task->addShader("line_shader");
    draw_task->addShader("checkboard_shader");
    draw_task->addShader("deferredl_shader");
    draw_task->addShader("deferredg_shader");
    draw_task->addShader("deferredg_skin_shader");
    draw_task->addShader("simple_display_shader");
    draw_task->addShader("toon_shader");
    draw_task->setFramebufferCamera(sceneCamera.get());
    //------------
    draw_task->assignLightToShader("forward","default_shader",dirLight.get());
    draw_task->assignLightToShader("forward","toon_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_pbr_shader",dirLight.get());
    //draw_task->assignLightToShader("forward","default_shader",pointLight.get());
    draw_task->assignLightToShader("forward","default_skin_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_skin_pbr_shader",dirLight.get());
    //draw_task->assignLightToShader("forward","default_skin_shader",pointLight.get());
    draw_task->assignLightToShader("forward","instance_shader",dirLight.get());
    //draw_task->assignLightToShader("forward","instance_shader",pointLight.get());
    draw_task->assignLightToShader("forward","checkboard_shader",dirLight.get());
    draw_task->assignLightToShader("deferred_light","deferredl_shader",dirLight.get());
    //draw_task->assignLightToShader("forward","phong_shader",pointLight.get());
    draw_task->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());

    //Doodle and node manipulator
    doodle_manip = std::make_shared<module::doodle::DoodleManipulator>(dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get()),this,skel_anim.get(),ext_dvc::slate::SlateManager::GetSingleton());
    doodle_manip->ImportActions("../externalAPIs/figurines/MotionDoodle3D/Ressources/default_action_set.actions");
    module::doodle::LoadLMATrainingFile("../externalAPIs/figurines/MotionDoodle3D/Ressources/LMA.json");
    node_manip = std::make_shared<module::doodle::NodeManipulator>(scene::SceneGraphManager::Get("default_scene")->getNode("cube").get(),ext_dvc::slate::SlateManager::GetSingleton());

    /*    scene::GameObject3D* base_room = dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Base-training_stage").get());
    glm::vec4 mass_room = base_room->getWorldMatrix()*glm::vec4(base_room->computeMassCenter<render::vertex3D>(),1.0f);
    doodle_manip->setTranslationShift(-glm::vec3(mass_room.x,mass_room.y,mass_room.z));
    node_manip->setTranslationShift(-glm::vec3(mass_room.x,mass_room.y,mass_room.z));
    physics::PhysicsManager::GetSingleton()->AddBoxCollider(base_room,glm::vec3(10.0f,0.5f,10.0f),base_room->computeMassCenter<render::vertex3D>() - glm::vec3(0,1.0f,0),0.0f);*/

    //Gui tasks
    gui_manager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());
    m_editor = std::make_shared<module::gui::SkyEditor>(
                this,
                gui_manager
                );

    module::gui::SkyWindow* window = m_editor->getSkyWindow();
    window->addWidget<module::gui::NodeList>(
                "Node List",
                module::gui::SkyWindow::DockSlot::Left,
                scene::SceneGraphManager::Get("default_scene").get()
                );
    window->addWidget<module::gui::ResourceList>(
                "Resources",
                module::gui::SkyWindow::DockSlot::Left
                );
    window->addWidget<module::gui::ActionPlayer>(
                "Action Player",
                module::gui::SkyWindow::DockSlot::Center,
                skel_anim.get(),
                scene::SceneGraphManager::Get("default_scene").get(),
                dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get())
                );
    window->addWidget<module::gui::ActionAnimator>(
                "Action Animator",
                module::gui::SkyWindow::DockSlot::Center,
                skel_anim.get()
                );
    window->addWidget<module::doodle::MotionDoodleControls>(
                "Doodle Controls",
                module::gui::SkyWindow::DockSlot::Center,
                doodle_manip.get()
                );
    
    auto viewport = window->addWidget<module::gui::Viewport>(
                "Viewport",
                module::gui::SkyWindow::DockSlot::Center
                );
    viewport->addDrawTask(draw_task.get());
    viewport->setActiveCamera(sceneCamera);
    
    window->addWidget<module::gui::ShaderEditor>(
                "Shader Editor",
                module::gui::SkyWindow::DockSlot::Center
                );
}

void SampleDefault::initUserRessources()
{
    //Init Camera, Lights and Shaders
    sceneCamera = std::make_shared<scene::Camera>("camera");
    sceneCamera->setPosition(glm::vec3(-10,3,0));
    sceneCamera->setDirection(glm::vec3(2.0,-1.0,1.0));


    dirLight = ressource::Ressource::Create<ressource::DirLight>("dirLight", 0);
    //dirLight->setDiffuse(glm::vec4(5,5,5,1));
    std::dynamic_pointer_cast<ressource::DirLight>(dirLight)->setDirection(glm::vec3(1,-1,0));
    //pointLight = ressource::Ressource::Create<ressource::PointLight>("pointLight", 0);

    sceneCamera->updateCameraWithCurrentAttributes();

}

void SampleDefault::initTasks()
{
    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));


    addTask(draw_task);
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame,m_editor->getGuiManager()));
    addTask(core::makeFunctionCallTask(&SampleDefault::doodlePhyTask,this));

    m_editor->addTasksToEngine();

    //addTask(core::makeFunctionCallTask(&SampleDefault::playSandBag, this));
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender,m_editor->getGuiManager()));
    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}



SampleDefault::~SampleDefault()
{
}

void SampleDefault::doodlePhyTask()
{
    takeIco();
    for(auto& node: scene::SceneGraphManager::Get("default_scene").get()->getNodes())
    {
        if(node.second->hasParameter("rigid_body"))
        {
            if(std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(node.second->getParameter("rigid_body"))->value.getLocalInertia() != btVector3(0,0,0))
            {
                btTransform tr;
                std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(node.second->getParameter("rigid_body"))->value.getMotionState()->getWorldTransform(tr);
                float y = tr.getOrigin().y();
                tr = physics::Mat4TobtTransform(node.second->getWorldMatrix());
                tr.setOrigin(btVector3(tr.getOrigin().x(),y,tr.getOrigin().z()));
                std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(node.second->getParameter("rigid_body"))->value.getMotionState()->setWorldTransform(tr);
            }
        }
    }
    physics::PhysicsManager::GetSingleton()->ComputeSimulation(1.0f/60.0f);
    for(auto& node: scene::SceneGraphManager::Get("default_scene").get()->getNodes())
    {
        if(node.second->hasParameter("rigid_body"))
        {
            if(std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(node.second->getParameter("rigid_body"))->value.getLocalInertia() != btVector3(0,0,0))
            {
                btTransform transf;
                std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(node.second->getParameter("rigid_body"))->value.getMotionState()->getWorldTransform(transf);
                node.second->setWorldMatrix(physics::btTransformToMat4(transf));
            }
        }
    }

}

void SampleDefault::takeIco()
{
    std::unordered_map<std::string,animation::Animation*> played_anim = skel_anim->getPlayedAnimations(skel_anim->getTimeStamp());
    for(auto & anim : played_anim)
    {
        if(anim.second->getName() == "Armature|Take")
        {
            took_ico = true;
        }
        if(anim.second->getName() == "Armature|Throw" && took_ico)
        {
            scene::GameObject3D* ico =dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Icosphere-bob").get());
            std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(ico->getParameter("rigid_body"))->value.setFlags(BT_ENABLE_GYROSCOPIC_FORCE_IMPLICIT_BODY);
            std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(ico->getParameter("rigid_body"))->value.applyForce(
                        btVector3(0,100,0),btVector3(0,100,0));
            took_ico = false;
        }
    }
    if(took_ico)
    {
        animation::GameObjectSkinned* bob = dynamic_cast<animation::GameObjectSkinned*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh.001-bob").get());
        scene::GameObject3D* ico =dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Icosphere-bob").get());
        ico->setLocalMatrix(glm::mat4(1.0f));
        ico->setWorldMatrix(
                    bob->getWorldMatrix()*bob->getBoneTransform(bob->getConstSkeleton().getBone("Lower_arm_L").bone_ID));
    }
}

int main()
{
    SampleDefault engineInstance;
    //For random use
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);
    engineInstance.init();
    engineInstance.run();

    return 0;
}

