#ifndef DRAWTASKEDITOR_H
#define DRAWTASKEDITOR_H

#include "viewport.h"
#include "imgui/addons/imguinodegrapheditor/imguinodegrapheditor.h"


namespace module
{

namespace gui
{

enum DrawNodeType {
    FBO_NODE = 0,
    COLOR_NODE = 1,
};

struct FrameBufferInfo
{
    FrameBufferInfo(): m_fbo(nullptr){}
    FrameBufferInfo(skyengine::core::FrameBuffer* fbo): m_fbo(fbo){}
    skyengine::core::FrameBuffer* m_fbo;
    std::map<std::string,skyengine::scene::baseNodeParameter*> m_fbo_uniforms;
    std::map<std::string,skyengine::ressource::Texture*> m_fbo_textures;
};

// used in the "add Node" menu (and optionally as node title names)
static const char* DrawNodeNames[2] = {"FBO","COLOR"};
class FrameBufferNode : public ImGui::Node
{
    protected:
    typedef ImGui::Node  Base;  //Base Class
    typedef FrameBufferNode ThisClass;
    FrameBufferNode() : Base() {}
    static const int TYPE = FBO_NODE;

    int m_fbo_out;       // field
    ImVec4 m_color;
    std::unordered_map<std::string,FrameBufferInfo> m_fbo_info;


    // Support static method for enumIndex (the signature is the same used by ImGui::Combo(...))
    static bool GetTextFromEnumIndex(void* ,int value,const char** pTxt) {
        if (!pTxt) return false;
        static const char* values[] = {"APPLE","LEMON","ORANGE"};
        static int numValues = (int)(sizeof(values)/sizeof(values[0]));
        if (value>=0 && value<numValues) *pTxt = values[value];
        else *pTxt = "UNKNOWN";
        return true;
    }

    virtual const char* getTooltip() const {return "ColorNode tooltip.";}
    virtual const char* getInfo() const {return "ColorNode info.\n\nThis is supposed to display some info about this node.";}
    /*virtual void getDefaultTitleBarColors(ImU32& defaultTitleTextColorOut,ImU32& defaultTitleBgColorOut,float& defaultTitleBgColorGradientOut) const {
        // [Optional Override] customize Node Title Colors [default values: 0,0,-1.f => do not override == use default values from the Style()]
        defaultTitleTextColorOut = IM_COL32(220,220,220,255);defaultTitleBgColorOut = IM_COL32(0,75,0,255);defaultTitleBgColorGradientOut = -1.f;
    }*/

    public:

    void AddFrameBufferInfo(const std::string& name, const FrameBufferInfo& frame_info)
    {
        m_fbo_info[name] = frame_info;
        std::vector<char> nm;
        nm.resize(name.size());
        name.copy(&(nm[0]),name.size(),0);
        fields.addFieldTextWrapped(&(nm[0]),name.size());
        //fields.addFieldEnum(&m_fbo_out,"COLOR0\0COLOR1\0DEPTH\0\0","output","Choose your favourite");
    }

    // create:
    static ThisClass* Create(const ImVec2& pos) {
        // 1) allocation
        // MANDATORY (NodeGraphEditor::~NodeGraphEditor() will delete these with ImGui::MemFree(...))
    // MANDATORY even with blank ctrs. Reason: ImVector does not call ctrs/dctrs on items.
    ThisClass* node = new ThisClass();

        // 2) main init
        node->init("FrameBuffer node",pos,"prec","next",TYPE);

        // 3) init fields ( this uses the node->fields variable; otherwise we should have overridden other virtual methods (to render and serialize) )
        //node->fields.addFieldEnum(&node->m_fbo_out,"COLOR0\0COLOR1\0DEPTH\0\0","output","Choose your favourite");
        node->fields.addFieldColor(&node->m_color.x,true,"Color","color with alpha");
        // 4) set (or load) field values
        node->m_fbo_out = 0;
        return node;
    }


};

class ColorNode : public ImGui::Node {
    protected:
    typedef ImGui::Node Base;  //Base Class
    typedef ColorNode ThisClass;
    ColorNode() : Base(),m_color(nullptr) {}
    static const int TYPE = COLOR_NODE;

    ImVec4* m_color;       // field

    // Support static method for enumIndex (the signature is the same used by ImGui::Combo(...))
    static bool GetTextFromEnumIndex(void* ,int value,const char** pTxt) {
        if (!pTxt) return false;
        static const char* values[] = {"APPLE","LEMON","ORANGE"};
        static int numValues = (int)(sizeof(values)/sizeof(values[0]));
        if (value>=0 && value<numValues) *pTxt = values[value];
        else *pTxt = "UNKNOWN";
        return true;
    }

    virtual const char* getTooltip() const {return "ColorNode tooltip.";}
    virtual const char* getInfo() const {return "ColorNode info.\n\nThis is supposed to display some info about this node.";}
    /*virtual void getDefaultTitleBarColors(ImU32& defaultTitleTextColorOut,ImU32& defaultTitleBgColorOut,float& defaultTitleBgColorGradientOut) const {
        // [Optional Override] customize Node Title Colors [default values: 0,0,-1.f => do not override == use default values from the Style()]
        defaultTitleTextColorOut = IM_COL32(220,220,220,255);defaultTitleBgColorOut = IM_COL32(0,75,0,255);defaultTitleBgColorGradientOut = -1.f;
    }*/

    public:

    void setColor(ImVec4* color)
    {
        m_color = color;
        if(fields.size() == 0)
            fields.addFieldColor(&(m_color->x),"Clear Color","color with alpha");
    }

    // create:
    static ThisClass* Create(const ImVec2& pos) {
        // 1) allocation
        // MANDATORY (NodeGraphEditor::~NodeGraphEditor() will delete these with ImGui::MemFree(...))
    // MANDATORY even with blank ctrs. Reason: ImVector does not call ctrs/dctrs on items.
        ThisClass* node = new ThisClass();

        // 2) main init
        node->init("ColorNode",pos,"","r;g;b;a",TYPE);

        // 3) init fields ( this uses the node->fields variable; otherwise we should have overridden other virtual methods (to render and serialize) )
        //node->fields.addFieldColor(&node->m_color.x,true,"Color","color with alpha");

        // 4) set (or load) field values
        //node->Color = ImColor(255,255,0,255);

        return node;
    }


    // casts:
    inline static ThisClass* Cast(Node* n) {return Node::Cast<ThisClass>(n,TYPE);}
    inline static const ThisClass* Cast(const Node* n) {return Node::Cast<ThisClass>(n,TYPE);}
};

static ImGui::Node* DrawNodeFactory(int nt,const ImVec2& pos,const ImGui::NodeGraphEditor& /*nge*/)
{
    switch (nt) {
    case FBO_NODE:{ return FrameBufferNode::Create(pos);}
    case COLOR_NODE:{ return ColorNode::Create(pos);}
    default:
    IM_ASSERT(true);    // Missing node type creation
    return NULL;
    }
    return NULL;
}

class DrawTaskEditor : public SkyWidget
{
public:
    DrawTaskEditor(const std::string &name, module::gui::SkyWindow *window, std::shared_ptr<ActiveObjectEvent> act_evt, std::shared_ptr<SelectObjectEvent> select_evt, Viewport* viewport);
    void draw() override;
    void update();
private:
    Viewport* m_viewport;
    ImGui::NodeGraphEditor m_graph_node;
};

}
}
#endif // DRAWTASKEDITOR_H
