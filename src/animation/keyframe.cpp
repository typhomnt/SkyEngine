#include "keyframe.h"
namespace skyengine
{

namespace animation
{
KeyFrame::KeyFrame(float time_stamp):
    m_time_stamp(time_stamp)
{

}

KeyFrame::~KeyFrame()
{

}

float KeyFrame::getTimeStamp() const
{
    return m_time_stamp;
}

void KeyFrame::setTimeStamp(float time_stamp)
{
    m_time_stamp = time_stamp;
    for(auto& key : m_frame_keys)
        key.second->time = time_stamp;
}

void KeyFrame::setAnimation(Animation* animation)
{
    m_animation = animation;
}

bool KeyFrame::addKeyParameter(const std::string& key_name, BaseTimeParameter* parameter)
{
    if(!math::ApproxEqual(parameter->time ,m_time_stamp,float(ANIM_EPS1)))
        return false;
    else
        m_frame_keys[key_name] = parameter;

    return true;
}


}
}
