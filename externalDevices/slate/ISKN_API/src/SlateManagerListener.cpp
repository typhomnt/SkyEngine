#include "SlateManagerListener.h"

namespace ext_dvc
{

namespace slate
{

SlateManagerListener::SlateManagerListener(SlateManager *manager, bool is_active) :
    StatusListener(manager->m_status_event), DescriptionListener(manager->m_description_event),SoftwareListener(manager->m_software_event),Pen2DListener(manager->m_pen2d_event),
    Pen3DListener(manager->m_pen3d_event),  Object3DListener(manager->m_object3d_event), AccelerometerListener(manager->m_accelerometer_event),  HardwareListener(manager->m_hardware_event),
    DiskStatusListener(manager->m_diskstatus_event),  FileDescriptorListener(manager->m_filedescriptor_event),  FileContentListener(manager->m_filecontent_event), FileContentsListener(manager->m_filecontents_event),
    FileOperationListener(manager->m_fileoperation_event), ErrorListener(manager->m_error_event)
{
    Object3DListener::setListenerActive(is_active);
}


SlateManagerListener::~SlateManagerListener()
{
}


void SlateManagerListener::setTranslationShift(const glm::vec3& t)
{
    m_translation_shift = t;
}

void SlateManagerListener::setRotationShift(const glm::vec3& r)
{
    m_rotation_shift = r;
}

}

}
