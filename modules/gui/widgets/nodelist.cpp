#include "nodelist.h"

#include "render/renderable.h"
#include "ressources/phongmaterial.h"
#include "ressources/pbrmaterial.h"
#include "ressources/ressourcemanager.h"
#include "ressources/shader.h"
#include "scene/gameobject3d.h"


namespace module
{

namespace gui
{

NodeList::NodeList(const std::string& name, SkyWindow *sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                   std::shared_ptr<SelectObjectEvent> select_evt, skyengine::scene::SceneGraph* sceneGraph)
    : SkyWidget(name,sky_win,act_evt,select_evt)
    , m_sceneGraph(sceneGraph)
{}


void NodeList::draw()
{
    if (!m_sceneGraph)
        return;

    // Get all shaders (TODO : more efficient way to do that ?)
    m_shaders.clear();
    m_guiShaders.clear();
    for (auto& res : skyengine::ressource::RessourceManager::GetSingleton()->getRessources())
    {
        if (res.second->is<skyengine::ressource::Shader>())
        {
            m_shaders.push_back(res.first);
            m_guiShaders.push_back(res.first.c_str());
        }
    }

    drawNode(m_sceneGraph->getRoot().get());
}


void NodeList::drawNode(skyengine::scene::SceneGraphNode* node)
{
    if (!node)
        return;

    if (ImGui::TreeNode(node->getName().c_str()))
    {
        m_sky_win->sendActiveObject(node);
        if(node->isRenderable())
        {
            auto renderable = dynamic_cast<skyengine::render::Renderable*>(node);

            std::string renderShader = renderable->getRenderShader()->getName();
            int shaderIdx = std::distance(
                m_shaders.begin(),
                std::find(m_shaders.begin(), m_shaders.end(), renderShader)
            );

            ImGui::Combo("Render Shader", &shaderIdx, m_guiShaders.data(), m_guiShaders.size());
            if (m_shaders[shaderIdx] != renderShader)
            {
                auto newShader = std::dynamic_pointer_cast<skyengine::ressource::Shader>(
                    skyengine::ressource::RessourceManager::Get(m_shaders[shaderIdx])
                );
                renderable->setRenderShader(newShader.get());
            }

            skyengine::scene::GameObject3D* obj = dynamic_cast<skyengine::scene::GameObject3D*>(node);
            if(obj)
            {
                auto obj_pg_mat = std::dynamic_pointer_cast<skyengine::ressource::PhongMaterial>(obj->getMaterial());
                if(obj_pg_mat)
                {
                    ImGui::ColorEdit4("Object Emissive", (float*)&obj_pg_mat->getEmissiveP()[0]);
                    ImGui::ColorEdit4("Object Ambient", (float*)&obj_pg_mat->getAmbientP()[0]);
                    ImGui::ColorEdit4("Object Diffuse", (float*)&obj_pg_mat->getDiffuseP()[0]);
                    ImGui::ColorEdit4("Object Specular", (float*)&obj_pg_mat->getSpecularP()[0]);
                    ImGui::SliderFloat("Object Shininess", obj_pg_mat->getShininessP(),0.0f,120.0f);
                    if(ImGui::Button("Convert To PBR"))
                    {
                        obj->setMaterial(obj_pg_mat->ToPBRMat());
                        if(obj->getRenderShader()->getName() != "default_skin_shader")
                            obj->setRenderShader(
                                        std::dynamic_pointer_cast<skyengine::ressource::Shader>(
                                            skyengine::ressource::RessourceManager::Get("default_pbr_shader")
                                            ).get());
                        else
                            obj->setRenderShader(
                                        std::dynamic_pointer_cast<skyengine::ressource::Shader>(
                                            skyengine::ressource::RessourceManager::Get("default_skin_pbr_shader")
                                            ).get());
                    }
                }

                auto obj_pbr_mat = std::dynamic_pointer_cast<skyengine::ressource::PBRMaterial>(obj->getMaterial());
                if(obj_pbr_mat)
                {
                    ImGui::ColorEdit3("Object Albedo", (float*)&obj_pbr_mat->getAlbedoP()[0]);
                    ImGui::SliderFloat("Object Metallic", obj_pbr_mat->getMetallicP(),0.0f,1.0f);
                    ImGui::SliderFloat("Object Roughness", obj_pbr_mat->getRoughnessP(),0.0f,1.0f);
                    ImGui::SliderFloat("Object Ao", obj_pbr_mat->getAoP(),0.0f,1.0f);
                    if(ImGui::Button("Convert To Phong"))
                    {
                        obj->setMaterial(obj_pbr_mat->ToPhongMat());
                        if(obj->getRenderShader()->getName() != "default_skin_pbr_shader")
                            obj->setRenderShader(
                                        std::dynamic_pointer_cast<skyengine::ressource::Shader>(
                                            skyengine::ressource::RessourceManager::Get("default_shader")
                                            ).get());
                        else
                            obj->setRenderShader(
                                        std::dynamic_pointer_cast<skyengine::ressource::Shader>(
                                            skyengine::ressource::RessourceManager::Get("default_skin_shader")
                                            ).get());
                    }
                }
            }

            if(ImGui::Button("show")) dynamic_cast<skyengine::render::Renderable*>(node)->show();
            if(ImGui::Button("hide")) dynamic_cast<skyengine::render::Renderable*>(node)->hide();
            if(ImGui::Button("show wire")) dynamic_cast<skyengine::render::Renderable*>(node)->showWire();
            if(ImGui::Button("hide wire")) dynamic_cast<skyengine::render::Renderable*>(node)->hideWire();
        }

        for(skyengine::scene::SceneGraphNode* child: node->getChildren())
            drawNode(child);

        ImGui::TreePop();
    }
}

} // namespace gui

} // namespace module
