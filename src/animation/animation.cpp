#include "animation.h"
#include <thread>
namespace skyengine
{

namespace animation
{
Animation::Animation(const std::string &animation_name):
    ressource::Ressource(animation_name), duration(0.0f),tick_per_second(0.0f),m_preparation_time(-1.0f)
  , m_anticipation_time(-1.0f), m_action_time(-1.0f) ,m_follow_throught_time(-1.0f), m_transition_time(-1.0f)
{

}

Animation::~Animation()
{

}

void Animation::addKeyToGroup(const std::string& group_name, const std::string& key_name)
{
    m_key_groups[group_name].push_back(key_name);
}

void Animation::addKeyFrame(std::shared_ptr<KeyFrame> keyframe)
{
    if(HasKeyFrameAtTime(keyframe->getTimeStamp()))
    {
        utility::Logger::Error(m_name + ": Cannot insert frame at time " + std::to_string(keyframe->getTimeStamp()) + " frame already exist");
        return;
    }
    if(keyframes.size() > 0)
    {
        if(keyframe->getTimeStamp() < keyframes[0]->getTimeStamp())
        {
            keyframes.insert(keyframes.begin(),keyframe);
            keyframe->setAnimation(this);
            return;
        }
        if(keyframe->getTimeStamp() > keyframes[keyframes.size() - 1]->getTimeStamp())
        {
            keyframes.insert(keyframes.end(),keyframe);
            keyframe->setAnimation(this);
            return;
        }
        for(unsigned int i = 1 ; i < keyframes.size() ; i++)
            if(keyframes[i-1]->getTimeStamp() < keyframe->getTimeStamp()
                    &&  keyframes[i+1]->getTimeStamp() > keyframe->getTimeStamp())
            {
                keyframes.insert(keyframes.begin() + i, keyframe);
                keyframe->setAnimation(this);
                return;
            }
    }
    else
    {
        keyframes.push_back(keyframe);
        keyframe->setAnimation(this);
    }
}


void Animation::addKeyFrame(const KeyFrame& keyframe)
{
    addKeyFrame(std::make_shared<KeyFrame>(keyframe));
}

void Animation::addKeyFrame(float time_stamp)
{
    if(!HasKeyFrameAtTime(time_stamp))
    {

        if(keyframes.size() == 0 || time_stamp < keyframes[0]->getTimeStamp())
            keyframes.insert(keyframes.begin(),std::make_shared<KeyFrame>(time_stamp));
        else if(time_stamp > keyframes[keyframes.size() - 1]->getTimeStamp())
            keyframes.insert(keyframes.end(),std::make_shared<KeyFrame>(time_stamp));
        else
        {
            for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
                if(keyframes[i]->getTimeStamp() > time_stamp
                        && keyframes[i-1]->getTimeStamp() < time_stamp)
                    keyframes.insert(keyframes.begin()+i,std::make_shared<KeyFrame>(time_stamp));
        }
    }
}

KeyFrame* Animation::getKeyFrameAtTime(float time_stamp)
{
    for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
        if(keyframes[i]->getTimeStamp() == time_stamp)
            return keyframes[i].get();

    return nullptr;
}

void Animation::addKey(const std::string& key_name, std::shared_ptr<BaseTimeParameter> key_value)
{

    if(utility::MapContains<std::string,std::vector<std::shared_ptr<BaseTimeParameter>>>(m_time_keys,key_name))
    {
        if(m_time_keys[key_name].size() == 0)
        {
            m_time_keys[key_name].insert(m_time_keys[key_name].begin(),key_value);
        }
        else if(key_value->time < m_time_keys[key_name][0]->time)
        {
            m_time_keys[key_name].insert(m_time_keys[key_name].begin(),key_value);
        }
        else if(key_value->time > m_time_keys[key_name][m_time_keys[key_name].size() - 1]->time)
        {
            m_time_keys[key_name].insert(m_time_keys[key_name].end(),key_value);

        }
        else
        {
            for(unsigned int i = 1 ; i < m_time_keys[key_name].size() - 1 ; i++)
                if(m_time_keys[key_name][i-1]->time < key_value->time
                        &&  m_time_keys[key_name][i+1]->time > key_value->time)
                {
                    m_time_keys[key_name].insert(m_time_keys[key_name].begin() + i,key_value);
                    break;
                }
        }
        //TODO correct this
        /*if(!HasKeyFrameAtTime(key_value->time))
        {
            addKeyFrame(key_value->time);
        }
        getKeyFrameAtTime(key_value->time)->addKeyParameter(key_name,key_value.get());*/
        ClampInterpolationToEnd(key_name);

    }
    else
    {
        m_time_keys[key_name] = std::vector<std::shared_ptr<BaseTimeParameter>>{key_value};
        //setInterpolationFunction(key_name,utility::InterpolationFunction::getDefaultInterpolationFunc());
    }
}

void Animation::setDuration(const float& duration)
{
    this->duration = duration;
}

void Animation::setTickPerSecond(const float& tick_per_second)
{
    this->tick_per_second = tick_per_second;
}


float Animation::getDuration() const
{
    return duration;
}

float Animation::getTickPerSecond() const
{
    return tick_per_second;
}


unsigned int Animation::getKeyframeNbr() const
{
    return (unsigned int)keyframes.size();
}

void Animation::setPreparationTime(float time)
{
    m_preparation_time = time;
}

void Animation::setAnticipationTime(float time)
{
    m_anticipation_time = time;
}

void Animation::setActionTime(float time)
{
    m_action_time = time;
}

void Animation::setFollowThroughTime(float time)
{
    m_follow_throught_time = time;
}

void Animation::setTransitionTime(float time)
{
    m_transition_time = time;
}

unsigned int Animation::getNearestKeyFrameId(float time)
{
    if(time <= keyframes[0]->getTimeStamp())
        return 0;

    for(unsigned int i = 0 ; i < keyframes.size()-1; i++)
    {
        float mean_time = keyframes[i]->getTimeStamp() + keyframes[i+1]->getTimeStamp();
        if(time >=  (mean_time)/2.0f && time <= keyframes[i+1]->getTimeStamp())
            return i + 1;
        else if (time <=  (mean_time)/2.0f && time >= keyframes[i]->getTimeStamp())
            return i;
    }

    return (unsigned int)keyframes.size() - 1;


}


std::pair<BaseTimeParameter*,BaseTimeParameter*> Animation::getFrameKeys(const std::string& key_name, float time_stamp)
{
    if(m_time_keys[key_name].size() == 1)
        return std::pair<BaseTimeParameter*,BaseTimeParameter*>(m_time_keys[key_name][0].get(),m_time_keys[key_name][0].get());

    unsigned int key_index = getNearestKey(key_name,time_stamp);

    if(key_index == m_time_keys[key_name].size() - 1)
    {
        return std::pair<BaseTimeParameter*,BaseTimeParameter*>(m_time_keys[key_name][m_time_keys[key_name].size() - 1].get(),
                m_time_keys[key_name][m_time_keys[key_name].size() - 1].get());
    }

    unsigned int next_key_index = key_index + 1;
    assert(next_key_index < m_time_keys[key_name].size());
    return std::pair<BaseTimeParameter*,BaseTimeParameter*>(m_time_keys[key_name][key_index].get(),m_time_keys[key_name][next_key_index].get());

}


void Animation::setInterpolationFunction(const std::string &key, utility::AbstractInterpolationFunction* fct, unsigned int st_fr, unsigned int ed_frame)
{
    if(utility::MapContains(m_time_keys,key))
    {
        unsigned int ed_fr;
        if(ed_frame == 0)
            ed_fr = m_time_keys[key].size() - 1;
        else
            ed_fr = ed_frame;
        m_keys_interpolation_func[key][st_fr] = std::pair<unsigned int,  utility::AbstractInterpolationFunction*>(ed_fr,fct);
    }
}

void Animation::ClampInterpolationToEnd(const std::string& key)
{
    if(utility::MapContains(m_keys_interpolation_func,key))
        m_keys_interpolation_func[key].rbegin()->second.first = m_time_keys[key].size() -1;
}

void Animation::ComputeAnimation(float time_stamp)
{    
    for(auto& key: m_keys_interpolation_func)
    {

        unsigned int nearest = getNearestKey(key.first,time_stamp);
        for(auto& interval: key.second)
        {
            if(nearest >= interval.first && nearest <= interval.second.first)
            {
                std::pair<BaseTimeParameter*,BaseTimeParameter*> keys = getFrameKeys(key.first,time_stamp);
                float delta_time = keys.second->time - keys.first->time;
                float interpolation_factor = utility::clamp((time_stamp - keys.first->time) / delta_time,0.0f,1.0f);
                m_last_computed_keys[key.first] = interval.second.second->ComputeInterpolation(*(keys.first),*(keys.second),interpolation_factor);
            }

        }
    }

}

void Animation::Simplify()
{
    if(m_keypoints.size() > 0)
    {
        std::vector<unsigned int> keyframe_rmv_indices;
        unsigned int offset_rm = 0;
        for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
        {
            KeyFrame* key_p = keyframes[i].get();
            if(!utility::vectorContainsIf<KeyFrame*>(m_keypoints,[key_p](const KeyFrame* keyf){return keyf == key_p;})
                    && !utility::vectorContainsIf<KeyFrame*>(m_breakdowns,[key_p](const KeyFrame* keyf){return keyf == key_p;}))
            {
                keyframe_rmv_indices.push_back(i);
                for(auto& key : m_time_keys)
                {
                    removeKey(key.first,keyframes[i]->getTimeStamp());
                    ClampInterpolationToEnd(key.first);
                }
            }
        }

        for(unsigned int i = 0; i < keyframe_rmv_indices.size();++i)
        {
            keyframes.erase(keyframes.begin() + keyframe_rmv_indices[i] - offset_rm);
            offset_rm++;
        }
    }
}

void Animation::removeKey(const std::string& key_name, float time_stamp)
{
    int remove_index = -1;
    for(unsigned int i = 0 ; i < m_time_keys.at(key_name).size() ; i++)
        if(math::ApproxEqual<float>(m_time_keys.at(key_name).at(i)->time,time_stamp,(float)ANIM_EPS1))
            remove_index = i;
    if(remove_index != -1)
        m_time_keys.at(key_name).erase(m_time_keys.at(key_name).begin() + remove_index);

}

void Animation::removeKey(const std::string& key_name)
{
    m_time_keys.erase(key_name);
}

void Animation::addKeypoint(KeyFrame* keyframe)
{
    m_keypoints.push_back(keyframe);
}

void Animation::addBreakdown(KeyFrame* keyframe)
{
    m_breakdowns.push_back(keyframe);
}

void Animation::copyAnimationProperties(Animation& output_anim)
{
    output_anim.duration = duration;
    output_anim.tick_per_second = tick_per_second;

    output_anim.m_preparation_time = m_preparation_time;
    output_anim.m_anticipation_time = m_anticipation_time;
    output_anim.m_action_time = m_action_time;
    output_anim.m_follow_throught_time = m_follow_throught_time;
    output_anim.m_transition_time = m_transition_time;

    output_anim.m_time_keys.clear();
    output_anim.m_key_groups.clear();
    output_anim.keyframes.clear();
    output_anim.m_keys_interpolation_func = m_keys_interpolation_func;
    for(auto& key : m_time_keys)
    {
        for (std::shared_ptr<BaseTimeParameter>& key_val : key.second)
            output_anim.m_time_keys[key.first].push_back(key_val->copy());
    }

    output_anim.m_key_groups = m_key_groups;

    for(std::shared_ptr<KeyFrame>& keyframe : keyframes)
    {
        std::shared_ptr<KeyFrame> keyframe_dup = std::make_shared<KeyFrame>(keyframe->getTimeStamp());
        for(auto& frame_key : keyframe->getFrameKeys())
            keyframe_dup->addKeyParameter(frame_key.first,output_anim.getKey(frame_key.first,keyframe->getTimeStamp()).get());
        output_anim.addKeyFrame(keyframe_dup);
    }
}

bool Animation::HasKeyFrameAtTime(float time) const
{
    for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
        if(keyframes[i]->getTimeStamp() == time)
            return true;

    return false;
}

std::shared_ptr<rapidjson::Value> Animation::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Ressource::SerializeProp(al);

    value->AddMember(rapidjson::Value("Duration", al),
                     rapidjson::Value(duration),
                     al);
    value->AddMember(rapidjson::Value("TickSec", al),
                     rapidjson::Value(tick_per_second),
                     al);
    value->AddMember(rapidjson::Value("PrepTime", al),
                     rapidjson::Value(m_preparation_time),
                     al);
    value->AddMember(rapidjson::Value("AntTime", al),
                     rapidjson::Value(m_anticipation_time),
                     al);
    value->AddMember(rapidjson::Value("ActTime", al),
                     rapidjson::Value(m_action_time),
                     al);
    value->AddMember(rapidjson::Value("FolTime", al),
                     rapidjson::Value(m_follow_throught_time),
                     al);
    value->AddMember(rapidjson::Value("TransTime", al),
                     rapidjson::Value(m_transition_time),
                     al);
    for(unsigned int i = 0 ; i < m_keypoints.size() ; ++i)
        value->AddMember(rapidjson::Value(std::string("KeyP" + std::to_string(i)).c_str(), al),
                         rapidjson::Value(m_keypoints[i]->getTimeStamp()),
                         al);
    for(unsigned int i = 0 ; i < m_breakdowns.size() ; ++i)
        value->AddMember(rapidjson::Value(std::string("Brk" + std::to_string(i)).c_str(), al),
                         rapidjson::Value(m_breakdowns[i]->getTimeStamp()),
                         al);
    rapidjson::Value key_array(rapidjson::kArrayType);
    for(auto& key : m_time_keys)
        key_array.PushBack(rapidjson::Value(key.first.c_str(),al), al);
    value->AddMember(rapidjson::Value("Keys", al),
                     key_array,
                     al);
    utility::JSONHelper::SaveVec(*value.get(),"LabanEff",m_laban_efforts,al);

    return value;
}

}
}
