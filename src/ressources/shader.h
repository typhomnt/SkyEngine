#ifndef SHADER_H
#define SHADER_H

#include "utils/skyengine_dll.h"

#include "ressource.h"

#include <memory>
#include <string>
#include <unordered_map>

#include <scene/nodeparameter.h>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "../utils/glcheck.h"

#define MAX_BUFFER_SIZE 255

#ifdef __GUI__
namespace module {
namespace gui {
    class ShaderEditor;
}
}
#endif // __GUI__


namespace skyengine
{

namespace ressource
{

enum UniformType
{
    FLOAT      = GL_FLOAT,
    FLOAT_VEC2 = GL_FLOAT_VEC2,
    FLOAT_VEC3 = GL_FLOAT_VEC3,
    FLOAT_VEC4 = GL_FLOAT_VEC4,
    FLOAT_MAT3 = GL_FLOAT_MAT3,
    FLOAT_MAT4 = GL_FLOAT_MAT4,
    TEXTURE_2D = GL_SAMPLER_2D,
    INT        = GL_INT,
    BOOL       = GL_BOOL,
    NO_TYPE    = -1
};

struct Uniform
{
    GLint location;
    GLint size;
    UniformType type;

    Uniform(GLint _location, GLint _size, UniformType _type) :
        location(_location),
        size(_size),
        type(_type)
    {}
};


class SKYENGINE_API Shader : public Ressource
{

#ifdef __GUI__
    friend class module::gui::ShaderEditor;
#endif // __GUI__

public:
    Shader(const std::string& name);
    Shader(const std::string& name,
           const std::string& vertexFile,
           const std::string& fragmentFile,
           const std::string& geomFile = "");

    ~Shader();

    const std::string& getVertexFile() const { return m_vertexFile; }
    const std::string& getFragmentFile() const { return m_fragmentFile; }

    /**
     * Returns "" if no geometry shader.
     */
    const std::string& getGeomFile() const { return m_geomFile; }

    bool compileAndLink();

    void use() const { GLCHECK(glUseProgram(m_pid)); }
    static void clearUse() {GLCHECK(glUseProgram(0)); }

    GLint getUniformLocation(const char *variableName) const;
    unsigned int getUniformSize(const char* variableName) const;
    UniformType getUniformType(const std::string &uniform_name) const;

    const std::unordered_map<std::string, Uniform>& getUniformsMap() const { return m_uniforms; }
    inline const std::unordered_map<std::string,GLuint>& getSamplers() const { return m_uniform_sampler; }
    bool containsUniform(const std::string& uniform_name) const;

    GLint getSamplerUnit(GLuint texture_location) const;
    void addSamplerUnitTextureLocation(GLuint texture_location, GLuint sampler_number);
    void setSamplerUnitTextureLocation(GLuint texture_location, GLuint sampler_number);

    //TODO TEST new
    inline std::string getTextureIDFromUnit(GLuint sampler_id) const;
    void setTextureToSampler(const std::string& texture_name, const std::string& uniform_sampler);
    //void setUniformToSamplerUnit();
    void bindTextures();

    void setUniformValue1i(const char* uniform_name, GLuint value) const;
    void setUniformValue1f(const char* uniform_name, float value) const;
    void setUniformValue2f(const char *uniform_name, const glm::vec2 &value) const;
    void setUniformValue3f(const char* uniform_name, const glm::vec3 &value) const;
    void setUniformValue4f(const char* uniform_name, const glm::vec4 &value) const;
    void setUniformValueMatrix3fv(const char *uniform_name, const glm::mat3 &value) const;
    void setUniformValueMatrix4fv(const char* uniform_name, const glm::mat4 &value) const;

    //Pointer version
    void setUniformValue1i(const char* uniform_name, GLuint* value) const;
    void setUniformValue1f(const char* uniform_name, float* value) const;
    void setUniformValue2f(const char *uniform_name, const glm::vec2 *value) const;
    void setUniformValue3f(const char* uniform_name, const glm::vec3 *value) const;
    void setUniformValue4f(const char* uniform_name, const glm::vec4 *value) const;
    void setUniformValueMatrix3fv(const char *uniform_name, const glm::mat3 *value) const;
    void setUniformValueMatrix4fv(const char* uniform_name, const glm::mat4 *value) const;

    void setUniformValue(scene::baseNodeParameter* uniform_parameter);

    static void InitDefaultShader();

    std::string getTag() override { return "Shader"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);

private:
    std::string m_vertexFile;
    std::string m_geomFile;
    std::string m_fragmentFile;

    GLuint m_pid;

    std::unordered_map<std::string, Uniform> m_uniforms;
    std::unordered_map<GLuint, GLuint> texture_to_sampler;
    //NEW sampler management
    //assign sampler uniform name to sampler location (automatic)
    std::unordered_map<std::string,GLuint> m_uniform_sampler;
    //assign texture id to sampler name
    std::unordered_map<GLuint, std::string> m_sampler_linked_texture;

    void getShaderUniforms();

    bool LoadProp(const rapidjson::Value& value) override;
};

inline std::string Shader::getTextureIDFromUnit(GLuint sampler_id) const
{
    auto it = m_sampler_linked_texture.find(sampler_id);
    if (it == m_sampler_linked_texture.end())
        return "";
    else
        return it->second;
}

} // namespace ressource

} // namespace skyengine

#endif // SHADER_H
