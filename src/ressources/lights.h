#ifndef __LIGHTS_H__
#define __LIGHTS_H__

#include "utils/skyengine_dll.h"

#include "ressource.h"

#include <memory>

#include <glm/glm.hpp>

#include "shader.h"


namespace skyengine
{

namespace ressource
{

class SKYENGINE_API Light : public Ressource
{
public:
    Light(const std::string& name, unsigned int id = 0);

    Light(const std::string& name,
          unsigned int id,
          const glm::vec4& ambient,
          const glm::vec4& diffuse,
          const glm::vec4& specular);

    glm::vec4 getAmbient() const { return m_ambient; }
    glm::vec4 getDiffuse() const { return m_diffuse; }
    glm::vec4 getSpecular() const { return m_specular; }

    void setAmbient(const glm::vec4& ambient) { m_ambient = ambient; }
    void setDiffuse(const glm::vec4& diffuse) { m_diffuse = diffuse; }
    void setSpecular(const glm::vec4& specular) { m_specular = specular; }

    virtual void sendToShader(const Shader& shader) const = 0;

    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);

protected:
    // Light id, used in shader
    unsigned int m_id;

    // Common data
    glm::vec4 m_ambient;
    glm::vec4 m_diffuse;
    glm::vec4 m_specular;

    virtual bool LoadProp(const rapidjson::Value& value);
};


class SKYENGINE_API DirLight : public Light
{
public:
    DirLight(const std::string& name, unsigned int id = 0);

    DirLight(const std::string& name,
             unsigned int id,
             const glm::vec4& ambient,
             const glm::vec4& diffuse,
             const glm::vec4& specular,
             const glm::vec3& direction);

    glm::vec3 getDirection() const { return m_direction; }

    void setDirection(const glm::vec3& direction) { m_direction = direction; }

    virtual void sendToShader(const Shader& shader) const;

    virtual std::string getTag() { return "DirLight"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);

    static std::shared_ptr<DirLight> Load(const std::string& name, const rapidjson::Value& value);

private:
    glm::vec3 m_direction;

    bool LoadProp(const rapidjson::Value& value) override;
};


class SKYENGINE_API PointLight : public Light
{
public:
    PointLight(const std::string& name, unsigned int id = 0);

    PointLight(const std::string& name,
               unsigned int id,
               const glm::vec4& ambient,
               const glm::vec4& diffuse,
               const glm::vec4& specular,
               const glm::vec3& position,
               float constant,
               float linear,
               float quadratic);

    glm::vec3 getPosition() const { return m_position; }
    float getConstantAttenuation() const { return m_constant; }
    float getLinearAttenuation() const { return m_linear; }
    float getQuadraticAttenuation() const { return m_quadratic; }

    void setPosition(const glm::vec3& position) { m_position = position; }
    void setConstantAttenuation(float constant) { m_constant = constant; }
    void setLinearAttenuation(float linear) { m_linear = linear; }
    void setQuadraticAttenuation(float quadratic) { m_quadratic = quadratic; }

    virtual void sendToShader(const Shader& shader) const;

    virtual std::string getTag() { return "PointLight"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);

    static std::shared_ptr<PointLight> Load(const std::string& name, const rapidjson::Value& value);

protected:
    glm::vec3 m_position;
    float m_constant;
    float m_linear;
    float m_quadratic;

    bool LoadProp(const rapidjson::Value& value) override;
};


class SpotLight : public PointLight
{
public:
    SpotLight(const std::string& name, unsigned int id = 0);

    SpotLight(const std::string& name,
              unsigned int id,
              const glm::vec4& ambient,
              const glm::vec4& diffuse,
              const glm::vec4& specular,
              const glm::vec3& position,
              float constant,
              float linear,
              float quadratic,
              const glm::vec3& direction,
              float cutoff);

    glm::vec3 getDirection() const { return m_direction; }
    float getCutoff() const { return m_cutoff; }

    void setDirection(const glm::vec3& direction) { m_direction = direction; }
    void setCutoff(float cutoff) { m_cutoff = cutoff; }

    virtual void sendToShader(const Shader& shader) const;

    virtual std::string getTag() { return "SpotLight"; }
    std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<>& al);

private:
    glm::vec3 m_direction;
    float m_cutoff;

    bool LoadProp(const rapidjson::Value& value) override;
};

} // namespace ressource

} // namespace skyengine


#endif // __LIGHTS_H__
