#ifndef __MATH_UTILS_H__
#define __MATH_UTILS_H__

#include <cmath>
#include <functional>
#include <memory>
#include <vector>
#include <glm/gtx/quaternion.hpp>
#include <geometry/hermitespline.h>
#include <iostream>
namespace skyengine
{

namespace math
{

#define DEFAULT_INTERPOLATION_TENSION 0.5f
#define DEFAULT_TANGENT_MULTIPLIER 1.0f
#define DEFAULT_INTERPOLATION_STEPS 10.f

template<typename T>
inline bool ApproxEqual(T a, T b, T eps)
{
    return std::abs(a - b) < eps;
}

glm::quat QuatDiv(const glm::quat& quat1, const glm::quat& quat2);

glm::quat RelativeQuat(const glm::quat& quat1, const glm::quat& quat2);
glm::quat ScaleQuat(const glm::quat& quat_, float scale_fac);

glm::quat VecToQuat(const glm::vec4&);
glm::vec4 QuatToVec(const glm::quat&);

glm::mat4 InterpolateMatrix(const glm::mat4& start, const glm::mat4& end, float interpolation_fac);

float LinearPowerFunc(float linear_fact,float power_fact, float x);

template<typename PointT>
PointT CardinalTangent(PointT start_point, PointT end_point, float tension = DEFAULT_INTERPOLATION_TENSION, float tangent_multiplier =  DEFAULT_TANGENT_MULTIPLIER)
{
    assert(tension >= 0.0f && tension <= 1.0f);
    PointT p = end_point - start_point;
    return (tangent_multiplier * (1.0f - tension)*0.5f*(p)) * 2.0f;
}

template<typename PointT>
PointT HermitePointInterpolation(const PointT& start_point,const PointT& end_point,const PointT& start_tangent,const PointT& end_tangent, float inbetween_weight)
{
    return (2*float(pow(inbetween_weight,3)) - 3*inbetween_weight*inbetween_weight + 1)*start_point
            + float((pow(inbetween_weight,3)) - 2*inbetween_weight*inbetween_weight + inbetween_weight)*(start_tangent)
            + float((pow(inbetween_weight,3)) - inbetween_weight*inbetween_weight)*(end_tangent)
            + (-2*float(pow(inbetween_weight,3)) + 3*float(pow(inbetween_weight,2)))*end_point;
}

template<typename PointT>
PointT CatmullInterpolation(const PointT& start_point
                                 ,const PointT& end_point
                                 ,float inbetween_weight
                            , float tension = DEFAULT_INTERPOLATION_TENSION
        ,float tangent_multiplier = DEFAULT_TANGENT_MULTIPLIER )
{
    PointT tangent = CardinalTangent(start_point,end_point,tension,tangent_multiplier);
    return HermitePointInterpolation(start_point,end_point,tangent,tangent,inbetween_weight);
}

glm::quat HermiteQuaternionInterpolation(const glm::quat&start, const glm::quat&end, float weigtht);

std::function<glm::quat(const glm::quat&,const glm::quat&, float weigtht)>  SpacedHermiteGeneration(float space);

//TODO generic dynamic time warping
template<typename T>
unsigned int DynamicTimeWarping(const std::vector<T>& input, const std::vector<T>& ref,std::function<unsigned int(const T&, const T&)> dist)
{
    unsigned int weights[input.size()+1][ref.size()+1];
    for(unsigned int j = 1 ; j <= ref.size(); j++)
        for(unsigned int i = 1 ; i <= input.size(); i++)
            weights[i][j] = 0;
    for(unsigned int i = 1 ; i <= input.size(); i++)
    {
        weights[i][0] = i;
    }

    for(unsigned int j = 1 ; j <= ref.size(); j++)
    {
        weights[0][j] = j;
    }
    unsigned int penalty = 0;
    for(unsigned int j = 1 ; j <= ref.size(); j++)
        for(unsigned int i = 1 ; i <= input.size(); i++)
        {
            penalty = dist(input[i],ref[j]);
            weights[i][j] = std::min(weights[i-1][j] + 1, std::min(weights[i][j-1] + 1, weights[i-1][j-1] + penalty));
        }
    return weights[input.size()][ref.size()];
}

/**
 * @brief  Computes the minimal distance between a 3D point and a 3D segment.
 * @param  A      Input 3D point
 * @param  L0, L1 Two points representing the 3D segment
 * @return Minimal distance between P and [L0, L1] segment
 */

template<typename PointT>
float PointSegmentDistance(const PointT& A, const PointT& L0, const PointT& L1)
{

    PointT u = normalize(L1 - L0);

    float t = dot(A - L0, u);
    PointT P = L0 + t * u;
    if(dot(P,L0)*dot(P,L1) < 0)
        return length(P - A);
    else
        return std::min(length(P-L0),length(P-L1));
}


/**
 * @brief  Computes the minimal distance between a 3D point and a 3D line.
 * @param  A      Input 3D point
 * @param  L0, L1 Two points representing the 3D line
 * @return Minimal distance between P and (L0, L1) line
 */
float PointLineDistance(const glm::vec3& A, const glm::vec3& L0, const glm::vec3& L1);

/**
 * @brief  Computes a triangle normal
 * @param  T0, T1, T2 Triangle points
 * @return Triangle normalized normal
 */
glm::vec3 TriangleNormal(const glm::vec3& T0, const glm::vec3& T1, const glm::vec3& T2);


/**
 * @brief Simple plane, defined by its normal and offset
 */
struct Plane
{
    glm::vec3 normal;
    float origin;

    Plane() = default;

    Plane(const glm::vec3& _normal, float _origin)
        : normal(glm::normalize(_normal))
        , origin(_origin)
    {}

    Plane(const glm::vec3& _normal, const glm::vec3& O)
        : normal(glm::normalize(_normal))
        , origin(glm::dot(normal, O))
    {}

    /**
     * @brief Returns the signed distance between the point and the plane.
     */
    float distance(const glm::vec3& P) const
    {
        return glm::dot(normal, P) - origin;
    }
};

template<typename Point_T>
std::shared_ptr<geometry::HermiteSpline<Point_T>> HermiteFit(const std::vector<Point_T>& inputs, unsigned int samples_nbr)
{
    if(inputs.size() < 2)
        return nullptr;
    else
    {
        unsigned int point_dim = Point_T::dim();
        //System matrix
        glm::mat4 A(0.0f);
        //Second member vector decomposed in each dimensions
        std::vector<glm::vec4> Bs;
        Bs.resize(point_dim);
        float step = 1.0f / static_cast<float>(samples_nbr - 1);
        float alpha = 0.01f;
        float weight;
        for(unsigned int i = 0 ; i < samples_nbr ; ++i)
        {
            float s = i*step;
            float s2 = s*s;
            float s3 = s*s2;

            //weight test
            if(i == 0 || i == samples_nbr - 1)
                weight = 1.0f;
            else
                weight = 0.0f;

            //Hermite poly and deriv
            float  H[4] =
            {
                2.0f*s3 - 3.0f * s2 + 1.0f,
                s3 - 2.0f* s2 + s,
                -2.0f * s3 + 3.0f*s2,
                s3 - s2
            };

            float  Hd[4] =
            {
                6.0f * s * (s -1.0f),
                3.0f * s2 - 4.0f *s + 1.0f,
                6.0f * s * (-s + 1.0f),
                3.0f * s2 - 2.0f * s
            };

            //Sampling function of a sequence of point

            unsigned int curve_id = (inputs.size()-1)*s;
            Point_T point = inputs[curve_id]/*TODO*/;
            Point_T tangent;
            if(curve_id == 0)
                tangent = inputs[1] - inputs[0];
            else
                tangent = inputs[curve_id] - inputs[curve_id-1];


            for(unsigned int j = 0 ; j < 4; ++j)
            {
                for(unsigned int k = 0 ; k < 4; ++k)
                    A[j][k] += H[j] * H[k] + weight*alpha*Hd[j] * Hd[k];

                for(unsigned int k = 0 ; k < Bs.size() ; ++k)
                    Bs[k][j] += H[j] * point[k] +  weight*alpha*Hd[j]*tangent[k];
            }
        }

        //System Solve
        glm::mat4 invA = glm::inverse(A);
        std::vector<glm::vec4> coords;
        coords.resize(point_dim);
        for(unsigned int k = 0 ; k < Bs.size() ; ++k)
            coords[k] =  Bs[k] * invA;

        Point_T st_vec;
        Point_T ed_vec;
        Point_T st_tangent;
        Point_T ed_tangent;

        for(unsigned int k = 0 ; k < point_dim ; ++k)
        {
            st_vec[k] = coords[k][0];
            ed_vec[k] = coords[k][2];
            st_tangent[k] = coords[k][1];
            ed_tangent[k] = coords[k][3];
        }

        return std::make_shared<geometry::HermiteSpline<Point_T>>(st_vec,ed_vec,st_tangent,ed_tangent);
    }        
}

} // namespace math
} // namespace skyengine

#endif // __MATH_UTILS_H__

