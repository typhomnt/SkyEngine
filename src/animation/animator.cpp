#include "animator.h"
#include "../utils/utilities.h"
namespace skyengine
{

namespace animation
{
Animator::Animator(): AnimatorInterface()
{

}

void Animator::addAnimationModifier(std::shared_ptr<geometry::AbstractTransformModifier> animation_modifier, const std::string& modifier_name)
{
    utility::MapInsert<std::string,std::shared_ptr<geometry::AbstractTransformModifier> >(m_animation_modifiers,modifier_name,animation_modifier);
}

void Animator::addAnimation(const std::string& element_name, float start_time, float end_time, Animation* animation, unsigned int layered_order, std::shared_ptr<geometry::AbstractTransformModifier> modifier , bool add_transition)
{
    utility::MapInsert<std::string,std::shared_ptr<LayeredAnimationElement> > (m_composed_anim_els,element_name,std::make_shared<LayeredAnimationElement>(element_name,start_time,end_time,layered_order,animation,modifier));
    if(m_end_time < end_time)
        m_end_time = end_time;

    if(add_transition && m_composed_anim_els.size() > 1)
    {
        //we find the previous element of the same layers
        float max_end = 0.0f;
        float max_start = 0.0f;
        std::string last_el = "";
        for(auto& anim_el : m_composed_anim_els)
        {
            if(anim_el.second->layer == layered_order && anim_el.second->end_time <= start_time && anim_el.second->end_time >= max_end)
            {
                max_start = anim_el.second->start_time;
                max_end = anim_el.second->end_time;
                last_el = anim_el.first;
            }
        }
        if(last_el != "")
            addTransition(last_el + std::string("  ") + element_name
                          ,max_end - 0.1*(max_end - max_start)
                          ,start_time + 0.2f*(end_time - start_time)
                          ,last_el
                          ,element_name);
    }
}

void Animator::removeAnimation(const std::string& animation_name)
{
    utility::MapRemove<std::string,std::shared_ptr<LayeredAnimationElement> >(m_composed_anim_els,animation_name);
}


bool Animator::setElementModifier(const std::string& element_name, std::shared_ptr<geometry::AbstractTransformModifier> modifier)
{
    if(utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,element_name))
    {
        m_composed_anim_els[element_name]->modifier = modifier;
        return true;
    }
    return false;
}

bool Animator::applyElementModifier(const std::string& element_name)
{
    if(utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,element_name))
    {
        /*  bool reset_modif = false;
        if(m_composed_anim_els[element_name]->modified_animation == nullptr)
        {*/
        m_composed_anim_els[element_name]->modified_animation = std::dynamic_pointer_cast<Animation>(m_composed_anim_els[element_name]->animation->Duplicate());
        /*    reset_modif = true;
        }*/
        if(m_composed_anim_els[element_name]->modifier != nullptr)
        {
            //if(reset_modif)
            setElementModifier(element_name,m_composed_anim_els[element_name]->modifier);
            m_composed_anim_els[element_name]->modifier->apply();
            return true;
        }
        else
            return false;
    }
    return false;
}


void Animator::setElementStartTime(const std::string& element_name, float start_time)
{
    if(utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,element_name))
        m_composed_anim_els[element_name]->start_time = start_time;
}

void Animator::setElementEndTime(const std::string& element_name, float end_time)
{
    if(utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,element_name))
        m_composed_anim_els[element_name]->end_time = end_time;
}

void Animator::ComputeElement(const std::string& element_name,float time_stamp)
{
    if(m_composed_anim_els[element_name]->modified_animation != nullptr)
        m_composed_anim_els[element_name]->modified_animation->ComputeAnimation(time_stamp - m_composed_anim_els[element_name]->start_time);
    else
        m_composed_anim_els[element_name]->animation->ComputeAnimation(time_stamp - m_composed_anim_els[element_name]->start_time);
}

#ifdef __GUI__
void Animator::setId(unsigned int id)
{
    m_id = id;
}
#endif

void Animator::clear()
{       
    clearModifiers();
    clearModifierOrdering();
    clearAnimationCompoEls();
    clearTransitions();
    reset();

}

void Animator::clearModifiers()
{
    m_animation_modifiers.clear();
}

void Animator::clearModifierOrdering()
{
    m_modifier_layering_order.clear();
}

void Animator::clearAnimationCompoEls()
{
    m_composed_anim_els.clear();    
}

void Animator::clearTransitions()
{
    m_transitions.clear();
    m_played_trans.clear();
}

void Animator::reset()
{
    m_time_stamp = m_start_time;
    m_end_time = m_start_time;
}

void Animator::resetTimeStamp()
{
    m_time_stamp = m_start_time;
}


bool Animator::clampElementToEnd(const std::string& element_name)
{
    if(utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,element_name))
    {
        Animation* anim;
        if(m_composed_anim_els[element_name]->modified_animation != nullptr)
            anim = m_composed_anim_els[element_name]->modified_animation.get();
        else
            anim = m_composed_anim_els[element_name]->animation;

        m_composed_anim_els[element_name]->end_time = m_composed_anim_els[element_name]->start_time + anim->getDuration();
        return true;
    }
    return false;
}

void Animator::clampToEnd()
{
    float max_end = 0.0f;
    for(auto& anim_el : m_composed_anim_els)
    {
        if(max_end <= anim_el.second->end_time)
            max_end = anim_el.second->end_time;
    }
    m_end_time = max_end;
}

bool Animator::addTransition(const std::string& trans_name,float start_time, float end_time, const std::string& src_el_name,const std::string& trg_el_name)
{
    if(utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,src_el_name)
            && utility::MapContains<std::string,std::shared_ptr<LayeredAnimationElement>>(m_composed_anim_els,trg_el_name)
            && m_composed_anim_els[src_el_name]->layer == m_composed_anim_els[trg_el_name]->layer
            && m_composed_anim_els[src_el_name]->end_time <= m_composed_anim_els[trg_el_name]->start_time)
    {
        utility::MapInsert(m_transitions,trans_name,std::make_shared<Transition>(trans_name,start_time,end_time,
                                                                                 m_composed_anim_els[src_el_name].get(),
                                                                                 m_composed_anim_els[trg_el_name].get()));
        return true;
    }
    return false;
}

void Animator::computeAnimation(float time_stamp)
{
    m_played_trans.clear();
    m_animation_layering_order.clear();
    for(auto& anim_el : m_composed_anim_els)
    {
        if(time_stamp <= anim_el.second->end_time
                && time_stamp >= anim_el.second->start_time)
        {
            ComputeElement(anim_el.first,time_stamp);
        }

    }
    for(auto& trans : m_transitions)
    {
        if(time_stamp <= trans.second->end_time
                && time_stamp >= trans.second->start_time)
        {
            m_played_trans[trans.second->source_anim->layer] = trans.second.get();
            //compute source and target anim if necessary
            if(time_stamp > trans.second->source_anim->end_time)
            {
                ComputeElement(trans.second->source_anim->name,trans.second->source_anim->end_time);
            }

            if(time_stamp < trans.second->target_anim->start_time)
            {
                ComputeElement(trans.second->target_anim->name,trans.second->target_anim->start_time);
            }
        }
    }

    //Push animation layering and transitions
    for(auto& anim_el : m_composed_anim_els)
    {
        if(time_stamp <= anim_el.second->end_time
                && time_stamp >= anim_el.second->start_time)
        {
            m_animation_layering_order[anim_el.second->layer] = anim_el.second.get();
        }
    }


    for(auto& anim_tr : m_played_trans)
    {
        if( time_stamp <= anim_tr.second->end_time
                && time_stamp >= anim_tr.second->start_time)
            m_animation_layering_order[anim_tr.first] = nullptr;

    }

}



}
}
