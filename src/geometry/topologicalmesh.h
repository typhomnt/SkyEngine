#ifndef TOPOLOGICALMESH_H
#define TOPOLOGICALMESH_H

#include "ressources/mesh.h"

#include <cassert>
#include <memory>
#include <vector>

#include "halfedge.h"


namespace skyengine
{

namespace geometry
{

template<typename T>
class TopologicalMesh : public ressource::Mesh<T>
{
    using IndexType = unsigned int;
    using VertexType = T;

public:
    TopologicalMesh(const std::string& name);

    void getFaceVertices(IndexType faceId, std::vector<IndexType>& vertices) const;

    void getVertexNeighbors(IndexType vertexId, std::vector<IndexType>& vertices) const;

    void getVertexIncidentEdges(IndexType vertexId, std::vector<IndexType>& edges) const;

    void getVertexIncidentFaces(IndexType vertexId, std::vector<IndexType>& faces) const;

    void getFaceAdjacentFaces(IndexType faceId, std::vector<IndexType>& faces) const;

    void getFaceHalfEdges(IndexType faceId, std::vector<IndexType>& halfEdges) const;

    inline const std::vector<std::shared_ptr<TVertex>>& getTVertices() const;
    inline const std::vector<std::shared_ptr<Halfedge>>& getEdges() const;
    inline const std::vector<std::shared_ptr<Face>>& getFaces() const;

    inline const TVertex& getTVertex(IndexType id) const;
    inline const Halfedge& getHalfedge(IndexType id) const;
    inline const Face& getFace(IndexType id) const;


    inline void clearTVertices();
    inline void clearHalfedges();
    inline void clearFaces();

    unsigned int addVertex(const VertexType&);
    unsigned int addTVertex(const glm::vec3& p = glm::vec3());

    unsigned int addHalfedge(TVertex* v, Halfedge* prev = nullptr, Halfedge* next = nullptr, Halfedge* opposite = nullptr, Face* f = nullptr);
    unsigned int addHalfedge(unsigned int v_id, Halfedge* prev = nullptr, Halfedge* next = nullptr, Halfedge* opposite = nullptr, Face* f = nullptr);

    unsigned int addFace(Halfedge *h);
    unsigned int addFace(const std::vector<Halfedge*>& h);
    unsigned int addFace(const std::vector<unsigned int>& vertices);
    unsigned int addFaceHE(const std::vector<unsigned int>& h);

    inline unsigned int getFaceEdgeNbr(IndexType id) const;

    void removeVertex(IndexType);
    void removeFace(IndexType, bool remove_edges = true);
    void removeHalfedge(IndexType edgeId);
    void setVertex(IndexType, const VertexType&);

    void setHalfedgeProp(IndexType id, Halfedge* prev = nullptr, Halfedge* next = nullptr, Halfedge* opposite = nullptr, Face* f = nullptr);

    void ComputeMeshFromTopo();
    void ComputeTopoFromMesh();
    void ComputeTopoFromMesh(const ressource::Mesh<T>& mesh);

    void Quandrangulize();

private:
    std::vector<std::shared_ptr<TVertex>> m_tvertices;
    std::vector<std::shared_ptr<Halfedge>> m_halfedges;
    std::vector<std::shared_ptr<Face>> m_faces;

    void updateOpposite(Halfedge& h);
};

template<typename T>
TopologicalMesh<T>::TopologicalMesh(const std::string& name)
    : ressource::Mesh<T>(name)
{}

template<typename T>
void TopologicalMesh<T>::getFaceVertices(IndexType faceId, std::vector<IndexType>& vertices) const
{
    assert(faceId < m_faces.size());
    vertices.clear();

    Face* f = m_faces[faceId].get();

    Halfedge* he = f->e;
    do {
        vertices.push_back(he->v->id);
        he = he->next;
    } while (he != f->e);
}

template<typename T>
void TopologicalMesh<T>::getVertexNeighbors(IndexType vertexId, std::vector<IndexType>& vertices) const
{
    assert(vertexId < m_tvertices.size());
    vertices.clear();

    TVertex* v = m_tvertices[vertexId].get();

    Halfedge* he = v->e;
    do {
        vertices.push_back(he->opposite->v->id);
        he = he->opposite->next;
    } while (he != nullptr && he != v->e);
}

template<typename T>
void TopologicalMesh<T>::getVertexIncidentEdges(IndexType vertexId, std::vector<IndexType>& edges) const
{
    assert(vertexId < m_tvertices.size());
    edges.clear();

    TVertex* v = m_tvertices[vertexId].get();

    Halfedge* he = v->e;
    do {
        if(he->opposite != nullptr)
        {

            edges.push_back(he->opposite->id);
            he = he->opposite->next;
        }
        else
            return;
    } while (he != nullptr && he != v->e);
}

template<typename T>
void TopologicalMesh<T>::getVertexIncidentFaces(IndexType vertexId, std::vector<IndexType>& faces) const
{
    assert(vertexId < m_tvertices.size());
    faces.clear();

    TVertex* v = m_tvertices[vertexId].get();

    Halfedge* he = v->e;
    do {
        //TODO chek for null ?
        if(he->opposite != nullptr)
        {
            faces.push_back(he->f->id);
            he = he->opposite->next;
        }else
            return;
    } while (he != nullptr && he != v->e);
}

template<typename T>
void TopologicalMesh<T>::getFaceAdjacentFaces(IndexType faceId, std::vector<IndexType>& faces) const
{
    assert(faceId < m_faces.size());
    faces.clear();

    Face* f = m_faces[faceId].get();

    Halfedge* he = f->e;
    do {
        if (he->opposite)
            faces.push_back(he->opposite->f->id);
        he = he->next;
    } while (he != nullptr && he != f->e);
}

template<typename T>
void TopologicalMesh<T>::getFaceHalfEdges(IndexType faceId, std::vector<IndexType>& halfEdges) const
{
    assert(faceId < m_faces.size());
    halfEdges.clear();

    Face* f = m_faces[faceId].get();

    Halfedge* he = f->e;
    do {
        halfEdges.push_back(he->id);
        he = he->next;
    } while (he != nullptr && he != f->e);
}

template<typename T>
unsigned int TopologicalMesh<T>::addVertex(const VertexType& v)
{
    assert(this->m_vertices.size() < size_t(IndexType(-1)));
    this->m_vertices.push_back(v);
    this->reload_vertices = true;
    return addTVertex(v.position());
}

template<typename T>
unsigned int TopologicalMesh<T>::addTVertex(const glm::vec3& p)
{
    std::shared_ptr<TVertex> new_tv = std::make_shared<TVertex>(this->m_tvertices.size());
    new_tv->e = nullptr;
    new_tv->p = p;
    this->m_tvertices.push_back(new_tv);
    return IndexType(new_tv->id);
}

template<typename T>
void TopologicalMesh<T>::setVertex(IndexType id, const VertexType& v)
{    
    ressource::Mesh<T>::setVertex(id, v);
    std::shared_ptr<TVertex> new_tv = std::make_shared<TVertex>(id);
    new_tv->e = nullptr;
    new_tv->p = v.position();
    m_tvertices[id] = new_tv;
}

template<typename T>
unsigned int TopologicalMesh<T>::addHalfedge(TVertex* v,Halfedge* prev, Halfedge* next, Halfedge* opposite, Face* f)
{
    std::shared_ptr<Halfedge> h = std::make_shared<Halfedge>(m_halfedges.size());
    h->v = v;
    h->prev = prev;
    h->next = next;
    h->opposite = opposite;
    h->f = f;
    m_halfedges.push_back(h);
    v->e =  h.get();
    return IndexType(h->id);
}

template<typename T>
unsigned int TopologicalMesh<T>::addHalfedge(unsigned int v_id, Halfedge* prev , Halfedge* next , Halfedge* opposite, Face* f)
{
    return addHalfedge(m_tvertices[v_id].get(),prev,next,opposite,f);
}


template<typename T>
unsigned int TopologicalMesh<T>::addFaceHE(const std::vector<unsigned int>& h)
{
    std::vector<Halfedge *> halfedges;
    for(unsigned int i = 0 ; i < h.size(); ++i)
        halfedges.push_back(m_halfedges[h[i]].get());
    return addFace(halfedges);
}

template<typename T>
unsigned int TopologicalMesh<T>::addFace(const std::vector<Halfedge *> &h)
{
    std::shared_ptr<Face> f = std::make_shared<Face>(m_faces.size());
    f->e = h[0];

    for(unsigned int  i = 0 ; i < h.size() ; ++i)
    {
        if(i != 0)
            h[i]->prev = h[(i-1)%h.size()];
        else
            h[i]->prev = h[h.size() - 1];
        h[i]->next = h[(i+1)%h.size()];
        h[i]->f = f.get();

    }

    for(unsigned int  i = 0 ; i < h.size() ; ++i)
        updateOpposite(*(h[i]));


    this->m_faces.push_back(f);
    return f->id;

}

template<typename T>
unsigned int TopologicalMesh<T>::addFace(Halfedge* h)
{
    std::shared_ptr<Face> f = std::make_shared<Face>(m_faces.size());
    f->e = h;
    std::vector<IndexType> vertices;
    getFaceVertices(f->id,vertices);
    Halfedge* fh = h;
    do{
        updateOpposite(fh);
        fh = fh->next;
    } while (fh != nullptr && fh != h);
    assert(vertices.size() == 3 || vertices.size() == 4);
    this->addIndex(vertices[0]);
    this->addIndex(vertices[1]);
    this->addIndex(vertices[2]);
    if(vertices.size() == 4)
    {
        this->addIndex(vertices[2]);
        this->addIndex(vertices[3]);
        this->addIndex(vertices[0]);
    }


    this->m_faces.push_back(f);
    return  IndexType(f->id);
}

template<typename T>
unsigned int TopologicalMesh<T>::addFace(const std::vector<unsigned int>& vertices)
{
    std::vector<unsigned int> he;
    for (unsigned int vid : vertices)
        he.push_back(addHalfedge(m_tvertices[vid].get()));
    return addFaceHE(he);
}

template<typename T>
void TopologicalMesh<T>::removeVertex(IndexType id)
{
    ressource::Mesh<T>::removeVertex(id);
    //TODO Decrease Vertices, faces and edges Ids
    std::vector<IndexType> faces;
    getVertexIncidentFaces(id,faces);
    //std::sort(faces.begin(),face.end());
    for(unsigned int i = 0 ; i < faces.size() ; i++)
    {
        removeFace(faces[i]);
    }
}

template<typename T>
void TopologicalMesh<T>::removeFace(IndexType faceId,bool remove_edges)
{

    assert(faceId < m_faces.size());

    Face* f = m_faces[faceId].get();
    Halfedge* he = f->e;
    do {
        unsigned int id = he->id;
        he->f = nullptr;
        he = he->next;
        if(remove_edges)
            removeHalfedge(id);
    } while (he != nullptr && he != f->e);
    //TODO the line below + index shift or find and remove
    m_faces.erase(m_faces.begin() + faceId);
    for(unsigned int i = faceId ; i < m_faces.size() ; ++i)
        m_faces[i]->id--;
}

template<typename T>
void TopologicalMesh<T>::removeHalfedge(IndexType edgeId)
{
    assert(edgeId < m_halfedges.size());
    Halfedge* he = m_halfedges[edgeId].get();
    if(he->opposite != nullptr)
        he->opposite->opposite = nullptr;
    if(he->next != nullptr)
        he->next->prev = nullptr;
    if(he->prev != nullptr)
        he->prev->next = nullptr;
    if(he->f != nullptr)
        removeFace(he->f->id,false);
    m_halfedges.erase(m_halfedges.begin() + edgeId);
    for(unsigned int i = edgeId ; i < m_halfedges.size() ; ++i)
        m_halfedges[i]->id--;

}

template<typename T>
inline const std::vector<std::shared_ptr<TVertex>>& TopologicalMesh<T>::getTVertices() const
{
    return m_tvertices;
}

template<typename T>
inline const std::vector<std::shared_ptr<Halfedge>>& TopologicalMesh<T>::getEdges() const
{
    return m_halfedges;
}

template<typename T>
inline const std::vector<std::shared_ptr<Face>>& TopologicalMesh<T>::getFaces() const
{
    return m_faces;
}

template<typename T>
inline const TVertex& TopologicalMesh<T>::getTVertex(IndexType id) const
{
    assert(id < m_tvertices.size());
    return *(m_tvertices[id].get());
}

template<typename T>
inline const Halfedge& TopologicalMesh<T>::getHalfedge(IndexType id) const
{
    assert(id < m_halfedges.size());
    return *(m_halfedges[id].get());
}

template<typename T>
inline const Face& TopologicalMesh<T>::getFace(IndexType id) const
{
    assert(id < m_faces.size());
    return *(m_faces[id].get());
}

template<typename T>
void TopologicalMesh<T>::setHalfedgeProp(IndexType id, Halfedge* prev , Halfedge* next , Halfedge* opposite , Face* f)
{
    assert(id < m_halfedges.size());

    m_halfedges[id]->prev = prev;
    m_halfedges[id]->next = next;
    m_halfedges[id]->opposite = opposite;
    m_halfedges[id]->f = f;
}

template<typename T>
inline void TopologicalMesh<T>::clearTVertices()
{
    m_tvertices.clear();
}

template<typename T>
inline void TopologicalMesh<T>::clearHalfedges()
{
    m_halfedges.clear();
}

template<typename T>
inline void TopologicalMesh<T>::clearFaces()
{
    m_faces.clear();
}

template<typename T>
void TopologicalMesh<T>::ComputeMeshFromTopo()
{
    this->clearVertices();
    this->clearIndices();
    for(const std::shared_ptr<TVertex>& v : m_tvertices)
        ressource::Mesh<T>::addVertex(T(v->p));
    for(const std::shared_ptr<Face>& f : m_faces)
    {
        Halfedge* he = f->e;
        unsigned int v_count = 0;
        do {
            v_count++;
            he = he->next;
        } while (he != f->e);

        assert(v_count == 3 || v_count == 4);
        if(v_count == 3)
        {
            do {
                this->addIndex(he->v->id);
                he = he->next;
            } while (he != f->e);
        }
        else
        {
            this->addIndex(he->v->id);
            this->addIndex(he->prev->v->id);
            this->addIndex(he->next->v->id);

            this->addIndex(he->prev->v->id);
            this->addIndex(he->next->next->v->id);
            this->addIndex(he->next->v->id);
        }
    }

}

template<typename T>
void TopologicalMesh<T>::ComputeTopoFromMesh()
{
    clearTVertices();
    clearHalfedges();
    clearFaces();

    for(const VertexType& v : this->m_vertices)
        addTVertex(v.position());


    unsigned int vertex_p_face = this->getNbrVertexPerFace();
    for(unsigned int i = 0 ; i < this->m_indices ; i += vertex_p_face)
    {
        std::vector<unsigned int> he_id;
        for(unsigned int j = i ; j < i + vertex_p_face; j++)
        {
            he_id.push_back(addHalfedge(this->m_tvertices[this->m_indices[j]].get()));
        }
        addFaceHE(he_id);
    }
}
template<typename T>
void TopologicalMesh<T>::ComputeTopoFromMesh(const ressource::Mesh<T>& mesh)
{
    clearTVertices();
    clearHalfedges();
    clearFaces();

    this->m_mode = mesh.getMode();
    for(const VertexType& v : mesh.vertices())
        addTVertex(v.position());


    unsigned int vertex_p_face = mesh.getNbrVertexPerFace();
    std::vector<unsigned int> indices = mesh.indices();
    for(unsigned int i = 0 ; i < indices.size() ; i += vertex_p_face)
    {
        std::vector<unsigned int> vids;
        for (unsigned int j = i; j < i + vertex_p_face; ++j)
            vids.push_back(indices[j]);
        addFace(vids);
    }
}

template<typename T>
void TopologicalMesh<T>::updateOpposite(Halfedge& h)
{
    for(const std::shared_ptr<Halfedge>& he : this->m_halfedges)
    {
        if(he->v->id == h.next->v->id && he->next->v->id == h.v->id)
        {
            h.opposite = he.get();
            he->opposite = &h;
            return;
        }
    }
    //if opposite edge doesn't exist
    // h.opposite = this->m_halfedges[addHalfedge(h.next->v->id)].get();
}

template<typename T>
inline unsigned int TopologicalMesh<T>::getFaceEdgeNbr(IndexType id) const
{
    std::vector<IndexType> hs;
    getFaceHalfEdges(id,hs);
    return hs.size();

}

template<typename T>
void TopologicalMesh<T>::Quandrangulize()
{
    for(unsigned int j = 0 ; j < this->m_halfedges.size() ; j++)
    {
        Halfedge* he = this->m_halfedges[j].get();
        if(he->opposite != nullptr)
        {
             Halfedge* hopp = he->opposite;
            if(getFaceEdgeNbr(he->f->id) == 3 && getFaceEdgeNbr(he->opposite->f->id) == 3)
            {
                std::vector<IndexType> hs;
                std::vector<IndexType> hopps;                

                getFaceHalfEdges(he->f->id,hs);
                getFaceHalfEdges(he->opposite->f->id,hopps);

                IndexType h_id = he->id;
                IndexType h_opp_id = he->opposite->id;

                std::vector<Halfedge*> new_hs;
                for(unsigned int i = 0 ; i < hs.size() ; i++)
                {
                    if(hs[i] != h_id)
                        new_hs.push_back(this->m_halfedges[hs[i]].get());
                }


                for(unsigned int i = 0 ; i < hopps.size() ; i++)
                    if(hopps[i] != h_opp_id)
                        new_hs.push_back(this->m_halfedges[hopps[i]].get());


                removeHalfedge(he->id);
                //becareful to shifts
                removeHalfedge(hopp->id);

                addFace(new_hs);

            }
        }
    }
}

} // namespace geometry

} // namespace skyengine

#endif // TOPOLOGICALMESH_H
