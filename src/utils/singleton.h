#ifndef SINGLETON_H
#define SINGLETON_H
namespace skyengine
{

namespace utility
{
/**
 * The singleton class offers a quick way to provide singleton for a given class. Simply inherit from it.
 */
template<typename TYPE>
class Singleton
{
public:

    // Copy and assignment are forbidden
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

    virtual ~Singleton() {}

    /**
     * @brief GetSingleton
     * @return the singleton for this class.
     */
    static TYPE *GetSingleton()
    {
        if (Singleton<TYPE>::singleton_ == nullptr)
        {
            Singleton<TYPE>::singleton_ = new TYPE();
        }

        return Singleton<TYPE>::singleton_;
    }

protected:
    static TYPE* singleton_;

    Singleton()
    {
        if (Singleton<TYPE>::singleton_ == nullptr)
        {
            Singleton<TYPE>::singleton_ = static_cast<TYPE*>(this);
        }
    }
};

template<typename TYPE>
TYPE* Singleton<TYPE>::singleton_(nullptr);

}
}
#endif // SINGLETON_H
