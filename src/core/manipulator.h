#ifndef MANIPULATOR_H
#define MANIPULATOR_H

#include "utils/skyengine_dll.h"

#include <memory>
#include "task.h"
#include "skyengine.h"

namespace skyengine
{

namespace core
{

class SKYENGINE_API Manipulator {

public:
    Manipulator() {}
    virtual ~Manipulator() {}

    virtual void update(double t, double dt)  =0;

};

class ManipulatorUpdateTask : public Task {
public:
    ManipulatorUpdateTask(std::shared_ptr<Manipulator> m) : manipulator {m} {}

    virtual void run() {manipulator->update(engine->getTotalTime(), engine->getDeltaTime());}

private:
    std::shared_ptr<Manipulator> manipulator;

};

}
}

#endif // MANIPULATOR_H
