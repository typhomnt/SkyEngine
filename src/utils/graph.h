#ifndef GRAPH_H
#define GRAPH_H

#include <cassert>
#include <memory>
#include <vector>


namespace skyengine
{

namespace utility
{

template <typename N>
struct GraphNode
{
    GraphNode(const N& data) : data(data) {}
    ~GraphNode(){}

    /*Adds an outgoing connection.*/
    void addConnection(GraphNode<N>* neighbor) { neighbors.push_back(neighbor); }

    /**
     * @brief Removes a GraphNode connection (does not delete the neighbor)
     * @param node Neighbor to remove (must not be NULL)
     */
    void removeConnection(GraphNode<N>* node);

    /**
     * @brief GraphNode degree
     * @return This GraphNode degree (i.e. its number of neighbors)
     */
    size_t degree() const { return neighbors.size(); }

    N data;
    std::vector<GraphNode<N>*> neighbors;
};

template<typename N>
void GraphNode<N>::removeConnection(GraphNode<N>* node)
{
    assert(node);

    for (auto it = neighbors.begin(); it != neighbors.end(); ++it)
    {
        if (*it == node)
        {
            neighbors.erase(it);
            return;
        }
    }
}



template <typename N>
class Graph
{
public:

    Graph();
    ~Graph();

    /*Add a node into this graph.*/
    void addNode(const GraphNode<N>& node);
    void addNode(const std::shared_ptr<GraphNode<N>>& node);

    GraphNode<N>* getNode(size_t index) const;
    size_t size() const { return m_nodes.size(); }

    /*Erases a node from this graph.*/
    void deleteNode(GraphNode<N> *node);

    const std::vector<std::shared_ptr<GraphNode<N>>>& getNodes() const { return m_nodes; }

private:

    /*This is the master list holding all nodes of this graph.*/
    std::vector<std::shared_ptr<GraphNode<N>> > m_nodes;
};

template<typename N>
Graph<N>::Graph()
{}

template<typename N>
Graph<N>::~Graph()
{}

template<typename N>
void Graph<N>::addNode(const GraphNode<N>& node)
{
    m_nodes.push_back(std::make_shared<GraphNode<N>>(node));
}

template<typename N>
void Graph<N>::addNode(const std::shared_ptr<GraphNode<N>>& node)
{
    m_nodes.push_back(node);
}

template<typename N>
GraphNode<N>* Graph<N>::getNode(size_t index) const
{
    assert(index < m_nodes.size());
    return m_nodes[index].get();
}

template<typename N>
void Graph<N>::deleteNode(GraphNode<N>* node)
{
    assert(node);

    for (auto it = m_nodes.begin(); it != m_nodes.end(); ++it)
    {
        (*it)->removeConnection(node);
        if (*it == node)
            m_nodes.erase(it);
    }
}

} // namespace utility

} // namespace skyengine

#endif // GRAPH_H
