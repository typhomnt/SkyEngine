#include "camera.h"
#include <glm/gtx/norm.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <utils/jsonhelper.h>
namespace skyengine
{

namespace scene
{

Camera::Camera(const std::string &name, SceneGraph *owner, const unsigned int type, const glm::mat4 &world_matrix):
    SceneGraphNode(name,owner,type,world_matrix),
    m_position(-1.0f, 0.0f, 0.0f),
    m_direction(1.0f, 0.0f, 0.0f),
    m_curr_direction(1.0f, 0.0f, 0.0f),
    m_up_vector(0.0f, 1.0f, 0.0f),
    m_pitch(0.0f),
    m_yaw(0.0f),
    m_fov(45.0f),
    m_ratio(4.0f/3.0f)
{}

void Camera::setPosition(const glm::vec3& position)
{
    m_position = position;
}

void Camera::setDirection(const glm::vec3& direction)
{
    glm::vec3 normalized_dir = glm::normalize(direction);

    m_direction = normalized_dir;
    m_curr_direction = normalized_dir;

    m_pitch = utility::clamp(
        (float)asin(normalized_dir.y),
        float(-M_PI * 0.5f + 0.01f),
        float(M_PI * 0.5f - 0.01f)
    );
    m_yaw = float(atan2(-normalized_dir.z, normalized_dir.x));
}

void Camera::setCurrDirection(const glm::vec3& curr_direction)
{
    m_curr_direction = curr_direction;
}

void Camera::setUpVector(const glm::vec3& up_vector)
{
    m_up_vector= up_vector;
}

void Camera::setFov(float fov)
{
    m_fov = fov;
}

void Camera::setRatio(float ratio)
{
    m_ratio = ratio;
}

void Camera::setPitch(float pitch)
{
    m_pitch = pitch;
}

void Camera::setYaw(float yaw)
{
    m_yaw = yaw;
}

void Camera::updateCameraWithCurrentAttributes()
{
    // Projection matrix :  Field of View, 4:3 ratio, display range : 0.1 unit <-> 1000 units
    m_projection_matrix = glm::perspective(m_fov, m_ratio, 0.1f, 1000.0f);

    // Camera matrix
    m_view_matrix = glm::lookAt(m_position,           // Camera is here
                                m_position + m_curr_direction, // and looks here : at the same position, plus "direction"
                                m_up_vector);
}

void Camera::resetCameraOrientation()
{
    m_pitch = 0.0f;
    m_yaw = 0.0f;
    m_curr_direction = m_direction;
}

void Camera::setPositionDirectionFromWMatrix()
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_world_matrix,scale,rotation,translation,skew,perspective);

    glm::vec4 rot_vev = glm::toMat4(rotation)*glm::vec4(m_direction,1.0f);
    setPosition(translation);
    setDirection(glm::vec3(rot_vev.x,rot_vev.y,rot_vev.z));
}

void Camera::setWorldMatrixFromPositionDirection()
{
    glm::vec3 direction_n = glm::normalize(m_curr_direction);
    glm::quat rot_quat(glm::vec3(glm::dot(direction_n,glm::vec3(1,0,0)),glm::dot(direction_n,glm::vec3(0,1,0)),glm::dot(direction_n,glm::vec3(0,0,1))));
    setWorldMatrix(glm::translate(glm::mat4(1.0f),m_position)*glm::toMat4(rot_quat));
}

void Camera::setWorldMatrix(const glm::mat4& world_matrix)
{
    SceneGraphNode::setWorldMatrix(world_matrix);
    setPositionDirectionFromWMatrix();

}

std::shared_ptr<rapidjson::Value> Camera::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    std::shared_ptr<rapidjson::Value> obj = SceneGraphNode::SerializeProp(al);

    utility::JSONHelper::SaveVec(*(obj.get()), "Position", m_position, al);
    utility::JSONHelper::SaveVec(*(obj.get()), "Direction", m_direction, al);
    utility::JSONHelper::SaveVec(*(obj.get()), "CurrentDirection", m_curr_direction, al);
    utility::JSONHelper::SaveVec(*(obj.get()), "UpVector", m_up_vector, al);

    obj->AddMember(rapidjson::Value("Pitch", al),
                   rapidjson::Value(m_pitch),
                   al);
    obj->AddMember(rapidjson::Value("Yaw", al),
                   rapidjson::Value(m_yaw),
                   al);
    obj->AddMember(rapidjson::Value("FoV", al),
                   rapidjson::Value(m_fov),
                   al);
    obj->AddMember(rapidjson::Value("AspectRatio", al),
                   rapidjson::Value(m_ratio),
                   al);

    return obj;
}

void Camera::LoadProp(const rapidjson::Value& value)
{
    SceneGraphNode::LoadProp(value);

    utility::JSONHelper::ReadVec(value["Position"], m_position);
    utility::JSONHelper::ReadVec(value["Direction"], m_position);
    utility::JSONHelper::ReadVec(value["CurrentDirection"], m_position);
    utility::JSONHelper::ReadVec(value["UpVector"], m_position);
    m_pitch = value["Pitch"].GetFloat();
    m_yaw = value["Yaw"].GetFloat();
    m_fov = value["FoV"].GetFloat();
    m_ratio = value["AspectRatio"].GetFloat();

    updateCameraWithCurrentAttributes();
}

} // namespace scene

} // namespace skyengine
