#include "physicsmanager.h"
#include "utils/utilities.h"
#include "BulletCollision/CollisionDispatch/btCollisionDispatcher.h"
#include "BulletCollision/BroadphaseCollision/btDispatcher.h"
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
namespace skyengine
{

namespace physics
{
PhysicsManager::PhysicsManager()
{
    ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
    m_conf = std::make_shared<btDefaultCollisionConfiguration>();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_collision_disp = std::make_shared<btCollisionDispatcher>(m_conf.get());

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    m_broad_interface = std::make_shared<btDbvtBroadphase>();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    m_const_solver = std::make_shared<btSequentialImpulseConstraintSolver>();

    m_dyn_world = std::make_shared<btDiscreteDynamicsWorld>(m_collision_disp.get(), m_broad_interface.get(), m_const_solver.get(), m_conf.get());

    m_dyn_world->setGravity(btVector3(0, -10, 0));
}

void PhysicsManager::ComputeSimulation(float dt)
{
    m_dyn_world->stepSimulation(dt, 10);
}

void PhysicsManager::AddBoxCollider(scene::GameObject3D *node, const glm::vec3& dimensions, const glm::vec3& origin, float m)
{

    node->addParameter(std::make_shared<scene::NodeParameter<btBoxShape> >("box_collider",1,btBoxShape(btVector3(dimensions.x, dimensions.z, dimensions.y))));


    btTransform nodeTransform = Mat4TobtTransform(node->getWorldMatrix());
    btCollisionShape* collider = &(std::dynamic_pointer_cast<scene::NodeParameter<btBoxShape>>(node->getParameter("box_collider"))->value);
    nodeTransform.setOrigin(btVector3(origin.x, origin.y,origin.z));

    btScalar mass(m);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.0f);
    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        collider->calculateLocalInertia(mass, localInertia);

    //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* motionState = new btDefaultMotionState(nodeTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, collider, localInertia);

    node->addParameter(std::make_shared<scene::NodeParameter<btRigidBody> >("rigid_body",1,btRigidBody(rbInfo)));
    //add the body to the dynamics world
    m_dyn_world->addRigidBody(&(std::dynamic_pointer_cast<scene::NodeParameter<btRigidBody>>(node->getParameter("rigid_body"))->value));
}



void PhysicsManager::setEngine(core::SkyEngine* skyengine)
{
    m_engine = skyengine;
}

bool PhysicsManager::AddPhyTask(std::shared_ptr<core::Task> phy_task)
{
    if(!skyengine::utility::MapContains(m_phy_tasks,phy_task->getName()))
    {
        skyengine::utility::MapInsert(m_phy_tasks,phy_task->getName(),phy_task.get());
        m_engine->addTask(phy_task);
        return true;
    }
    return false;
}

glm::mat4 btTransformToMat4(const btTransform& transf)
{
    btQuaternion quat = transf.getRotation();
    btVector3 transl = transf.getOrigin();
    glm::quat rotation(quat.w(),quat.x(),quat.y(),quat.z());
    glm::vec3 translation(transl.x(),transl.y(),transl.z());
    return glm::translate(glm::mat4(1.0f),translation)*glm::toMat4(rotation);
}

btTransform Mat4TobtTransform(const glm::mat4& transf)
{
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(transf, scale, rotation, translation, skew, perspective);
    return btTransform(btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w),btVector3(translation.x,translation.y,translation.z));
}

}
}

