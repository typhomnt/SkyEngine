#ifndef __GUI_UTILS_H__
#define __GUI_UTILS_H__

#include <string>

#include <imgui/imgui.h>


namespace module
{

namespace gui
{

/**
 * @brief Utility class providing methods for interaction with GUI (themes ...).
 */
class GuiUtils
{
public:
    /**
     * @brief   Applies the given GUI theme.
     * @details A JSON file, named @e themeName.json, must be defined in the @e themes directory.
     * @param   themeName Theme name (without JSON extension)
     */
    static void ApplyTheme(const std::string& themeName);

    /**
     * @brief Saves the current GUI theme to a JSON file.
     * @details The file will be saved in the @e themes directory and named @e themeName.json.
     * @param themeName Theme name (without JSON extension)
     */
    static void SaveTheme(const std::string& themeName);
};

} // namespace gui

} // namespace module

#endif // __GUI_UTILS_H__
