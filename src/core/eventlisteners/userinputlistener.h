#ifndef USERINPUTLISTENER_H
#define USERINPUTLISTENER_H

#include "utils/skyengine_dll.h"

#include "../listener.h"
#include "glm/glm.hpp"


namespace skyengine
{

namespace core
{

class KeyEvent : public Listenable<KeyEvent>
{
public:
    KeyEvent() {}
    virtual ~KeyEvent() {}

    virtual void send(int k, int s, int a, int m) { key_ = k; scancode_ = s; action_ = a; mods_ = m; notifyListeners(); }

    int key()      const {return key_;   }
    int scancode() const {return scancode_; }
    int action()   const {return action_;}
    int mods()     const {return mods_;}

protected:
    int key_, scancode_, action_, mods_;
};

class CharEvent : public Listenable<CharEvent>
{
public:
    CharEvent(){}
    virtual ~CharEvent(){}

    virtual void send(unsigned int codepoint_) {codepoint = codepoint_; notifyListeners(); }
    unsigned int code() const {return codepoint;}

protected:
    unsigned int codepoint;
};

class CharModsEvent : public Listenable<CharModsEvent>
{
public:
    CharModsEvent(){}
    virtual ~CharModsEvent(){}

    virtual void send(unsigned int codepoint, int m){codepoint_ = codepoint; mods_ = m; notifyListeners(); }
    unsigned int code() const {return codepoint_;}
    int mods()     const {return mods_;}

protected:
    unsigned int codepoint_;
    int mods_;
};

class MouseButtonEvent : public Listenable<MouseButtonEvent>
{
public:
    MouseButtonEvent(){}
    virtual ~MouseButtonEvent(){}

    virtual void send(int b, int a, int m) { button_ = b; action_ = a; mods_ = m; notifyListeners(); }

    int button() const {return button_;}
    int action() const {return action_;}
    int mods() const   {return mods_;}

protected:
    int button_, action_, mods_;
};

class MouseMoveEvent : public Listenable<MouseMoveEvent>
{
public:
    MouseMoveEvent(){}
    virtual ~MouseMoveEvent(){}

    virtual void send(glm::vec2 p) {pos_ = p; notifyListeners(); }

    glm::vec2 pos() const  { return pos_;}

protected:
    glm::vec2 pos_;
};

class MouseEnterEvent : public Listenable<MouseEnterEvent>
{
public:
    MouseEnterEvent(){}
    virtual ~MouseEnterEvent() {}

    virtual void send(bool e) {enter_ = e; notifyListeners();}

    bool enter() const {return enter_;}

protected:
    bool enter_;
};

class MouseScrollEvent : public Listenable<MouseScrollEvent>
{
public:
    MouseScrollEvent(){}
    virtual ~MouseScrollEvent() {}

    virtual void send(glm::vec2 ofst) {offset_ = ofst; notifyListeners();}

    glm::vec2 offset() const {return offset_;}
protected:
    glm::vec2 offset_;
};

class PathDropEvent : public Listenable<PathDropEvent>
{
public:
    PathDropEvent() {}
    virtual ~PathDropEvent() {}

    virtual void send(std::vector<std::string> p) { paths_= p;  notifyListeners(); }

    const std::vector<std::string>& paths() const {return paths_;}

protected:
    std::vector<std::string> paths_;
};


class JoystickConfigEvent : public Listenable<JoystickConfigEvent>
{
public:
    JoystickConfigEvent() {}
    virtual ~JoystickConfigEvent() {}

    virtual void send(int joy, int config) {joy_ = joy; config_ = config;  notifyListeners(); }

    int config() const {return config_;}

protected:
    int joy_;
    int config_;
};

/*
 Left and right on left sticker.
 Up and down on left sticker.
 Left and right back triggers.
 Up and down on right sticker.
 Left and right on right sticker.
 */
class JoystickAxisEvent : public Listenable<JoystickAxisEvent>
{
public:
    JoystickAxisEvent() {}
    virtual ~JoystickAxisEvent() {}

    virtual void send(int joy, const std::vector<float>& v) {joy_ = joy, values_= v;  notifyListeners(); }

    const std::vector<float>& values() const {return values_;}
    int joy() {return joy_;}

protected:
    int joy_;
    std::vector<float> values_;
};


/*
A
B
X
Y
Left shoulder button
Right shoulder button
Back button <
Start button >
Left sticker button
Right sticker button
D-Pad Up
D-Pad Right
D-Pad Down
D-Pad Left
*/
class JoystickButtonEvent : public Listenable<JoystickButtonEvent>
{
public:
    JoystickButtonEvent() {}
    virtual ~JoystickButtonEvent() {}

    virtual void send(int joy, const std::vector<char>& v) {joy_ = joy; values_= v;  notifyListeners(); }

    const std::vector<char>& values() const {return values_;}
    int joy() {return joy_;}

protected:
    int joy_;
    std::vector<char> values_;
};

using KeyListener         = Listener<KeyEvent>;
using CharListener        = Listener<CharEvent>;
using CharModsListener    = Listener<CharModsEvent>;
using MouseButtonListener = Listener<MouseButtonEvent>;
using MouseMoveListener   = Listener<MouseMoveEvent>;
using MouseEnterListener   = Listener<MouseEnterEvent>;
using MouseScrollListener = Listener<MouseScrollEvent>;
using PathDropListener = Listener<PathDropEvent>;
using JoystickConfigListener = Listener<JoystickConfigEvent>;
using JoystickAxisListener = Listener<JoystickAxisEvent>;
using JoystickButtonListener = Listener<JoystickButtonEvent>;


class UserInputManager
{
public:
    UserInputManager() :
        keyEvent{std::make_shared<KeyEvent>()}, charEvent{std::make_shared<CharEvent>()},
        charModsEvent{std::make_shared<CharModsEvent>()}, mouseButtonEvent{std::make_shared<MouseButtonEvent>()},
        mouseMoveEvent{std::make_shared<MouseMoveEvent>()}, mouseEnterEvent{std::make_shared<MouseEnterEvent>()},
        mouseScrollEvent{std::make_shared<MouseScrollEvent>()}, pathDropEvent{std::make_shared<PathDropEvent>()},
        joystickConfigEvent{std::make_shared<JoystickConfigEvent>()},joystickAxisEvent{std::make_shared<JoystickAxisEvent>()},
        joystickButtonEvent{std::make_shared<JoystickButtonEvent>()}
    {}
    virtual ~UserInputManager(){}


    std::shared_ptr<KeyEvent>               keyEvent;
    std::shared_ptr<CharEvent>              charEvent;
    std::shared_ptr<CharModsEvent>          charModsEvent;
    std::shared_ptr<MouseButtonEvent>       mouseButtonEvent;
    std::shared_ptr<MouseMoveEvent>         mouseMoveEvent;
    std::shared_ptr<MouseEnterEvent>        mouseEnterEvent;
    std::shared_ptr<MouseScrollEvent>       mouseScrollEvent;
    std::shared_ptr<PathDropEvent>          pathDropEvent;
    std::shared_ptr<JoystickConfigEvent>    joystickConfigEvent;
    std::shared_ptr<JoystickAxisEvent>      joystickAxisEvent;
    std::shared_ptr<JoystickButtonEvent>    joystickButtonEvent;

};

class SKYENGINE_API UserInputListener :
        public KeyListener, public CharListener,
        public CharModsListener, public MouseButtonListener,
        public MouseMoveListener, public MouseEnterListener,
        public MouseScrollListener, public PathDropListener,
        public JoystickConfigListener, public JoystickAxisListener,
        public JoystickButtonListener
{
public:
    UserInputListener(std::shared_ptr<UserInputManager> uim) :
        KeyListener(uim->keyEvent), CharListener(uim->charEvent),
        CharModsListener(uim->charModsEvent), MouseButtonListener(uim->mouseButtonEvent),
        MouseMoveListener(uim->mouseMoveEvent), MouseEnterListener(uim->mouseEnterEvent),
        MouseScrollListener(uim->mouseScrollEvent), PathDropListener(uim->pathDropEvent),
        JoystickConfigListener(uim->joystickConfigEvent), JoystickAxisListener(uim->joystickAxisEvent),
        JoystickButtonListener(uim->joystickButtonEvent){}
    virtual ~UserInputListener(){}
    void setListenerActive(bool b){KeyListener::setListenerActive(b);CharListener::setListenerActive(b);
                                   CharModsListener::setListenerActive(b);MouseButtonListener::setListenerActive(b);
                                                              MouseMoveListener::setListenerActive(b);MouseEnterListener::setListenerActive(b);
                                                                                         MouseScrollListener::setListenerActive(b);PathDropListener::setListenerActive(b);
                                                                                                                    JoystickConfigListener::setListenerActive(b);JoystickAxisListener::setListenerActive(b);
                                                                                                                                               JoystickButtonListener::setListenerActive(b);}
};

}
}


#endif // USERINPUTLISTENER_H
