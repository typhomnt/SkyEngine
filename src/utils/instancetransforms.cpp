#include "instancetransforms.h"

namespace skyengine
{
namespace utility
{

std::vector<ressource::BufferAttrib> Tmat4::getBufferAttribs(GLuint first_index)
{
    std::vector<ressource::BufferAttrib> attribs;
    attribs.push_back(ressource::BufferAttrib(first_index, 4, GL_FLOAT, (size_t)0, 4*sizeof(glm::vec4),GL_FALSE,1));
    attribs.push_back(ressource::BufferAttrib(first_index + 1, 4, GL_FLOAT, (size_t)sizeof(glm::vec4), 4*sizeof(glm::vec4),GL_FALSE,1));
    attribs.push_back(ressource::BufferAttrib(first_index + 2, 4, GL_FLOAT, (size_t)(2*sizeof(glm::vec4)), 4*sizeof(glm::vec4),GL_FALSE,1));
    attribs.push_back(ressource::BufferAttrib(first_index + 3, 4, GL_FLOAT, (size_t)(3*sizeof(glm::vec4)), 4*sizeof(glm::vec4),GL_FALSE,1));
    return attribs;
}


std::vector<ressource::BufferAttrib> Tvec3::getBufferAttribs(GLuint first_index)
{
    std::vector<ressource::BufferAttrib> attribs;
    attribs.push_back(ressource::BufferAttrib(first_index, 3, GL_FLOAT, (size_t)0, sizeof(glm::vec3),GL_FALSE,1));
    return attribs;
}

}
}
