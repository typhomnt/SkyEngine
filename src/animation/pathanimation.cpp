#include "pathanimation.h"
#include "utils/logger.h"
#include "utils/utilities.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "math/mathutils.h"
namespace skyengine
{

namespace animation
{
PathAnimation::PathAnimation(const std::string &animation_name): Animation(animation_name),m_path(std::make_shared<geometry::GeometryCurve<TimedParameter<glm::vec3>>>())
{

}

PathAnimation::~PathAnimation()
{

}

void PathAnimation::clearPath()
{
    if(m_path != nullptr)
    {
        m_path->RemovePoints();
        for(auto& key: m_time_keys)
        {
            m_time_keys[key.first].clear();
        }
        keyframes.clear();
    }
}

void PathAnimation::setPath(const geometry::GeometryCurve<TimedParameter<glm::vec3>>& path, const std::string key_name)
{
    for(unsigned int i = 0 ; i < path.GetSize(); i++)
        addPoint(path.GetPoint(i),key_name);

}

void PathAnimation::setPath(const geometry::GeometryCurve<glm::vec3>& path,const std::string key_name)
{
    for(unsigned int i = 0 ; i < path.GetSize(); i++)
        addPoint(TimedParameter<glm::vec3>(path.GetPoint(i),i),key_name);
}

void PathAnimation::addPoint(const glm::vec3& point,const std::string &key_name)
{
    addPoint(TimedParameter<glm::vec3>(point,m_path->GetPoint(m_path->GetSize() -1).time + 1.0f),key_name);
}

void PathAnimation::addPoint(const TimedParameter<glm::vec3>& point, const std::string &key_name)
{
    m_path->AddPoint(point);
    glm::mat4 transform_matrix = glm::translate(glm::mat4(1.0f),point.value);
    if(key_name == "")
        for(auto& key : m_time_keys)
            addKey(key.first,std::make_shared<TimedParameter<glm::mat4>>(transform_matrix,point.time));
    else
        addKey(key_name,std::make_shared<TimedParameter<glm::mat4>>(transform_matrix,point.time));
}

void PathAnimation::addKey(const std::string& key_name, std::shared_ptr<BaseTimeParameter> key_value)
{
    if(std::dynamic_pointer_cast<TimedParameter<glm::mat4>>(key_value) == nullptr && std::dynamic_pointer_cast<TimedParameter<glm::mat4*>>(key_value) == nullptr)
    {
        utility::Logger::Error("Cannot add type key other than mat4 or mat4* for path animation");
        return;
    }

    Animation::addKey(key_name,key_value);
    KeyFrame keyframe(key_value->time);
    keyframe.addKeyParameter(key_name,key_value.get());
    addKeyFrame(keyframe);

}

void PathAnimation::ComputeAnimation(float time_stamp)
{
    if(m_path->GetSize() == 0)
        return;
    unsigned int point_index = 0;
    for(unsigned int i = 0; i < m_path->GetSize() ; i++)
    {
        if (time_stamp <= m_path->GetPoint(i).time + ANIM_EPS1)
        {
            if(math::ApproxEqual<float>(time_stamp,m_path->GetPoint(i).time,ANIM_EPS1))
                point_index = i;
            else
                point_index= i - 1;
        }
    }
    if(time_stamp >= m_path->GetPoint(m_path->GetSize() - 1).time + ANIM_EPS1)
        point_index = m_path->GetSize() -1;

    glm::mat4 translation_matrix;
    glm::mat4 rotation_follow;
    if(point_index ==  m_path->GetSize() -1)
    {
        translation_matrix = glm::translate(glm::mat4(1.0f),m_path->GetPoint(point_index).value);
    }
    else
    {
        unsigned int next_point_index = point_index + 1;
        assert(next_point_index < m_path->GetSize());
        float delta_time = m_path->GetPoint(next_point_index).time - m_path->GetPoint(point_index).time;
        float interpolation_factor = utility::clamp((time_stamp - m_path->GetPoint(point_index).time) / delta_time,0.0f,1.0f);
        translation_matrix = glm::translate(glm::mat4(1.0f),(1.0f - interpolation_factor)*m_path->GetPoint(point_index).value + interpolation_factor*m_path->GetPoint(next_point_index).value);
    }
    if(point_index >= 1)
    {
        /*glm::vec3 dir = m_path->GetPoint(point_index).value -  m_path->GetPoint(point_index - 1).value;
        dir /= glm::l1Norm(dir);
        float x_axis_angle = acos(glm::dot(glm::vec3(1.0,0,0),dir));
        float y_axis_angle= acos(glm::dot(glm::vec3(0.0,1.0,0),dir));
        float z_axis_angle= acos(glm::dot(glm::vec3(0.0,0,1.0),dir));
        rotation_follow = glm::toMat4(glm::quat(glm::vec3(x_axis_angle,y_axis_angle,z_axis_angle)));*/
        translation_matrix *= rotation_follow;
        //translation_matrix =  translation_matrix * glm::eulerAngleXYZ(m_additionnal_rot[point_index].x,m_additionnal_rot[point_index].y,m_additionnal_rot[point_index].z);
    }
    for(auto& key: m_time_keys)
    {
        //m_last_computed_keys[key.first] = std::make_shared<TimedParameter<glm::mat4>>(interpolateKey<glm::mat4>(key.first,time_stamp,math::InterpolateMatrix),time_stamp);
        m_last_computed_keys[key.first] = std::make_shared<TimedParameter<glm::mat4>>(translation_matrix,time_stamp);
    }
}

}
}
