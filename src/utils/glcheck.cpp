#include "glcheck.h"

#include <sstream>

#include <GL/glew.h>

#include "logger.h"

using skyengine::utility::Logger;


void checkGLErrors(const char *func, const char *file, int line)
{
    GLenum error = GL_NO_ERROR;
    while ( (error = glGetError()) != GL_NO_ERROR )
    {
        std::stringstream ss;
        ss << "OpenGL error in " << func << " (" << file << ", line " << line << ") : ";

        switch (error)
        {
            case GL_INVALID_ENUM:
                ss << "Invalid enum";
                break;
            case GL_INVALID_VALUE:
                ss << "Invalid value";
                break;
            case GL_INVALID_OPERATION:
                ss << "Invalid operation";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                ss << "Invalid Framebuffer Operation";
                break;
            case GL_OUT_OF_MEMORY:
                ss << "Out of Memory";
                break;
            case GL_STACK_OVERFLOW:
                ss << "Stack Overflow";
                break;
            case GL_STACK_UNDERFLOW:
                ss << "Stack Underflow";
                break;
            default:
                ss << "Unknown error";
                break;
        }

        Logger::Error(ss.str());
    }
}
