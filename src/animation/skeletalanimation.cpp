#include "skeletalanimation.h"
#include "animationmacros.h"
#include <iostream>
#include "../utils/utilities.h"
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include "geometry/geometryalgorithms.h"

#ifdef __LMA__
#include <ActionDetector/motionmanager.h>
#include <Utils/utility.h>
#include "doodle/doodleutils.h"

#endif

namespace skyengine
{

namespace animation
{


SkeletalAnimation::SkeletalAnimation(const std::string &animation_name):
    Animation(animation_name)
{    
    m_last_computed_poses.resize(MAX_BONES);
}

SkeletalAnimation::~SkeletalAnimation()
{
    if(m_last_pose_skel != nullptr)
        ressource::RessourceManager::Remove(m_last_pose_skel->getName());
}


void SkeletalAnimation::setAnimationSkeleton(Skeleton *animation_skeleton)
{    
    m_animation_skeleton = animation_skeleton;
    m_last_pose_skel = std::dynamic_pointer_cast<Skeleton>(animation_skeleton->Duplicate());
}


Skeleton* SkeletalAnimation::getAnimationSkeleton() const
{
    return m_animation_skeleton;
}



std::unordered_map<std::string, std::map<float, Pose3D> > SkeletalAnimation::getSkeletalAnimationPoses()
{
    std::unordered_map<std::string, std::map<float,Pose3D> > animation_poses;
    for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
    {
        glm::vec3 translation_vec;
        glm::vec3 scale_vec;
        glm::quat rotation_quat;

        //Local transforms computation for eash key frame
        ComputeSkelAnim(keyframes[i]->getTimeStamp());

        for(auto& anim_bone : m_key_groups)
        {
            translation_vec = std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(getLastComputedKey(anim_bone.first + "_position"))->value;
            scale_vec = std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(getLastComputedKey(anim_bone.first + "_scale"))->value;
            rotation_quat = std::dynamic_pointer_cast<TimedParameter<glm::quat>>(getLastComputedKey(anim_bone.first + "_rotation"))->value;
            animation_poses[anim_bone.first][keyframes[i]->getTimeStamp()] = Pose3D(scale_vec,rotation_quat,translation_vec);
        }
    }
    return animation_poses;

}

void SkeletalAnimation::ComputeMvtSortedBones()
{
    std::map<std::string,float> bone_energy_map;
    std::vector<std::string> sorted_energy_vector;
    std::unordered_map<std::string, std::map<float,Pose3D> > animation_poses;

    //Energy map init
    for(auto& anim_bone : m_key_groups)
        bone_energy_map[anim_bone.first] = 0.0f;

    //For each keyframe we compute bone tranform energy and accumulate it
    animation_poses = getSkeletalAnimationPoses();

    //Energy computation, note the -1 for i border because we deal with speeds
    for(unsigned int i = 0 ; i < keyframes.size() - 1 ; ++i)
        for(auto& anim_bone : m_key_groups)
            bone_energy_map[anim_bone.first] += animation_poses[anim_bone.first][keyframes[i+1]->getTimeStamp()].dist(animation_poses[anim_bone.first][keyframes[i]->getTimeStamp()]);


    //We sort results using map ordering
    std::multimap<float,std::string> sorted_energy_map = utility::MapFlip<std::string,float>(bone_energy_map);
    for(auto& bone_energy : sorted_energy_map)
    {
        sorted_energy_vector.insert(sorted_energy_vector.begin(),bone_energy.second);
        m_mvt_sorted_bones[bone_energy.first] = bone_energy.second;
        //std::cout << " bone " << bone_energy.second << " energy " << bone_energy.first << std::endl;
    }
}

void SkeletalAnimation::computeBoneLocalTrajectories()
{
    m_bone_local_trajectories.clear();
    for(auto& anim_bone : m_key_groups)
    {
        m_bone_local_trajectories[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }

    std::unordered_map<std::string, std::map<float,Pose3D> > animation_poses;
    animation_poses = getSkeletalAnimationPoses();

    for(std::shared_ptr<KeyFrame> keyframe : keyframes)
    {
        for(auto& anim_bone : m_key_groups)
        {
            m_bone_local_trajectories[anim_bone.first]->AddPoint(KeyPose3D(animation_poses[anim_bone.first][keyframe->getTimeStamp()],keyframe->getTimeStamp()));
        }
    }
}

void SkeletalAnimation::computeBoneLocalSpeeds()
{
    m_bone_local_speed_curves.clear();
    for(auto& anim_bone : m_key_groups)
    {
        m_bone_local_speed_curves[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }

    std::unordered_map<std::string, std::map<float,Pose3D> > animation_poses;
    animation_poses = getSkeletalAnimationPoses();

    for(auto& anim_bone : m_key_groups)
        m_bone_local_speed_curves[anim_bone.first]->AddPoint(KeyPose3D());

    for(unsigned int i = 1 ; i < keyframes.size() ; ++i)
        for(auto& anim_bone : m_key_groups)
            m_bone_local_speed_curves[anim_bone.first]->AddPoint(KeyPose3D
                                                                 ((animation_poses[anim_bone.first][keyframes[i]->getTimeStamp()] -  animation_poses[anim_bone.first][keyframes[i-1]->getTimeStamp()])
                    /(keyframes[i]->getTimeStamp() - keyframes[i-1]->getTimeStamp())
                    ,keyframes[i]->getTimeStamp()));

}

void SkeletalAnimation::computeBoneLocalAffsSpeeds()
{
    m_bone_local_aff_speed_curves.clear();
    for(auto& anim_bone : m_key_groups)
    {
        m_bone_local_aff_speed_curves[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }

    for(auto& anim_bone : m_key_groups)
    {
        std::vector<TimedParameter<glm::vec3>> scale;
        std::vector<TimedParameter<glm::vec3>> rotation;
        std::vector<TimedParameter<glm::vec3>> position;


        for(auto& scales : getKey(anim_bone.first + "_scale"))
        {
            scale.push_back(*(std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(scales).get()));
        }

        for(auto& rotations : getKey(anim_bone.first + "_rotation"))
        {
            rotation.push_back(TimedParameter<glm::vec3>(glm::eulerAngles(std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations)->value)
                                                         ,rotations->time));
        }

        for(auto& positions : getKey(anim_bone.first + "_position"))
        {
            position.push_back(*(std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(positions).get()));
        }


        geometry::ComputeCurveAffineSpeed(scale,animation::TimedParameter<glm::vec3>(),1);
        geometry::ComputeCurveAffineSpeed(rotation,animation::TimedParameter<glm::vec3>(),1);
        geometry::ComputeCurveAffineSpeed(position,animation::TimedParameter<glm::vec3>(),1);


        for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
            m_bone_local_aff_speed_curves[anim_bone.first]->AddPoint(
                        KeyPose3D(Pose3D(scale[i].value,glm::quat(rotation[i].value),position[i].value)
                                  ,keyframes[i]->getTimeStamp()));

    }

}

void SkeletalAnimation::computeBoneLocalAcceleration()
{
    m_bone_local_accel_curves.clear();
    for(auto& anim_bone : m_key_groups)
    {
        m_bone_local_accel_curves[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }

    std::unordered_map<std::string, std::map<float,Pose3D> > animation_poses;
    animation_poses = getSkeletalAnimationPoses();


    for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
        for(auto& anim_bone : m_key_groups)
        {
            unsigned int prec_i = i-1;
            unsigned int succ_i = i+1;
            if(i == 0)
                prec_i = 0;
            if(i == keyframes.size() - 1)
                succ_i = i;
            float dt1 = keyframes[succ_i]->getTimeStamp() - keyframes[i]->getTimeStamp();
            float dt2 = keyframes[i]->getTimeStamp() - keyframes[prec_i]->getTimeStamp();
            if(i == 0)
                dt2 = 1.0f;
            if(i == keyframes.size() - 1)
                dt1 = 1.0f;

            m_bone_local_accel_curves[anim_bone.first]->AddPoint(
                        KeyPose3D(
                            (animation_poses[anim_bone.first][keyframes[succ_i]->getTimeStamp()]
                        + animation_poses[anim_bone.first][keyframes[prec_i]->getTimeStamp()] - animation_poses[anim_bone.first][keyframes[i]->getTimeStamp()]*2.0f)
                    /((dt2*dt2) + (dt1*dt1)),keyframes[i]->getTimeStamp()));
        }
}

void SkeletalAnimation::computeBoneGlobalTrajectories()
{
    m_bone_global_trajectories.clear();
    //TODO do better
    for(auto& anim_bone : m_animation_skeleton->getBones())
    {
        m_bone_global_trajectories[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }
    for(std::shared_ptr<KeyFrame> keyframe : keyframes)
    {
        ComputeSkelAnim(keyframe->getTimeStamp());
        m_last_pose_skel->updateHierarchicalTransforms();
        for(auto& anim_bone : m_last_pose_skel->getBones())
        {
            m_bone_global_trajectories[anim_bone.first]->AddPoint(KeyPose3D(anim_bone.second->global_pose,keyframe->getTimeStamp()));
        }
    }
}
void SkeletalAnimation::computeBoneGlobalSpeeds()
{

    m_bone_global_speed_curves.clear();
    /*
    std::unordered_map<std::string, std::map<float,Pose3D> > animation_global_poses;
    for(auto& anim_bone : m_key_groups)
    {
        m_bone_global_speed_curves[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }

    for(std::shared_ptr<KeyFrame> keyframe : keyframes)
    {
        ComputeBoneTransform(keyframe->getTimeStamp(),animation_skeleton->getRootBone(), animation_skeleton->getGlobalInverseTransform());
        for(auto& anim_bone : m_animated_bone_sequence)
        {
            animation_global_poses[anim_bone.first][keyframe->getTimeStamp()] = anim_bone.second->bone->global_pose;
        }
    }

    for(unsigned int i = 1 ; i < keyframes.size() ; ++i)
    {
        for(auto& anim_bone : m_animated_bone_sequence)
            m_bone_global_speed_curves[anim_bone.first]->AddPoint(
                        KeyPose3D((animation_global_poses[anim_bone.first][keyframes[i]->getTimeStamp()]
                        - animation_global_poses[anim_bone.first][keyframes[i-1]->getTimeStamp()])/(keyframes[i]->getTimeStamp() - keyframes[i-1]->getTimeStamp())
                    ,keyframes[i]->getTimeStamp()));
    }
*/
}

void SkeletalAnimation::computeBoneGlobalAcceleration()
{
    m_bone_global_accel_curves.clear();
    /*
    std::unordered_map<std::string, std::map<float,Pose3D> > animation_global_poses;
    for(auto& anim_bone : m_animated_bone_sequence)
    {
        m_bone_global_accel_curves[anim_bone.first] = std::make_shared<Bone3DTrajectory>();
    }

    for(std::shared_ptr<KeyFrame> keyframe : keyframes)
    {
        ComputeBoneTransform(keyframe->getTimeStamp(),animation_skeleton->getRootBone(), animation_skeleton->getGlobalInverseTransform());
        for(auto& anim_bone : m_animated_bone_sequence)
        {
            animation_global_poses[anim_bone.first][keyframe->getTimeStamp()] = anim_bone.second->bone->global_pose;
        }
    }

    for(unsigned int i = 0 ; i < keyframes.size() ; ++i)
    {
        for(auto& anim_bone : m_animated_bone_sequence)
        {
            unsigned int prec_i = i-1;
            unsigned int succ_i = i+1;
            if(i == 0)
                prec_i = 0;
            if(i == keyframes.size() - 1)
                succ_i = i;
            float dt1 = keyframes[succ_i]->getTimeStamp() - keyframes[i]->getTimeStamp();
            float dt2 = keyframes[i]->getTimeStamp() - keyframes[prec_i]->getTimeStamp();
            if(i == 0)
                dt2 = 1.0f;
            if(i == keyframes.size() - 1)
                dt1 = 1.0f;

            m_bone_global_accel_curves[anim_bone.first]->AddPoint(
                        KeyPose3D(
                            ((animation_global_poses[anim_bone.first][keyframes[succ_i]->getTimeStamp()] - animation_global_poses[anim_bone.first][keyframes[i]->getTimeStamp()])*dt2
                    -(animation_global_poses[anim_bone.first][keyframes[i]->getTimeStamp()]-animation_global_poses[anim_bone.first][keyframes[prec_i]->getTimeStamp()])*dt1)
                    /(dt2*dt1*dt1),keyframes[i]->getTimeStamp()));

        }
    }
    */
}

std::unordered_map<std::string,Bone3DTrajectory> SkeletalAnimation::getBonesLocalTrajectoryUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_local_trajectories);
}

std::unordered_map<std::string,Bone3DTrajectory>  SkeletalAnimation::getBonesLocalSpeedUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_local_speed_curves);
}

std::unordered_map<std::string,Bone3DTrajectory> SkeletalAnimation::getBonesLocalAffSpeedUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_local_aff_speed_curves);
}

std::unordered_map<std::string,Bone3DTrajectory>  SkeletalAnimation::getBonesLocalAccelUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_local_accel_curves);
}

std::unordered_map<std::string,Bone3DTrajectory> SkeletalAnimation::getBonesGlobalTrajectoryUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_global_trajectories);
}

std::unordered_map<std::string,Bone3DTrajectory> SkeletalAnimation::getBonesGlobalSpeedUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_global_speed_curves);
}

std::unordered_map<std::string,Bone3DTrajectory> SkeletalAnimation::getBonesGlobalAccelUnRef() const
{
    return utility::MapDeRef<std::string,Bone3DTrajectory>(m_bone_global_accel_curves);
}

void SkeletalAnimation::ComputeAnimation(float time_stamp)
{   
    //TODO remove for Invalidate system
    if(m_animation_skeleton->getBones().size() != m_last_pose_skel->getBones().size())
        m_last_pose_skel = std::dynamic_pointer_cast<Skeleton>(m_animation_skeleton->Duplicate());
    ComputeSkelAnim(time_stamp);
    //----
    ComputeLPoseLSkel();
}

void SkeletalAnimation::ComputeSkelAnim(float time_stamp)
{
    float time_in_ticks = time_stamp ;//* tick_per_second;
    float animation_time = fmod(time_in_ticks, duration + ANIM_EPS1/**tick_per_second*/);

    ComputeBoneTransform(animation_time);
    //IK Solveving
    for(const auto& chain : m_animation_skeleton->getIKChains())
    {
        IKTriangulationSolve(chain);
    }
}

void SkeletalAnimation::ComputeLPoseLSkel()
{
    m_last_pose_skel->updateHierarchicalTransforms();
    for(auto bone : m_last_pose_skel->getBones())
    {
        if(!bone.second->is_modifier)
            m_last_computed_poses[bone.second->bone_ID] = bone.second->hierarchical_transform;
    }
}

void SkeletalAnimation::ComputeBoneTransform(float animation_time)
{
    Animation::ComputeAnimation(animation_time);
    for(auto& bn : m_last_pose_skel->getBones())
    {
        if(containsKeyGroup(bn.first))
        {
            //Check if contains key
            glm::vec3 translation_vec;
            glm::vec3 scale_vec(1.0f);
            glm::quat rotation_quat(1,0,0,0);

            if(containsKey(bn.first + "_position"))
                translation_vec = std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(getLastComputedKey(bn.first + "_position"))->value;
            if(containsKey(bn.first + "_scale"))
                scale_vec = std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(getLastComputedKey(bn.first + "_scale"))->value;
            if(containsKey(bn.first + "_rotation"))
                rotation_quat = std::dynamic_pointer_cast<TimedParameter<glm::quat>>(getLastComputedKey(bn.first + "_rotation"))->value;

            glm::mat4 translation_matrix = glm::translate(glm::mat4(1.0f),translation_vec);
            glm::mat4 scale_matrix = glm::scale(glm::mat4(1.0f),scale_vec);
            glm::mat4 rotation_matrix = glm::toMat4(rotation_quat);

            bn.second->local_pose = Pose3D(scale_vec,rotation_quat,translation_vec);
            bn.second->local_transform = translation_matrix*rotation_matrix*scale_matrix;
        }
    }
}


void SkeletalAnimation::analyzeAnimation(float bone_importance_thres,float min_speed_thres)
{
    //First step: sort bones by local mvt importance
    std::map<float,std::string> sorted_bones = getMvtSortedBones();
    std::multimap<std::string,float> sorted_bones_revert = utility::MapFlip(sorted_bones);
    float max_energy = sorted_bones.rbegin()->first;
    //Second step: Cluster animations into (pre anticipation), anticipation, action, follow through
    computeBoneLocalSpeeds();
    computeBoneLocalAcceleration();
    computeBoneGlobalTrajectories();

    //Here we compute animation keypoints which are frames where speed reach local minimum
    //We compute acceleration and keep frame in which are near (threshold) the acceleration minimum (near 0)
    std::map<unsigned int, float> frame_cinetic;
    std::map<unsigned int, float> frame_inertia;
    std::map<unsigned int, float> frame_cinetic_driv;
    std::map<unsigned int, Pose3D > frame_speeds;
    std::map<unsigned int, Pose3D> frame_accels;
    float min_cinetic = FLT_MAX;
    float min_inertia = FLT_MAX;
    std::vector<unsigned int> keypoints;
    for(unsigned int i = 0 ; i < getKeyframeNbr() ; ++i)
    {
        frame_cinetic[i] = 0.0f;
        frame_inertia[i] = 0.0f;
    }

    for(auto& bone_trajectory : getBonesLocalAccelUnRef())
    {
        unsigned int i = 0;
        if(sorted_bones_revert.equal_range(bone_trajectory.first).first->second > max_energy - max_energy*bone_importance_thres)
        {
            for(auto& bone_keypose : bone_trajectory.second.GetPoints())
            {

                frame_inertia[i] += bone_keypose.value.norm();
                frame_accels[i] += bone_keypose.value;
                i++;
            }
        }
    }

    for(auto& bone_trajectory : getBonesLocalSpeedUnRef())
    {
        unsigned int i = 0;
        if(sorted_bones_revert.equal_range(bone_trajectory.first).first->second > max_energy - max_energy*bone_importance_thres)
        {
            for(auto& bone_keypose : bone_trajectory.second.GetPoints())
            {

                frame_cinetic[i] += bone_keypose.value.norm();
                frame_speeds[i] += bone_keypose.value;
                i++;
            }
        }
    }

    //WE discard first and last frame by putting FLT_MAX
    /* frame_cinetic[0] = FLT_MAX;
    frame_cinetic[getKeyframeNbr() - 1] = FLT_MAX;*/



    for(unsigned int i = 1 ; i < getKeyframeNbr() - 1 ; ++i)
    {
        if(frame_cinetic[i] < min_cinetic)
            min_cinetic = frame_cinetic[i];
        if(frame_inertia[i] < min_inertia)
            min_inertia = frame_inertia[i];
        frame_cinetic_driv[i] = (frame_cinetic[i+1] - frame_cinetic[i])
                /(getKeyFrame(i+1)->getTimeStamp() - getKeyFrame(i)->getTimeStamp());
    }
    for(unsigned int i = 1 ; i < getKeyframeNbr() - 1; ++i)
    {
        //Here we have to identify aniticipation action and recovery
        //for finner clustering we could use Kmeans with keypoint + 1 as cluster nbr and
        //initialized at keypoint i - 1 + keypoint i /2
        utility::Logger::Debug(std::string("Accel and speed at point ") + std::to_string(i) +
                               std::string(" ") + std::to_string(frame_cinetic[i]) + std::string(" ") + std::to_string(frame_cinetic_driv[i]));
        if(math::ApproxEqual(frame_cinetic[i],min_cinetic,min_cinetic*min_speed_thres)
                /*||(math::ApproxEqual(frame_inertia[i],min_inertia,min_inertia*min_speed_thres*0.25f))*/  /*TODO Same for acceleration*/)
        {
            keypoints.push_back(i);
            utility::Logger::Info("Keypoint at : " + std::to_string(i));
        }
        else if (frame_cinetic_driv[i-1]*frame_cinetic_driv[i] < 0)
        {
            addBreakdown(keyframes[i].get());
            utility::Logger::Info("Breakdown at : " + std::to_string(i));
        }
    }
    //Filter neighbours
    float min_speed_ngb = frame_cinetic[0];
    std::vector<unsigned int> remove_points;
    unsigned int min_speed_index = 0;
    keypoints.insert(keypoints.begin(),0);
    keypoints.push_back(getKeyframeNbr()-1);
    for(unsigned int i = 1 ; i < keypoints.size() ; ++i)
    {

        if(keypoints[i] == keypoints[i-1] + 1)
        {
            remove_points.push_back(keypoints[i-1]);
            if(i < keypoints.size() - 1 && keypoints[i] != keypoints[i + 1] - 1)
                remove_points.push_back(keypoints[i]);
            if(frame_cinetic[keypoints[i]] < min_speed_ngb)
            {
                min_speed_ngb =  frame_cinetic[keypoints[i]];
                min_speed_index = keypoints[i];
            }
        }
        else
        {
            if (min_speed_index != getKeyframeNbr())
            {
                utility::vectorRemoveValue(remove_points,min_speed_index);
            }
            min_speed_index = getKeyframeNbr();
            min_speed_ngb = FLT_MAX;
        }
    }

    for(const unsigned int& key_index : remove_points)
        utility::vectorRemoveValue(keypoints,key_index);
    utility::vectorRemoveValue(keypoints,(unsigned int)(0));
    utility::vectorRemoveValue(keypoints,getKeyframeNbr() - 1);

    unsigned int keypoint_nbr = keypoints.size();
    switch (keypoint_nbr)
    {
    case 0:
        setPreparationTime(-1);
        setAnticipationTime(0);
        setActionTime(getKeyframeNbr()-1);
        setFollowThroughTime(-1);
        setTransitionTime(-1);
        break;
    case 1:
        //TODO establish when we should choose between anticipation and follow throuh
        setPreparationTime(-1);
        setAnticipationTime(keypoints[0]);
        setActionTime(getKeyframeNbr()-1);
        setFollowThroughTime(-1);
        setTransitionTime(-1);
        break;
    case 2:
        //TODO establish when we should choose between anticipation and follow throuh
        setPreparationTime(-1);
        setAnticipationTime(keypoints[0]);
        setActionTime(keypoints[1]);
        setFollowThroughTime(getKeyframeNbr()-1);
        setTransitionTime(-1);
        break;
    case 3:
        //TODO establish when we should choose between anticipation and follow throuh
        setPreparationTime(-1);
        setAnticipationTime(keypoints[0]);
        setActionTime(keypoints[1]);
        setFollowThroughTime(keypoints[2]);
        setTransitionTime(getKeyframeNbr()-1);
        break;
    case 4:
        //TODO establish when we should choose between anticipation and follow throuh
        setPreparationTime(-1);
        setAnticipationTime(keypoints[0]);
        setActionTime(keypoints[1]);
        setFollowThroughTime(keypoints[2]);
        setTransitionTime(keypoints[3]);
        break;
    case 5:
        //TODO establish when we should choose between anticipation and follow throuh
        setPreparationTime(keypoints[0]);
        setAnticipationTime(keypoints[1]);
        setActionTime(keypoints[2]);
        setFollowThroughTime(keypoints[3]);
        setTransitionTime(keypoints[4]);
        break;
    default:
        utility::Logger::Error("Incorrect number of keypoint for animation " + m_name + " " + std::to_string(keypoint_nbr));
        break;
    }

    addKeypoint(getFirstKeyFrame());
    for(unsigned int& index : keypoints)
    {
        addKeypoint(getKeyFrame(index));
        utility::Logger::Info("Final Keypoint at : " + std::to_string(index));
    }
    addKeypoint(getLastKeyFrame());



    //KMean Clustering
    /*    unsigned int clust_nbr = std::min(getKeyframeNbr(),(unsigned int)(2));
          std::unordered_map<std::string,std::map<float,unsigned int>> bone_frames_clust;
     *
     * for(auto& bone_trajectory : getBonesLocalSpeedUnRef())
    {
        float max_speed = FLT_MIN;
        for(auto& bone_keypose : bone_trajectory.second.GetPoints())
        {
            float speed = bone_keypose.value.norm();
            if(max_speed < speed)
                max_speed = speed;
        }
        for(auto& bone_keypose : bone_trajectory.second.GetPoints())
            bone_keypose.m_time_influence = (1.0f - 0.5f*max_speed/bone_trajectory.second.GetPoints().rbegin()->time);

        std::vector<KeyPose3D> clusters;
        for(unsigned int  i = 0 ; i < clust_nbr ;++i)
            clusters.push_back(bone_trajectory.second.GetPoint(i*bone_trajectory.second.GetSize()/clust_nbr));
        bone_frames_clust[bone_trajectory.first] = geometry::ClusterKMean<KeyPose3D,float>(getKeyPoseMap(bone_trajectory.second),clust_nbr,clusters,200);

    }


    for(auto& bone_clust : bone_frames_clust)
    {
        std::vector<int> clust_vector;
        std::vector<int> smoothed_clust_vector;
        std::vector<float> mean_clust;
        std::vector<int> clust_elems;
        mean_clust.resize(clust_nbr);
        clust_elems.resize(clust_nbr);
        for (int i = 0; i < clust_nbr; ++i)
        {
            mean_clust[i] = 0.0f;
            clust_elems[i] = 0;
        }
        for(auto& frame_clust : bone_clust.second)
        {
            mean_clust[frame_clust.second] += frame_clust.first;
            clust_elems[frame_clust.second] ++;
            clust_vector.push_back(frame_clust.second);
            //std::cerr << "bone " << bone_clust.first << " frame " << frame_clust.first << " clust " << frame_clust.second << std::endl;
        }
        geometry::SmoothCurve(clust_vector,smoothed_clust_vector);
        for(unsigned int i = 0 ; i < clust_vector.size() ; ++i)
        {
            //smoothed_clust_vector[i] = std::round(smoothed_clust_vector[i]);
            std::cerr << "bone " << bone_clust.first << " clust " << smoothed_clust_vector[i]<< std::endl;
        }
    }*/

    //Save result into image
    /* std::vector<unsigned int> color(clust_nbr);
    for(unsigned int i = 0 ; i < clust_nbr ; ++i)
        color[i] = utility::RGBToInt(utility::getRandomColor(),true);

    std::unordered_map<std::string,std::shared_ptr<ressource::Image> > frame_cluster_images;
    for(auto& bone : sorted_bones)
    {
        frame_cluster_images[bone.second] = ressource::Ressource::Create<ressource::Image>(bone.second +"_Frame_Clusters");
    }

    unsigned int frame_thickness = 5;
    unsigned int width = 200;
    for(auto& bone_clust : bone_frames_clust)
    {
        std::vector<unsigned char> image_data;
        image_data.resize(bone_clust.second.size()*frame_thickness*width*4);
        unsigned int count = 0;
        for(auto& frame_clust : bone_clust.second)
        {
            for(unsigned int j = 0; j <  frame_thickness ; j++)
                for(unsigned int i = 0 ; i < width ; i++)
                    for(unsigned int k = 0; k < 4; k++)
                        image_data[4*i + (count*frame_thickness + j)*width*4 + k] = (color[frame_clust.second] & 255<<(3-k)*8) >> ((3-k)*8);

            count++;
        }
        frame_cluster_images[bone_clust.first]->setData(width,(unsigned int)bone_clust.second.size()*frame_thickness,4,&(image_data[0]));
        frame_cluster_images[bone_clust.first]->saveToFile(frame_cluster_images[bone_clust.first]->getName()+".png");

    }*/

    //---------------------------------
#ifdef __LMA__
    //Laban Analysis
    unsigned int count = 0;
    motionDoodle::LabanVector animation_laban;
    for(auto& bone_trajectory : getBonesGlobalTrajectoryUnRef())
    {
        if(sorted_bones_revert.equal_range(bone_trajectory.first).first->second > max_energy - max_energy*bone_importance_thres)
        {
            //Conversion to vec7 and Laban classification;
            if(motionDoodle::MotionManager::m_laban_traj_analyser->getIsTrained())
            {
                std::vector<motionDoodle::Vector7D> curve = module::doodle::AnimationTrajToDoodleTraj(bone_trajectory.second);
                animation_laban += motionDoodle::utility::LabanWordTupleToLabanVector(motionDoodle::MotionManager::m_laban_traj_analyser->Classify(curve));
                ++count;
            }
        }
    }
    if(count != 0)
    {
        animation_laban /= count;
        m_laban_efforts.x = animation_laban.space;
        m_laban_efforts.y = animation_laban.time;
        m_laban_efforts.z = animation_laban.weight;
        utility::Logger::Info("Animation " + m_name + " space " + std::to_string(animation_laban.space)
                              + " time " + std::to_string(animation_laban.time)
                              + " weight " + std::to_string(animation_laban.weight));
    }
    //--------------
#endif
}


std::shared_ptr<ressource::Ressource> SkeletalAnimation::Duplicate()
{
    std::string anim_name = ressource::RessourceManager::getDuplicateName(m_name);
    std::shared_ptr<SkeletalAnimation> anim = Ressource::Create<SkeletalAnimation>(anim_name);
    {
        std::unique_lock<std::mutex> lock(*(anim->m_lock.get()));
        copyAnimationProperties(*(anim.get()));

        anim->setAnimationSkeleton(m_animation_skeleton);        
        for(auto & local_traj : m_bone_local_trajectories)
            anim->m_bone_local_trajectories[local_traj.first] = std::make_shared<Bone3DTrajectory>(*(local_traj.second.get()));
        for(auto & local_speed : m_bone_local_speed_curves)
            anim->m_bone_local_speed_curves[local_speed.first] = std::make_shared<Bone3DTrajectory>(*(local_speed.second.get()));
        for(auto & local_accel : m_bone_local_accel_curves)
            anim->m_bone_local_accel_curves[local_accel.first] = std::make_shared<Bone3DTrajectory>(*(local_accel.second.get()));

        for(auto & global_traj : m_bone_global_trajectories)
            anim->m_bone_global_trajectories[global_traj.first] = std::make_shared<Bone3DTrajectory>(*(global_traj.second.get()));
        for(auto & global_speed : m_bone_global_speed_curves)
            anim->m_bone_global_speed_curves[global_speed.first] = std::make_shared<Bone3DTrajectory>(*(global_speed.second.get()));
        for(auto & global_accel : m_bone_global_accel_curves)
            anim->m_bone_global_accel_curves[global_accel.first] = std::make_shared<Bone3DTrajectory>(*(global_accel.second.get()));


        anim->m_mvt_sorted_bones = m_mvt_sorted_bones;
    }
    return anim;

}

void SkeletalAnimation::computeBoneTargetAnimation(const std::string& eff_name, const std::string &tg_name)
{
    //TODO invalidate system for optimization
    computeBoneGlobalTrajectories();
    std::unordered_map<std::string,Bone3DTrajectory> traj = getBonesGlobalTrajectoryUnRef();
    for(auto& eff_traj : traj[eff_name].GetPoints())
    {

        addKey(tg_name + "_position",std::make_shared<TimedParameter<glm::vec3>>(eff_traj.value.position,eff_traj.time));
    }
    addKeyToGroup(tg_name,tg_name + "_position");
}

void SkeletalAnimation::CreateArmaAnim(scene::SceneGraph* sg)
{
    m_last_pose_skel->CreateArmature(sg);    
}


void SkeletalAnimation::IKTriangulationSolve(const std::pair<std::string,unsigned int>& chain)
{
    //Check if there are assigned target to save computation
    if(!m_animation_skeleton->containsTargets(chain.first) || chain.second <= 1)
        return;
    std::vector<Bone3D*> bone_chain;
    int chain_length = chain.second;
    Bone3D* curr_bone = m_last_pose_skel->getBonePt(chain.first);
    std::unordered_map<std::string,float> bn_chain_remain_l;
    while(chain_length >= 0 && curr_bone != nullptr)
    {
        bone_chain.insert(bone_chain.begin(),curr_bone);
        curr_bone = curr_bone->parent;
        chain_length--;
    }
    float tmp_length = 0;
    for(int i = std::min(int(bone_chain.size() - 1),0) ; i >= 0 ; --i)
    {
        bn_chain_remain_l[bone_chain[i]->bone_name] = tmp_length;
        tmp_length += glm::l1Norm(bone_chain[i+1]->global_pose.position - bone_chain[i]->global_pose.position);
    }

    glm::vec3 target = m_last_pose_skel->getBonePt(m_animation_skeleton->getIKEndTargets(chain.first)[0])->global_pose.position;
    //TODO do we keep the end effector
    for(unsigned int i = 0 ; i < bone_chain.size() - 1 ; ++i)
    {
        //Only one target per end effector for this algorithm
        glm::vec3 joint_to_target = target - bone_chain[i]->global_pose.position;
        glm::vec3 joint_vec = bone_chain[i+1]->global_pose.position - bone_chain[i]->global_pose.position;
        float tgt_length = glm::l1Norm(joint_to_target);
        float jt_length = glm::l1Norm(joint_vec);
        float jt_remain_length = bn_chain_remain_l[bone_chain[i]->bone_name];
        float theta;
        glm::vec3 rotation_vec;
        if(tgt_length >= jt_remain_length + jt_length)
        {
            rotation_vec = glm::vec3(0,0,1);
            theta = -acos(glm::dot(glm::normalize(joint_vec),glm::normalize(joint_to_target)));
        }
        else if(tgt_length <= fabs(jt_length - jt_remain_length))
        {

            rotation_vec = glm::vec3(0,0,1);
            theta  = -acos(glm::dot(glm::normalize(joint_vec),glm::normalize(joint_to_target)));
            jt_remain_length = glm::l1Norm(target -bone_chain[i+1]->global_pose.position);
            if(jt_remain_length > jt_length)
                theta += M_PI;

        }
        else
        {
            rotation_vec = glm::cross(joint_vec,joint_to_target);
            theta  = -acos(glm::dot(glm::normalize(joint_vec),glm::normalize(joint_to_target)))
                    - acos(-(jt_remain_length*jt_remain_length - jt_length*jt_length -tgt_length*tgt_length)/(2.0f*jt_length*tgt_length));
        }


        bone_chain[i]->local_pose.rotation = glm::rotate(bone_chain[i]->local_pose.rotation,theta,rotation_vec);
        bone_chain[i]->local_transform = glm::rotate(bone_chain[i]->local_transform ,theta,rotation_vec);
        bone_chain[i]->hierarchical_transform  = glm::rotate(bone_chain[i]->hierarchical_transform ,theta,rotation_vec);


    }
}

}
}
