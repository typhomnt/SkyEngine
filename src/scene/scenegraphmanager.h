#ifndef SCENEGRAPH_MANAGER_H
#define SCENEGRAPH_MANAGER_H

#include "utils/singleton.h"

#include <map>
#include <memory>
#include <string>

#include "utils/skyengine_dll.h"

#include "scenegraph.h"


namespace skyengine
{
namespace core
{
    class SkyEngine;
}
namespace scene
{

class SceneGraph;
class SKYENGINE_API SceneGraphManager : public utility::Singleton<SceneGraphManager>
{
public:
    friend SceneGraphManager* Singleton<SceneGraphManager>::GetSingleton();

    static void Add(const std::string& name, const std::shared_ptr<SceneGraph>& scene);
    static std::shared_ptr<SceneGraph> Get(const std::string& name);
    static void Remove(const std::string& name);
    static void setSkyEngineTasks(core::SkyEngine*);

    SceneGraphManager(const SceneGraphManager&) = delete;
    SceneGraphManager& operator=(const SceneGraphManager&) = delete;

private:
    std::unordered_map<std::string, std::shared_ptr<SceneGraph>> m_scenes;

    SceneGraphManager();
};

} // namespace scene

} // namespace skyengine


#endif // SCENEGRAPH_MANAGER_H
