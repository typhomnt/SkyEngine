#version 330 core


// Struct definitions
struct DirLight
{
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct PointLight
{
    vec3 position;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
};

struct SpotLight
{
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float constant;
    float linear;
    float quadratic;
    float cutoff;
};

struct Material
{
    vec4 emissive;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

// Uniform variables
uniform Material material;
uniform vec3 camPosition;
uniform sampler2D myTextureSampler;
uniform sampler2D myTextureSampler2;
uniform sampler2D myTextureSampler3;
uniform sampler2D myTextureSampler4;

// Uniform lights.
// TODO : for now, only 1 DirLight and 1 PointLight
#define NB_LIGHTS 1
uniform DirLight dirLight[NB_LIGHTS];
uniform PointLight pointLight[NB_LIGHTS];

// Input variables
in vec3 fragPos;
in vec3 fragNormal;
in vec2 fragUV;
in vec3 eyeVec;
// Output variable
out vec4 fragColor;


vec4 computeDirLighting(DirLight dirLight)
{
    // Ambient
    vec4 color = material.emissive + material.ambient * dirLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(-dirLight.direction);

    float NdotL = max(dot(N, L),0.0);
    if (NdotL > 0.0)
    {
        color += material.diffuse * dirLight.diffuse * NdotL;

        // Specular
        vec3 V = normalize(camPosition - fragPos);
        vec3 R = normalize(reflect(-L, N));

        float RdotV = max(dot(R, V), 0.0);
        color += material.specular * dirLight.specular * pow(RdotV, material.shininess);
    }

    return color;
}


vec4 computePointLighting(PointLight pointLight)
{
    // Ambient
    vec4 color = material.emissive + material.ambient * pointLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(pointLight.position - fragPos);

    float NdotL = dot(N, L);
    if (NdotL > 0.0)
    {
        color += material.diffuse * pointLight.diffuse * NdotL;

        // Specular
        vec3 V = normalize(camPosition - fragPos);
        vec3 R = normalize(reflect(-L, N));

        float RdotV = max(dot(R, V), 0.0);
        color += material.specular * pointLight.specular * pow(RdotV, material.shininess);
    }

    // Attenuation factor
    float d = length(pointLight.position - fragPos);
    float att =   pointLight.constant
            + pointLight.linear * d
            + pointLight.quadratic * d * d;

    return att * color;
}


vec4 computeSpotLighting(SpotLight spotLight)
{
    // Ambient
    vec4 color = material.emissive + material.ambient * spotLight.ambient;

    // Diffuse
    vec3 N = normalize(fragNormal);
    vec3 L = normalize(spotLight.position - fragPos);

    float spotFactor = dot(-L, normalize(spotLight.direction));
    if (spotFactor > spotLight.cutoff)
    {
        float NdotL = dot(N, L);
        if (NdotL > 0.0)
        {
            color += material.diffuse * spotLight.diffuse * NdotL;

            // Specular
            vec3 V = normalize(camPosition - fragPos);
            vec3 R = normalize(reflect(-L, N));

            float RdotV = max(dot(R, V), 0.0);
            color += material.specular * spotLight.specular * pow(RdotV, material.shininess);
        }

        // Attenuate color with spot factor
        color *= spotFactor;
    }

    // Attenuation factor
    float d = length(spotLight.position - fragPos);
    float att =   spotLight.constant
            + spotLight.linear * d
            + spotLight.quadratic * d * d;

    return att * color;
}


void main()
{
    fragColor = vec4(0.0);

    for (int i = 0; i < NB_LIGHTS; ++i)
        fragColor += computeDirLighting(dirLight[i]);
    for (int i = 0; i < NB_LIGHTS; ++i)
        fragColor += computePointLighting(pointLight[i]);

    fragColor *= texture(myTextureSampler, fragUV);
    fragColor *= texture(myTextureSampler2, fragUV);
     fragColor *= texture(myTextureSampler3, fragUV);
    fragColor *= texture(myTextureSampler4, fragUV);

    //TEST rim light
    //float rim = smoothstep(0.0,1.0,1.0 - max(dot(normalize(camPosition - fragPos),normalize(fragNormal)),0.0));
    //fragColor *= (1.0 + 2.0*rim);
    //fragColor += vec4(fragColor.xyz*(1.0f + rim), rim);
    //fragColor *= vec4(1.0f,1.0f,1.0f,1.0f)*(1.0f - rim)  + rim*vec4(vec3(0.5,0.2,2.0)*(1.0f - rim) +  vec3(2.0,0.5,0.1f)*rim, 1.0f);
    fragColor = clamp(fragColor, 0.0, 1.0);

}
