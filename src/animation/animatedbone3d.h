#ifndef ANIMATEDBONE3D_H
#define ANIMATEDBONE3D_H
#include "bone3d.h"
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include <unordered_map>
#include "geometry/geometrycurve.h"
#include "timeparameter.h"
namespace skyengine
{

namespace animation
{

struct KeyPose3D : public TimedParameter<Pose3D>
{
    float m_time_influence;
    KeyPose3D(float time_influence = 1.0f) : TimedParameter<Pose3D>(Pose3D(),0.0f), m_time_influence(time_influence){}
    KeyPose3D(const Pose3D& value, const float& time,float time_influence = 0.2f) :TimedParameter<Pose3D>(value,time), m_time_influence(time_influence){}

    glm::vec3 translation() const
    {
        return value.translation();
    }

    glm::quat rotation() const
    {
        return value.rotation;
    }

    float norm() const
    {
        return value.norm() + time*m_time_influence;
    }

    float getId() const
    {
        return time;
    }

    inline KeyPose3D operator + (const KeyPose3D &pose) const
    {
        return KeyPose3D(value + pose.value, time + pose.time, m_time_influence /* + pose.m_time_influence*/);
    }

    inline KeyPose3D operator - (const KeyPose3D &pose) const
    {
        return KeyPose3D(value - pose.value, time - pose.time, m_time_influence /* - pose.m_time_influence*/);
    }

    inline KeyPose3D operator*(const float& f) const
    {
        return KeyPose3D(value*f,time*f);
    }

    inline KeyPose3D operator/(const float& f) const
    {
        return KeyPose3D(value/f,time/f);
    }

    inline KeyPose3D& operator+=(const KeyPose3D &pose)
    {
        value += pose.value;
        time += pose.time;
        return *this;
    }

    inline KeyPose3D& operator/=(const float& f)
    {
        value /= f;
        time /= f;
        return *this;
    }

    inline bool operator==(const KeyPose3D &pose)
    {
        return value == pose.value && time == pose.time;
    }

};

inline KeyPose3D operator*(const float& f, const KeyPose3D& key_pose)
{
       return KeyPose3D(key_pose*f);
}

inline float dot(const KeyPose3D& pose_1, const KeyPose3D& pose_2)
{
    return dot(pose_1.value,pose_2.value) + pose_1.time*pose_2.time;
}

inline KeyPose3D normalize(const KeyPose3D& pose)
{
    return pose/pose.norm();
}


inline float length(const KeyPose3D& pose)
{
    return pose.norm();
}



using Bone3DTrajectory = geometry::GeometryCurve<KeyPose3D>;
using AnimatedPoseTrajectory = geometry::GeometryCurve<KeyPose3D>;


struct AnimatedBone3D
{
    Bone3D* bone;
    std::vector<TimedParameter<glm::vec3> > positions_keys;
    std::vector<TimedParameter<glm::quat> > rotation_keys;
    std::vector<TimedParameter<glm::vec3> > scale_keys;

};


}
}

#endif // ANIMATEDBONE3D_H
