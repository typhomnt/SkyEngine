//
//  BleScanner.h
//  ISKN_API
//
//  Created by ALOUI Rabeb on 12/04/15.
//  Copyright (c) 2015 ISKN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#include "BleScanListener.h"

@interface BleScanner : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate>
{
CBCentralManager *Manager ;  //Core Bluetooth Object ie iOS device
BleScanListener  *ScanListener;
CBUUID           *ServiceUUID ;
NSMutableArray   *devicesList ;
NSMutableArray   *scannedDevicesList ;
    ;
bool             isASlate;
NSMutableArray   *notifyingChar ;
}
- (id)init;
- (void)startScan:(BleScanListener  *)scanListener;
- (void)stopScan;
- (CBCentralManager *)getManager;
- (NSMutableArray*)getDevicesList;
// Implement CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central;
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI;
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral;

@end
