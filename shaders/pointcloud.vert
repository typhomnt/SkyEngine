#version 330

// Uniforms
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

// Inputs
layout(location = 0) in vec3 inVertex;
layout(location = 1) in float inSize;
layout(location = 2) in vec4 inColor;

// Output
out VS_OUT
{
    vec4 color;
} vs_out;

void main()
{
    gl_Position = P * V * M * vec4(inVertex, 1.0);
    gl_PointSize = inSize;
    vs_out.color = inColor;
}
