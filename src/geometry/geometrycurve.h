#ifndef GEOMETRYCURVE_H
#define GEOMETRYCURVE_H
#include <memory>
#include <assert.h>
#include <vector>
#include <set>
#include <algorithm>
#include "abstractcurve.h"
namespace skyengine
{

namespace geometry
{
/**
 * The GeometryCurve class represents any kind of polygon. It offers methods to
 * add/remove points, intersection tests, center computation, split...
 */
template<typename PointT>
class GeometryCurve: public AbstractCurve
{
public:

    /////////////////////////////
    // Constructors/destructor //
    /////////////////////////////
    GeometryCurve(): AbstractCurve(){}

    GeometryCurve(const GeometryCurve &other, unsigned int start = -1, unsigned int end = -1);

    virtual ~GeometryCurve() {}

    virtual void updated() {}


    /**
     * Returns a new pointer that contains the same data as this GeometryPrimitive.
     * The return GeometryPrimitive can be a subpart of this one.
     * @param start The first Point to copy.
     * @param end The last point to copy.
     */
    virtual std::shared_ptr<GeometryCurve<PointT> > clone(unsigned int start = - 1, unsigned int end = -1) const;

    /**
     * Add a point to this polygon.
     * @param p the position of the new point.
     * @param index the index of the new point.
     */
    virtual void AddPoint(const PointT &p, unsigned int index = -1);

    /**
     * Removes a point from this polygon.
     * @param index the index of the point that should be removed.
     */
    virtual void RemovePoint(unsigned int index);

    /**
     * Removes every points from this GeometryPrimitive.
     */
    virtual void RemovePoints();

    /**
     * Copy points from a given GeometryPrimitive to this one.
     * @param other An arbitrary GeometryPrimitive.
     * @param reversed If true, points will be copied in reverse order.
     */
    virtual void CopyPoints(const GeometryCurve &other, unsigned int reversed);

    /**
     * Sets the position of point at given index to the given position.
     * @param index index of the point to be replaced.
     * @param p the new value of that point.
     */
    virtual void SetPoint(unsigned int index, const PointT &p);

    /**
     * Returns an editable vector to this GeometryPrimitive's points.
     */
    std::vector<PointT> &GetPoints();

    /**
     * Returns an editable vector to this GeometryPrimitive's points.
     */
    const std::vector<PointT> &GetPoints() const;

    /**
     * Returns an editable version of a point of this GeometryPrimitive.
     * @param index index of the point to be returned.
     */
    PointT &GetPoint(unsigned int index);

    /**
     * Returns an editable version of a point of this GeometryPrimitive.
     * @param index index of the point to be returned.
     */
    const PointT &GetPoint(unsigned int index) const;

    unsigned int GetSize() const;

    /**
     * Returns the first point of this GeometryPrimitive.
     */
    virtual PointT &GetStart();

    /**
     * Returns the first point of this GeometryPrimitive.
     */
    virtual const PointT &GetStart() const;

    /**
     * Returns the last point of this GeometryPrimitive.
     */
    virtual PointT &GetEnd();

    /**
     * Returns the last point of this GeometryPrimitive.
     */
    virtual const PointT &GetEnd() const;


    /**
     * Comparison operator between 2 GeometryPrimitives.
     * Mandatory for std containers insertions.
     */
    friend bool operator<(const GeometryCurve& g1, const GeometryCurve& g2)
    {
        return g1.m_points.size() < g2.m_points.size();
    }

    /**
     * Comparison operator between 2 GeometryPrimitives.
     * Mandatory for std containers insertions.
     */
    bool operator==(const GeometryCurve& g2) const
    {
        if (m_points.size() != g2.m_points.size()) {
            return false;
        }
        for (unsigned int i = 0; i < m_points.size(); ++i) {
            if (m_points[i] != g2.m_points[i] && m_points[i] != g2.m_points[g2.m_points.size() - 1 - i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reverse this GeometryPrimitive's points.
     */
    void Invert()
    {
        std::reverse(m_points.begin(), m_points.end());
    }

    /**
     * Splits this GeometryPrimitive at a given point. Results will vary depending on the type of GeometryPrimitive.
     * A segment will return a new segment. A splitted triangle will return 2 new triangles. etc...
     * @param index index at which the split should occur. Point after this position will be moved to pos, and every other points moved to new primitives.
     * @param pos The new position of the point where the split occured.
     * @param newPrimitives the list of primitives created by this splitting.
     * @return the index of the moved position, which is also the first point of other primitives (index + 1 for segments).
     */
    virtual unsigned int Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryCurve> > &newPrimitives);

    /**
     * Returns the mid point of this GeometryPrimitive.
     */
    //virtual PointT GetCenter();

    virtual void setIsLoop(bool is_loop);

    inline bool isLoop();

protected:
    std::vector<PointT> m_points; //< The list of points contained in this polygon.
    bool m_is_loop;

};

template<typename PointT>
GeometryCurve<PointT>::GeometryCurve(const GeometryCurve &rhs, unsigned int start, unsigned int end) : AbstractCurve()
{
    start = std::max(0, (int)start);
    end = std::max((int) rhs.GetSize(), (int)end);
    m_points.insert(m_points.begin(), rhs.m_points.begin() + start, rhs.m_points.begin() + end);
}

template<typename PointT>
void GeometryCurve<PointT>::RemovePoint(unsigned int index)
{
    assert(index < m_points.size());
    m_points.erase(m_points.begin() + index);
}

template<typename PointT>
void GeometryCurve<PointT>::RemovePoints()
{
    m_points.clear();
}

template<typename PointT>
void GeometryCurve<PointT>::AddPoint(const PointT &p, unsigned int index)
{
    if (index == (unsigned int) -1) {
        m_points.push_back(p);
    } else {
        m_points.insert(m_points.begin() + index, p);
    }
}

template<typename PointT>
PointT &GeometryCurve<PointT>::GetPoint(unsigned int index)
{
    assert(index < m_points.size() && (int)index >= 0 && "invalid index provided in geometry primitive");
    return m_points[index];
}

template<typename PointT>
const PointT &GeometryCurve<PointT>::GetPoint(unsigned int index) const
{
    assert(index < m_points.size() && (int)index >= 0 && "invalid index provided in geometry primitive");
    return m_points[index];
}

template<typename PointT>
std::vector<PointT> & GeometryCurve<PointT>::GetPoints()
{
    return m_points;
}

template<typename PointT>
const std::vector<PointT> & GeometryCurve<PointT>::GetPoints() const
{
    return m_points;
}

template<typename PointT>
void GeometryCurve<PointT>::CopyPoints(const GeometryCurve &other, unsigned int reversed)
{
    m_points.clear();
    if (reversed) {
        m_points.insert(m_points.begin(), other.m_points.rbegin(), other.m_points.rend());
    } else {
        m_points.insert(m_points.begin(), other.m_points.begin(), other.m_points.end());
    }
}

template<typename PointT>
void GeometryCurve<PointT>::SetPoint(unsigned int index, const PointT &p)
{
    if (index < GetSize()) {
        m_points[index] = p;
    } else {
        if (index == GetSize() && isLoop()) {
            m_points[0] = p;
        } else {
            m_points.push_back(p);
        }
    }
}

template<typename PointT>
unsigned int GeometryCurve<PointT>::GetSize() const
{
    return (unsigned int) m_points.size();
}

template<typename PointT>
PointT &GeometryCurve<PointT>::GetStart()
{
    assert(m_points.size() > 0);
    return m_points[0];
}

template<typename PointT>
const PointT &GeometryCurve<PointT>::GetStart() const
{
    assert(m_points.size() > 0);
    return m_points[0];
}

template<typename PointT>
PointT &GeometryCurve<PointT>::GetEnd()
{
    assert(m_points.size() > 0);
    return m_points[m_points.size() - 1];
}

template<typename PointT>
const PointT &GeometryCurve<PointT>::GetEnd() const
{
    assert(m_points.size() > 0);
    return m_points[m_points.size() - 1];
}

template<typename PointT>
std::shared_ptr<GeometryCurve<PointT> > GeometryCurve<PointT>::clone(unsigned int start, unsigned int end) const
{
    return std::make_shared<GeometryCurve<PointT> > (*this,start,end);
}

template<typename PointT>
unsigned int GeometryCurve<PointT>::Split(unsigned int index, const PointT &pos, std::set<std::shared_ptr<GeometryCurve> > &newPrimitives)
{
    std::shared_ptr<GeometryCurve> end = clone();
    if (isLoop()) {
        setIsLoop(false);
        end->setIsLoop(false);
        end->AddPoint(GetPoint(0), end->GetSize());
    }

    for (unsigned int i = index + 1; i < m_points.size() - 1; ++i) {
        RemovePoint(index);
    }
    for (unsigned int i = 1; i < index + 1; ++i) {
        end->RemovePoint(0);
    }

    SetPoint(index + 1, pos);
    end->SetPoint(0, pos);

    newPrimitives.insert(end);
    return index + 1;
}

/*template<typename PointT>
PointT GeometryCurve<PointT>::GetCenter()
{
    assert(m_points.size() >= 1);
    PointT res = m_points[0];
    for (unsigned int i = 0; i < m_points.size(); ++i) {
        res += m_points[i];
    }
    res = res * 1.0 /  m_points.size();
    return res;
}*/
template<typename PointT>
void GeometryCurve<PointT>::setIsLoop(bool is_loop)
{
    m_is_loop = is_loop;
}

template<typename PointT>
inline bool GeometryCurve<PointT>::isLoop()
{
    return m_is_loop;
}
}
}

#endif // GEOMETRYCURVE_H
