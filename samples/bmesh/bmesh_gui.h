#ifndef CVH_GUI_H
#define CVH_GUI_H

#include "gui/guimanager.h"

#include "render/renderable.h"


namespace module
{

namespace gui
{

class CVHGui : public GuiTask
{
public:
    CVHGui(skyengine::core::SkyEngine* engine,
           GLFWwindow* window,
           skyengine::scene::SceneGraph* sceneGraph,
           GuiManager* manager);

    void run();

private:
    skyengine::scene::SceneGraph* m_sceneGraph;

    int m_currentItem;
    skyengine::render::Renderable* m_currentMesh;
};

} // namespace gui

} // namespace module

#endif // CVH_GUI_H
