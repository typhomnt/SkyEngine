#ifndef GEOMETRYALGORITHMS_H
#define GEOMETRYALGORITHMS_H

#include "utils/skyengine_dll.h"

#include <assert.h>
#include <cmath>
#include <iostream>
#include <unordered_map>
#include <map>
#include "ressources/mesh.h"
#include "render/Vertex3D.h"
#include "animation/timeparameter.h"
#include "geometrycurve.h"
#include "topologicalmesh.h"
#include "math/mathutils.h"

namespace skyengine
{
namespace geometry
{

#define EPS1 0.0001f
#define EPS2 0.001f
#define EPS3 0.00001f


std::shared_ptr<ressource::Mesh<render::vertex3D>> SKYENGINE_API SpherePrimitive(unsigned int u_cuts_in, unsigned int v_cuts_in);
std::shared_ptr<ressource::Mesh<render::vertex3D>> SKYENGINE_API CubePrimitive(float size);
std::shared_ptr<ressource::Mesh<render::vertex3D>> SKYENGINE_API CurvePrimitive(const GeometryCurve<glm::vec3>& curve);

//TODO transform into template
std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveSpeed(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_speed = animation::TimedParameter<glm::vec3>());
std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveAccel(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_accel = animation::TimedParameter<glm::vec3>(), const animation::TimedParameter<glm::vec3>& end_accel = animation::TimedParameter<glm::vec3>());
std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveJerk(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_accel = animation::TimedParameter<glm::vec3>(), const animation::TimedParameter<glm::vec3>& end_accel = animation::TimedParameter<glm::vec3>());
std::vector<animation::TimedParameter<float>> SKYENGINE_API ComputeCurveCurvature(const std::vector<animation::TimedParameter<glm::vec3>>& curve);
std::vector<animation::TimedParameter<float>> SKYENGINE_API ComputeCurveTorsion(const std::vector<animation::TimedParameter<glm::vec3>>& curve);

/**
 * @brief ComputeCurveAffineSpeed
 * @param curve
 * @param init_speed
 * @param func_mode changes affine function computation (0 for 2D, 1 for 3D)
 * @return
 */
std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveAffineSpeed(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_speed = animation::TimedParameter<glm::vec3>(), unsigned int func_mode = 0);
std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveAffineAccel(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_accel = animation::TimedParameter<glm::vec3>(), const animation::TimedParameter<glm::vec3>& end_accel = animation::TimedParameter<glm::vec3>(),unsigned int func_mode = 0);

template<typename Point_T, typename Id_T>
std::map<Id_T, unsigned int> ClusterKMean(std::map<Id_T,Point_T> points, unsigned int clust_nbr,std::vector<Point_T>& clusters = std::vector<Point_T>(), unsigned int iter_max = UINT32_MAX)
{

    assert(clust_nbr < points.size());
    //init clusters
    if(clusters.size() == 0)
    {
        clusters.resize(clust_nbr);
        //random cluster

        typedef typename std::map<Id_T,Point_T>::iterator point_iter;
        for(Point_T& clust : clusters)
        {
            for(point_iter it = points.begin(); it != points.end(); ++it)
            {
                clust += (it->second)*2.0f*float(rand())/float(RAND_MAX);
            }
            clust /= (float)points.size();
        }
    }
    std::map<Id_T, unsigned int> clust_map;
    bool clust_changed = true;
    //k mean algo
    unsigned int iter_nbr = 0;
    while(clust_changed && iter_nbr < iter_max)
    {
        clust_changed = false;
        //Assign each point to the nearest cluster
        for(auto& point : points)
        {
            unsigned int clust_index = 0;
            float near_dist = FLT_MAX;
            for(unsigned int i = 0 ; i < clusters.size() ; ++i)
            {
                float dist = (clusters[i] - point.second).norm();
                if (dist < near_dist)
                {
                    clust_index = i;
                    near_dist = dist;
                }
            }
            //assigned point to i
            clust_map[point.first] = clust_index;

        }

        // Assign each cluster to the mean of its assign points
        for(unsigned int i = 0 ; i < clusters.size() ; ++i)
        {
            Point_T new_clust;
            unsigned int size = 0;
            for(const std::pair<Id_T,unsigned int>& clus_point : clust_map)
                if(clus_point.second == i)
                {
                    new_clust += points[clus_point.first];
                    size++;
                }

            if(size != 0)
                new_clust /= (float)size;

            if((new_clust - clusters[i]).norm() > EPS3)
            {
                //std::cerr << (new_clust - clusters[i]).norm() << std::endl;
                clust_changed = true;
                clusters[i] = new_clust;
            }

        }

        iter_nbr++;
    }
    return clust_map;
}


/**
 * Simplification algorithm based on Ramer-Douglas-Peucker implementation.
 * @param input_points the list of points we want to simplify. Can be a loop.
 * @param[out] res the list of points for the simplified curve.
 * @param epsilon the maximum error allowed between the resulting curve and the original one.
 */
template<typename PointT>
void Simplify(std::vector<PointT> &input_points, std::vector<PointT> &res, float epsilon = 0.05f)
{
    if (input_points.size() < 3)
    {
        res = input_points;
        return;
    }

    PointT first = input_points[0];
    PointT last = input_points[input_points.size() - 1];

    int index_furthest = -1;
    double max_distance = -1.0;
    double dist = 0.0;

    if (first == last)
    { // if we are in a loop, split the curve in 2 parts.
        for (unsigned int i = 1; i < input_points.size() - 1; ++i)
        {
            float norm = (first - input_points[i]).norm();
            dist = norm*norm;
            if (dist > max_distance)
            {
                index_furthest = i;
                max_distance = dist;
            }
        }
    }
    else
    {
        // finding furthest outlier
        for (unsigned int i = 1; i < input_points.size() - 1; i++)
        {
            dist = math::PointSegmentDistance(first, last, input_points[i]);
            if (dist > max_distance)
            {
                index_furthest = i;
                max_distance = dist;
            }
        }
    }

    if (max_distance > epsilon)
    {

        // simplify recursively

        std::vector<PointT> firstPart(input_points.begin() + 0, input_points.begin() + index_furthest + 1);
        std::vector<PointT> secondPart(input_points.begin() + index_furthest, input_points.end());

        std::vector<PointT> res1, res2;

        Simplify(firstPart, res1, epsilon);
        Simplify(secondPart, res2, epsilon);

        // concatenate results minus end/startpoint that will be the same
        res.insert(res.end(), res1.begin(), res1.end());
        res.insert(res.end(), res2.begin()+1, res2.end());
    }
    else
    {
        // simplification : on supprime les points intermédiaires
        res.push_back(first);
        res.push_back(last);
    }
}


/**
 * Smoothing algorithm based on Savitzky-Goley filter implementation.
 * The window size is set to 9.
 * @param input_points the list of points we want to smooth. Can be a loop.
 * @param[out] res the list of points for the smoothed curve.
 */
template<typename PointT>
void SmoothCurve(std::vector<PointT> &input_points, std::vector<PointT> &res)
{
    if (input_points.size() < 9)
    {
        res = input_points;
        return;
    }


    // The coefficient of the smoothing are referenced in a table but
    // We should check if there is a way to compute them
    unsigned int in_size = input_points.size();
    float norm_factor = 231.0f;
    res.clear();
    res.resize(input_points.size());
    for(unsigned int i = 0 ; i < input_points.size() ; i++)
    {
        if(i == 0)
            res[i] = ((1.0f/norm_factor)*(59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2] + 14.0f*input_points[i+3] - 21.0f*input_points[i+4]));
        else if(i == 1)
            res[i] =((1.0f/norm_factor)*(54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2] + 14.0f*input_points[i+3] - 21.0f*input_points[i+4]));
        else if(i == 2)
            res[i] = ((1.0f/norm_factor)*(39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2] + 14.0f*input_points[i+3] - 21.0f*input_points[i+4]));
        else if(i == 3)
            res[i] =((1.0f/norm_factor)*(14.0f*input_points[i-3] + 39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2] + 14.0f*input_points[i+3] - 21.0f*input_points[i+4]));
        else if(i == in_size - 4)
            res[i] =((1.0f/norm_factor)*(-21.0f*input_points[i-4] + 14.0f*input_points[i-3] + 39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2] + 14.0f*input_points[i+3]));
        else if(i == in_size - 3)
            res[i] =((1.0f/norm_factor)*(-21.0f*input_points[i-4] + 14.0f*input_points[i-3] + 39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2]));
        else if(i == in_size - 2)
            res[i] = ((1.0f/norm_factor)*(-21.0f*input_points[i-4] + 14.0f*input_points[i-3] + 39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1]));
        else if(i == in_size - 1)
            res[i] = ((1.0f/norm_factor)*(-21.0f*input_points[i-4] + 14.0f*input_points[i-3] + 39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i]));
        else
            res[i] = ((1.0f/norm_factor)*(-21.0f*input_points[i-4] + 14.0f*input_points[i-3] + 39.0f*input_points[i-2] + 54.0f*input_points[i-1] + 59.0f*input_points[i] + 54.0f*input_points[i+1] + 39.0f*input_points[i+2] + 14.0f*input_points[i+3] - 21.0f*input_points[i+4]));


    }
}

/**
 * Create a filtered Hermit sampled curve of the input
 *
 */
template<typename PointT>
void HermiteFiltering(std::vector<PointT> &input_points, std::vector<PointT> &res, float epsilon = 0.05f)
{
    unsigned int input_size = input_points.size();
    res.clear();
    std::vector<PointT> tmp_res;
    Simplify(input_points,tmp_res,epsilon);

    float size_ratio = static_cast<float>(input_size)/static_cast<float>(tmp_res.size());
    //TODO find an appropriated multiplier
    size_ratio *= 1.2f;
    for(unsigned int i = 0 ; i < tmp_res.size()-1 ; ++i)
        for(float f = 0.0f; f <= 1.0f -  1.0f/size_ratio ; f += 1.0f/size_ratio)
            res.push_back(math::CatmullInterpolation(tmp_res[i],tmp_res[i+1],f));

    res.push_back(tmp_res[tmp_res.size() - 1]);

}

/**
 * Compress or dilate in terms of time the input curve to match the given duration
 * This function suppose that the input curve is time ordered.
 * @param[out] input curve to be time atdapted
 * @param adapting duration
 */
template<typename PointT>
void AdapatUniformTimeCurve(GeometryCurve<animation::TimedParameter<PointT>>& input_c, float adapted_duration)
{
    assert(input_c .GetSize() > 1);
    float total_time = input_c.GetEnd().time- input_c.GetStart().time;
    float time_sc = (adapted_duration/total_time);

    for(unsigned int i = 1 ; i < input_c.GetSize(); ++i)
        input_c.GetPoint(i) =  input_c.GetPoint(0) + (input_c.GetPoint(i) - input_c.GetPoint(0))*time_sc;

}

template<typename PointT>
bool ComputeNormalsForMesh(ressource::Mesh<PointT> & mesh)
{
    std::map<unsigned int,std::vector<std::pair<unsigned int, unsigned int>>> face_index;
    std::vector<unsigned int> indices = mesh.indices();
    unsigned int nb_vec_face = mesh.getNbrVertexPerFace();
    for(unsigned int i = 0 ; i < indices.size() ; i += nb_vec_face)
        for(unsigned int j = 0 ; j < nb_vec_face ; ++j)
            face_index[indices[i+j]].push_back(std::pair<unsigned int,unsigned int>(i,j));

    for(auto& index_face : face_index)
    {
        glm::vec3 mean_normal;
        glm::vec3 v_pos = mesh.vertex(index_face.first).position();
        for(const std::pair<unsigned int, unsigned int> face : index_face.second)
        {
            mean_normal +=  glm::cross(mesh.vertex(indices[face.first + (face.second +1)%nb_vec_face]).position() - v_pos,
                    mesh.vertex(indices[face.first + (face.second -1)%nb_vec_face]).position() - v_pos);
        }
        mean_normal = glm::normalize(mean_normal);
        mesh.vertex(index_face.first).setNormal(mean_normal);
    }

    return true;
}

template<typename PointT>
std::shared_ptr<TopologicalMesh<PointT>> CatmullClarkSubdivision(const TopologicalMesh<PointT>& initial_mesh)
{
    std::unordered_map<unsigned int,glm::vec3> face_points;
    std::unordered_map<unsigned int,glm::vec3> edge_points;
    std::unordered_map<unsigned int, glm::vec3> unique_edge_points;
    std::unordered_map<unsigned int, unsigned int> edge_point_ids;
    std::unordered_map<unsigned int, unsigned int> face_point_ids;
    std::unordered_map<unsigned int, unsigned int> new_vertex_point_ids;

    std::shared_ptr<TopologicalMesh<PointT>> subdivided_mesh = std::make_shared<TopologicalMesh<PointT>>(initial_mesh.getName());

    //Face points
    for(const std::shared_ptr<Face>& f : initial_mesh.getFaces())
    {
        glm::vec3 face_point;
        unsigned int vertex_count = 0;
        Halfedge* he = f->e;
        do {
            face_point += he->v->p;
            vertex_count++;
            he = he->next;
        } while (he != f->e);
        face_point /= float(vertex_count);
        face_points[f->id] = face_point;
    }

    //Edge points
    for(const std::shared_ptr<Halfedge>& h : initial_mesh.getEdges())
    {
        if(h->opposite != nullptr && edge_points.find(h->opposite->id) != edge_points.end())
        {
            edge_points[h->id] = edge_points[h->opposite->id];
            continue;
        }
        glm::vec3 edge_point;
        edge_point += h->v->p;
        edge_point += h->next->v->p;
        edge_point += face_points[h->f->id];
        if(h->opposite != nullptr)
        {
            edge_point += face_points[h->opposite->f->id];
            edge_point /= 4.0f;
        }
        else
            edge_point /= 3.0f;

        edge_points[h->id] = edge_point;
        unique_edge_points[h->id] = edge_point;
    }



    //Verex points
    for(const std::shared_ptr<TVertex>& v : initial_mesh.getTVertices())
    {
        std::vector<unsigned int> adj_faces;
        initial_mesh.getVertexIncidentFaces(v->id,adj_faces);
        std::vector<unsigned int> adj_edges;
        initial_mesh.getVertexIncidentEdges(v->id,adj_edges);
        glm::vec3 face_av;
        glm::vec3 edge_av;
        for(unsigned int i = 0 ; i < adj_faces.size() ; ++i)
            face_av += face_points[adj_faces[i]];
        face_av /= adj_faces.size();

        for(unsigned int i = 0 ; i < adj_edges.size() ; ++i)
            edge_av += (initial_mesh.getHalfedge(adj_edges[i]).v->p + v->p)/2.0f;
        edge_av /= adj_edges.size();

        new_vertex_point_ids[v->id] = subdivided_mesh->addVertex(PointT((face_av + 2.0f*edge_av + (adj_edges.size() - 3.0f)*v->p)/float(adj_edges.size())));
    }


    for(auto& edge_point : unique_edge_points)
    {
        edge_point_ids[edge_point.first] =  subdivided_mesh->addVertex(edge_point.second);
    }
    for(const std::shared_ptr<Halfedge>& h : initial_mesh.getEdges())
        if(!utility::MapContains(edge_point_ids,h->id))
        {
            //assert(utility::MapContains(edge_point_ids,h->opposite->id));
            edge_point_ids[h->id] = edge_point_ids[h->opposite->id];
        }


    for(auto& face_point : face_points)
    {
        face_point_ids[face_point.first] =  subdivided_mesh->addVertex(face_point.second);
    }

    for(const std::shared_ptr<Face>& f : initial_mesh.getFaces())
    {
        Halfedge* he = f->e;
        do {
            std::vector<unsigned int> new_half_h;
            new_half_h.push_back(subdivided_mesh->addHalfedge(edge_point_ids[he->id]));
            new_half_h.push_back(subdivided_mesh->addHalfedge(face_point_ids[f->id]));
            new_half_h.push_back(subdivided_mesh->addHalfedge(edge_point_ids[he->prev->id]));
            new_half_h.push_back(subdivided_mesh->addHalfedge(new_vertex_point_ids[he->v->id]));
            subdivided_mesh->addFaceHE(new_half_h);

            he = he->next;
        } while (he != f->e);
    }
    subdivided_mesh->ComputeMeshFromTopo();
    ComputeNormalsForMesh(*(subdivided_mesh.get()));
    return subdivided_mesh;
}



}
}
#endif // GEOMETRYALGORITHMS_H
