#include "bmesh_gui.h"

#include "geometry/convexhull.h"
#include "geometry/topologicalmesh.h"
#include "ressources/pointcloud.h"

using namespace skyengine;

namespace module
{

namespace gui
{

CVHGui::CVHGui(skyengine::core::SkyEngine *engine,
               GLFWwindow *window,
               skyengine::scene::SceneGraph *sceneGraph,
               GuiManager *manager)
    : GuiTask(engine, manager)
    , m_sceneGraph(sceneGraph)
    , m_currentItem(-1)
    , m_currentMesh(nullptr)
{
    // For now, hide all meshes (which, in this sample, are considered as root children)
    auto root = m_sceneGraph->getRoot();
    for (const auto& child : root->getChildren())
    {
        auto renderable = dynamic_cast<skyengine::render::Renderable*>(child);
        if (renderable)
            renderable->hide();
    }
}

void CVHGui::run()
{
    ImGui::GetStyle().Colors[ImGuiCol_TitleBg] = ImVec4(1.0f, 0.0f, 0.0f, 0.8f);

    bool sh_t = true;
    ImGui::Begin("3D ConvexHull", &sh_t, ImVec2(200, 100));

    // Combo Box
    std::vector<const char*> comboList;

    auto root = m_sceneGraph->getRoot();
    const auto& meshes = root->getChildren();
    for (const auto& child : meshes)
        comboList.push_back(child->getName().c_str());

    int oldItem = m_currentItem;
    ImGui::Combo("Input mesh", &m_currentItem, comboList.data(), comboList.size());
    if (m_currentItem != oldItem)
    {
        if (m_currentMesh)
            m_currentMesh->hide();

        m_currentMesh = dynamic_cast<render::Renderable*>(meshes[m_currentItem]);
        if (m_currentMesh)
            m_currentMesh->show();
    }

    // Button
    if (ImGui::Button("Compute Convex Hull !"))
    {
        // Test  Catmull
         //std::shared_ptr<skyengine::geometry::TopologicalMesh<skyengine::render::vertex3D>> subdi = std::make_shared<skyengine::geometry::TopologicalMesh<skyengine::render::vertex3D>>("subdi_mesh");
         //subdi->ComputeTopoFromMesh(*(dynamic_cast<skyengine::ressource::Mesh<skyengine::render::vertex3D>*>(/*m_currentMesh->getMesh().get()*/skyengine::geometry::CubePrimitive(2.0f).get())));
         //subdi->Quandrangulize();
         //m_currentMesh->setMesh(std::dynamic_pointer_cast<skyengine::ressource::Mesh<skyengine::render::vertex3D>>(skyengine::geometry::CatmullClarkSubdivision<skyengine::render::vertex3D>(*(subdi.get()))->Duplicate()));
       // m_currentMesh->setMesh(skyengine::geometry::CatmullClarkSubdivision<skyengine::render::vertex3D>(*(std::dynamic_pointer_cast<skyengine::geometry::TopologicalMesh<skyengine::render::vertex3D>>(m_currentMesh->getMesh()).get())));
        // std::cout << "TODO !" << std::endl;

    }

    ImGui::End();
}

} // namespace gui

} // namespace module
