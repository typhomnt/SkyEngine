#include "viewport.h"

#include "ressources/texture.h"


namespace module
{

namespace gui
{

Viewport::Viewport(const std::string& name, SkyWindow* skyWindow
                   ,std::shared_ptr<ActiveObjectEvent> act_evt
                   ,std::shared_ptr<SelectObjectEvent> select_evt)
    : SkyWidget(name, skyWindow,act_evt,select_evt)
    , skyengine::core::RenderSurface()
    , m_active_camera(nullptr)
{}


glm::ivec2 Viewport::getSize() const
{
    return resizeEvent_->size();
}

void Viewport::resize(glm::ivec2 size)
{
    resizeEvent_->send(size);
}

glm::ivec2 Viewport::getPos() const
{
    return moveEvent_->pos();
}

void Viewport::move(glm::ivec2 pos)
{    
    moveEvent_->send(pos);
}

void Viewport::addDrawTask(skyengine::render::DrawTask* drawTask)
{
    m_drawTask = drawTask;
    for(auto& fbo: drawTask->m_task_framebuffers)
    {
        fbo.second->skyengine::core::Listener<skyengine::core::RenderSurface::ResizeEvent>::setListenerActive(true);
        fbo.second->skyengine::core::WindowEventListener::setListenerActive(false);
        fbo.second->skyengine::core::Listener<skyengine::core::RenderSurface::ResizeEvent>::setListenable(resizeEvent_);
    }
}

void Viewport::setActiveCamera(std::shared_ptr<skyengine::scene::Camera> camera)
{
    if(m_active_camera != nullptr)
    {
        m_sky_win->getEngine()->removeTask(m_active_camera->getName());

    }
    skyengine::core::OpenGLWindow* ogl_win =  skyengine::core::OpenGLWindow::getOpenGLWindow(m_sky_win->getManager()->getWindow());
    std::shared_ptr<skyengine::scene::CameraManipulator> manip = std::make_shared<skyengine::scene::CameraManipulator>(
                ogl_win,
                ogl_win->userInputManager(),
                m_drawTask->m_task_fbo_order.back()->resizeEvent(),
                camera
                );
    m_sky_win->getEngine()->addTask(std::make_shared<skyengine::core::ManipulatorUpdateTask>(
                                        manip
                                        ),camera->getName());

    m_active_camera = camera.get();
    m_active_camera_manip = manip.get();
}

void Viewport::draw()
{
    if (!m_drawTask)
        return;

    //TEST Hovered
    if(m_active_camera_manip != nullptr)
        m_active_camera_manip->skyengine::core::UserInputListener::setListenerActive(m_hovered);

    //ImVec2 wpos = ImGui::GetCursorScreenPos();
    ImVec2 wpos = ImGui::GetWindowPos();
    ImVec2 wsize = ImGui::GetWindowSize();    
    glm::ivec2 pos((int)wpos.x, (int)wpos.y);
    if (pos != getPos())
    {
        move(pos);
        for (auto& fboIt : m_drawTask->m_task_framebuffers)
            fboIt.second->move(pos);
    }

    glm::ivec2 size((int)wsize.x, (int)wsize.y);
    if (size != getSize())
    {
        resize(size);

        for (auto& fboIt : m_drawTask->m_task_framebuffers)
            fboIt.second->resize(size);
    }

    m_drawTask->run();

    // FIXME : a bit ugly (only works with one drawtask)
    auto lastFbo = m_drawTask->m_task_fbo_order.back();
    auto tex = lastFbo->getAttachmentTex(skyengine::core::FrameBuffer::AttachmentMode::COLORATTACHMENT0);

    ImDrawList* drawList = ImGui::GetWindowDrawList();
    drawList->AddImage(
                (void*)tex->getTextureID(),
                ImVec2(pos.x, pos.y + size.y),
                ImVec2(pos.x + size.x, pos.y)
                );
}

} // namespace gui

} // namespace module
