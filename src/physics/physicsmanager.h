#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H
#include "core/skyengine.h"
#include "utils/singleton.h"
#include "btBulletDynamicsCommon.h"
#include "scene/gameobject3d.h"
namespace skyengine
{

namespace physics
{

class PhysicsManager : public utility::Singleton<PhysicsManager>
{
public:

    friend PhysicsManager* Singleton<PhysicsManager>::GetSingleton();

    void AddBoxCollider(scene::GameObject3D* node, const glm::vec3& dimensions, const glm::vec3& origin, float m = 0.0f);
    bool AddPhyTask(std::shared_ptr<core::Task> phy_task);
    void setEngine(core::SkyEngine* skyengine);
    void ComputeSimulation(float dt);
private:
    PhysicsManager();
    std::unordered_map<std::string,core::Task*> m_phy_tasks;
    core::SkyEngine* m_engine;
    //Physic globals

    std::shared_ptr<btDefaultCollisionConfiguration> m_conf;
    std::shared_ptr<btCollisionDispatcher> m_collision_disp;
    std::shared_ptr<btBroadphaseInterface> m_broad_interface;
    std::shared_ptr<btSequentialImpulseConstraintSolver> m_const_solver;
    std::shared_ptr<btDiscreteDynamicsWorld> m_dyn_world;
};

glm::mat4 btTransformToMat4(const btTransform& transf);
btTransform Mat4TobtTransform(const glm::mat4& transf);
}
}
#endif // PHYSICSMANAGER_H
