#ifndef GUIMANAGER_H
#define GUIMANAGER_H

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <GL/glew.h>    // This example is using gl3w to access OpenGL functions (because it is small). You may use glew/glad/glLoadGen/etc. whatever already works for you.
#include <GLFW/glfw3.h>

#include "core/openglwindow.h"
#include "core/task.h"
#include "geometry/geometryalgorithms.h"
#include "render/renderable.h"
#include "core/eventlisteners/userinputlistener.h"


#ifdef _WIN32
#undef APIENTRY
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <GLFW/glfw3native.h>
#endif


namespace module
{

namespace gui
{

class SkyWindow;

class GuiManager : public skyengine::core::UserInputListener
{
public:
    GuiManager(GLFWwindow* window);
    ~GuiManager();
    bool GuiInit();

    inline GLFWwindow* getWindow();
    inline ImVec4* getGColor(){return &(g_clear_color);}

    static int          g_ShaderHandle, g_VertHandle, g_FragHandle;
    static int          g_AttribLocationTex, g_AttribLocationProjMtx;
    static int          g_AttribLocationPosition, g_AttribLocationUV, g_AttribLocationColor;
    static unsigned int g_VboHandle, g_VaoHandle, g_ElementsHandle;
    static bool         g_MousePressed[3];
    static  float       g_MouseWheel;
    static bool         g_stat_window_openned;

    void GuiNewFrame();
    void GuiRender();
    void ApplicationInfoWindow();
    virtual void listened(const skyengine::core::KeyEvent*);
    virtual void listened(const skyengine::core::CharEvent*);
    virtual void listened(const skyengine::core::MouseButtonEvent*);
    virtual void listened(const skyengine::core::MouseScrollEvent*);


private:
    GLFWwindow*  g_Window;
    double       g_Time;
    GLuint       g_FontTexture;

    bool g_init;
    ImVec4 g_clear_color;

    void GuiShutdown();
    void NewFrame();

    // Use if you want to reset your rendering device without losing ImGui state.
    void InvalidateDeviceObjects();
    bool CreateDeviceObjects();

    static const char* GetClipboardText(void* user_data);
    static void SetClipboardText(void* user_data, const char* text);
    bool CreateFontsTexture();
};

class GuiTask : public skyengine::core::Task
{
public:
    GuiTask(skyengine::core::SkyEngine* engine,gui::GuiManager* manager)
        : m_gui_manager(manager)
    {
        this->engine = engine;
    }

    inline gui::GuiManager* getManager() const {return m_gui_manager;}
    ~GuiTask(){}
private:
    gui::GuiManager* m_gui_manager;

};

class RenderableSelectionEvent : public  skyengine::core::Listenable<RenderableSelectionEvent>
{
public:
    RenderableSelectionEvent() {}
    virtual ~RenderableSelectionEvent() {}

    virtual void send(skyengine::render::Renderable* renderable)
    { renderable_ = renderable; notifyListeners(); }

    skyengine::render::Renderable* renderable() const {return renderable_;}

protected:
    skyengine::render::Renderable* renderable_;
};

class RenderableActiveEvent : public  skyengine::core::Listenable<RenderableActiveEvent>
{
public:
    RenderableActiveEvent() {}
    virtual ~RenderableActiveEvent() {}

    virtual void send(skyengine::render::Renderable* renderable)
    { renderable_ = renderable; notifyListeners(); }

    skyengine::render::Renderable* renderable() const {return renderable_;}

protected:
    skyengine::render::Renderable* renderable_;
};

class SkyEditor
{
public:
    SkyEditor(skyengine::core::SkyEngine* engine,
              const std::shared_ptr<gui::GuiManager>& gui_manager = nullptr);

    bool AddGuiTask(const std::shared_ptr<GuiTask>& task);
    bool RemoveTask(const std::string& name);

    inline std::shared_ptr<gui::GuiManager> getGuiManager();

    SkyWindow* getSkyWindow() const { return m_window.get(); }

    void addTasksToEngine();

protected:
    skyengine::core::SkyEngine* m_engine;
    std::shared_ptr<gui::GuiManager> m_gui_manager;

    std::shared_ptr<SkyWindow> m_window;
    std::unordered_map<std::string, std::shared_ptr<GuiTask>> m_guiTasks;
};

inline GLFWwindow* GuiManager::getWindow()
{
    return g_Window;
}


//SkyEditor
inline std::shared_ptr<gui::GuiManager> SkyEditor::getGuiManager()
{
    return m_gui_manager;
}

} // namespace gui

} // namespace module

#endif // GUIMANAGER_H

