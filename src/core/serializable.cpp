#include "serializable.h"

#include "ressources/ressourcemanager.h"
#include "scene/scenegraphmanager.h"


namespace skyengine
{

namespace core
{

bool SaveScene(const std::string& file_name,scene::SceneGraph& sg)
{
    rapidjson::Document doc;
    doc.SetObject();
    sg.Serialize(doc,doc.GetAllocator());
    ressource::RessourceManager::GetSingleton()->Serialize(doc,doc.GetAllocator());
    return utility::JSONHelper::Save(file_name,doc);
}

bool LoadScene(const std::string& file_name)
{
    rapidjson::Document doc;
    bool res = utility::JSONHelper::Load(file_name, doc);

    if (res)
    {
        ressource::RessourceManager::Load(doc["Resources"].GetObject());

        auto sg = scene::SceneGraphManager::Get("default_scene");
        sg->LoadProp(doc["Scene"].GetObject());
    }

    return res;
}

} // namespace core

} // namespace skyengine
