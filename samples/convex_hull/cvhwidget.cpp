#include "cvhwidget.h"

#include "geometry/convexhull.h"
#include "ressources/pointcloud.h"
#include "scene/gameobject3d.h"

using namespace skyengine;


CVHWidget::CVHWidget(const std::string& name,
                     module::gui::SkyWindow* window,
                     std::shared_ptr<module::gui::ActiveObjectEvent> aoEvent,
                     std::shared_ptr<module::gui::SelectObjectEvent> soEvent,
                     scene::SceneGraph* sceneGraph,
                     ImGuiWindowFlags flags)
    : module::gui::SkyWidget { name, window, aoEvent, soEvent, flags }
    , m_sceneGraph(sceneGraph)
    , m_meshIndex(-1)
    , m_currentMesh(nullptr)
{}

void CVHWidget::draw()
{
    auto& meshes = m_sceneGraph->getRoot()->getChildren();

    std::vector<const char*> guiMeshes;
    guiMeshes.reserve(meshes.size());
    for (scene::SceneGraphNode* child : meshes)
        guiMeshes.push_back(child->getName().c_str());

    ImGui::Combo("Input Mesh", &m_meshIndex, guiMeshes.data(), guiMeshes.size());

    if (ImGui::Button("Compute Convex Hull") && m_meshIndex != -1)
    {
        auto shader = std::dynamic_pointer_cast<ressource::Shader>(
            ressource::RessourceManager::Get("default_shader")
        );

        auto cvhModel = std::make_shared<scene::GameObject3D>(m_currentMesh->getName() + "_cvHull");
        cvhModel->setRenderShader(shader.get());

        auto v3dMesh = std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D>>(m_currentMesh->getMesh());
        if (v3dMesh)
            cvhModel->setMesh(geometry::ConvexHull3D(*v3dMesh));
        else
        {
            auto pcMesh = std::dynamic_pointer_cast<ressource::PointCloud>(m_currentMesh->getMesh());
            if (pcMesh)
                cvhModel->setMesh(geometry::ConvexHull3D(*pcMesh));
        }

        m_sceneGraph->addNode(cvhModel);
    }
}
