#include "doodle_gui.h"
#include "geometry/geometryalgorithms.h"
#include "Utils/actiongeneratorutility.h"
#include "Utils/utility.h"
#include "addons/imguivariouscontrols/imguivariouscontrols.h"
#include <sstream>
#include <ostream>
#include "doodleutils.h"
#include "utils/jsonhelper.h"
#include <imgui/addons/imguifilesystem/imguifilesystem.h>
namespace module
{

namespace doodle
{


MotionDoodleControls::MotionDoodleControls(const std::string& name,module::gui::SkyWindow* sky_win,
                                           std::shared_ptr<module::gui::ActiveObjectEvent> act_evt,
                                           std::shared_ptr<module::gui::SelectObjectEvent> select_evt,
                                           DoodleManipulator* doodle_manipulator)
    :  gui::SkyWidget(name,sky_win,act_evt,select_evt)
    , m_doodle_manipulator(doodle_manipulator)
    , m_show_geo_feat(false)
    , m_is_training(false)
    , m_cv_buff {""}
{
}

MotionDoodleControls::~MotionDoodleControls()
{

}

void MotionDoodleControls::draw()
{    

    //----------------------------------
    static ImGuiFs::Dialog dialog;
    std::string path;
    //Slate-----------------------------
    if(!ext_dvc::slate::SlateManager::isConnected())
        if(ImGui::Button("Connect"))
            ext_dvc::slate::SlateManager::initSingletonSlate();
    ImGui::Text("Slate time %.3f ms", m_doodle_manipulator->getLastClockTime());
    ImGui::Text("Slate freq %.3f hz", m_doodle_manipulator->getCurrentFreq());
    //----------------------------------------------

    //Training
    ImGui::Checkbox("Train", &m_is_training);
    if(!m_is_training)
    {
        ImGui::Checkbox("Drawing",m_doodle_manipulator->getDrawingMode());
        if(*m_doodle_manipulator->getDrawingMode())
        {
            if(ImGui::Button("Compute Animation"))
            {
                m_doodle_manipulator->UpdateFromDrawing();
            }
        }

        ImGui::Checkbox("No clamp",m_doodle_manipulator->getNoClamp());

        //Load Curve----------------------
        path = dialog.chooseFileDialog(ImGui::Button("Load Curve"));
        if(path.size() >0)
        {
            m_doodle_manipulator->LoadCurveFile(/*skyengine::utility::stringTrim(std::string(file),'\n').c_str()*/path.c_str());
        }
        //--------------------------------


        //Load Examples-------------------
        if(ImGui::Button("Load Examples"))
        {
            char file[8192];
#ifdef __linux__
            FILE *f = popen("zenity --file-selection --directory --multiple", "r");
            fgets(file, 8192, f);
            //multiple file managament
            std::vector<std::string> dirs = skyengine::utility::stringSplit(skyengine::utility::stringTrim(std::string(file),'\n'),'|');
            m_doodle_manipulator->getManager()->TrainLabanRecognizer(dirs);
#endif
        }
        //--------------------------------


        if(motionDoodle::MotionManager::m_laban_traj_analyser->getIsTrained())
        {
            if(ImGui::Button("Save Train Params"))
            {
                rapidjson::Document doc;
                doc.SetObject();
                for(auto& laban_prop : motionDoodle::MotionManager::m_laban_traj_analyser->getPropLaws())
                {
                    rapidjson::Value class_obj(rapidjson::kObjectType);
                    class_obj.SetObject();
                    for(auto& prop : laban_prop.second)
                    {
                        rapidjson::Value array(rapidjson::kArrayType);
                        array.PushBack(prop.second.first, doc.GetAllocator());
                        array.PushBack(prop.second.second, doc.GetAllocator());
                        class_obj.AddMember(rapidjson::Value(prop.first.c_str(), doc.GetAllocator()),
                                            array,
                                            doc.GetAllocator())
                                /*write in json*/;
                    }
                    doc.AddMember(rapidjson::Value(motionDoodle::utility::LabanWordToString(laban_prop.first).c_str(), doc.GetAllocator()),
                                  class_obj,
                                  doc.GetAllocator());
                }
                skyengine::utility::JSONHelper::Save("LMA.json",doc);
            }
        }



        path = dialog.chooseFileDialog(ImGui::Button("Load LMA Train"));
        if (path.size() > 0)
        {
            LoadLMATrainingFile(path);
        }



        int nbr_eg = m_doodle_manipulator->getManager()->GetLabanTrainEgNbr(motionDoodle::LabanWord::NEUTRAL);
        ImGui::InputInt("Curve Analyser Nbr of Example",&(nbr_eg));

        std::vector<const char*> files = {"Walk_Neutral","Jump_Neutral","Hit_Neutral","Side_Neutral","Nod_Neutral","No_Neutral","Punch_Neutral","Kick_Neutral",
                                          "Walk_Light","Jump_Light","Hit_Light","Side_Light","Nod_Light","No_Light","Punch_Light","Kick_Light",
                                          "Walk_Strong","Jump_Strong","Hit_Strong","Side_Strong","Nod_Strong","No_Strong","Punch_Strong","Kick_Strong",
                                          "Walk_Sudden","Jump_Sudden","Hit_Sudden","Side_Sudden","Nod_Sudden","No_Sudden","Punch_Sudden","Kick_Sudden",
                                          "Walk_Sustained","Jump_Sustained","Hit_Sustained","Side_Sustained","Nod_Sustained","No_Sustained","Punch_Sustained","Kick_Sustained",
                                          "Walk_LiSud","Jump_LiSud","Hit_LiSud","Side_LiSud","Nod_LiSud","No_LiSud","Punch_LiSud","Kick_LiSud",
                                          "Walk_LiSust","Jump_LiSust","Hit_LiSust","Side_LiSust","Nod_LiSust","No_LiSust","Punch_LiSust","Kick_LiSust",
                                          "Walk_StroSud","Jump_StroSud","Hit_StroSud","Side_StroSud","Nod_StroSud","No_StroSud","Punch_StroSud","Kick_StroSud",
                                          "Walk_StroSus","Jump_StroSus","Hit_StroSus","Side_StroSus","Nod_StroSus","No_StroSus","Punch_StroSus","Kick_StroSus"};
        if(ImGui::Combo("File",&m_current_file_name,&files[0],files.size()))
            std::strcpy(m_cv_buff,files[m_current_file_name]);
        ImGui::InputText("Curve Name",m_cv_buff,128);
        if(ImGui::Button(("Save Curve")))
        {
            m_doodle_manipulator->SaveCurve(std::string(std::string(m_cv_buff) + ".fic").c_str());
        }

        ImGui::Separator();

        ImGui::Checkbox("show geometry curves",&m_show_geo_feat);

        if(m_show_geo_feat)
        {
            ShowGeometryFeatures();
        }
        ImGui::Text(std::string("Last Detected Action: " + m_doodle_manipulator->getLastAction()).c_str());
    }
    else
    {
        *(m_doodle_manipulator->getDrawingMode()) = true;
        char buff[128];
        ImGui::InputText("Action Name",buff,128);
        ImGui::Text("Training curve number: %u",m_training_curves.size());
        if(ImGui::Button("Save Curve As example"))
        {
            m_training_vec.push_back(m_doodle_manipulator->getLastDoodleVec());
            m_training_curves.push_back(std::dynamic_pointer_cast<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>>(m_doodle_manipulator->getDrawingNode()->Duplicate()));
            //m_doodle_manipulator->getDrawingNode() = std::dynamic_pointer_cast<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>>(m_doodle_manipulator->getDrawingNode()->Duplicate());
        }
        if(m_training_curves.size() > 2)
        {
            if(ImGui::Button("Learn"))
            {
                LearnActionExpression();
            }
            if(ImGui::Button("Clear"))
            {
                for(auto& curve : m_training_curves)
                    curve->getGraphOwner()->deleteNode(curve->getName());
                m_training_curves.clear();
            }
        }
    }

    ImGui::Separator();
    ShowSegmenterProperties();

}

void MotionDoodleControls::ShowSegmenterProperties()
{
    if(ImGui::SliderFloat("Rotation Sensitivity",m_doodle_manipulator->getManager()->getMotionSegmenter()->getRotationSensitivity(),0.00001f,0.0001f,"%.5f"))
    {
        m_doodle_manipulator->getManager()->getMotionSegmenter()->ComputeMinEnergies();
    }
    if(ImGui::SliderFloat("Movement Sensitivity",m_doodle_manipulator->getManager()->getMotionSegmenter()->getMvtSensitivity(),0.002f,0.060f,"%.5f"))
    {
        m_doodle_manipulator->getManager()->getMotionSegmenter()->ComputeMinEnergies();
    }
    if(ImGui::SliderInt("Direction Sensitivity",m_doodle_manipulator->getManager()->getMotionSegmenter()->getDirectionSensitivity(),1,10))
    {
        m_doodle_manipulator->getManager()->getMotionSegmenter()->ComputeMinChangeState();
    }
    ImGui::SliderFloat("Rotation Translation Thres",m_doodle_manipulator->getManager()->getMotionSegmenter()->getRotTransThresFac(),800.0f,2600.0f,"%.5f");

    std::vector<std::string> scenario_actions;
    for(auto& action : m_doodle_manipulator->getScenario()->getMotionStyleSheetRef().getActions())
        scenario_actions.push_back("Action: " + action.getActionName()+ " RegExp "
                                   + action.getActionRegExp() + " Levenstein exp: " + action.getActionLevExp());
    ImGui::Text(std::string("Segmenter current token " + m_doodle_manipulator->getLastPushedToken()).c_str());
    ImGui::Text(std::string("Manager current sequence " + m_doodle_manipulator->getManager()->getCurrentExpr()).c_str());
    for(const std::string& action : scenario_actions)
        ImGui::BulletText(action.c_str());
}

void MotionDoodleControls::ShowGeometryFeatures()
{
    std::vector<motionDoodle::Vector7D> action_trajectory;
    std::vector<motionDoodle::Vector7D> noise_traj = m_doodle_manipulator->getLastCurve();
    skyengine::geometry::SmoothCurve(noise_traj,action_trajectory);
    if(action_trajectory.size() > 0)
    {
        if(m_doodle_manipulator->getManager()->IsLabanTrained())
        {
            std::tuple<motionDoodle::LabanWord,motionDoodle::LabanWord,motionDoodle::LabanWord> lab_coords = motionDoodle::MotionManager::m_laban_traj_analyser->Classify(action_trajectory);
            std::string class_space = motionDoodle::utility::LabanWordToString(std::get<0>(lab_coords));
            std::string class_time= motionDoodle::utility::LabanWordToString(std::get<1>(lab_coords));
            std::string class_weight = motionDoodle::utility::LabanWordToString(std::get<2>(lab_coords));

            std::vector<char> cstrs(class_space.c_str(), class_space.c_str() + class_space.size() + 1);
            std::vector<char> cstrw(class_weight.c_str(), class_weight.c_str() + class_weight.size() + 1);
            std::vector<char> cstrt(class_time.c_str(), class_time.c_str() + class_time.size() + 1);
            ImGui::InputText("class space",&cstrs[0],11);
            ImGui::InputText("class weight",&cstrw[0],11);
            ImGui::InputText("class time",&cstrt[0],11);
        }

        /*//Length
        float traj_length = 0.0f;
        for(unsigned int i = 1 ; i < action_trajectory.size() ; i++)
            traj_length += glm::l1Norm(action_trajectory[i].value - action_trajectory[i-1].value);

        ImGui::Text("Total length %.3f", traj_length);

        //Speed-----------------------------------
        std::vector<skyengine::animation::TimedParameter<glm::vec3>> speed_noise= skyengine::geometry::ComputeCurveSpeed(action_trajectory);
        std::vector<glm::vec3> speed_vec;
        speed_vec.resize(speed_noise.size());

        for (unsigned int i = 0; i < speed_noise.size(); ++i)
        {
            speed_vec[i] = speed_noise[i].value;
        }
        std::vector<float> speed_norm;
        float max_speed = FLT_MIN;
        float moy_speed = 0.0;
        float min_speed = FLT_MAX;
        speed_norm.resize(speed_vec.size());
        for(unsigned int i = 0 ; i < speed_vec.size() ; i++)
        {
            float norm = glm::l1Norm(speed_vec[i]);
            if(norm > max_speed)
                max_speed = norm;
            if(norm < min_speed)
                min_speed = norm;
            moy_speed += norm;
            speed_norm[i] = norm;
        }
        moy_speed /= speed_norm.size();



        //Acceleration----------------------------
        std::vector<skyengine::animation::TimedParameter<glm::vec3>> accel = skyengine::geometry::ComputeCurveAccel(action_trajectory);
        std::vector<float> accel_norm;
        float max_accel = FLT_MIN;
        float moy_accel = 0.0;
        float min_accel = FLT_MAX;
        accel_norm.resize(accel.size());
        for(unsigned int i = 0 ; i < accel.size() ; i++)
        {
            float norm = glm::l1Norm(accel[i].value);
            if(norm > max_accel)
                max_accel = norm;
            if(norm < min_accel)
                min_accel = norm;
            moy_accel += norm;
            accel_norm[i] = norm;
        }
        moy_accel /= accel_norm.size();

        //Curvature------------------------------
        std::vector<skyengine::animation::TimedParameter<float>> curvature_t = skyengine::geometry::ComputeCurveCurvature(action_trajectory);
        std::vector<float> curvature;
        for(unsigned int i = 0 ; i < curvature_t.size() ; i++)
        {
            curvature.push_back(curvature_t[i].value);
        }
        float max_curv = FLT_MIN;
        float moy_curv = 0.0;
        float min_curv = FLT_MAX;
        for(unsigned int i = 0 ; i < curvature.size() ; i++)
        {
            if(curvature[i] > max_curv)
                max_curv = curvature[i];
            if(curvature[i]  < min_curv)
                min_curv = curvature[i];
            moy_curv += curvature[i];
        }
        moy_curv /= curvature.size();

        //Torsion---------------------------------
        std::vector<skyengine::animation::TimedParameter<float>> torsion_t = skyengine::geometry::ComputeCurveTorsion(action_trajectory);
        std::vector<float> torsion;
        for(unsigned int i = 0 ; i < torsion_t.size() ; i++)
            torsion.push_back(torsion_t[i].value);
        float max_tors = -FLT_MAX;
        float moy_tors = 0.0;
        float min_tors = FLT_MAX;
        for(unsigned int i = 0 ; i < torsion.size() ; i++)
        {
            if(torsion[i] > max_tors)
                max_tors = torsion[i];
            if(torsion[i] < min_tors)
                min_tors = torsion[i];
            moy_tors += torsion[i];
        }
        moy_tors /= torsion.size();

        // Affine Speed--------------------------
        std::vector<skyengine::animation::TimedParameter<glm::vec3>> aff_speed = skyengine::geometry::ComputeCurveAffineSpeed(action_trajectory);
        std::vector<float> aff_speed_norm;
        aff_speed_norm.resize(aff_speed.size());
        for(unsigned int i = 0 ; i < aff_speed.size() ; i++)
            aff_speed_norm[i] = (glm::l1Norm(aff_speed[i].value));
        float max_aff = FLT_MIN;
        float moy_aff = 0.0;
        float min_aff = FLT_MAX;
        for(unsigned int i = 0 ; i < aff_speed_norm.size() ; i++)
        {
            if(aff_speed_norm[i] > max_aff)
                max_aff = aff_speed_norm[i];
            if(aff_speed_norm[i] < min_aff)
                min_aff = aff_speed_norm[i];
            moy_aff += aff_speed_norm[i];
        }
        moy_aff /= aff_speed_norm.size();
        //Affine Acce----------------------------
        std::vector<skyengine::animation::TimedParameter<glm::vec3>> aff_accel = skyengine::geometry::ComputeCurveAffineAccel(action_trajectory);
        std::vector<float> aff_accel_norm;
        aff_accel_norm.resize(aff_accel.size());
        for(unsigned int i = 0 ; i < aff_accel.size() ; i++)
            aff_accel_norm[i] = (glm::l1Norm(aff_accel[i].value));
        float max_accel_aff = FLT_MIN;
        float moy_accel_aff = 0.0;
        float min_accel_aff = FLT_MAX;
        for(unsigned int i = 0 ; i < aff_accel_norm.size() ; i++)
        {
            if(aff_accel_norm[i] > max_accel_aff)
                max_accel_aff = aff_accel_norm[i];
            if(aff_accel_norm[i] < min_accel_aff)
                min_accel_aff = aff_accel_norm[i];
            moy_accel_aff += aff_accel_norm[i];
        }
        moy_accel_aff /= aff_accel_norm.size();

        //Plots
        ImGui::PlotLines("",&speed_norm[0],speed_norm.size(),0, "Speed Norm", 0.0, max_speed, ImVec2(500,300));
        ImGui::SameLine();
        ImGui::PlotLines("",&accel_norm[0],accel_norm.size(),0, "Accel Norm", 0.0, max_accel, ImVec2(500,300));
        ImGui::SameLine();
        ImGui::PlotLines("",&curvature[0],curvature.size(),0,"Curvature",0.0,max_curv,ImVec2(500,300));
        ImGui::SameLine();
        ImGui::PlotLines("",&torsion[0],torsion.size(),0,"Torsion",min_tors,max_tors,ImVec2(500,300));
        ImGui::SameLine();
        ImGui::PlotLines("",&aff_speed_norm[0],aff_speed_norm.size(),0,"Affine Speed Norm",0.0,max_aff,ImVec2(500,300));
        ImGui::SameLine();
        ImGui::PlotLines("",&aff_accel_norm[0],aff_accel_norm.size(),0,"Affine accel Norm",0.0,max_accel_aff,ImVec2(500,300));
        ImGui::NewLine();
        //Total Time
        float traj_time = 0.0f;
        for(unsigned int i = 0 ; i < action_trajectory.size() - 1 ; i++)
            traj_time += action_trajectory[i+1].time - action_trajectory[i].time;
        std::sort(speed_norm.begin(),speed_norm.end());
        std::sort(accel_norm.begin(),accel_norm.end());
        std::sort(curvature.begin(),curvature.end());
        std::sort(torsion.begin(),torsion.end());
        std::sort(aff_speed_norm.begin(),aff_speed_norm.end());
        std::sort(aff_accel_norm.begin(),aff_accel_norm.end());
        ImGui::Text("Total Time %.3f", traj_time);
        ImGui::Text("Start-End distance: %.3f", glm::l1Norm(action_trajectory[0].value - action_trajectory[action_trajectory.size() - 1].value));
        ImGui::Text("Mean Speed: %.3f", moy_speed);
        ImGui::SameLine();
        ImGui::Text("Max Speed: %.3f", max_speed);
        ImGui::SameLine();
        ImGui::Text("Min Speed: %.3f", min_speed);
        ImGui::SameLine();
        ImGui::Text("Q1 Speed: %.3f", speed_norm[(speed_norm.size() - 1)/4]);
        ImGui::SameLine();
        ImGui::Text("Q3 Speed: %.3f", speed_norm[3*(speed_norm.size() - 1)/4]);
        ImGui::Text("Mean Accel: %.3f", moy_accel);
        ImGui::SameLine();
        ImGui::Text("Max Accel: %.3f", max_accel);
        ImGui::SameLine();
        ImGui::Text("Min Accel: %.3f", min_accel);
        ImGui::SameLine();
        ImGui::Text("Q1 Accel: %.3f", accel_norm[(accel_norm.size() - 1)/4]);
        ImGui::SameLine();
        ImGui::Text("Q3 Accel: %.3f", accel_norm[3*(accel_norm.size() - 1)/4]);
        ImGui::Text("Mean Curv: %.3f", moy_curv);
        ImGui::SameLine();
        ImGui::Text("Max Curv: %.3f", max_curv);
        ImGui::SameLine();
        ImGui::Text("Min Curv: %.3f", min_curv);
        ImGui::SameLine();
        ImGui::Text("Q1 Curv: %.3f", curvature[(curvature.size() - 1)/4]);
        ImGui::SameLine();
        ImGui::Text("Q3 Curv: %.3f", curvature[3*(curvature.size() - 1)/4]);
        ImGui::Text("Mean Tors: %.3f", moy_tors);
        ImGui::SameLine();
        ImGui::Text("Max Tors: %.3f", max_tors);
        ImGui::SameLine();
        ImGui::Text("Min Tors: %.3f", min_tors);
        ImGui::SameLine();
        ImGui::Text("Q1 Tors: %.3f", torsion[(torsion.size() - 1)/4]);
        ImGui::SameLine();
        ImGui::Text("Q3 Tors: %.3f", torsion[3*(torsion.size() - 1)/4]);
        ImGui::Text("Mean Aff: %.3f", moy_aff);
        ImGui::SameLine();
        ImGui::Text("Max Aff: %.3f", max_aff);
        ImGui::SameLine();
        ImGui::Text("Min Aff: %.3f", min_aff);
        ImGui::SameLine();
        ImGui::Text("Q1 Aff: %.3f", aff_speed_norm[(aff_speed_norm.size() - 1)/4]);
        ImGui::SameLine();
        ImGui::Text("Q3 Aff: %.3f", aff_speed_norm[3*(aff_speed_norm.size() - 1)/4]);
        ImGui::Text("Mean Aff: %.3f", moy_accel_aff);
        ImGui::SameLine();
        ImGui::Text("Max Aff: %.3f", max_accel_aff);
        ImGui::SameLine();
        ImGui::Text("Min Aff: %.3f", min_accel_aff);
        ImGui::SameLine();
        ImGui::Text("Q1 Aff: %.3f", aff_accel_norm[(aff_accel_norm.size() - 1)/4]);
        ImGui::SameLine();
        ImGui::Text("Q3 Aff: %.3f", aff_accel_norm[3*(aff_accel_norm.size() - 1)/4]);
        if(ImGui::Button("Save properties"))
        {
            std::ofstream curve_stream;
            curve_stream.open("curves_prop.csv",std::ios::out | std::ios::app);
            curve_stream << moy_speed << ","
                         << max_speed << ","
                         << min_speed << ','
                         << speed_norm[(speed_norm.size() - 1)/4] << ","
                         << speed_norm[3*(speed_norm.size() - 1)/4] << ","
                         << speed_norm[3*(speed_norm.size() - 1)/4] - speed_norm[(speed_norm.size() - 1)/4] << ","
                                                                                                            << moy_accel << ","
                                                                                                            << max_accel<< ","
                                                                                                            << min_accel << ','
                                                                                                            << accel_norm[(accel_norm.size() - 1)/4] << ","
                                                                                                            << accel_norm[3*(accel_norm.size() - 1)/4] << ","
                                                                                                            << accel_norm[3*(accel_norm.size() - 1)/4] - accel_norm[(accel_norm.size() - 1)/4] << ","
                                                                                                                                                                                               << moy_curv << ","
                                                                                                                                                                                               << max_curv<< ","
                                                                                                                                                                                               << min_curv << ','
                                                                                                                                                                                               << curvature[(curvature.size() - 1)/4] << ","
                                                                                                                                                                                               << curvature[3*(curvature.size() - 1)/4] << ","
                                                                                                                                                                                               << curvature[3*(curvature.size() - 1)/4] - curvature[(curvature.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                              << moy_tors << ","
                                                                                                                                                                                                                                                                              << max_tors<< ","
                                                                                                                                                                                                                                                                              << min_tors<< ','
                                                                                                                                                                                                                                                                              << torsion[(torsion.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                              << torsion[3*(torsion.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                              << torsion[3*(torsion.size() - 1)/4] - torsion[(torsion.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                     << moy_aff << ","
                                                                                                                                                                                                                                                                                                                                                     << max_aff<< ","
                                                                                                                                                                                                                                                                                                                                                     << min_aff<< ','
                                                                                                                                                                                                                                                                                                                                                     << aff_speed_norm[(aff_speed_norm.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                     << aff_speed_norm[3*(aff_speed_norm.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                     << aff_speed_norm[3*(aff_speed_norm.size() - 1)/4] - aff_speed_norm[(aff_speed_norm.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                                                                                                                        << moy_accel_aff << ","
                                                                                                                                                                                                                                                                                                                                                                                                                                                        << max_accel_aff<< ","
                                                                                                                                                                                                                                                                                                                                                                                                                                                        << min_accel_aff<< ','
                                                                                                                                                                                                                                                                                                                                                                                                                                                        << aff_accel_norm[(aff_accel_norm.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                                                                                                                        << aff_accel_norm[3*(aff_accel_norm.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                                                                                                                        << aff_accel_norm[3*(aff_accel_norm.size() - 1)/4] - aff_accel_norm[(aff_accel_norm.size() - 1)/4] << ","
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           << "\n";
            curve_stream.close();
        }*/


    }
}

void MotionDoodleControls::LearnActionExpression()
{
    assert(m_training_curves.size() == m_training_vec.size());
    unsigned int max_point = 0;
    skyengine::animation::KeyPose3D ref_start_pose;
    motionDoodle::Vector7D ref_start_vec;
    unsigned int ref_max_curve;
    unsigned int count = 0;
    for(unsigned int i = 0; i < m_training_curves.size(); ++i)
    {
        if(m_training_curves[i]->GetSize() > max_point)
        {
            max_point = m_training_curves[i]->GetSize();
            ref_start_pose = m_training_curves[i]->GetStart();
            ref_start_vec = m_training_vec[i][0];
            ref_max_curve = count;
        }
        count++;
    }

    //TODO add missing point in training curve such that every training has the same size max_point
    for(auto& curve : m_training_curves)
    {
        unsigned int size = curve->GetSize();
        assert(size >= 2);
        if(size < max_point)
        {
            for(unsigned int i = 0 ; i < max_point - size; ++i)
            {
                curve->AddPoint(0.5f*(curve->GetPoint(0) + curve->GetPoint(1)),1);
            }
        }
    }
    for(auto& curve : m_training_vec)
    {
        unsigned int size = curve.size();
        assert(size >= 2);
        if(size < max_point)
        {
            for(unsigned int i = 0 ; i < max_point - size; ++i)
            {
                curve.insert(curve.begin() +1, 0.5f*(curve[0] + curve[1]));
            }
        }
    }
    //We align the curves
    for(unsigned int i = 0 ; i < m_training_curves.size(); ++i)
    {
        if(i != ref_max_curve)
        {
            skyengine::animation::KeyPose3D first = ref_start_pose - m_training_curves[i]->GetStart();
            for(unsigned int j = 0 ; j < m_training_curves[i]->GetSize(); ++j)
            {
                m_training_curves[i]->GetPoint(j) = m_training_curves[i]->GetPoint(j) + first;
            }
        }
    }
    for(unsigned int i = 0 ; i < m_training_vec.size(); ++i)
    {
        if(i != ref_max_curve)
        {
            motionDoodle::Vector7D first = ref_start_vec - m_training_vec[i][0];
            for(unsigned int j = 0 ; j < m_training_vec[i].size(); ++j)
            {
                m_training_vec[i][j] = m_training_vec[i][j] + first;
            }
        }
    }
    //--------------------
    /*std::vector<std::vector<motionDoodle::Vector7D>> trainings;

    for(auto & draw_curve : m_training_curves)
    {
        std::vector<motionDoodle::Vector7D> training;
        for(unsigned int i = 0 ; i <  max_point ; ++i)
        {
            training.push_back(doodle::KeyPoseToVec7(draw_curve->GetPoint(i)));
        }
        trainings.push_back(training);
    }*/

    std::vector<motionDoodle::Vector7D> mean_curve;
    std::vector<motionDoodle::Vector7D> hermite_curve;
    mean_curve = motionDoodle::utility::ComputeAverageCurve(m_training_vec);
    std::shared_ptr<skyengine::geometry::HermiteSpline<motionDoodle::Vector7D>> hermite_fit;
    hermite_fit = skyengine::math::HermiteFit(mean_curve,mean_curve.size()/2);
    hermite_curve = hermite_fit->ComputePointSpline(mean_curve.size()/2);
    skyengine::utility::Logger::Debug(motionDoodle::utility::ComputeRegularExpressionFromCurve(hermite_curve,SLATE_MEAN_FREQ));
    //m_doodle_manipulator->getDrawingNode()->setTrajectory(DoodleTrajToAnimationTraj(mean_curve,glm::vec3(0.0000001)));
    //m_training_curves.push_back(std::dynamic_pointer_cast<skyengine::geometry::TrajectoryNode<skyengine::animation::KeyPose3D>>(m_doodle_manipulator->getDrawingNode()->Duplicate()));
    m_doodle_manipulator->getDrawingNode()->setTrajectory(DoodleTrajToAnimationTraj(hermite_curve,glm::vec3(0.0000001)));

}

}
}
