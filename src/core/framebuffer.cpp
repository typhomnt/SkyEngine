#include "framebuffer.h"
#include "ressources/ressourcemanager.h"
#include "utils/glcheck.h"
#include "utils/logger.h"
#include "utils/utilities.h"
#include "scene/quadobject.h"
#include "ressources/image.h"


namespace skyengine
{

namespace core
{

FrameBuffer FrameBuffer::DefaultFrameBuffer(unsigned int width, unsigned int height,std::shared_ptr<WindowEventManager> wem)
{
    FrameBuffer default_framebuffer(wem);
    GLCHECK(glDeleteFramebuffers(1, &default_framebuffer.m_frame_buffer_ID));
    default_framebuffer.m_frame_buffer_ID = 0;
    default_framebuffer.resize(glm::ivec2(width,height));
    return default_framebuffer;
}

FrameBuffer::FrameBuffer(std::shared_ptr<WindowEventManager> wem): RenderSurface()
  ,WindowEventListener(wem)
  ,core::Listener<core::RenderSurface::ResizeEvent>(nullptr)
  ,m_bind_mode(READ_DRAW)
  ,m_wem(wem)
{
    GLCHECK(glGenFramebuffers(1, &m_frame_buffer_ID));
    core::Listener<core::RenderSurface::ResizeEvent>::setListenerActive(false);
    resize(glm::ivec2(1,1));
}

glm::ivec2 FrameBuffer::getSize() const
{
   return resizeEvent_->size();
}

void FrameBuffer::listened(const WindowResizeEvent* e)
{
    resize(e->size());
}

void FrameBuffer::listened(const core::RenderSurface::ResizeEvent* e)
{
    resize(e->size());
}

void FrameBuffer::resize(glm::ivec2 size)
{
    //TODO manage ratio
    unsigned int new_width = size.x;
    unsigned int new_height = size.y;
    /*if(size.x > size.y)
        new_height = (920.0f/1024.0f)*size.x;
    else
        new_width = (1024.0f/920.0f)*size.y;*/
    resizeEvent()->send(glm::ivec2(new_width,new_height));

    //TODO update all texures and renderbuffers
    for(auto& attachment_tex : m_texture_attachment)
    {
         attachment_tex.second->bind();
         GLCHECK(glTexImage2D(attachment_tex.second->getTarget(), 0, attachment_tex.second->getInternalFormat(), new_width, new_height, 0, attachment_tex.second->getFormat(), attachment_tex.second->getType(), nullptr));
         attachment_tex.second->unbind();

    }
    for(auto& attachment_render : m_render_buffer_attachment)
    {
        GLCHECK(glBindRenderbuffer(GL_RENDERBUFFER, attachment_render.second));

        if(attachment_render.first == DEPTHATTACHMENT)
        {
            GLCHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, new_width, new_height));
        }
        else
        {
            GLCHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, new_width, new_height));
        }
        GLCHECK(glBindRenderbuffer(GL_RENDERBUFFER, 0));
    }
}

glm::ivec2 FrameBuffer::getPos() const
{
    return moveEvent_->pos();
}

void FrameBuffer::move(glm::ivec2 pos)
{
    moveEvent_->send(pos);
}

int FrameBuffer::checkStatus() const
{
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch(status)
    {
    case GL_FRAMEBUFFER_COMPLETE:
        return 0;
    case GL_FRAMEBUFFER_UNDEFINED:
        utility::Logger::GetSingleton()->Error("Framebuffer object: undefined");
        return -1;
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        utility::Logger::GetSingleton()->Error("Framebuffer object: incomplete attachement");
        return -1;
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        utility::Logger::GetSingleton()->Error("Framebuffer object: incomplete missing attachement");
        return -1;
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        utility::Logger::GetSingleton()->Error("Framebuffer object: incomplete draw buffer");
        return -1;
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        utility::Logger::GetSingleton()->Error("Framebuffer object: incomplete read buffer");
        return -1;
    case GL_FRAMEBUFFER_UNSUPPORTED:
        utility::Logger::GetSingleton()->Error("Framebuffer object: unsupported");
        return -1;
    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
        utility::Logger::GetSingleton()->Error("Framebuffer object: incomplete multisample");
        return -1;
    case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
        utility::Logger::GetSingleton()->Error("Framebuffer object: incomplete layer targets");
        return -1;
    default:
        utility::Logger::GetSingleton()->Error("Framebuffer object: unknown error");
        return -1;

    }

    return 0;
}

void FrameBuffer::bindAttachment(AttachmentMode attachment,GLenum internal_format, GLenum format, GLenum type, unsigned int width, unsigned int height)
{
    m_render_buffer_attachment.erase(attachment);

    std::string texture_name = "Frame"
                             + std::to_string(m_frame_buffer_ID)
                             + "Attachment"
                             + std::to_string(attachment);
    std::shared_ptr<ressource::Texture> attachment_texture = ressource::Ressource::Create<ressource::Texture>(
        texture_name,
        internal_format,
        format,
        type,
        width,
        height
    );

    GLCHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, attachment_texture->getTextureID(), 0));
    m_texture_attachment[attachment] = attachment_texture.get();
}

void FrameBuffer::bindAttachment(AttachmentMode attachment)
{
    //TODO
    //Check that attachment does not already exist (remove if it is the case)
    m_texture_attachment.erase(attachment);
    GLuint buffer_id;
    GLCHECK(glGenRenderbuffers(1, &buffer_id));
    m_render_buffer_attachment[attachment] = buffer_id;

    GLCHECK(glBindRenderbuffer(GL_RENDERBUFFER, buffer_id));

    if(attachment == DEPTHATTACHMENT)
    {
        GLCHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, resizeEvent_->size().x, resizeEvent_->size().y));
    }
    else
    {
        GLCHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, resizeEvent_->size().x, resizeEvent_->size().y));
    }

    GLCHECK(glFramebufferRenderbuffer(m_bind_mode,attachment,GL_RENDERBUFFER,buffer_id));
    GLCHECK(glBindRenderbuffer(GL_RENDERBUFFER, 0));


}

void FrameBuffer::unBindAttachment(AttachmentMode attachment)
{
    utility::MapRemove<AttachmentMode,ressource::Texture*>(m_texture_attachment,attachment);
    //TODO remove in ressource manager if it success ?
    utility::MapRemove<AttachmentMode,GLuint>(m_render_buffer_attachment,attachment);
}

void FrameBuffer::setFrameBuffer()
{
    for(auto& attachment_tex : m_texture_attachment)
        GLCHECK(glFramebufferTexture(m_bind_mode, attachment_tex.first, attachment_tex.second->getTextureID(), 0/*TODO put levels*/));
    for(auto& attachment_render_buf : m_render_buffer_attachment)
        GLCHECK(glFramebufferRenderbuffer(m_bind_mode, attachment_render_buf.first, GL_RENDERBUFFER/*TODO manage bind mode for render_buffer*/, attachment_render_buf.second));

    std::vector<GLenum> drawBuffers;
    drawBuffers.reserve(m_texture_attachment.size() + m_render_buffer_attachment.size());

    GLsizei i = 0;
    for(auto& attachment_tex : m_texture_attachment)
    {
        if(attachment_tex.first != DEPTHATTACHMENT
                && attachment_tex.first != STENCILATTACHMENT
                && attachment_tex.first != DEPTHSTENCILATTACHMENT)
        {
            drawBuffers.push_back(attachment_tex.first);
            i++;
        }
    }
    for(auto& attachment_render : m_render_buffer_attachment)
    {
        if(attachment_render.first != DEPTHATTACHMENT
                && attachment_render.first != STENCILATTACHMENT
                && attachment_render.first != DEPTHSTENCILATTACHMENT)
        {
            drawBuffers.push_back(attachment_render.first);
            i++;
        }
    }

    GLCHECK(glDrawBuffers(i, &drawBuffers[0]));
}


void FrameBuffer::bind()
{
    bind(READ_DRAW);
}

void FrameBuffer::bind(BindMode mode)
{
    GLCHECK(glBindFramebuffer(mode, m_frame_buffer_ID));
    m_bind_mode = mode;
}

void FrameBuffer::unBind()
{
    GLCHECK(glBindFramebuffer(m_bind_mode, 0));
}

void FrameBuffer::displayAttachment(AttachmentMode attachment)
{
    FrameBuffer default_framebuffer = DefaultFrameBuffer(resizeEvent_->size().x,resizeEvent_->size().y,m_wem);
    scene::QuadObject render_quad("render_quad");
    render_quad.setMaterial(nullptr);
    ressource::Shader* simple_shader = std::dynamic_pointer_cast<ressource::Shader>(ressource::RessourceManager::Get("simple_display_shader")).get();
    render_quad.setRenderShader(simple_shader);

    default_framebuffer.bind();
    simple_shader->use();

    std::string name = "Frame" + std::to_string(m_frame_buffer_ID) + "Attachment" + std::to_string(attachment);
    simple_shader->setTextureToSampler(name,"renderedTexture");
    simple_shader->bindTextures();
    render_quad.getMesh()->drawBuffers();

    ressource::Shader::clearUse();
    default_framebuffer.unBind();
}

void FrameBuffer::Copy(FrameBuffer& source,
                       AttachmentMode readBuffer,
                       FrameBuffer& destination,
                       GLbitfield mask)
{
    // Source
    source.bind(READ);
    if(readBuffer != DEPTHATTACHMENT && readBuffer != DEPTHSTENCILATTACHMENT && readBuffer != STENCILATTACHMENT)
        GLCHECK(glReadBuffer(readBuffer));

    // Destination
    destination.bind(DRAW);

    // Process blit
    GLCHECK(glBlitFramebuffer(
                source.m_up_left_corner.x,
                source.m_up_left_corner.y,
                source.m_up_left_corner.x + source.resizeEvent_->size().x,
                source.m_up_left_corner.y + source.resizeEvent_->size().y,
                destination.m_up_left_corner.x,
                destination.m_up_left_corner.y,
                destination.m_up_left_corner.x + destination.resizeEvent_->size().x,
                destination.m_up_left_corner.y + destination.resizeEvent_->size().y,
                mask,
                GL_NEAREST
    ));

    source.unBind();
    destination.unBind();
}

FrameBuffer::~FrameBuffer()
{
    if (m_frame_buffer_ID)
    {
        GLCHECK(glDeleteFramebuffers(1, &m_frame_buffer_ID));
        m_frame_buffer_ID = 0;
    }
    m_texture_attachment.clear();
    m_render_buffer_attachment.clear();
}

} // namespace core

} // namespace skyengine
