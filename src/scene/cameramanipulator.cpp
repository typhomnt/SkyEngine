#include "cameramanipulator.h"
#include "../utils/utilities.h"
#include <math/mathutils.h>

#include "GL/glew.h"
#include <GLFW/glfw3.h>

namespace skyengine
{

namespace scene
{

CameraManipulator::CameraManipulator(core::OpenGLWindow* win, const std::shared_ptr<core::UserInputManager>& uim, const std::shared_ptr<core::RenderSurface::ResizeEvent> &wem,
                                     const std::shared_ptr<Camera>& cam) :
    core::Manipulator(), core::UserInputListener(uim), core::Listener<core::RenderSurface::ResizeEvent>(wem),
    m_camera(cam),
    m_window(win),
    m_move_speed(100.f),
    m_sensitivity(0.01f),
    m_displacement(),
    m_mouse_delta(0.0f),
    m_first_mouse_move(true),
    m_left_button_pressed(false)
{}

void CameraManipulator::update(double /*t*/, double dt)
{
    if(m_window->hasMainFocus())
    {
        std::shared_ptr<Camera> p_cam = m_camera.lock();
        assert(p_cam && "Error: no or deleted camera in mamipulator");

        // rotation
        if (m_left_button_pressed)
        {
            if (m_first_mouse_move)
            {
                m_first_mouse_move = false;
                m_mouse_prev = m_mouse_cur;
            }

            m_mouse_delta = m_mouse_cur - m_mouse_prev;
            m_mouse_prev = m_mouse_cur;

            glm::vec2 shift = m_sensitivity * m_mouse_delta;

            float new_pitch = p_cam->getPitch() -  shift.y;
            float new_yaw = p_cam->getYaw() - shift.x;
            //clamp to avoid wavering camera
            new_pitch = utility::clamp(new_pitch, float(-M_PI*0.5f + 0.01), float(M_PI*0.5f - 0.01));

            p_cam->setYaw(new_yaw);
            p_cam->setPitch(new_pitch);

            glm::vec3 direction(
                        cos(new_pitch) * cos(new_yaw),
                        sin(new_pitch),
                        cos(new_pitch) * sin(-new_yaw)
                        );

            p_cam->setCurrDirection(glm::normalize(direction));
            //p_cam->setDirection(glm::normalize(direction));
        }
        else
        {
            m_first_mouse_move = true;
        }

        // displacement
        glm::vec3 side = glm::normalize(glm::cross(p_cam->getCurrDirection(),p_cam->getUpVector()));
        glm::vec3 up   = -glm::cross(p_cam->getCurrDirection(),side);


        p_cam->setPosition( p_cam->getPosition() + (float)dt * m_move_speed * (
                                side * m_displacement.x +
                                up * m_displacement.y+
                                p_cam->getCurrDirection() * m_displacement.z));

        m_displacement.z = 0.0f;

        p_cam->updateCameraWithCurrentAttributes();
    }
    else
    {
        m_left_button_pressed =false;
    }
}

void CameraManipulator::listened(const core::KeyEvent* event)
{
    if(event->action() == GLFW_RELEASE)
    {
        switch (event->key()) {
        case GLFW_KEY_UP:
        case GLFW_KEY_DOWN:
            m_displacement.y = 0.0;
            break;
        case GLFW_KEY_LEFT:
        case GLFW_KEY_RIGHT:
            m_displacement.x = 0.0;
            break;
        default:
            break;
        }
    }

    if(event->action() == GLFW_PRESS)
    {
        switch (event->key()) {
        case GLFW_KEY_UP:
            m_displacement.y = 1.0;
            break;
        case GLFW_KEY_DOWN:
            m_displacement.y = -1.0;
            break;
        case GLFW_KEY_LEFT:
            m_displacement.x = -1.0;
            break;
        case GLFW_KEY_RIGHT:
            m_displacement.x = 1.0;
            break;
        default:
            break;
        }
    }
}

void CameraManipulator::listened(const core::MouseMoveEvent* event)
{
    m_mouse_cur = event->pos();
}

void CameraManipulator::listened(const core::MouseButtonEvent* event)
{
    if(event->button() == GLFW_MOUSE_BUTTON_LEFT && (event->action() == GLFW_PRESS || event->action() == GLFW_RELEASE))
        m_left_button_pressed = (event->action() == GLFW_PRESS);
}

void CameraManipulator::listened(const core::MouseScrollEvent* event)
{
    m_displacement.z = event->offset().y;
}

void CameraManipulator::listened(const core::RenderSurface::ResizeEvent *event)
{
    std::shared_ptr<Camera> p_cam = m_camera.lock();
    p_cam->setRatio(float(event->size().x)/float(event->size().y));
}

void CameraManipulator::listened(const core::JoystickAxisEvent* event)
{

    float x_d = event->values()[0];
    float y_d = -event->values()[1];
    if(math::ApproxEqual(x_d,0.f,0.2f))
        x_d = 0.0f;
    if(math::ApproxEqual(y_d,0.f,0.2f))
        y_d = 0.0f;

    m_displacement.x = x_d;
    m_displacement.y = y_d;
}

void CameraManipulator::listened(const core::JoystickButtonEvent* event)
{
    char a_bt = event->values()[0];
    char b_bt = event->values()[1];
    char x_bt = event->values()[2];
    char y_bt = event->values()[3];
    char start_bt = event->values()[7];
    if(utility::bool_cast(a_bt))
        utility::Logger::Debug("A button");
    if(utility::bool_cast(b_bt))
        utility::Logger::Debug("B button");
    if(utility::bool_cast(x_bt))
        utility::Logger::Debug("X button");
    if(utility::bool_cast(y_bt))
        utility::Logger::Debug("Y button");
    if(utility::bool_cast(start_bt))
        utility::Logger::Debug("Start button");
}





} // namespace scene

} // namespace skyengine
