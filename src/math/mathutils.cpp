#include "mathutils.h"

namespace skyengine
{

namespace math
{

glm::quat RelativeQuat(const glm::quat& quat1, const glm::quat& quat2)
{
     return glm::normalize(quat1*glm::inverse(quat2));
}

glm::quat ScaleQuat(const glm::quat& quat_, float scale_fac)
{
    glm::quat sc_quat = quat_;
    if(fabs(scale_fac) <= 0.01f)
    {
        return glm::quat(1,0,0,0);
    }
    else
        sc_quat.w /= scale_fac;

    return glm::normalize(sc_quat);
}

glm::quat QuatDiv(const glm::quat& quat1, const glm::quat& quat2)
{
    float quat2_n = (float)quat2.length();

    float q0 = (quat1.w * quat2.w +quat1.x * quat2.x + quat1.y * quat2.y + quat1.z * quat2.z) / quat2_n;
    float q1 = (quat1.x * quat2.w - quat1.w * quat2.x - quat1.z * quat2.y + quat1.y * quat2.z) / quat2_n;
    float q2 = (quat1.y * quat2.w +quat1.z * quat2.x - quat1.w * quat2.y - quat1.x * quat2.z) / quat2_n;
    float q3 = (quat1.z * quat2.w - quat1.y * quat2.x + quat1.x * quat2.y - quat1.w * quat2.z) / quat2_n;

    return glm::quat(q0,q1,q2,q3);
}

glm::quat VecToQuat(const glm::vec4& v)
{
    return glm::quat(v.w,v.x,v.y,v.z);
}

glm::vec4 QuatToVec(const glm::quat& q)
{
    return glm::vec4(q.x,q.y,q.z,q.w);
}

glm::mat4 InterpolateMatrix(const glm::mat4& start, const glm::mat4& end, float interpolation_fac)
{
    return (1.0f - interpolation_fac)*start + interpolation_fac*end;
}

float LinearPowerFunc(float linear_fact,float power_fact, float x)
{
    if(math::ApproxEqual(x,0.0f,0.001f) && power_fact < 0)
        return 0.0f;
    else
        return(linear_fact*pow(x,power_fact));
}

glm::quat HermiteQuaternionInterpolation(const glm::quat& start ,const glm::quat& end, float weigtht)
{
    glm::quat relative_st  = start*glm::inverse(end);
    glm::quat relative_ts  = glm::inverse(relative_st);
    glm::quat inter_quat;

    inter_quat.w = HermitePointInterpolation(start.w,end.w,1.0f,1.0f,weigtht);
    inter_quat.x = HermitePointInterpolation(start.x,end.x,1.0f,1.0f,weigtht);
    inter_quat.y = HermitePointInterpolation(start.y,end.y,1.0f,1.0f,weigtht);
    inter_quat.z = HermitePointInterpolation(start.z,end.z,1.0f,1.0f,weigtht);
    return glm::normalize(inter_quat);
    //CardinalTangent(glm::vec3(),glm::vec3());

    //return glm::normalize(HermitePointInterpolation(start,end,relative_st,relative_ts,weigtht));

}

std::function<glm::quat(const glm::quat&,const glm::quat&, float weigtht)>  SpacedHermiteGeneration(float space)
{
    return [space](const glm::quat& start_quaternion, const glm::quat& end_quaternion, float interpolation_factor)
    {
        /*glm::vec4 relative_st  = QuatToVec(start_quaternion*glm::inverse(end_quaternion));
        glm::vec4 relative_ts  = QuatToVec(end_quaternion*glm::inverse(start_quaternion));
        return HermitePointInterpolation(start_quaternion,end_quaternion,VecToQuat(CardinalTangent(relative_st,relative_ts,(-space + 1.0f)*0.5f)),VecToQuat(CardinalTangent(relative_ts,relative_st,(-space + 1.0f)*0.5f)),interpolation_factor);*/
        /* glm::quat relative_st  = start*glm::inverse(end);
        glm::quat relative_ts  = glm::inverse(relative_st);
        glm::quat inter_quat;*/
        glm::quat inter_quat;
        float start_tangent_factor;
        float end_tangent_factor;

        start_tangent_factor = space;
        end_tangent_factor = space;



        inter_quat.w = HermitePointInterpolation(start_quaternion.w,end_quaternion.w,start_tangent_factor,end_tangent_factor,interpolation_factor);
        inter_quat.x = HermitePointInterpolation(start_quaternion.x,end_quaternion.x,1.0f,1.0f,interpolation_factor);
        inter_quat.y = HermitePointInterpolation(start_quaternion.y,end_quaternion.y,1.0f,1.0f,interpolation_factor);
        inter_quat.z = HermitePointInterpolation(start_quaternion.z,end_quaternion.z,1.0f,1.0f,interpolation_factor);
        return glm::normalize(inter_quat);

    };
}

float PointLineDistance(const glm::vec3& A, const glm::vec3& L0, const glm::vec3& L1)
{
    // Source :
    // https://math.stackexchange.com/questions/1905533/find-perpendicular-distance-from-point-to-line-in-3d

    glm::vec3 u = glm::normalize(L1 - L0);

    float t = glm::dot(A - L0, u);
    glm::vec3 P = L0 + t * u;

    return glm::length(P - A);
}

glm::vec3 TriangleNormal(const glm::vec3& T0, const glm::vec3& T1, const glm::vec3& T2)
{
    glm::vec3 N = glm::cross(T1 - T0, T2 - T0);

    float norm = glm::length(N);
    if (norm < 1e-5f)
        return glm::vec3(0.0f);

    return glm::normalize(N);

}

}
}
