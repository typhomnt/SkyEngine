#ifndef ANIMATIONMANAGER_H
#define ANIMATIONMANAGER_H

#include "utils/skyengine_dll.h"

#include "../utils/singleton.h"
#include <map>
#include "animator.h"
namespace skyengine
{

namespace animation
{

class SKYENGINE_API AnimationManager : public utility::Singleton<AnimationManager>
{
public:
    friend AnimationManager* Singleton<AnimationManager>::GetSingleton();

    AnimationManager(const AnimationManager&) = delete;
    AnimationManager& operator=(const AnimationManager&) = delete;

    static inline std::map<unsigned int,std::shared_ptr<Animator>>& GetAnimators();
    static inline std::shared_ptr<Animator> GetAnimator(unsigned int animator_id);

    static inline bool Exist(unsigned int animator_id);
    static unsigned int AddAnimator(std::shared_ptr<Animator> animator);
private:
    std::map<unsigned int,std::shared_ptr<Animator>> m_animators;
    unsigned int m_animator_count;

    AnimationManager();
    unsigned int addAnimator(std::shared_ptr<Animator> animator);
    inline bool exist(unsigned int animator_id);
};

inline bool AnimationManager::exist(unsigned int animator_id)
{
    return utility::MapContains(m_animators,animator_id);
}

inline bool AnimationManager::Exist(unsigned int animator_id)
{
    AnimationManager* manager = GetSingleton();
    return manager->exist(animator_id);
}

inline std::map<unsigned int, std::shared_ptr<Animator> > &AnimationManager::GetAnimators()
{
    AnimationManager* manager = GetSingleton();
    return manager->m_animators;
}



inline std::shared_ptr<Animator> AnimationManager::GetAnimator(unsigned int animator_id)
{
    AnimationManager* manager = GetSingleton();
    if(manager->exist(animator_id))
        return manager->m_animators[animator_id];
    else
        return nullptr;
}


}
}
#endif // ANIMATIONMANAGER_H
