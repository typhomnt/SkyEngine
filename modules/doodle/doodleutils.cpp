#include "doodleutils.h"
#include "ActionDetector/motionmanager.h"
#include "Utils/utility.h"
namespace module
{

namespace doodle
{

std::vector<motionDoodle::Vector7D> AnimationTrajToDoodleTraj(const skyengine::animation::AnimatedPoseTrajectory& curve)
{
    std::vector<motionDoodle::Vector7D> vec;
    for(const skyengine::animation::KeyPose3D& pose : curve.GetPoints())
    {
        glm::vec3 euler = glm::eulerAngles(pose.value.rotation);
        vec.push_back(motionDoodle::Vector7D(pose.value.position.x,pose.value.position.y,pose.value.position.z,euler.y,euler.z,euler.x,pose.time));
    }
    return vec;
}

skyengine::animation::AnimatedPoseTrajectory DoodleTrajToAnimationTraj(const std::vector<motionDoodle::Vector7D>& path, const glm::vec3 pos_shift)
{
    skyengine::animation::AnimatedPoseTrajectory curve;
    for(const motionDoodle::Vector7D& point : path)
        curve.AddPoint(skyengine::animation::KeyPose3D(skyengine::animation::Pose3D(glm::vec3(1,1,1),glm::quat(glm::vec3(point.vector.gab.y, point.vector.gab.x,point.vector.gab.z)),glm::vec3(point.vector.xyz.x,point.vector.xyz.y*(pos_shift != glm::vec3()),point.vector.xyz.z) + pos_shift),point.time));

    return curve;
}

bool LoadLMATrainingFile(const std::string& path)
{
    rapidjson::Document doc;
    if (skyengine::utility::JSONHelper::Load(path, doc))
    {
        for(rapidjson::Value::MemberIterator itr = doc.MemberBegin(); itr != doc.MemberEnd(); ++itr)
        {
            for(rapidjson::Value::MemberIterator mtr = itr->value.MemberBegin(); mtr != itr->value.MemberEnd(); ++ mtr)
            {
                motionDoodle::MotionManager::m_laban_traj_analyser->setLabanPropLaw(motionDoodle::utility::stringToLabanWord(itr->name.GetString()),mtr->name.GetString(),mtr->value[0].GetFloat(),mtr->value[1].GetFloat());
            }
        }

        motionDoodle::MotionManager::m_laban_traj_analyser->setIsTrained(true);

        std::vector<std::pair<std::string,std::function<float(const std::vector<motionDoodle::Vector7D>&)> >> properties =
        {{"mean_speed",motionDoodle::utility::ComputeCurveSpeedMean}
                ,{"mean_aff_speed",motionDoodle::utility::ComputeCurveAffineSpeedMean}
                ,{"mean_curv",motionDoodle::utility::ComputeCurveCurvatureMean}
                ,{"mean_accel_neg",motionDoodle::utility::ComputeCurveDecelMean}
                ,{"mean_accel",motionDoodle::utility::ComputeCurveAccelMean}
                ,{"mean_accel_pos",motionDoodle::utility::ComputeCurvePosAccelMean}
                ,{"mean_tors",motionDoodle::utility::ComputeCurveTorsionMean}
                ,{"mean_aff_accel",motionDoodle::utility::ComputeCurveAffineAccelMean}
                ,{"mean_jerk",motionDoodle::utility::ComputeCurveJerkMean}};

        for(auto& prop : properties)
            motionDoodle::MotionManager::m_laban_traj_analyser->setPropertyFunction(prop.first,prop.second);
        return true;
    }
    return false;
}
}
}
