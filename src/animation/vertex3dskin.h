#ifndef VERTEX3DSKIN_H
#define VERTEX3DSKIN_H
#include <vector>
#include <assert.h>
#include <iostream>
#include "../ressources/mesh.h"
#include "../render/Vertex3D.h"
#include "animationmacros.h"
namespace skyengine
{

namespace animation
{


struct Vertex3DSkin: public render::vertex3D
{
    int bones_IDs[NUM_BONES_PER_VEREX];
    float weights[NUM_BONES_PER_VEREX];
    int vertex_ID;

    Vertex3DSkin(): render::vertex3D(), bones_IDs{0}, weights{0.0f}, vertex_ID(-1){}
    Vertex3DSkin(const render::vertex3D& v): render::vertex3D(v), bones_IDs{0}, weights{0.0f}, vertex_ID(-1){}
    Vertex3DSkin(int vertex_ID):render::vertex3D(),  bones_IDs{0}, weights{0.0f}, vertex_ID(vertex_ID){}
    void SetBoneData(const unsigned int& bone_id, const float& weight)
    {
        for (unsigned int i = 0 ; i < NUM_BONES_PER_VEREX ; i++)
        {
            if (weights[i] == 0.0f)
            {
                bones_IDs[i] = bone_id;
                weights[i] = weight;
                return;
            }
            // std::cout << "index of  bone " << i << " " << weights[i] << std::endl;
        }
    }

    static std::vector<ressource::BufferAttrib> getBufferAttribs()
    {
        Vertex3DSkin tmp;
        char* tmp_p = (char*) &tmp;
#define ind(elem) ((char*)(&tmp.elem) - tmp_p)
        std::vector<ressource::BufferAttrib> attribs;
        attribs.push_back(ressource::BufferAttrib(0,3,GL_FLOAT,ind(pos)));
        attribs.push_back(ressource::BufferAttrib(1,2,GL_FLOAT,ind(uvs)));
        attribs.push_back(ressource::BufferAttrib(2,3,GL_FLOAT,ind(normal)));
        attribs.push_back(ressource::BufferAttrib(3,NUM_BONES_PER_VEREX,GL_INT,ind(bones_IDs[0]), ressource::BufferAttrib::SpecialAttrib::Int));
        attribs.push_back(ressource::BufferAttrib(4,NUM_BONES_PER_VEREX,GL_FLOAT,ind(weights[0])));
        attribs.push_back(ressource::BufferAttrib(5,1,GL_INT,ind(vertex_ID),ressource::BufferAttrib::SpecialAttrib::Int));
        return attribs;
    }

    static size_t BufferSize()
    {
        return vertex3D::BufferSize() + sizeof(bones_IDs) + sizeof(weights) + sizeof(vertex_ID);
    }

    static unsigned int MagicNumber()
    {
        return 1;
    }

    static Vertex3DSkin readFromStream(std::ifstream& in_s)
    {
        Vertex3DSkin new_v(vertex3D::readFromStream(in_s));
        utility::binary_read(in_s,new_v.bones_IDs);
        utility::binary_read(in_s,new_v.weights);
        utility::binary_read(in_s,new_v.vertex_ID);
        return new_v;
    }

    void writeStream(std::ostream& in_s)
    {
        vertex3D::writeStream(in_s);
        for(unsigned int i = 0 ; i< NUM_BONES_PER_VEREX; ++i)
            utility::binary_write(in_s,bones_IDs[i]);
        for(unsigned int i = 0 ; i< NUM_BONES_PER_VEREX; ++i)
            utility::binary_write(in_s,weights[i]);
        utility::binary_write(in_s,vertex_ID);
    }
};

} // namespace animation
} // namespace skyengine
#endif // VERTEX3DSKIN_H
