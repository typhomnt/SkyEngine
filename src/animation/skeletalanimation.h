#ifndef SKELETALANIMATION_H
#define SKELETALANIMATION_H

#include "utils/skyengine_dll.h"

#include "animation.h"
#include "skeletalkeyframe.h"
#include "animatedbone3d.h"
#include "skeleton.h"
#include <functional>
#include <map>

#ifdef __GUI__
namespace module
{

namespace gui
{
class ActionAnimator;
class AnimationEditor;
}
}
#endif

namespace skyengine
{

namespace animation
{

class SkeletalKeyFrame;
class SKYENGINE_API SkeletalAnimation : public Animation
{

#ifdef __GUI__
    friend class module::gui::ActionAnimator;
    friend class module::gui::AnimationEditor;
#endif

public:
    SkeletalAnimation(const std::string& animation_name);
    ~SkeletalAnimation();
    void setAnimationSkeleton(Skeleton* m_animation_skeleton);
    Skeleton* getAnimationSkeleton() const;

    //Animation computation
    void ComputeAnimation(float time_stamp);
    void ComputeBoneTransform(float animation_time);
    inline const std::vector<glm::mat4>& getLastComputedPose() const;
    inline std::shared_ptr<Skeleton> getLastPoseSkel();

    //Skeletal Animation properties---------------------------
    void ComputeMvtSortedBones();
    void computeBoneLocalTrajectories();
    void computeBoneLocalSpeeds();
    void computeBoneLocalAffsSpeeds();
    void computeBoneLocalAcceleration();
    void computeBoneGlobalTrajectories();
    void computeBoneGlobalSpeeds();
    void computeBoneGlobalAcceleration();

    //------------Getters----------------------//
    std::unordered_map<std::string,Bone3DTrajectory> getBonesLocalTrajectoryUnRef() const;
    std::unordered_map<std::string,Bone3DTrajectory> getBonesLocalSpeedUnRef() const;
    std::unordered_map<std::string,Bone3DTrajectory> getBonesLocalAffSpeedUnRef() const;
    std::unordered_map<std::string,Bone3DTrajectory> getBonesLocalAccelUnRef() const;
    std::unordered_map<std::string,Bone3DTrajectory> getBonesGlobalTrajectoryUnRef() const;
    std::unordered_map<std::string,Bone3DTrajectory> getBonesGlobalSpeedUnRef() const;
    std::unordered_map<std::string,Bone3DTrajectory> getBonesGlobalAccelUnRef() const;

    inline const std::map<float, std::string> &getMvtSortedBones();
    //-------------------------------------------------------

    //Ressource Functions-----------------------------------
    virtual std::shared_ptr<ressource::Ressource> Duplicate();
    //------------------------------------------------------

    void analyzeAnimation(float bone_importance_thres = 0.2f, float min_speed_thres = 0.5f);
    void computeBoneTargetAnimation(const std::string& eff_name, const std::string& tg_name);
    void CreateArmaAnim(scene::SceneGraph*);

protected:


    //Skeletal Animation properties---------------------------
    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_local_trajectories;
    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_local_speed_curves;
    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_local_aff_speed_curves;
    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_local_accel_curves;

    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_global_trajectories;
    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_global_speed_curves;
    std::unordered_map<std::string,std::shared_ptr<Bone3DTrajectory> > m_bone_global_accel_curves;

    //Mvt sorted bone in increasing order (last bone being the most important one)
    std::map<float,std::string> m_mvt_sorted_bones;
    //----------------------------------------------------------

    Skeleton* m_animation_skeleton;
    std::shared_ptr<Skeleton> m_last_pose_skel;
    std::vector<glm::mat4> m_last_computed_poses;

    std::unordered_map<std::string, std::map<float,Pose3D> > getSkeletalAnimationPoses();


    //IK Solver-----------------------------------------------------
    void IKTriangulationSolve(const std::pair<std::string,unsigned int>&);
    //-----------------------------------------------------
    void ComputeSkelAnim(float time_stamp);
    void ComputeLPoseLSkel();
};


inline const std::map<float,std::string>& SkeletalAnimation::getMvtSortedBones()
{
    if(m_mvt_sorted_bones.size() == 0)
        ComputeMvtSortedBones();
    return m_mvt_sorted_bones;
}

inline const std::vector<glm::mat4>& SkeletalAnimation::getLastComputedPose() const
{
    return m_last_computed_poses;
}

inline std::shared_ptr<Skeleton> SkeletalAnimation::getLastPoseSkel()
{
    return m_last_pose_skel;
}

}
}
#endif // SKELETALANIMATION_H
