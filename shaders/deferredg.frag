#version 330 core
layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

in vec3 fragPos;
in vec3 fragNormal;
in vec2 fragUV;

struct Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

// Uniform variables
uniform Material material;
uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;

void main()
{
    // store the fragment position vector in the first gbuffer texture
    gPosition = vec4(fragPos,0);
    // also store the per-fragment normals into the gbuffer
    gNormal = vec4(normalize(fragNormal),material.shininess);
    //gNormal = normalize(fragNormal);
    // and the diffuse per-fragment color
    gAlbedoSpec = material.diffuse;
    gAlbedoSpec.rgb *= texture(texture_diffuse1, fragUV).rgb;
    // store specular intensity in gAlbedoSpec's alpha component
    gAlbedoSpec.a = 0;
    //gAlbedoSpec.a *= texture(texture_specular1, fragUV).r;
}
