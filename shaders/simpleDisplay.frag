#version 330 core

in vec2 UV;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 color2;
/*layout (location = 2) out vec4 color3;
layout (location = 3) out vec4 color4;
layout (location = 4) out vec4 color5;
layout (location = 5) out vec4 color6;
layout (location = 6) out vec4 color7;
layout (location = 7) out vec4 color8;*/

//For FBO attachments
uniform sampler2D renderedTexture;
uniform sampler2D renderedTexture2;
/*uniform sampler2D renderedTexture3;
uniform sampler2D renderedTexture4;
uniform sampler2D renderedTexture5;
uniform sampler2D renderedTexture6;
uniform sampler2D renderedTexture7;
uniform sampler2D renderedTexture8;*/

void main()
{
       const float gamma = 2.2;
       vec3 hdrColor = texture(renderedTexture, UV).rgb;

       // reinhard tone mapping
      /* vec3 mapped = hdrColor / (hdrColor + vec3(1.0));
       // gamma correction
       mapped = pow(mapped, vec3(1.0 / gamma));*/

       float exposure = 1.0f;
        // Exposure tone mapping
        vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);
        // Gamma correction
        mapped = pow(mapped, vec3(1.0 / gamma));

       color = vec4(mapped, 1.0);
    //color = texture(renderedTexture, UV);
    //color2 = texture(renderedTexture2, UV);
    //color = vec4(1.0, 0.0, 0.0, 1.0);

    /*color3 = texture(renderedTexture3, UV);
    color4 = texture(renderedTexture4, UV);
    color5 = texture(renderedTexture5, UV);
    color6 = texture(renderedTexture6, UV);
    color7 = texture(renderedTexture7, UV);
    color8 = texture(renderedTexture8, UV);*/
    //float v = pow(texture(renderedTexture, UV).r,256);
    //color = vec4(v,v,v,1);
}
