#ifndef __PHONG_MATERIAL_H__
#define __PHONG_MATERIAL_H__

#include "utils/skyengine_dll.h"

#include "material.h"
#include "texture.h"

#include <memory>

#include <glm/glm.hpp>


namespace skyengine
{

namespace ressource
{
class PBRMaterial;

class SKYENGINE_API PhongMaterial : public Material
{
public:
    PhongMaterial(const std::string& name);

    PhongMaterial(const std::string& name,
                  const glm::vec4& emissive,
                  const glm::vec4& ambient,
                  const glm::vec4& diffuse,
                  const glm::vec4& specular,
                  float shininess);

    const glm::vec4& getEmissive() const { return m_emissive; }
    const glm::vec4& getAmbient() const { return m_ambient; }
    const glm::vec4& getDiffuse() const { return m_diffuse; }
    const glm::vec4& getSpecular() const { return m_specular; }
    float getShininess() const { return m_shininess; }

    void setEmissive(const glm::vec4& emissive) { m_emissive = emissive; }
    void setAmbient(const glm::vec4& ambient) { m_ambient = ambient; }
    void setDiffuse(const glm::vec4& diffuse) { m_diffuse = diffuse; }
    void setSpecular(const glm::vec4& specular) { m_specular = specular; }
    void setShininess(float shininess) { m_shininess = shininess; }

    virtual void sendToShader(const Shader& shader) const override;

    void addTexture(Texture* tex, unsigned int index = 0);


    /**
     * Usual materials
     */
    static std::shared_ptr<PhongMaterial> None;
    static std::shared_ptr<PhongMaterial> Default;
    static std::shared_ptr<PhongMaterial> Turquoise;

#ifdef __GUI__
    glm::vec4* getEmissiveP() { return &m_emissive; }
    glm::vec4* getAmbientP() { return &m_ambient; }
    glm::vec4* getDiffuseP() { return &m_diffuse; }
    glm::vec4* getSpecularP() { return &m_specular; }
    float* getShininessP() { return &m_shininess; }
#endif // __GUI__

    virtual std::string getTag() { return "PhongMaterial"; }
    virtual std::shared_ptr<rapidjson::Value> SerializeProp(rapidjson::MemoryPoolAllocator<> &al);

    std::shared_ptr<PBRMaterial> ToPBRMat();

private:
    glm::vec4 m_emissive;
    glm::vec4 m_ambient;
    glm::vec4 m_diffuse;
    glm::vec4 m_specular;
    float     m_shininess;

    std::vector<Texture*> m_textures;

    bool LoadProp(const rapidjson::Value& value) override;

    std::vector<std::string> m_texNames;
    void FinishLoading() override;
};

} // namespace ressource

} // namespace skyengine

#endif // __PHONG_MATERIAL_H__
