#include "skyengine.h"

#include "utils/logger.h"

#include "GLFW/glfw3.h"

#include <iostream>

namespace skyengine {
namespace core {


SkyEngine::SkyEngine() :
    total_t(0.0)
{
    utility::Logger::Info("Start Sky Engine");

    // Initialise GLFW
    if(!glfwInit())
    {
        utility::Logger::Fatal("[SkyEngine::SkyEngine] Failed to initialize GLFW");
        std::exit(-1);
    }

    auto errorCallback = [](int num, const char * descr)
    {
        utility::Logger::Error(utility::mkstring("[GLFW] Error ", num, ": ", descr));
    };
    glfwSetErrorCallback(errorCallback);

    utility::Logger::Info(utility::mkstring(" - GLFW version ", glfwGetVersionString()));
}

void SkyEngine::initOpenGL()
{
    utility::Logger::Info("Initialize GLEW");

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile

    if (glewInit() != GLEW_OK)
    {
        utility::Logger::Fatal("Failed to initialize GLEW\n");
        glfwTerminate();
        exit(-1);
    }

    /* Remove potential opengl error from glew. */
    glGetError();

    utility::Logger::Info(utility::mkstring(" - GL version ", glGetString(GL_VERSION)));
    utility::Logger::Info(utility::mkstring(" - GLSL version ", glGetString(GL_SHADING_LANGUAGE_VERSION)));
    utility::Logger::Info(utility::mkstring(" - Vendor: ", glGetString(GL_VENDOR)));
    utility::Logger::Info(utility::mkstring(" - Renderer: ", glGetString(GL_RENDERER)));
}

SkyEngine::~SkyEngine()
{
    utility::Logger::Info("End Sky Engine");
    glfwTerminate();
}

void SkyEngine::run()
{
    utility::Logger::Info("Start of main loop");

    running_ = true;

    while(running_)
    {
        //Delta time between frames
        double currentTime = glfwGetTime();
        delta_t = currentTime - total_t;
        total_t = currentTime;

        // first version of scheduler, works well for sequential tasks...
        for(auto pt : task_list)
        {
            pt->run();
        }

        // /!\Remove first then add
        for(auto& task : remove_list)
            removeTaskInternal(task);

        remove_list.clear();

        for(auto task : add_list)
            addTaskInternal(task);

        add_list.clear();


    }

    utility::Logger::Info("End of main loop");

}

void SkyEngine::requestTerminate()
{
    utility::Logger::Info("Main loop exit requested");
    running_ = false;
}

void SkyEngine::addTask(std::shared_ptr<Task> task)
{
    add_list.push_back(task);
}

void SkyEngine::addTask(std::shared_ptr<Task> task,const std::string& task_name)
{
    addTask(task);
    task->m_task_name = task_name;
}

void SkyEngine::removeTask(const std::string& task_name)
{
    remove_list.push_back(task_name);
}

void SkyEngine::removeTaskInternal(const std::string& task_name)
{
    for(unsigned int i = 0 ; i < task_list.size() ; ++i)
        if(task_list[i]->m_task_name == task_name)
        {
            task_list.erase(task_list.begin() + i);
            return;
        }
}

void SkyEngine::addTaskInternal(std::shared_ptr<Task> task)
{
    task->engine = this;
    task_list.push_back(task);
}






}
}
