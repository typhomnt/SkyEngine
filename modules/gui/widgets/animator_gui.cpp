#include "animator_gui.h"
#include "utils/utilities.h"
#include <animation/skeletalanimationmodifier.h>
#include <animation/skeletalanimator.h>
#include "gui/gui_external_widgets.h"
#include "scene/scenegraphmanager.h"
namespace module
{

namespace gui
{
ActionAnimator::ActionAnimator(const std::string& name, SkyWindow *sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                               std::shared_ptr<SelectObjectEvent> select_evt, skyengine::animation::Animator* animator)
    : SkyWidget(name,sky_win,act_evt,select_evt), m_animator(animator),m_current_anim(nullptr)
{
    m_animator->addAnimationModifier(std::make_shared<skyengine::animation::SkeletalAnimationModifier>(),"Laban_Modifier");
}

ActionAnimator::~ActionAnimator()
{

}

void ActionAnimator::draw()
{    
    ImGui::Text(std::string("Animator current time: " + std::to_string(m_animator->getTimeStamp())).c_str());
    ImGui::SliderFloat("Animator speed",m_animator->getPlaySpeed(),1.0f/150.0f,1.0f/5.0f);
    if(ImGui::Button("Replay",ImVec2(50,50)))
    {
        m_sky_win->getEngine()->removeTask("Animator");
        m_animator->playAnimation(m_sky_win->getEngine());
        m_animator->resetTimeStamp();
    }
    ImGui::SameLine();

    if(ImGui::Button("Update MoPa",ImVec2(50,50)))
        m_animator->updateMotionPaths();

    ImGui::SameLine();

    if(ImGui::Button("Clear",ImVec2(50,50)))
    {
        m_animator->clear();
        m_elem_laban.clear();
    }

    ImGui::SameLine();

    if(ImGui::Button("Looping",ImVec2(50,50)))
    {
        m_animator->setIsLooping(!m_animator->getIsLooping());
    }


    //TIMELINE TEST
    if (ImGui::BeginTimeline("Animator time_line",0.0f,m_animator->getEndTime(),m_animator->getAnimatorsElements().size()))  // label, max_value, num_visible_rows, opt_exact_num_rows (for item culling)
    {
        std::vector<float*> start_end_times;
        start_end_times.reserve(m_animator->getAnimatorsElements().size()*2);
        unsigned int count = 0;
        for(auto& elem : m_animator->getAnimatorsElements())
        {
            if(!skyengine::utility::MapContains(m_elem_laban,elem.first))
                m_elem_laban[elem.first] = glm::vec3();

            ImGui::VSliderFloat(std::string("S " + elem.first).c_str(),ImVec2(70,20),&m_elem_laban[elem.first].x,-1.0f,1.0f);
            ImGui::SameLine();
            ImGui::VSliderFloat(std::string("T " + elem.first).c_str(),ImVec2(70,20),&m_elem_laban[elem.first].y,-1.0f,1.0f);

            ImGui::VSliderFloat(std::string("W " + elem.first).c_str(),ImVec2(70,20),&m_elem_laban[elem.first].z,-1.0f,1.0f);

            if(ImGui::Button(std::string("Apply " + elem.first).c_str()))
            {
                if(elem.second->modifier == nullptr)
                    m_animator->setElementModifier(elem.first,std::make_shared<skyengine::animation::SkeletalAnimationModifier>());
                if(dynamic_cast<skyengine::animation::SkeletalAnimationModifier*>(elem.second->modifier.get()) != nullptr)
                    dynamic_cast<skyengine::animation::SkeletalAnimationModifier*>(elem.second->modifier.get())->setLabanFactors(m_elem_laban[elem.first]);
                m_animator->applyElementModifier(elem.first);
                m_animator->clampElementToEnd(elem.first);
                m_animator->clampToEnd();
            }
            start_end_times.push_back(&(elem.second->start_time));
            start_end_times.push_back(&(elem.second->end_time));
            ImGui::SameLine();
            if(ImGui::TimelineEvent(elem.first.c_str()
                                    ,*(start_end_times[2*count])
                                    ,*(start_end_times[2*count + 1])
                                    ,elem.second->layer))
            {
                m_animator->clampToEnd();
            }
            count++;
        }
        for(auto& trans : m_animator->getAnimatorsTransitions())
        {
            start_end_times.push_back(&(trans.second->start_time));
            start_end_times.push_back(&(trans.second->end_time));
            ImGui::TimelineEvent(trans.first.c_str()
                                 ,*(start_end_times[2*count])
                    ,*(start_end_times[2*count + 1])
                    ,trans.second->source_anim->layer);
            count++;
        }

    }
    // So that it's always in [0,50]
    double time_in,time_out;
    time_in = 0.0;
    time_out = m_animator->getEndTime();
    ImGui::EndTimeline(5,time_in,time_out);  // num_vertical_grid_lines, current_time (optional), timeline_running_color (optional)
    ImGui::SameLine(100.0f);
}

ActionPlayer::ActionPlayer(const std::string& name, SkyWindow *sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                           std::shared_ptr<SelectObjectEvent> select_evt
                           ,skyengine::animation::SkeletalAnimator *animator
                           ,skyengine::scene::SceneGraph *sc_g
                           ,skyengine::animation::GameObjectSkinned *anim_object)
    : SkyWidget(name,sky_win,act_evt,select_evt), m_anim_activ_event(std::make_shared<AnimationActiveEvent>())
    ,m_animator(animator)
    ,m_preview_animator(std::make_shared<skyengine::animation::SkeletalAnimator>())
    ,m_anim_object(anim_object)
    ,m_current_anim(nullptr)
    ,m_current_displayed_animation(0)
{
    m_preview_animator->setSceneGraph(sc_g);
    if(anim_object != nullptr)
        setAnimatedObject(anim_object);
}

void ActionPlayer::setAnimatedObject(skyengine::animation::GameObjectSkinned* anim_object)
{
    m_anim_object = anim_object;
    m_preview_animator->setAnimatedSkeleton(m_anim_object->getPSkeleton());
}

void ActionPlayer::listened(const ActiveObjectEvent* e)
{
    skyengine::animation::GameObjectSkinned* anim_obj =  dynamic_cast<skyengine::animation::GameObjectSkinned*>(e->node());
    if(anim_obj != nullptr)
        setAnimatedObject(anim_obj);
}

void ActionPlayer::draw()
{
    std::vector<const char*> current_animations;

    if(m_anim_object != nullptr){
        for(auto& anim : m_anim_object->animations)
        {
            std::unique_lock<std::mutex> lock(*(anim.second->getMutex().get()));
            current_animations.push_back(anim.first.c_str());
        }

        if(current_animations.size() > 0)
        {

            if(ImGui::Combo("Animations",&m_current_displayed_animation,&current_animations[0],current_animations.size()))
            {
                m_current_anim = m_anim_object->animations[current_animations[(unsigned int)(m_current_displayed_animation)]];
                m_anim_activ_event->send(m_current_anim);
            }
            if(ImGui::Button("Change looping"))
                m_preview_animator->setIsLooping(!m_preview_animator->getIsLooping());
            if(ImGui::Button("Play Animation"))
            {
                if(m_current_anim != nullptr)
                {
                    m_sky_win->getEngine()->removeTask("Animator");
                    m_preview_animator->playAnimation(m_sky_win->getEngine());
                    m_preview_animator->clear();
                    m_preview_animator->addAnimation(m_current_anim->getName(),m_preview_animator->getEndTime(),m_current_anim->getLastKeyFrame()->getTimeStamp() + m_preview_animator->getEndTime(),m_current_anim,m_curr_layer,nullptr,true);

                }
            }
            if(ImGui::Button("Analyze Animation"))
            {
                if(m_current_anim != nullptr)
                {
                    skyengine::animation::SkeletalAnimation* skel_anim = dynamic_cast<skyengine::animation::SkeletalAnimation*>(m_current_anim);
                    if(skel_anim != nullptr)
                        skel_anim->analyzeAnimation();
                }
            }
            if(ImGui::Button("Compose Animation"))
            {
                if(m_current_anim != nullptr)
                {
                    m_animator->addAnimation(m_current_anim->getName(),m_animator->getEndTime(),m_current_anim->getLastKeyFrame()->getTimeStamp() + m_animator->getEndTime(),m_current_anim,m_curr_layer,nullptr,true);
                }
            }
            if(m_current_anim != nullptr)
            {
                ImGui::Text("Animation keyframe number: %u", m_current_anim->getKeyframeNbr());
                glm::vec3 lab_eff = m_current_anim->getLabanEfforts();
                ImGui::Text("Animation Laban: %f %f %f", lab_eff.x,lab_eff.y,lab_eff.z);
            }
        }
    }

}

AnimationEditor::AnimationEditor(const std::string& name
                                 ,SkyWindow *sky_win,std::shared_ptr<ActiveObjectEvent> act_evt,
                                 std::shared_ptr<SelectObjectEvent> select_evt
                                 ,std::shared_ptr<AnimationActiveEvent > event)
    :SkyWidget(name,sky_win,act_evt,select_evt)
    ,AnimationActiveListener(event)
    ,m_animation(nullptr)
    ,m_current_bone(0)
    ,m_curr_ch_lght(0)
    ,m_tg_buff{""}
{

}

void AnimationEditor::draw()
{
    if(m_animation != nullptr)
    {
        char buff[128];
        ImGui::InputText("text", buff,128);
        for(auto & groups : m_animation->getKeyGroups())
        {
            if (ImGui::TreeNode(groups.first.c_str()))
            {
                for(const std::string& key_name : groups.second)
                {
                    if(ImGui::TreeNode(key_name.c_str()))
                        ImGui::TreePop();
                }
                ImGui::TreePop();
            }
        }
        HighLevelModif();
        AnimConstraintMenu();
    }
}
void AnimationEditor::HighLevelModif()
{
    skyengine::animation::SkeletalAnimation* skel_anim = dynamic_cast<skyengine::animation::SkeletalAnimation*>(m_animation);
    if(skel_anim != nullptr)
    {
        if((ImGui::Button("Analyze Animation")))
        {
            skel_anim->analyzeAnimation();
        }
        if((ImGui::Button("Simplify Animation")))
        {
            skel_anim->analyzeAnimation();
            skel_anim->Simplify();
        }
        if((ImGui::Button("Compute IKs Targets")))
        {
            for(auto& tg_eff : skel_anim->getAnimationSkeleton()->getEffTargetMap())
                for(auto& tg : tg_eff.second)
                    skel_anim->computeBoneTargetAnimation(tg_eff.first,tg);
        }
        if((ImGui::Button("Create Armature")))
        {
            skel_anim->CreateArmaAnim(skyengine::scene::SceneGraphManager::Get("default_scene").get());
        }
    }
    if((ImGui::Button("Copy Animation")))
    {
        m_animation = dynamic_cast<skyengine::animation::Animation*>(m_animation->Duplicate().get());
    }
}

void AnimationEditor::AnimConstraintMenu()
{
    skyengine::animation::SkeletalAnimation* skel_anim = dynamic_cast<skyengine::animation::SkeletalAnimation*>(m_animation);
    if(skel_anim != nullptr)
    {
        std::vector<const char*> current_bones;
        for(auto& bone : skel_anim->m_animation_skeleton->getBones())
            current_bones.push_back(bone.first.c_str());
        ImGui::Combo("Bones",&m_current_bone,&current_bones[0],current_bones.size());
        if(!skel_anim->m_animation_skeleton->containsIKChain(current_bones[m_current_bone]))
        {
            ImGui::SliderInt("Chain Length",&m_curr_ch_lght,1,5);
            if(ImGui::Button("Add IK"))
                skel_anim->m_animation_skeleton->addIKChain(current_bones[m_current_bone],(unsigned int)m_curr_ch_lght);
        }
        for(auto& iks : skel_anim->m_animation_skeleton->getIKChains())
        {
            if(!skyengine::utility::MapContains(m_curr_anim_bones,iks.first))
                m_curr_anim_bones[iks.first] = std::pair<int,unsigned int*>(0,&(iks.second));

            ImGui::Separator();
            ImGui::Text(iks.first.c_str());
            ImGui::SliderInt(std::string("Ch length" + iks.first).c_str(),(int*)(m_curr_anim_bones[iks.first].second),1,5);

            if(ImGui::Combo(std::string("Bones" + iks.first).c_str(),&m_curr_anim_bones[iks.first].first,&current_bones[0],current_bones.size()))
            {
                skel_anim->m_animation_skeleton->setTargetToIK(iks.first,current_bones[m_curr_anim_bones[iks.first].first]);
            }

            ImGui::InputText("Target", m_tg_buff,128);
            if(ImGui::Button(std::string("Add" + iks.first+ " Target").c_str()) && m_tg_buff != "")
            {
               skel_anim->m_animation_skeleton->addTargetBoneToEffector(m_tg_buff,current_bones[m_curr_anim_bones[iks.first].first]);
            }
        }
    }
}

void AnimationEditor::setAnimation(skyengine::animation::Animation* animation)
{
    m_animation = animation;
}

ArmatureDisplay::ArmatureDisplay(const std::string& name, SkyWindow *sky_win,
                                 std::shared_ptr<ActiveObjectEvent> act_evt,
                                 std::shared_ptr<SelectObjectEvent> select_evt,skyengine::animation::Armature* armature):SkyWidget(name,sky_win,act_evt,select_evt),m_armature(armature), m_current_root_bone(0),m_current_core_bone(0),m_current_head_bone(0)
{

}

void ArmatureDisplay::draw()
{
    if(m_armature != nullptr)
    {
        displaySkelRec(m_armature->getSkeleton()->getRootBone());
        std::vector<const char*> current_skel_bones;
        unsigned int tmp_count = 0;
        for(auto& bone : m_armature->getSkeleton()->getBones())
        {
            current_skel_bones.push_back(bone.first.c_str());
            if(m_armature->getSkeleton()->getRootMvtBone() != nullptr && m_armature->getSkeleton()->getRootMvtBone()->bone_name == bone.first)
                m_current_root_bone = tmp_count;
            if(m_armature->getSkeleton()->getCoreMvtBone() != nullptr && m_armature->getSkeleton()->getCoreMvtBone()->bone_name == bone.first)
                m_current_core_bone = tmp_count;
            if(m_armature->getSkeleton()->getHeadMvtBone() != nullptr && m_armature->getSkeleton()->getHeadMvtBone()->bone_name == bone.first)
                m_current_head_bone = tmp_count;

            tmp_count++;
        }

        if(ImGui::Combo("Root Mvt Bone",&m_current_root_bone,&current_skel_bones[0],current_skel_bones.size()))
            m_armature->getSkeleton()->setRootMvtBone(std::string(current_skel_bones[m_current_root_bone]));
        if(ImGui::Combo("Core Bone",&m_current_core_bone,&current_skel_bones[0],current_skel_bones.size()))
            m_armature->getSkeleton()->setCoreMvtBone(std::string(current_skel_bones[m_current_core_bone]));
        if(ImGui::Combo("Head Bone",&m_current_head_bone,&current_skel_bones[0],current_skel_bones.size()))
            m_armature->getSkeleton()->setHeadMvtBone(std::string(current_skel_bones[m_current_head_bone]));
    }
}

void ArmatureDisplay::displaySkelRec(skyengine::animation::Bone3D* bone)
{
    if (ImGui::TreeNode(bone->bone_name.c_str()))
    {
        for(skyengine::animation::Bone3D* child: bone->children)
            displaySkelRec(child);
        ImGui::TreePop();
    }
}



}
}
