/**
 * Sources for the 3D Convex Hull algorithm (QuickHull) :
 * http://box2d.org/files/GDC2014/DirkGregorius_ImplementingQuickHull.pdf
 * http://thomasdiewald.com/blog/?p=1888
 */

#ifndef __CONVEXHULL_H__
#define __CONVEXHULL_H__

#include <array>
#include <stack>

#include "geometry/geometryalgorithms.h"
#include "geometry/topologicalmesh.h"
#include "math/mathutils.h"
#include "render/Vertex3D.h"
#include "ressources/mesh.h"
#include "utils/logger.h"


namespace skyengine
{

namespace geometry
{

struct CVHFace
{
    Face* face;
    math::Plane plane;
    bool visited;

    std::vector<unsigned int> vertices;

    CVHFace() = default;

    CVHFace(Face* _face, const math::Plane& _plane)
        : face(_face)
        , plane(_plane)
        , visited(false)
    {}

    CVHFace(Face* _face, const glm::vec3& N, const glm::vec3& O)
        : face(_face)
        , plane(N, O)
        , visited(false)
    {}

    void pushVertex(unsigned int vid) { vertices.push_back(vid); }
    bool empty() const { return vertices.empty(); }

    float distance(const glm::vec3& P) const { return plane.distance(P); }
};


inline void InitMesh(TopologicalMesh<render::vertex3D>& cvHull, const std::vector<glm::vec3>& V)
{
    float dcur = 0.0f, dmax = -1.0f;

    // 1 : Compute extreme points
    std::array<int, 6> ep {0, 0, 0, 0, 0, 0};
    float extremeValues[6] = {
        V[0].x, V[0].x,
        V[0].y, V[0].y,
        V[0].z, V[0].z
    };

    for (int i = 1; i < (int)V.size(); ++i)
    {
        const glm::vec3& P = V[i];
        if (P.x < extremeValues[0])
        {
            extremeValues[0] = P.x;
            ep[0] = i;
        }
        else if (P.x > extremeValues[1])
        {
            extremeValues[1] = P.x;
            ep[1] = i;
        }
        if (P.y < extremeValues[2])
        {
            extremeValues[2] = P.y;
            ep[2] = i;
        }
        else if (P.y > extremeValues[3])
        {
            extremeValues[3] = P.y;
            ep[3] = i;
        }
        if (P.z < extremeValues[4])
        {
            extremeValues[4] = P.z;
            ep[4] = i;
        }
        else if (P.z > extremeValues[5])
        {
            extremeValues[5] = P.z;
            ep[5] = i;
        }
    }

    // 2 : Find two most distant extreme points
    int i0 = -1, i1 = -1;
    for (size_t i = 0; i < ep.size(); ++i)
    {
        const glm::vec3& Pi = V[ep[i]];
        for (size_t j = i + 1; j < ep.size(); ++j)
        {
            const glm::vec3& Pj = V[ep[j]];

            dcur = glm::length2(Pi - Pj);
            if (dcur > dmax)
            {
                dmax = dcur;
                i0 = (int)ep[i];
                i1 = (int)ep[j];
            }
        }
    }
    assert(i0 != -1 && i1 != -1);
    dmax = 0.0f;

    // 3 : Find the most distant point to the line between the two chosen extreme points
    int i2 = -1;
    for (size_t i = 0; i < ep.size(); ++i)
    {
        dcur = math::PointLineDistance(V[ep[i]], V[i0], V[i1]);
        if (dcur > dmax)
        {
            dmax = dcur;
            i2 = (int)ep[i];
        }
    }
    assert(i2 != -1);
    dmax = 0.0f;

    // 4 : Find the most distant point to the base triangle
    glm::vec3 N = math::TriangleNormal(V[i0], V[i1], V[i2]);
    math::Plane plane(N, V[i0]);

    int i3 = -1;
    for (size_t i = 0; i < V.size(); ++i)
    {
        dcur = std::abs(plane.distance(V[i]));
        if (dcur > dmax)
        {
            dmax = dcur;
            i3 = (int)i;
        }
    }
    assert(i3 != -1);

    // 5 : Construct tetrahedron
    unsigned int e0 = cvHull.addTVertex(V[i0]);
    unsigned int e1 = cvHull.addTVertex(V[i1]);
    unsigned int e2 = cvHull.addTVertex(V[i2]);
    unsigned int e3 = cvHull.addTVertex(V[i3]);
    cvHull.addFace({ e0, e1, e2 });
    cvHull.addFace({ e0, e3, e1 });
    cvHull.addFace({ e1, e3, e2 });
    cvHull.addFace({ e2, e3, e0 });
}

inline void AssignVertices(const std::vector<glm::vec3>& inputVertices,
                           const std::vector<unsigned int>& vertices,
                           const std::vector<std::shared_ptr<CVHFace>>& faces)
{
    for (unsigned int vid : vertices)
    {
        const glm::vec3& P = inputVertices[vid];
        for (const std::shared_ptr<CVHFace>& face : faces)
        {
            if (face->distance(P) > 0.0f)
            {
                face->pushVertex(vid);
                break;
            }
        }
    }
}

inline unsigned int CVHMainLoop(TopologicalMesh<render::vertex3D>& cvHull,
                                const std::vector<glm::vec3>& V,
                                std::unordered_map<Face*, std::shared_ptr<CVHFace>>& faces)
{
    unsigned int nbIter = 0;

    // Push faces on stack
    std::stack<std::shared_ptr<CVHFace>> cvhStack;
    for (auto& it : faces)
        cvhStack.push(it.second);

    while (!cvhStack.empty())
    {
        std::shared_ptr<CVHFace>& F = cvhStack.top();
        std::vector<unsigned int> assg_vert = F->vertices;
        if (F->empty())
        {
            cvhStack.pop();
            continue;
        }
        nbIter++;
        Face* treated_face = F->face;

        // Get most distant point from face
        int iMax = -1;
        float dmax = 0.0f, dcur = 0.0f;
        for (unsigned int vid : F->vertices)
        {
            dcur = F->distance(V[vid]);
            if (dcur > dmax)
            {
                dmax = dcur;
                iMax = vid;
            }
        }
        assert(iMax != -1);
        const glm::vec3& pMax = V[iMax];
        unsigned int eMax = cvHull.addTVertex(pMax);

        // remove treated vertex
        unsigned int index;
        for(unsigned int i = 0 ; i < assg_vert.size() ;++i)
            if(assg_vert[i]== iMax)
                index = i;
        assg_vert.erase(assg_vert.begin() + index);
        // Reinit "visited" flag for CVHFaces
        for (auto it : faces)
            it.second->visited = false;
        faces[F->face]->visited = true;

        // Get "horizon edges"
        std::vector<Halfedge*> horizonEdges;
        std::set<Halfedge*> visitedEdges;

        std::stack<Halfedge*> heStack;
        Halfedge* he = F->face->e;
        do {
            bool pushedOnStack = false;

            // Test if halfedge is horizon edge
            if (he->opposite == nullptr)
                horizonEdges.push_back(he);
            else
            {
                std::shared_ptr<CVHFace>& cvhFace = faces[he->opposite->f];

                if (cvhFace->distance(pMax) < 0.0f)
                    horizonEdges.push_back(he);
                else if (!cvhFace->visited)
                {
                    cvhFace->visited = true;

                    heStack.push(he);
                    visitedEdges.insert(he);
                    he = he->opposite;
                    pushedOnStack = true;
                }
            }

            if (!pushedOnStack)
                visitedEdges.insert(he);
            if (he->next != F->face->e && visitedEdges.find(he->next) != visitedEdges.end())
            {
                he = heStack.top();
                heStack.pop();
            }
            he = he->next;

        } while (he != F->face->e);

        //pop now that F is required anymore
        cvhStack.pop();
        // Create new faces
        std::vector<std::shared_ptr<CVHFace>> newFaces;
        for (Halfedge* he : horizonEdges)
        {
            unsigned int e0 = he->v->id;
            unsigned int e1 = he->next->v->id;
            const glm::vec3& P0 = he->v->p;
            const glm::vec3& P1 = he->next->v->p;

            unsigned int fid = cvHull.addFace({ e1, eMax, e0 });
            Face& face = const_cast<Face&>(cvHull.getFace(fid));

            glm::vec3 N = math::TriangleNormal(P0, pMax, P1);
            auto cvhFace = std::make_shared<CVHFace>(&face, N, P0);

            faces.insert({ &face, cvhFace });
            newFaces.push_back(cvhFace);
            cvhStack.push(cvhFace);
        }

        AssignVertices(V, assg_vert, newFaces);
        faces.erase(faces.find(treated_face));
        cvHull.removeFace(treated_face->id);

    }

    return nbIter;
}

template<typename T>
std::shared_ptr<TopologicalMesh<render::vertex3D>> ConvexHull3D(const ressource::Mesh<T>& inputMesh)
{
    auto cvHull = std::make_shared<TopologicalMesh<render::vertex3D>>(inputMesh.getName() + "_cvHull");
    if (cvHull)
    {
        std::vector<glm::vec3> vertices;
        vertices.reserve(inputMesh.vertices().size());

        std::vector<unsigned int> indices;
        indices.reserve(vertices.capacity());

        unsigned int id = 0;
        for (const T& V : inputMesh.vertices())
        {
            vertices.push_back(V.position());
            indices.push_back(id++);
        }

        utility::Logger::Debug("3D Convex Hull -- Initialization");
        InitMesh(*cvHull, vertices);

        std::unordered_map<Face*, std::shared_ptr<CVHFace>> cvhFaces;
        std::vector<std::shared_ptr<CVHFace>> initFaces;
        for (const auto& face : cvHull->getFaces())
        {
            std::vector<unsigned int> V;
            cvHull->getFaceVertices(face->id, V);

            glm::vec3 N = math::TriangleNormal(
                cvHull->getTVertex(V[0]).p,
                cvHull->getTVertex(V[1]).p,
                cvHull->getTVertex(V[2]).p
            );
            auto cvhFace = std::make_shared<CVHFace>(
                face.get(),
                N,
                cvHull->getTVertex(V[0]).p
            );

            cvhFaces.insert({ face.get(), cvhFace });
            initFaces.push_back(cvhFace);
        }
        AssignVertices(vertices, indices, initFaces);

        utility::Logger::Debug("3D Convex Hull -- Start main loop");
        unsigned int nbIter = CVHMainLoop(*cvHull, vertices, cvhFaces);
        utility::Logger::Debug("3D Convex Hull -- Main loop done in " + std::to_string(nbIter) + " iterations");

        cvHull->ComputeMeshFromTopo();
        geometry::ComputeNormalsForMesh(*cvHull);
    }

    return cvHull;
}

} // namespace geometry

} // namespace skyengine

#endif // __CONVEXHULL_H__
