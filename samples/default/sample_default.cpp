
#include "core/skyengine.h"
#include "scene/scenegraphnode.h"
#include "commons/sampleskyengine.h"
#include "core/functioncalltask.h"
#include "core/modelimportermanager.h"
#include "ressources/lights.h"
#include "scene/cameramanipulator.h"

#include "scene/scenegraphmanager.h"
#include "scene/quadobject.h"
#include "geometry/meshmodifier.h"

#include "utils/utilities.h"
#include "render/drawtask.h"
#include "ressources/ressourcemanager.h"



using namespace skyengine;

// temporay task for scenemanager draw

class SampleDefault : public samples::SampleSkyEngine
{
public:
    SampleDefault()  : SampleSkyEngine() {}
    ~SampleDefault();
    virtual void initScene() override;
    void initUserRessources() override;
    virtual void initTasks() override;

protected:

    std::shared_ptr<scene::Camera> sceneCamera;

    std::shared_ptr<ressource::Light> dirLight;
    std::shared_ptr<ressource::Light> pointLight;

    std::shared_ptr<render::DrawTask> draw_task;    
    std::shared_ptr<module::gui::GuiManager> gui_manager;

};

void SampleDefault::initScene()
{

    // Write scene exemple
    scene::SceneGraphManager::Add("default_scene",std::make_shared<scene::SceneGraph>());
    core::ModelImporterManager* model_manager = core::ModelImporterManager::GetSingleton();

    //InitImporter
    model_manager->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());
    //-----------


    //Stage
    model_manager->GetSingleton()->loadModel("../meshes/frame.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Cube-frame").get())->hide();
    model_manager->GetSingleton()->loadModel("../meshes/basic_patform.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());
    geometry::MeshModifier::static_scale(glm::vec3(),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh-basic_patform").get())->getMesh()).get()),0.03f);
    geometry::MeshModifier::static_translate(glm::vec3(0,0,-0.1),*(std::dynamic_pointer_cast<ressource::Mesh<render::vertex3D> >(dynamic_cast<scene::GameObject3D*>(scene::SceneGraphManager::Get("default_scene")->getNode("Mesh-basic_patform").get())->getMesh()).get()));
    //Human
    model_manager->GetSingleton()->loadModel("../meshes/Ayane_cat_animac_varias.fbx", model_manager->GetSingleton()->getSceneGraph() ,scene::SceneGraphManager::Get("default_scene")->getRoot().get());

    //DrawTask
    draw_task = std::make_shared<render::DrawTask>(this/*, main_window->getSize()*/,main_window->eventManager());
    draw_task->addShader("default_shader");
    draw_task->addShader("default_skin_shader");
    draw_task->addShader("instance_shader");
    draw_task->addShader("phong_shader");
    draw_task->addUniformValue("forward","default_shader",new scene::NodeParameter<glm::vec3*>("camPosition",1,sceneCamera->getPositionPt()));
    draw_task->addUniformValue("forward","default_shader",new scene::NodeParameter<glm::mat4*>("V",1,sceneCamera->getViewMatrixPt()));
    draw_task->addUniformValue("forward","default_shader",new scene::NodeParameter<glm::mat4*>("P",1,sceneCamera->getProjectionMatrixPt()));
    draw_task->addUniformValue("forward","default_skin_shader",new scene::NodeParameter<glm::vec3*>("camPosition",1,sceneCamera->getPositionPt()));
    draw_task->addUniformValue("forward","default_skin_shader",new scene::NodeParameter<glm::mat4*>("V",1,sceneCamera->getViewMatrixPt()));
    draw_task->addUniformValue("forward","default_skin_shader",new scene::NodeParameter<glm::mat4*>("P",1,sceneCamera->getProjectionMatrixPt()));
    draw_task->addUniformValue("forward","instance_shader",new scene::NodeParameter<glm::vec3*>("camPosition",1,sceneCamera->getPositionPt()));
    draw_task->addUniformValue("forward","instance_shader",new scene::NodeParameter<glm::mat4*>("V",1,sceneCamera->getViewMatrixPt()));
    draw_task->addUniformValue("forward","instance_shader",new scene::NodeParameter<glm::mat4*>("P",1,sceneCamera->getProjectionMatrixPt()));
    draw_task->addUniformValue("forward","phong_shader",new scene::NodeParameter<glm::vec3*>("camPosition",1,sceneCamera->getPositionPt()));
    draw_task->addUniformValue("forward","phong_shader",new scene::NodeParameter<glm::mat4*>("V",1,sceneCamera->getViewMatrixPt()));
    draw_task->addUniformValue("forward","phong_shader",new scene::NodeParameter<glm::mat4*>("P",1,sceneCamera->getProjectionMatrixPt()));
    draw_task->assignLightToShader("forward","default_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_shader",pointLight.get());
    draw_task->assignLightToShader("forward","default_skin_shader",dirLight.get());
    draw_task->assignLightToShader("forward","default_skin_shader",pointLight.get());
    draw_task->assignLightToShader("forward","instance_shader",dirLight.get());
    draw_task->assignLightToShader("forward","instance_shader",pointLight.get());
    draw_task->assignLightToShader("forward","phong_shader",dirLight.get());
    draw_task->assignLightToShader("forward","phong_shader",pointLight.get());
    draw_task->setSceneGraph(scene::SceneGraphManager::Get("default_scene").get());

    //Gui tasks
    gui_manager = std::make_shared<module::gui::GuiManager>(main_window->getGLFWindow());    
}

void SampleDefault::initUserRessources()
{

    //Init Camera, Lights and Shaders
    sceneCamera = std::make_shared<scene::Camera>("camera");
    sceneCamera->setPosition(glm::vec3(-10,3,0));
    sceneCamera->setDirection(glm::vec3(2.0,-1.0,1.0));


    dirLight = ressource::Ressource::Create<ressource::DirLight>("dirLight", 0);
    pointLight = ressource::Ressource::Create<ressource::PointLight>("pointLight", 0);


    //---------------------------
    sceneCamera->updateCameraWithCurrentAttributes();

}


void SampleDefault::initTasks()
{
    addTask(core::makeFunctionCallTask(glClear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));


    addTask(draw_task);
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiNewFrame,gui_manager));
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::ApplicationInfoWindow,gui_manager));
    addTask(core::makeFunctionCallTask(&module::gui::GuiManager::GuiRender,gui_manager));
    addTask(core::makeFunctionCallTask(&core::OpenGLWindow::doneRendering, main_window));
}



SampleDefault::~SampleDefault()
{
}


int main()
{
    SampleDefault engineInstance;
    //For random use
    utility::InitRandomSeed();
    utility::Logger::SetMinLevel(utility::Logger::LogLevel::DEBUG);
    engineInstance.init();
    engineInstance.run();

    return 0;
}

