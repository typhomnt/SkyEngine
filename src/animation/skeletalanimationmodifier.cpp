#include "skeletalanimationmodifier.h"
#include <iostream>
#include "skeletalanimator.h"
#include "utils/utilities.h"
#include "math/mathutils.h"
namespace skyengine
{

namespace animation

{

SkeletalAnimationModifier::SkeletalAnimationModifier() : TransformModifier<SkeletalAnimation,glm::vec3,glm::vec3>(),
    m_scale_factor(1.0f),m_rotate_factor(1.0f),m_translate_factor(1.0f),m_rising_factor(0.0f),m_spreading_factor(0.0f),m_global_duration_factor(0.0f)

{

}


void SkeletalAnimationModifier::scale(const glm::vec3& /*pivot_point*/, SkeletalAnimation &data_scale)
{
    scalePhase(data_scale);
}


void SkeletalAnimationModifier:: translate(const glm::vec3& /*translate_vector*/,  SkeletalAnimation& /*data_translate*/)
{

}

void SkeletalAnimationModifier::rotate(const glm::vec3& /*pivot_point*/,  SkeletalAnimation& /*data_rotate*/)
{

}

void SkeletalAnimationModifier::scale_axis(const glm::vec3& /*pivot_point*/, const glm::vec3 &/*pivot_axis*/,  SkeletalAnimation& /*data_scale*/)
{

}

void SkeletalAnimationModifier::rotate_axis(const glm::vec3& /*pivot_point*/,const glm::vec3& /*pivot_axis*/, SkeletalAnimation &/*data_rotate*/)
{

}

void SkeletalAnimationModifier::apply()
{
    if(m_scale_factor != 1.0f)
        apply_scale();
    if(m_rotate_factor != 1.0f)
        apply_rotate();
    if(m_translate_factor != 1.0f)
        apply_translate();
    if(m_rising_factor != 0.0f || m_spreading_factor != 0.0f)
        apply_shape();
    if(m_laban_factors[0] != 0.0f || m_laban_factors[1] != 0.0f || m_laban_factors[2] != 0.0f)
        apply_effort();
    if(m_global_duration_factor != 0.0f)
        scale_to_duration();
}


void SkeletalAnimationModifier::apply_shape()
{

    if(m_modified_animation->getAnimationSkeleton()->getRootMvtBone() != nullptr
            && m_modified_animation->getAnimationSkeleton()->getCoreMvtBone() != nullptr
            && m_modified_animation->getAnimationSkeleton()->getHeadMvtBone() != nullptr)
    {
        glm::vec4 local_core_axis = glm::vec4(1,0,0,1);
        std::vector<std::shared_ptr<BaseTimeParameter> > rotations = m_modified_animation->getKey(m_modified_animation->getAnimationSkeleton()->getCoreMvtBone()->bone_name + "_rotation");

        for(unsigned int i  = 0 ; i < rotations.size() ; ++i)
        {
            glm::quat local_quat = glm::normalize(glm::quat(1.0f+m_rising_factor,local_core_axis.x*m_rising_factor,local_core_axis.y*m_rising_factor,local_core_axis.z*m_rising_factor));

            //glm::vec4 offset_quat = glm::vec4(local_quat.w,local_quat.x,local_quat.y,local_quat.z);
            std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[i])->value *=local_quat;// glm::normalize(glm::quat(offset_quat[0],offset_quat[1],offset_quat[2],offset_quat[3]));
        }

        for(auto & art_bone : m_modified_animation->getAnimationSkeleton()->getArticulationBones())
        {
            std::vector<std::shared_ptr<BaseTimeParameter> > art_rotations = m_modified_animation->getKey(art_bone.first + "_rotation");
            for(unsigned int i  = 0 ; i < art_rotations.size() ; ++i)
                if(art_bone.first.back() == 'R')
                    std::dynamic_pointer_cast<TimedParameter<glm::quat>>(art_rotations[i])->value *= glm::normalize(glm::quat(1.0f+m_spreading_factor,0,0,-m_spreading_factor));
                else
                    std::dynamic_pointer_cast<TimedParameter<glm::quat>>(art_rotations[i])->value *= glm::normalize(glm::quat(1.0f+m_spreading_factor,0,0,m_spreading_factor));
        }
    }
}

void SkeletalAnimationModifier::scale_to_duration()
{
    scale_frame_timing(0,m_modified_animation->getKeyframeNbr()- 1,0,true,m_global_duration_factor/m_modified_animation->getDuration());
    assert(math::ApproxEqual(m_modified_animation->getDuration(),m_global_duration_factor,0.001f));
}

void SkeletalAnimationModifier::apply_effort()
{
    unsigned int preparation = 0;
    unsigned int anticipation = 0;
    unsigned int action = 0;
    unsigned int follow_through = 0;
    unsigned int transition = 0;

    //TODO remove after invalidate
    m_modified_animation->analyzeAnimation();

    if(m_modified_animation->getPreparationTime() > 0.0f)
        preparation = m_modified_animation->getPreparationTime();

    if(m_modified_animation->getAnticipationTime() > 0.0f)
        anticipation = m_modified_animation->getAnticipationTime();

    if(m_modified_animation->getActionTime() > 0.0f)
        action = m_modified_animation->getActionTime();

    if(m_modified_animation->getFollowThroughTime() > 0.0f)
        follow_through = m_modified_animation->getFollowThroughTime();

    if(m_modified_animation->getTransitionTime() > 0.0f)
        transition = m_modified_animation->getTransitionTime();



    float space = m_laban_factors[0];
    float time = m_laban_factors[1];
    float weight = m_laban_factors[2];

    //TODO Idea for space
    /*
     * We sort ik targets using most relevant bones or most relevant targets directly
     * For most relevant ik we change the interpolation function using highly curved
     * Hermit curves, for other we stick with more linear ones.     *
     */
    if(space != 0.0f)
        for(auto& target : m_modified_animation->getAnimationSkeleton()->getTargets())
            m_modified_animation->setInterpolationFunction(target + "_position"
                                                           ,utility::InterpolationFunction<glm::vec3>::getSmoothInterpolationFunc((space+1.0f)*0.5f));




    //Time----
    if(time != 0)
    {
        if(preparation != 0)
            scale_frame_timing(0,preparation,0,true,(-time*0.9f + 1),2.f);
        if(anticipation != 0)
            scale_frame_timing(preparation,anticipation,preparation,true,(-fabs(time)*0.8f + 1),1.f);
        if(action !=0)
        {
            if(time > 0)
                scale_frame_timing(anticipation,action,anticipation,true,(-time*0.7f + 1),1.f);
            else
                scale_frame_timing(anticipation,action,anticipation,true,(-time*2.0f + 1),1.f - 2.0f*time);
        }
        if(follow_through !=0)
        {
            assert(action != 0);
            scale_frame_timing(action,follow_through,action,true,(-time*time*0.8f + 1),2.f);
        }
        if(transition!=0)
        {
            assert(follow_through != 0);
            scale_frame_timing(follow_through,follow_through,follow_through,true,(-time*0.5f + 1),1.f);
        }

        setScaleFactor(1.0f/((2.0f*fabs(time)+1.0f)));
        if(anticipation != 0)
            scalePhase(*m_modified_animation,preparation,anticipation);
        if(follow_through !=0)
            scalePhase(*m_modified_animation,action,follow_through);

    }

    if(weight > 0)
    {
        if(anticipation != 0)
        {
            float anticipation_time = m_modified_animation->getKeyFrame((unsigned int)anticipation)->getTimeStamp() - m_modified_animation->getKeyFrame(0)->getTimeStamp();
            scale_frame_timing(0,anticipation,0,true,pow(anticipation_time,1-(1+weight)),/*2.0f*weight +1.0f*/ (1+weight));
        }
        if(action != 0)
        {
            float action_time = m_modified_animation->getKeyFrame((unsigned int)action)->getTimeStamp() - m_modified_animation->getKeyFrame((unsigned int)anticipation)->getTimeStamp();
            float expo = (-log(1.0f+weight)/log(action_time)) +1.0f;
            expo = 1.0f;
            scale_frame_timing(anticipation,action,anticipation,true,1.0f/(1.0f + weight),expo);
            if(follow_through != 0)
            {
                float follow_through_time = m_modified_animation->getKeyFrame((unsigned int)follow_through)->getTimeStamp() - m_modified_animation->getKeyFrame((unsigned int)action)->getTimeStamp();
                scale_frame_timing(action,follow_through,action,true,pow(follow_through_time,1-(1+weight)),/*2.0f*weight +1.0f*/ (1+weight));
            }
        }


        setRisingFactor(0.1f*weight);
        setSpreadingFactor(0.1f*weight);
        apply_shape();
        if(anticipation != 0)
        {
            setScaleFactor(sqrt(weight)+1.0f);
            scalePhase(*m_modified_animation,preparation,anticipation);
        }

    }
    if(weight < 0)
    {

        setRisingFactor(0.1f*weight);
        apply_shape();
        if(anticipation != 0)
        {
            setScaleFactor(1.0f/((10.0f*fabs(weight)+1.0f)));
            std::unordered_set<std::string> down_bones;
            std::map<float,std::string> mvt_sorted_bones = m_modified_animation->getMvtSortedBones();
            float max_mvt_art_sp_eg;
            std::string most_mvt_art_sp;
            for (auto bone = mvt_sorted_bones.rbegin(); bone != mvt_sorted_bones.rend(); ++bone)
                if((m_modified_animation->getAnimationSkeleton()->isArtBone(bone->second)
                    ||m_modified_animation->getAnimationSkeleton()->isSpineBone(bone->second)))
                {
                    most_mvt_art_sp = bone->second;
                    max_mvt_art_sp_eg = bone->first;
                    break;
                }
            for(auto& bone : mvt_sorted_bones)
                if(!math::ApproxEqual(bone.first,max_mvt_art_sp_eg,0.1f*max_mvt_art_sp_eg)
                        && (m_modified_animation->getAnimationSkeleton()->isArtBone(bone.second)
                            ||m_modified_animation->getAnimationSkeleton()->isSpineBone(bone.second)))
                    down_bones.insert(bone.second);
            scalePhase(*m_modified_animation,0,0,down_bones);
        }

        m_modified_animation->computeBoneLocalAffsSpeeds();
        //First step: sort bones by local mvt importance
        std::map<float,std::string> sorted_bones = m_modified_animation->getMvtSortedBones();
        std::multimap<std::string,float> sorted_bones_revert = utility::MapFlip(sorted_bones);
        float max_energy = sorted_bones.rbegin()->first;
        std::map<unsigned int,float> mean_aff_speed;
        std::map<unsigned int,float> mean_speed;
        std::map<unsigned int,float> mean_curv;
        std::map<unsigned int,float> old_dt;
        float global_aff_speed_mean = 0.0f;
        unsigned int kept_bone_nbr = 0;
        unsigned int count = 0;
        float total_duration = m_modified_animation->getDuration();
        std::unordered_map<std::string,Bone3DTrajectory> aff_speeds = m_modified_animation->getBonesLocalAffSpeedUnRef();
        std::unordered_map<std::string,Bone3DTrajectory> speeds = m_modified_animation->getBonesLocalSpeedUnRef();

        for(unsigned int i = 0 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
        {
            mean_aff_speed[i] = 0.0f;
            mean_speed[i] = 0.0f;
        }
        for(auto & aff_sp : aff_speeds)
        {
            count = 0;
            if(sorted_bones_revert.equal_range(aff_sp.first).first->second > max_energy - max_energy*0.2)
            {
                kept_bone_nbr++;
                for(auto& bone_keypose : aff_sp.second.GetPoints())
                {
                    mean_aff_speed[count] += bone_keypose.value.norm();
                    mean_speed[count] += speeds[aff_sp.first].GetPoint(count).value.norm();
                    count++;
                }
            }
        }

        for(unsigned int i = 0 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
        {
            mean_aff_speed[i] /= kept_bone_nbr;
            mean_speed[i] /= kept_bone_nbr;
        }

        for(unsigned int i = 0 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
            global_aff_speed_mean += mean_aff_speed[i];

        global_aff_speed_mean /= m_modified_animation->getKeyframeNbr();


        for(unsigned int i = 0 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
        {
            mean_curv[i] = 0.0f;
        }
        for(unsigned int i = 1 ; i < m_modified_animation->getKeyframeNbr() ; i++)
        {

            mean_curv[i] += pow(mean_aff_speed[i]/mean_speed[i],3);
        }

        for(unsigned int i = 0 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
        {
            mean_curv[i] /= m_modified_animation->getAnimationSkeleton()->getBones().size();
        }


        old_dt[0] = 0;
        for(unsigned int i = 1 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
        {
            old_dt[i] =  m_modified_animation->getKeyFrame(i)->getTimeStamp() - m_modified_animation->getKeyFrame(i-1)->getTimeStamp();

        }

        for(unsigned int i = 1 ; i < m_modified_animation->getKeyframeNbr() ; ++i)
        {
            float new_time =  m_modified_animation->getKeyFrame(i-1)->getTimeStamp() + global_aff_speed_mean/pow(mean_curv[i],1.0f/3.0f);
            m_modified_animation->getKeyFrame(i)->setTimeStamp(new_time);
        }

        m_modified_animation->setDuration(m_modified_animation->getLastKeyFrame()->getTimeStamp());

        scale_frame_timing(0,m_modified_animation->getKeyframeNbr()- 1,0,true,total_duration/m_modified_animation->getDuration());

    }

    resetFactors();
}

void SkeletalAnimationModifier::apply_naive_effort()
{

}

void SkeletalAnimationModifier::scale_frame_timing(unsigned int start_index, unsigned int end_index, unsigned int pivot_index, bool keep_coherence_offset, float scale_factor, float expos_factor)
{
    assert(start_index <= end_index);
    assert(end_index < m_modified_animation->getKeyframeNbr());
    assert(pivot_index < m_modified_animation->getKeyframeNbr());
    float start_coherence_offset = 0.0f;
    float end_coherence_offset = 0.0f;
    if(keep_coherence_offset)
    {
        if(pivot_index < end_index)
        {
            end_coherence_offset = m_modified_animation->getKeyFrame(std::min(m_modified_animation->getKeyframeNbr()-1,end_index + 1))->getTimeStamp()
                    - m_modified_animation->getKeyFrame(end_index)->getTimeStamp();
        }
        if(pivot_index > start_index)
        {
            start_coherence_offset = m_modified_animation->getKeyFrame(std::max(0,int(start_index - 1)))->getTimeStamp()
                    - m_modified_animation->getKeyFrame(start_index)->getTimeStamp();
        }
    }

    for(unsigned int i = start_index ; i <= end_index ; i++)
    {

        float diff = (m_modified_animation->getKeyFrame(i)->getTimeStamp() - m_modified_animation->getKeyFrame(pivot_index)->getTimeStamp());
        float sign = utility::clamp(float(diff/fabs(diff)),-1.0f,1.0f);
        float new_time =  m_modified_animation->getKeyFrame(pivot_index)->getTimeStamp() + math::LinearPowerFunc(scale_factor*sign,expos_factor,fabs(diff));
        m_modified_animation->getKeyFrame(i)->setTimeStamp(new_time);
    }
    if(keep_coherence_offset)
    {
        if(pivot_index < end_index)
        {
            float new_offset = m_modified_animation->getKeyFrame(std::min(m_modified_animation->getKeyframeNbr()-1,end_index + 1))->getTimeStamp()
                    - m_modified_animation->getKeyFrame(end_index)->getTimeStamp();
            new_offset -= end_coherence_offset;
            for(unsigned int i = end_index + 1 ; i < m_modified_animation->getKeyframeNbr() ; i++)
            {
                m_modified_animation->getKeyFrame(i)->setTimeStamp(m_modified_animation->getKeyFrame(i)->getTimeStamp() - new_offset);
            }
        }
        if(pivot_index > start_index)
        {
            float new_offset = m_modified_animation->getKeyFrame(std::max(0,int(start_index - 1)))->getTimeStamp()
                    - m_modified_animation->getKeyFrame(start_index)->getTimeStamp();
            new_offset -= start_coherence_offset;
            for(unsigned int i = 0 ; i < start_index ; i++)
            {
                m_modified_animation->getKeyFrame(i)->setTimeStamp(m_modified_animation->getKeyFrame(i)->getTimeStamp() - new_offset);
            }
        }
    }

    m_modified_animation->setDuration(m_modified_animation->getLastKeyFrame()->getTimeStamp());

}

void SkeletalAnimationModifier::apply_scale()
{
    scale(glm::vec3(),*m_modified_animation);
}

void SkeletalAnimationModifier::apply_translate()
{
    scale(glm::vec3(),*m_modified_animation);
}

void SkeletalAnimationModifier::apply_rotate()
{
    scale(glm::vec3(),*m_modified_animation);
}

void SkeletalAnimationModifier::setScaleFactor(float factor)
{
    m_scale_factor = factor;
}
void SkeletalAnimationModifier::setRotateFactor(float factor)
{
    m_rotate_factor = factor;
}

void SkeletalAnimationModifier::setTranslateFactor(float factor)
{
    m_translate_factor = factor;
}

void SkeletalAnimationModifier::setRisingFactor(float factor)
{
    m_rising_factor = factor;
}

void SkeletalAnimationModifier::setSpreadingFactor(float factor)
{
    m_spreading_factor = factor;
}

void SkeletalAnimationModifier::setLabanFactors(glm::vec3 factors)
{
    m_laban_factors = factors;
}

void SkeletalAnimationModifier::setGlobalDurationFactor(float factor)
{
    m_global_duration_factor = factor;
}

void SkeletalAnimationModifier::setModifiedAnimation(SkeletalAnimation* animation)
{
    m_modified_animation = animation;
}

void SkeletalAnimationModifier::scalePhase(SkeletalAnimation &data_scale, unsigned int start, unsigned int end
                                           ,const std::unordered_set<std::string>& scale_bones)
{
    if(end == 0)
        end = data_scale.getKeyframeNbr() - 1;

    std::map<float,std::string> mvt_sorted_bones = data_scale.getMvtSortedBones();
    std::map<float,float> mvt_scale_factors;
    float max_energy = mvt_sorted_bones.rbegin()->first;

    for(auto & energy_bone : mvt_sorted_bones)
        mvt_scale_factors[energy_bone.first] = energy_bone.first/max_energy;


    for(auto& key_bone : mvt_sorted_bones)
    {
        if((data_scale.getAnimationSkeleton()->isArtBone(key_bone.second)
                || data_scale.getAnimationSkeleton()->isSpineBone(key_bone.second))
                && (scale_bones.size() == 0 || utility::MapContains(scale_bones,key_bone.second)))
        {
            std::vector<std::shared_ptr<BaseTimeParameter> > rotations = data_scale.getKey(key_bone.second + "_rotation");
            std::vector<std::shared_ptr<BaseTimeParameter> > positions = data_scale.getKey(key_bone.second + "_position");
            unsigned int pivot = data_scale.getNearestKey(key_bone.second + "_rotation",start);
            unsigned int pivot_t = data_scale.getNearestKey(key_bone.second + "_position",start);
            unsigned int end_id = end; //data_scale.getNearestKey(key_bone.second + "_rotation",end);
            std::vector<glm::quat> new_rot;
            unsigned int end_cp_index = std::min(size_t(end_id),rotations.size()-1);
            new_rot.resize(rotations.size());
            float scale_f = (1.0f +  (m_scale_factor - 1.0f)* mvt_scale_factors[key_bone.first]);
            for(unsigned int i = pivot_t + 1 ; i <= end_cp_index; i++)
            {
                glm::vec3 rel = std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(positions[i])->value
                                - std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(positions[pivot_t])->value;
                std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(positions[i])->value
                        = std::dynamic_pointer_cast<TimedParameter<glm::vec3>>(positions[pivot_t])->value + scale_f*rel;
            }
            //TODO solve position shift after end_cp index like for rotation
            for(unsigned int i = pivot + 1 ; i <= end_cp_index; i++)
            {
                glm::quat relative;

                relative = math::RelativeQuat(std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[pivot])->value,std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[i])->value);
                relative = math::ScaleQuat(relative,scale_f);

                new_rot[i] = glm::inverse(glm::inverse(std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[pivot])->value)*relative);
                std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[i])->value = new_rot[i];
            }
            if(end_cp_index < rotations.size()-1)
            {
                glm::quat correct_relative =
                        std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[end_cp_index+1])->value
                        *glm::inverse(std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[end_cp_index])->value);

                for(unsigned i = end_cp_index+1 ; i < rotations.size() ; i++)
                {
                    glm::quat correct_quat = correct_relative;
                    glm::quat inv_correct_quat = glm::inverse(correct_relative);
                    inv_correct_quat = math::ScaleQuat(inv_correct_quat,static_cast<float>(i -  end_cp_index -1)
                                                       /static_cast<float>(rotations.size()-1 - end_cp_index-1));
                    new_rot[i] = glm::inverse(glm::inverse(std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[i])->value)*correct_quat*inv_correct_quat);
                    std::dynamic_pointer_cast<TimedParameter<glm::quat>>(rotations[i])->value = new_rot[i];
                }
            }

        }

    }
}

void SkeletalAnimationModifier::applyStrong()
{

}

void SkeletalAnimationModifier::applyLight()
{

}

void SkeletalAnimationModifier::applySudden()
{

}

void SkeletalAnimationModifier::applySustained()
{

}

void SkeletalAnimationModifier::applyDirect()
{

}

void SkeletalAnimationModifier::applyIndirect()
{

}

}

}
