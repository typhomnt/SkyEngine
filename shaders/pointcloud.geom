#version 330

layout(points) in;
layout(triangle_strip, max_vertices=5) out;

in VS_OUT
{
    vec4 color;
} gs_in[];

out vec4 varColor;


void main()
{
    const float size = 0.05;

    varColor = gs_in[0].color;

    gl_Position = gl_in[0].gl_Position + vec4(-size, -size, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(size, -size, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(-size, size, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(size, size, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(0.0, 2.0 * size, 0.0, 0.0);
    varColor = vec4(1.0, 1.0, 1.0, 1.0);
    EmitVertex();

    EndPrimitive();
}
