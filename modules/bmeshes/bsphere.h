#ifndef BSPHERE_H
#define BSPHERE_H
#include <glm/glm.hpp>
namespace module
{

namespace bmeshes
{

struct BSphere
{
    BSphere(float radius, const glm::vec3& center): radius(radius),center(center){}

    float radius;
    glm::vec3 center;
};


}
}
#endif // BSPHERE_H
