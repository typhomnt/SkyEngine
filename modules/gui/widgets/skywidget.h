#ifndef __SKYWIDGET_H__
#define __SKYWIDGET_H__

#include <string>

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>
#include "skywindow.h"

namespace module
{

namespace gui
{


class SkyWindow;
class SkyWidget : public ActiveObjectListener, SelectObjectListener
{
public:
    SkyWidget(const std::string& name, SkyWindow* sky_win,
              std::shared_ptr<ActiveObjectEvent> act_evt,
              std::shared_ptr<SelectObjectEvent> select_evt,  ImGuiWindowFlags flags = 0)
        : ActiveObjectListener(act_evt)
        , SelectObjectListener(select_evt)
        , m_name(name)
        , m_show(true)
        , m_hovered(false)
        , m_flags(flags)
        , m_sky_win(sky_win)
    {}

    const std::string& getName() const { return m_name; }

    ImGuiWindowFlags getFlags() const { return m_flags; }
    void setFlags(ImGuiWindowFlags newFlags) { m_flags = newFlags; }

    bool isHovered() const {return m_hovered;}
    void setHovered(bool hovered) {m_hovered = hovered;}

    bool isVisible() const { return m_show; }
    void show() { m_show = true; }
    void hide() { m_show = false; }

    virtual void draw() = 0;

protected:
    std::string m_name;
    bool m_show;
    bool m_hovered;
    ImGuiWindowFlags m_flags;

    SkyWindow* m_sky_win;

};

} // namespace gui

} // namespace module

#endif // __SKYWIDGET_H__
