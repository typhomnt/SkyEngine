#version 330 core


// Uniform variables
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

// Input variables
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 normal;

// Output variables
out vec3 fragPos;
out vec3 fragNormal;
out vec2 fragUV;


void main()
{
    gl_Position = P * V * M * vec4(vertexPosition_modelspace, 1.0);

    vec4 vertex_world = M * vec4(vertexPosition_modelspace, 1.0);
    fragPos = vec3(vertex_world) / vertex_world.w;

    // TODO : This will work unless we apply non-uniform scaling on the model !
    fragNormal = mat3(M) * normal;

    fragUV = vertexUV;
}
