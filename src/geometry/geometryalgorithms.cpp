#include "geometryalgorithms.h"
#include  <glm/gtx/norm.hpp>
#include "math/mathutils.h"
namespace skyengine
{
namespace geometry
{

std::shared_ptr<ressource::Mesh<render::vertex3D> > SpherePrimitive(unsigned int u_cuts_in, unsigned int v_cuts_in)
{
    unsigned int u_cuts = u_cuts_in + 2;
    unsigned int v_cuts = v_cuts_in;
    std::shared_ptr<ressource::Mesh<render::vertex3D> >sphere_mesh = std::make_shared<ressource::Mesh<render::vertex3D> >("Sphere_mesh");
    //x^2 + y^2 + z^2 = 1
    sphere_mesh->addVertex(render::vertex3D(glm::vec3(0,1,0),glm::vec2(atan2(1,0)/(2.0f*M_PI) + 0.5,0.5 - asin(0.0f)/M_PI), glm::vec3(0,1,0)));
    for(float u = float(M_PI)/float(u_cuts) ; u < float(M_PI - EPS1) - float(M_PI)/float(u_cuts) ; u += float(M_PI)/float(u_cuts))
        for(float v = 0; v < float(2.0*M_PI - EPS1) ; v += float(2.0*M_PI)/float(v_cuts))
        {
            sphere_mesh->addVertex(render::vertex3D(glm::vec3(cos(v)*sin(u),cos(u),sin(v)*sin(u)),glm::vec2(atan2(cos(u),sin(u)*cos(v))/(2.0f*M_PI) + 0.5,0.5 - asin(sin(u)*sin(v))/M_PI), glm::vec3(sin(u)*cos(v),cos(u),sin(u)*sin(v))));
        }

    sphere_mesh->addVertex(render::vertex3D(glm::vec3(0,-1,0),glm::vec2(atan2(-1,0)/(2.0f*M_PI) + 0.5,0.5 - asin(0.0f)/M_PI), glm::vec3(0,-1,0)));

    //Now add indices
    for(int j = -1 ; j <= int(u_cuts_in-1) ; j++)
    {
        for(int i = 1 ; i <= (int)v_cuts ; i++)
        {

            if(j == -1)
            {
                sphere_mesh->addIndex(0);
                sphere_mesh->addIndex(i);
                sphere_mesh->addIndex(i%(v_cuts) + 1);
            }
            else if (j == int(u_cuts_in - 1))
            {
                sphere_mesh->addIndex(i + (u_cuts_in-1)*v_cuts);
                sphere_mesh->addIndex(u_cuts_in*v_cuts + 1);
                sphere_mesh->addIndex((i%v_cuts + 1 + (u_cuts_in-1)*v_cuts));
            }
            else
            {
                sphere_mesh->addIndex(i + j*v_cuts);
                sphere_mesh->addIndex(i + (j+1)*v_cuts);
                int diag_index_1 = i%v_cuts + 1 + ((j+1)*v_cuts);
                sphere_mesh->addIndex(diag_index_1);


                sphere_mesh->addIndex(i%v_cuts + 1 + (j+1)*(v_cuts));
                sphere_mesh->addIndex(i%v_cuts + 1 + j*v_cuts);
                sphere_mesh->addIndex(i + j*v_cuts );
            }
        }
    }
    return sphere_mesh;
}

std::shared_ptr<ressource::Mesh<render::vertex3D> >CubePrimitive(float size)
{
    std::shared_ptr<ressource::Mesh<render::vertex3D> >cube_mesh = std::make_shared<ressource::Mesh<render::vertex3D> >("Cube_mesh");
    cube_mesh->addVertex(render::vertex3D(glm::vec3(size*0.5f,size*0.5f,size*0.5f),glm::vec2(), glm::vec3(size*0.5f,size*0.5f,size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(size*0.5f,-size*0.5f,size*0.5f),glm::vec2(), glm::vec3(size*0.5f,-size*0.5f,size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(-size*0.5f,-size*0.5f,size*0.5f),glm::vec2(), glm::vec3(-size*0.5f,-size*0.5f,size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(-size*0.5f,size*0.5f,size*0.5f),glm::vec2(), glm::vec3(-size*0.5f,size*0.5f,size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(size*0.5f,size*0.5f,-size*0.5f),glm::vec2(), glm::vec3(size*0.5f,size*0.5f,-size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(size*0.5f,-size*0.5f,-size*0.5f),glm::vec2(), glm::vec3(size*0.5f,-size*0.5f,-size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(-size*0.5f,-size*0.5f,-size*0.5f),glm::vec2(), glm::vec3(-size*0.5f,-size*0.5f,-size*0.5f)));
    cube_mesh->addVertex(render::vertex3D(glm::vec3(-size*0.5f,size*0.5f,-size*0.5f),glm::vec2(), glm::vec3(-size*0.5f,size*0.5f,-size*0.5f)));

    //Front
    cube_mesh->addIndex(3);
    cube_mesh->addIndex(1);
    cube_mesh->addIndex(0);
    cube_mesh->addIndex(2);
    cube_mesh->addIndex(1);
    cube_mesh->addIndex(3);
    //Right
    cube_mesh->addIndex(0);
    cube_mesh->addIndex(1);
    cube_mesh->addIndex(4);
    cube_mesh->addIndex(1);
    cube_mesh->addIndex(5);
    cube_mesh->addIndex(4);
    //Back
    cube_mesh->addIndex(4);
    cube_mesh->addIndex(5);
    cube_mesh->addIndex(6);
    cube_mesh->addIndex(4);
    cube_mesh->addIndex(6);
    cube_mesh->addIndex(7);
    //Left
    cube_mesh->addIndex(7);
    cube_mesh->addIndex(6);
    cube_mesh->addIndex(3);
    cube_mesh->addIndex(2);
    cube_mesh->addIndex(3);
    cube_mesh->addIndex(6);
    //Up
    cube_mesh->addIndex(0);
    cube_mesh->addIndex(4);
    cube_mesh->addIndex(3);
    cube_mesh->addIndex(7);
    cube_mesh->addIndex(3);
    cube_mesh->addIndex(4);
    //Down
    cube_mesh->addIndex(1);
    cube_mesh->addIndex(2);
    cube_mesh->addIndex(5);
    cube_mesh->addIndex(6);
    cube_mesh->addIndex(5);
    cube_mesh->addIndex(2);

    return cube_mesh;
}

std::shared_ptr<ressource::Mesh<render::vertex3D> >CurvePrimitive(const GeometryCurve<glm::vec3>& curve)
{
    std::shared_ptr<ressource::Mesh<render::vertex3D> >curve_mesh = std::make_shared<ressource::Mesh<render::vertex3D> >("Curve_mesh",GL_STATIC_DRAW,GL_LINE_STRIP);

    curve_mesh->addVertex(render::vertex3D(curve.GetPoint(0)));
    for(unsigned int i = 1; i < curve.GetSize() ; i++)
    {
        curve_mesh->addVertex(render::vertex3D(curve.GetPoint(i)));
        curve_mesh->addIndex(i-1);
        curve_mesh->addIndex(i);
    }
    return curve_mesh;
}


std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveSpeed(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_speed)
{
    std::vector<animation::TimedParameter<glm::vec3>> speed{init_speed};
    speed.resize(curve.size());
    for(unsigned int i = 1 ; i < curve.size(); i++)
        speed[i] = (animation::TimedParameter<glm::vec3>((curve[i].value - curve[i-1].value)/(curve[i].time - curve[i-1].time),curve[i].time));
    return speed;
}

std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveAccel(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_accel, const animation::TimedParameter<glm::vec3>& end_accel)
{
    std::vector<animation::TimedParameter<glm::vec3>> accel{init_accel};
    accel.resize(curve.size());
    for(unsigned int i = 1 ; i < curve.size() - 1; i++)
        accel[i] = (animation::TimedParameter<glm::vec3>((curve[i+1].value + curve[i-1].value - 2.0f*curve[i].value)/
                float(pow(curve[i+1].time - curve[i].time,2) + pow(curve[i].time - curve[i-1].time,2)),curve[i].time));

    accel[curve.size() - 1] = (end_accel);
    return accel;
}

std::vector<animation::TimedParameter<float>> SKYENGINE_API ComputeCurveCurvature(const std::vector<animation::TimedParameter<glm::vec3>>& curve)
{
    std::vector<animation::TimedParameter<float>> curvature;
    std::vector<animation::TimedParameter<glm::vec3>> speed = ComputeCurveSpeed(curve,animation::TimedParameter<glm::vec3>(glm::vec3(),curve[0].time));
    std::vector<animation::TimedParameter<glm::vec3>> accel = ComputeCurveAccel(curve,animation::TimedParameter<glm::vec3>(glm::vec3(),curve[0].time),animation::TimedParameter<glm::vec3>(glm::vec3(),curve[curve.size() - 1].time));
    curvature.resize(curve.size());
    for(unsigned int i = 0 ; i < curve.size() ;i++)
    {
        float th_norm = pow(glm::l1Norm(speed[i].value),3.0f);
        float sq_deter = glm::l1Norm(glm::cross(speed[i].value,accel[i].value));
        float curv = sq_deter/th_norm;
        if(math::ApproxEqual(th_norm,0.0f,EPS3))
                curv = 0.0f;
        curvature[i] = (animation::TimedParameter<float>(curv,curve[i].time));
    }
    return curvature;
}

std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveJerk(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_accel, const animation::TimedParameter<glm::vec3>& end_accel)
{
    std::vector<animation::TimedParameter<glm::vec3>> jerk;
    std::vector<animation::TimedParameter<glm::vec3>> accel = ComputeCurveAccel(curve,init_accel,end_accel);
    jerk.resize(curve.size());
    for(unsigned int i = 1 ; i < accel.size(); i++)
        jerk[i] = (animation::TimedParameter<glm::vec3>((accel[i].value - accel[i-1].value)/(accel[i].time - accel[i-1].time),accel[i].time));
    return jerk;
}

std::vector<animation::TimedParameter<float>> SKYENGINE_API ComputeCurveTorsion(const std::vector<animation::TimedParameter<glm::vec3>>& curve)
{
    std::vector<animation::TimedParameter<float>> torsion;
    std::vector<animation::TimedParameter<glm::vec3>> speed = ComputeCurveSpeed(curve,animation::TimedParameter<glm::vec3>(glm::vec3(),curve[0].time));
    std::vector<animation::TimedParameter<glm::vec3>> accel = ComputeCurveAccel(curve,animation::TimedParameter<glm::vec3>(glm::vec3(),curve[0].time),animation::TimedParameter<glm::vec3>(glm::vec3(),curve[curve.size() - 1].time));
    std::vector<animation::TimedParameter<glm::vec3>> jerk = ComputeCurveJerk(curve,animation::TimedParameter<glm::vec3>(glm::vec3(),curve[0].time),animation::TimedParameter<glm::vec3>(glm::vec3(),curve[curve.size() - 1].time));
    torsion.resize(curve.size());
    for(unsigned int i = 0 ; i < curve.size() ; i++)
    {
        glm::vec3 cr_sa = glm::cross(speed[i].value,accel[i].value);
        float det = glm::dot(cr_sa,jerk[i].value);
        float tors = det/glm::l2Norm(cr_sa);
        if(math::ApproxEqual(glm::l2Norm(cr_sa),0.0f,EPS3))
                tors = 0.0f;
        torsion[i] = (animation::TimedParameter<float>(tors,curve[i].time));
    }
    return torsion;
}

std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveAffineSpeed(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3> &init_speed, unsigned int func_mode)
{
    std::vector<animation::TimedParameter<glm::vec3>> aff_speed;
    std::vector<animation::TimedParameter<glm::vec3>> speed = ComputeCurveSpeed(curve,init_speed);
    std::vector<animation::TimedParameter<float>> curvature = ComputeCurveCurvature(curve);
    std::vector<animation::TimedParameter<float>> torsion = ComputeCurveTorsion(curve);
    aff_speed.resize(curve.size());
    switch (func_mode) {
    case 0:

        for(unsigned int i = 0 ; i < speed.size() - 1 ; i++)
        {
            aff_speed[i] = (animation::TimedParameter<glm::vec3>(speed[i].value*float(pow(curvature[i].value,1.0f/3.0f)),speed[i].time));
        }
        break;
    case 1:

        for(unsigned int i = 0 ; i < speed.size() - 1 ; i++)
        {
            aff_speed[i] = (animation::TimedParameter<glm::vec3>(speed[i].value
                                                                 *float(pow(
                                                                            sqrt(curvature[i].value*curvature[i].value
                                                                            + torsion[i].value*torsion[i].value)
                                                                            ,1.0f/3.0f)),speed[i].time));
        }
        break;
    default:
        for(unsigned int i = 0 ; i < speed.size() - 1 ; i++)
        {
            aff_speed[i] = (animation::TimedParameter<glm::vec3>(speed[i].value*float(pow(curvature[i].value,1.0f/3.0f)),speed[i].time));
        }
        break;
    }


    return aff_speed;
}

std::vector<animation::TimedParameter<glm::vec3>> SKYENGINE_API ComputeCurveAffineAccel(const std::vector<animation::TimedParameter<glm::vec3>>& curve, const animation::TimedParameter<glm::vec3>& init_accel, const animation::TimedParameter<glm::vec3>& end_accel, unsigned int func_mode)
{
    std::vector<animation::TimedParameter<glm::vec3>> aff_accel{init_accel};
    std::vector<animation::TimedParameter<glm::vec3>> aff_speed = ComputeCurveAffineSpeed(curve,animation::TimedParameter<glm::vec3>(glm::vec3(),curve[0].time),func_mode);
    aff_accel.resize(curve.size());
    for(unsigned int i = 1 ; i < curve.size() - 1; i++)
        aff_accel[i] = animation::TimedParameter<glm::vec3>(aff_speed[i].value - aff_speed[i-1].value,aff_speed[i].time);

    aff_accel[curve.size() - 1] = (end_accel);
    return aff_accel;

}

}
}
