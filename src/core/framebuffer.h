#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "rendersurface.h"

#include <GL/glew.h>

#include "../ressources/texture.h"
#include "eventlisteners/windoweventlistener.h"

namespace skyengine
{

namespace core
{

class FrameBuffer : public RenderSurface,public core::WindowEventListener,public core::Listener<core::RenderSurface::ResizeEvent>
{
public:
    enum BindMode
    {
        DRAW = GL_DRAW_FRAMEBUFFER,
        READ = GL_READ_FRAMEBUFFER,
        READ_DRAW = GL_FRAMEBUFFER
    };

    enum AttachmentMode
    {
        COLORATTACHMENT0 = GL_COLOR_ATTACHMENT0,
        COLORATTACHMENT1 = GL_COLOR_ATTACHMENT1,
        COLORATTACHMENT2 = GL_COLOR_ATTACHMENT2,
        COLORATTACHMENT3 = GL_COLOR_ATTACHMENT3,
        COLORATTACHMENT4 = GL_COLOR_ATTACHMENT4,
        COLORATTACHMENT5 = GL_COLOR_ATTACHMENT5,
        COLORATTACHMENT6 = GL_COLOR_ATTACHMENT6,
        COLORATTACHMENT7 = GL_COLOR_ATTACHMENT7,
        DEPTHATTACHMENT = GL_DEPTH_ATTACHMENT,
        STENCILATTACHMENT = GL_STENCIL_ATTACHMENT,
        DEPTHSTENCILATTACHMENT = GL_DEPTH_STENCIL_ATTACHMENT
    };

    FrameBuffer(std::shared_ptr<WindowEventManager> wem);
    ~FrameBuffer();

    glm::ivec2 getSize() const;
    void resize(glm::ivec2 size);

    glm::ivec2 getPos() const override;
    void move(glm::ivec2 pos) override;

    inline ressource::Texture* getAttachmentTex(AttachmentMode attachment);

    void bindAttachment(AttachmentMode attachment, GLenum internal_format, GLenum format, GLenum type, unsigned int width, unsigned int height);
    void bindAttachment(AttachmentMode attachment);
    void unBindAttachment(AttachmentMode attachment);
    int checkStatus() const;
    void bind();
    void bind(BindMode mode);
    void unBind();
    void setFrameBuffer();
    inline GLuint getId() const;

    void listened(const WindowResizeEvent* e);
    void listened(const core::RenderSurface::ResizeEvent*);

    void displayAttachment(AttachmentMode attachment);

    static void Copy(FrameBuffer& source,
                     AttachmentMode readBuffer,
                     FrameBuffer& destination,
                     GLbitfield mask);

    static FrameBuffer DefaultFrameBuffer(unsigned int width, unsigned int height, std::shared_ptr<WindowEventManager> wem);

protected:
    GLuint m_frame_buffer_ID;
    BindMode m_bind_mode;
    glm::ivec2 m_up_left_corner;
    std::map<AttachmentMode,ressource::Texture*> m_texture_attachment;
    std::map<AttachmentMode,GLuint> m_render_buffer_attachment;
    std::shared_ptr<WindowEventManager> m_wem;
};

inline GLuint FrameBuffer::getId() const
{
    return m_frame_buffer_ID;
}

inline ressource::Texture* FrameBuffer::getAttachmentTex(AttachmentMode attachment)
{
    if(utility::MapContains(m_texture_attachment,attachment))
        return m_texture_attachment[attachment];
    else
        return nullptr;
}

}
}
#endif // FRAMEBUFFER_H
