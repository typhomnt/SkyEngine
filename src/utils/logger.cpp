#include "logger.h"

#include <iostream>
#include <stdexcept>


namespace skyengine
{

namespace utility
{

static const char* LogTags[5] = {
    "[DEBUG] ",
    "[INFO] ",
    "[WARN] ",
    "[ERROR] ",
    "[FATAL] ",
};


void Logger::SetMinLevel(LogLevel minLevel)
{
    Logger* logger = static_cast<Logger*>(GetSingleton());
    logger->m_minLevel = minLevel;
}

void Logger::Debug(const std::string& message)
{
    Log(LogLevel::DEBUG, message);
}

void Logger::Info(const std::string& message)
{
    Log(LogLevel::INFO, message);
}

void Logger::Warning(const std::string& message)
{
    Log(LogLevel::WARNING, message);
}

void Logger::Error(const std::string& message)
{
    Log(LogLevel::ERROR, message);
}

void Logger::Fatal(const std::string& message)
{
    Log(LogLevel::FATAL, message);
    std::terminate();
}

const std::vector<std::string>& Logger::GetLogs()
{
    Logger* logger = GetSingleton();
    return logger->m_logs;
}

void Logger::ClearLogs()
{
    Logger* logger = GetSingleton();
    logger->m_logs.clear();
}


Logger::Logger()
    : m_minLevel(LogLevel::WARNING)
    , m_logs {}
{}


void Logger::Log(LogLevel level, const std::string& message)
{
    Logger* logger = GetSingleton();
    if (logger->m_minLevel <= level)
    {
        std::string logMessage = LogTags[(size_t)level] + message;
        logger->m_logs.push_back(logMessage);

        if (level <= LogLevel::INFO)
            std::cout << logMessage << std::endl;
        else
            std::cerr << logMessage << std::endl;
    }
}

} // namespace utility

} // namespace skyengine
