#ifndef GAMEOBJECTANIMATION_H
#define GAMEOBJECTANIMATION_H
#include "animation.h"
#include "geometry/geometrycurve.h"
#include "geometry/trajectorynode.h"
#include "animatedbone3d.h"
#include "scene/gameobject3d.h"
namespace skyengine
{

namespace animation
{

class GameObjectAnimation : public Animation
{
public:
    GameObjectAnimation(const std::string& animation_name,scene::SceneGraphNode* game_node,scene::SceneGraph* scene_graph);
    virtual ~GameObjectAnimation();
    void setPath(const AnimatedPoseTrajectory& path);
    void setPath(const geometry::GeometryCurve<Pose3D>& path);
    void addPoint(const Pose3D& point);
    void addPoint(const KeyPose3D& point);
    void clearPath();

    void ComputeAnimation(float time_stamp) override;
private:
    scene::SceneGraphNode* m_game_node;
    std::shared_ptr<geometry::TrajectoryNode<KeyPose3D>> m_path;

    geometry::TrajectoryNode<KeyPose3D>* m_traj_node;
    scene::SceneGraph* m_scene_graph;
};

}
}
#endif // GAMEOBJECTANIMATION_H
