#ifndef IMAGE_H_
#define IMAGE_H_

#include "ressource.h"

#include <GL/glew.h>
#include <IL/il.h>


namespace skyengine
{
namespace ressource
{

class Image : public Ressource
{
public:
    Image(const std::string& name);
    Image(const std::string& name, const std::string& imageFile);
    Image(const std::string& name,
          const std::string& format,
          size_t size,
          void* bytes);
    Image(const std::string& name,
          unsigned int width,
          unsigned int height,
          unsigned int nbChannels,
          const unsigned char* data);
    Image(const std::string& name, const Image& other);

    ~Image();

    void loadFromFile(const std::string& fileName);
    void saveToFile(const std::string& outputFile);

    /**
     * This method can be used when image bytes are stored in memory.
     * format corresponds to image extension (jpg, png, dds...)
     */
    void loadFromBytes(const std::string& format, size_t size, void* bytes);

    inline unsigned int getWidth() const;
    inline unsigned int getHeight() const;

    /**
     * No transparency -> GL_RGB. Otherwise, GL_RGBA
     */
    inline GLenum getDataFormat() const;

    const unsigned char* getData() const;

    /**
     * nbChannels : 3 for RGB image, 4 for RGBA image.
     */
    void setData(unsigned int width,
                 unsigned int height,
                 unsigned int nbChannels,
                 const unsigned char* data);

private:
    // DevIL identifier
    ILuint m_ilID;

    // Dimensions
    ILuint m_width;
    ILuint m_height;
    GLenum m_dataFormat;

    // DevIL init status
    static bool ILLoaded;
    static void InitIL();

    static ILuint GenILImage();
};


inline unsigned int Image::getWidth() const
{
    return m_width;
}

inline unsigned int Image::getHeight() const
{
    return m_height;
}

inline GLenum Image::getDataFormat() const
{
    return m_dataFormat;
}

} // namespace ressource
} // namespace skyengine


#endif // IMAGE_H_
