#ifndef ARMATURE_H
#define ARMATURE_H
#include "scene/gameobject3d.h"
#include "skeleton.h"
namespace skyengine
{

namespace animation
{
class Armature : public scene::GameObject3D
{
public:
    Armature(const std::string& name, Skeleton* skel, scene::SceneGraph *owner = nullptr,
             const unsigned int type = Type::GameObject,
             const glm::mat4& world_matrix = glm::mat4(1.0f));

    inline Skeleton* getSkeleton();
    virtual void update(double,double);
    //virtual void setShaderUniforms();
protected:

    Skeleton* m_skel;

};

inline Skeleton* Armature::getSkeleton()
{
    return m_skel;
}

}
}
#endif // ARMATURE_H
