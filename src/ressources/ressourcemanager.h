#ifndef RESSOURCEMANAGER_H
#define RESSOURCEMANAGER_H

#include "utils/skyengine_dll.h"

#include <unordered_map>
#include <memory>
#include <iostream>

#include "core/serializable.h"
#include "utils/singleton.h"
#include "utils/utilities.h"


namespace skyengine
{

namespace ressource
{

class Ressource;

class SKYENGINE_API RessourceManager: public utility::Singleton<RessourceManager>, public core::Serializable
{
public:
    friend RessourceManager* Singleton<RessourceManager>::GetSingleton();


    /**
     * Static methods to access RessourceManager without using GetSingleton.
     */
    static void Add(const std::string& name, const std::shared_ptr<Ressource>& ressource);

    static bool Exists(const std::string& name);
    static std::shared_ptr<Ressource> Get(const std::string& name);

    static void Remove(const std::string& name);


    RessourceManager(const RessourceManager&) = delete;
    RessourceManager& operator=(const RessourceManager&) = delete;

    inline const std::unordered_map<std::string,std::shared_ptr<Ressource>>& getRessources() { return m_ressources; }
    static inline std::string getDuplicateName(const std::string& res_name);

    virtual void Serialize(rapidjson::Value&,rapidjson::MemoryPoolAllocator<>&);
    virtual std::string getTag() {return "ResMg";}

    static void Load(const rapidjson::Value& resources);

private:
    std::unordered_map<std::string,std::shared_ptr<Ressource> > m_ressources;
    RessourceManager();

};

inline std::string RessourceManager::getDuplicateName(const std::string &res_name)
{
    unsigned int res_nbr = 0;
    while(RessourceManager::Exists(res_name + ".0" + std::to_string(res_nbr)))
        res_nbr++;

    return res_name + ".0" + std::to_string(res_nbr);
}

} // namespace ressource

} // namespace skyengine

#endif // RESSOURCEMANAGER_H
