#include "skywindow.h"

#include <iostream>

#include <imgui/imgui.h>

#include "core/modelimportermanager.h"
#include "gui/guiutils.h"
#include "gui/widgets/nodelist.h"
#include "scene/scenegraphmanager.h"


namespace
{
    constexpr int STATUS_HEIGHT = 8;
}


namespace module
{

namespace gui
{

static ImGuiDockSlot ToImGuiEnum[6] = {
    ImGuiDockSlot_Left,
    ImGuiDockSlot_Right,
    ImGuiDockSlot_Left,
    ImGuiDockSlot_Bottom,
    ImGuiDockSlot_Top,
    ImGuiDockSlot_Float
};


SkyWindow::SkyWindow(skyengine::core::SkyEngine* engine,
                     gui::GuiManager* manager)
    : GuiTask(engine, manager)
    , m_active_obj_evt(std::make_shared<ActiveObjectEvent>())
    , m_select_obj_evt(std::make_shared<SelectObjectEvent>())
    , m_loadModelDialog {}
    , m_openDialog {}
    , m_saveDialog {}
    , m_themesMenu(false)
    , m_themeIdx(-1)
    , m_saveTheme{""}
    , m_aboutMenu(false)
{}


void SkyWindow::sendActiveObject(skyengine::scene::SceneGraphNode* node)
{
    m_active_obj_evt->send(node);
}

void SkyWindow::run()
{
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);

    int menuHeight = drawMenuBar();

    ImVec2 pos(0, menuHeight);
    ImVec2 size = ImGui::GetIO().DisplaySize;
    ImGui::SetNextWindowPos(pos);
    ImGui::SetNextWindowSize(ImVec2(size.x, size.y - 2 * pos.y - STATUS_HEIGHT));

    ImGuiWindowFlags dockFlags = ImGuiWindowFlags_NoTitleBar
        | ImGuiWindowFlags_NoResize
        | ImGuiWindowFlags_NoInputs
        | ImGuiWindowFlags_NoBringToFrontOnFocus;

    bool showDock = true;
    if (ImGui::Begin("Dock", &showDock, dockFlags))
    {
        ImGui::BeginDockspace();

        for (auto& it : m_dockedWidgets)
        {
            ImGuiDockSlot slot = ToImGuiEnum[(size_t)it.first];
            if (it.first == DockSlot::Center && m_dockedWidgets.find(DockSlot::Right) == m_dockedWidgets.end())
                slot = ImGuiDockSlot_Right;
            ImGui::SetNextDock(slot);

            auto& widgets = it.second;
            for (size_t i = 0; i < widgets.size(); ++i)
            {
                SkyWidget* widget = widgets[i];
                bool dockOpened = widget->isVisible();
                if (ImGui::BeginDock(widget->getName().c_str(), &dockOpened, widget->getFlags()))
                {
                    ImVec2 wpos = ImGui::GetWindowPos();
                    ImVec2 wsize = ImGui::GetWindowSize();
                    ImVec2 cpos = ImGui::GetMousePos();
                    widget->setHovered((cpos.x > wpos.x+1) && (cpos.y > wpos.y+1) && (cpos.x < wpos.x + wsize.x) && (cpos.y < wpos.y + wsize.y));
                    widget->draw();

                }
                ImGui::EndDock();

                if (!dockOpened)
                    widget->hide();
            }
        }

        ImGui::EndDockspace();
        ImGui::End();
    }

    drawStatusBar(menuHeight);

    ImGui::PopStyleVar();
}


int SkyWindow::drawMenuBar()
{
    int menuHeight = 0;

    if (ImGui::BeginMainMenuBar())
    {
        bool fileMenu = ImGui::BeginMenu("File");
        bool loadModel = false;
        bool openScene = false;
        bool saveScene = false;

        if (fileMenu)
        {
            loadModel = ImGui::MenuItem("Load Model", "CTRL+L");
            openScene = ImGui::MenuItem("Open Scene", "CTRL+O");
            saveScene = ImGui::MenuItem("Save Scene", "CTRL+S");
            ImGui::EndMenu();
        }

        if (!m_widgets.empty())
        {
            if (ImGui::BeginMenu("Docks"))
            {
                for (auto& it : m_widgets)
                {
                    if (ImGui::MenuItem(it.second->getName().c_str()))
                        it.second->show();
                }

                ImGui::EndMenu();
            }
        }

        /**
         * The file dialog trigger must be true for only one frame but the file dialog must
         * always be rendered.
         * It does not handle multiple file selection for now.
         */
        std::string loadModelPath = m_loadModelDialog.chooseFileDialog(loadModel);
        if (loadModelPath.size() > 0)
        {
            auto sg = skyengine::scene::SceneGraphManager::Get("default_scene").get();
            skyengine::core::ModelImporterManager::GetSingleton()->loadModel(loadModelPath, sg);
        }

        std::string openPath = m_openDialog.chooseFileDialog(openScene);
        if (openPath.size() > 0)
        {
            skyengine::core::LoadScene(openPath);

            auto it = m_widgets.find("Node List");
            if (it != m_widgets.end())
            {
                auto nodeList = std::dynamic_pointer_cast<NodeList>(it->second);
                nodeList->setSceneGraph(skyengine::scene::SceneGraphManager::Get("default_scene").get());
            }
        }

        std::string savePath = m_saveDialog.saveFileDialog(fileMenu && saveScene);
        if (savePath.size() > 0)
        {
            auto sg = skyengine::scene::SceneGraphManager::Get("default_scene").get();
            skyengine::core::SaveScene(savePath, *sg);
        }


        /** Settings Menu **/
        if (ImGui::BeginMenu("Settings"))
        {
            m_themesMenu = ImGui::MenuItem("Themes");
            ImGui::EndMenu();
        }

        if (m_themesMenu)
            drawThemesMenu();

        /** About Menu **/
        if (ImGui::BeginMenu("?"))
        {
            m_aboutMenu = ImGui::MenuItem("About ImGui");
            ImGui::EndMenu();
        }

        if (m_aboutMenu)
        {
            ImGui::Begin("About ImGui", &m_aboutMenu, ImGuiWindowFlags_AlwaysAutoResize);
            ImGui::Text("dear imgui, %s", ImGui::GetVersion());
            ImGui::Separator();
            ImGui::Text("By Omar Cornut and all github contributors.");
            ImGui::Text("ImGui is licensed under the MIT License, see LICENSE for more information.");
            ImGui::End();
        }

        menuHeight = ImGui::GetWindowSize().y;
        ImGui::EndMainMenuBar();
    }

    return menuHeight;
}

void SkyWindow::drawStatusBar(int menuHeight)
{
    ImVec2 size = ImGui::GetIO().DisplaySize;
    float height = menuHeight + STATUS_HEIGHT;

    ImGui::SetNextWindowSize(ImVec2(size.x, height), ImGuiSetCond_Always);
    ImGui::SetNextWindowPos(ImVec2(0, size.y - height), ImGuiSetCond_Always);

    ImGui::Begin(
        "StatusBar",
        nullptr,
        ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoResize
    );
    ImGui::Text("FPS : %f", ImGui::GetIO().Framerate);
    ImGui::End();
}

void SkyWindow::drawThemesMenu()
{
    if (ImGui::Begin("Themes settings", &m_themesMenu))
    {
        std::vector<std::string> files = skyengine::utility::getFilesInDir(
            "../themes",
            std::initializer_list<std::string> {"json"}
        );

        std::vector<const char*> guiFiles;
        guiFiles.reserve(files.size());
        for (std::string& f : files)
        {
            f = skyengine::utility::extractFileNameFromPath(f);
            guiFiles.push_back(f.c_str());
        }

        ImGui::Combo("", &m_themeIdx, guiFiles.data(), guiFiles.size());
        ImGui::SameLine();
        if (ImGui::Button("Apply theme") && m_themeIdx != -1)
            GuiUtils::ApplyTheme(guiFiles[m_themeIdx]);

        ImGui::InputText("##ThemeName", m_saveTheme, 50);
        ImGui::SameLine();
        if (ImGui::Button("Save theme") && strlen(m_saveTheme) > 0)
            GuiUtils::SaveTheme(m_saveTheme);
    }
    ImGui::End();
}

void SkyWindow::dockWidget(SkyWidget* widget, DockSlot slot)
{
    auto it = m_dockedWidgets.find(slot);
    if (it != m_dockedWidgets.end())
        it->second.push_back(widget);
    else
        m_dockedWidgets.insert({ slot, std::vector<SkyWidget*> {widget} });
}

} // namespace gui

} // namespace module
