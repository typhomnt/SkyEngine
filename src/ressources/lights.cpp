#include "lights.h"

#include "ressourcemanager.h"


namespace skyengine
{

namespace ressource
{

/**
 * Light class
 */

Light::Light(const std::string& name, unsigned int id) :
    Ressource(name),
    m_id(id),
    m_ambient(0.2f, 0.2f, 0.2f, 1.0f),
    m_diffuse(1.0f),
    m_specular(1.0f)
{}

Light::Light(const std::string& name,
             unsigned int id,
             const glm::vec4& ambient,
             const glm::vec4& diffuse,
             const glm::vec4& specular) :
    Ressource(name),
    m_id(id),
    m_ambient(ambient),
    m_diffuse(diffuse),
    m_specular(specular)
{}

std::shared_ptr<rapidjson::Value> Light::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Ressource::SerializeProp(al);

    value->AddMember(rapidjson::Value("Id", al), rapidjson::Value(m_id), al);
    utility::JSONHelper::SaveVec(*value, "Ambient", m_ambient, al);
    utility::JSONHelper::SaveVec(*value, "Diffuse", m_diffuse, al);
    utility::JSONHelper::SaveVec(*value, "Specular", m_specular, al);

    return value;
}

bool Light::LoadProp(const rapidjson::Value& value)
{
    m_id = value["Id"].GetInt();
    utility::JSONHelper::ReadVec(value["Ambient"], m_ambient);
    utility::JSONHelper::ReadVec(value["Diffuse"], m_diffuse);
    utility::JSONHelper::ReadVec(value["Specular"], m_specular);

    return false;
}


/**
 * DirLight class
 */

DirLight::DirLight(const std::string& name, unsigned int id) :
    Light(name, id),
    m_direction(0.0f, -1.0f, 0.0f)
{}

DirLight::DirLight(const std::string& name,
                   unsigned int id,
                   const glm::vec4& ambient,
                   const glm::vec4& diffuse,
                   const glm::vec4& specular,
                   const glm::vec3& direction) :
    Light(name, id, ambient, diffuse, specular),
    m_direction(direction)
{}

void DirLight::sendToShader(const Shader &shader) const
{
    char var[30];

    std::snprintf(var, 30, "dirLight[%u].ambient", m_id);
    shader.setUniformValue4f(var, m_ambient);

    std::snprintf(var, 30, "dirLight[%u].diffuse", m_id);
    shader.setUniformValue4f(var, m_diffuse);

    std::snprintf(var, 30, "dirLight[%u].specular", m_id);
    shader.setUniformValue4f(var, m_specular);

    std::snprintf(var, 30, "dirLight[%u].direction", m_id);
    shader.setUniformValue3f(var, m_direction);
}

std::shared_ptr<rapidjson::Value> DirLight::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Light::SerializeProp(al);
    utility::JSONHelper::SaveVec(*value, "Direction", m_direction, al);
    return value;
}

bool DirLight::LoadProp(const rapidjson::Value& value)
{
    Light::LoadProp(value);
    utility::JSONHelper::ReadVec(value["Direction"], m_direction);

    return false;
}


/**
 * PointLight class
 */

PointLight::PointLight(const std::string& name, unsigned int id) :
    Light(name, id),
    m_position(0.0f),
    m_constant(1.0f),
    m_linear(0.0f),
    m_quadratic(0.0f)
{}

PointLight::PointLight(const std::string& name,
                       unsigned int id,
                       const glm::vec4& ambient,
                       const glm::vec4& diffuse,
                       const glm::vec4& specular,
                       const glm::vec3& position,
                       float constant,
                       float linear,
                       float quadratic) :
    Light(name, id, ambient, diffuse, specular),
    m_position(position),
    m_constant(constant),
    m_linear(linear),
    m_quadratic(quadratic)
{}

void PointLight::sendToShader(const Shader &shader) const
{
    char var[30];

    std::snprintf(var, 30, "pointLight[%u].ambient", m_id);
    shader.setUniformValue4f(var, m_ambient);

    std::snprintf(var, 30, "pointLight[%u].diffuse", m_id);
    shader.setUniformValue4f(var, m_diffuse);

    std::snprintf(var, 30, "pointLight[%u].specular", m_id);
    shader.setUniformValue4f(var, m_specular);

    std::snprintf(var, 30, "pointLight[%u].position", m_id);
    shader.setUniformValue3f(var, m_position);

    std::snprintf(var, 30, "pointLight[%u].constant", m_id);
    shader.setUniformValue1f(var, m_constant);

    std::snprintf(var, 30, "pointLight[%u].linear", m_id);
    shader.setUniformValue1f(var, m_linear);

    std::snprintf(var, 30, "pointLight[%u].quadratic", m_id);
    shader.setUniformValue1f(var, m_quadratic);
}

std::shared_ptr<rapidjson::Value> PointLight::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = Light::SerializeProp(al);

    utility::JSONHelper::SaveVec(*value, "Position", m_position, al);
    value->AddMember(rapidjson::Value("Constant", al), rapidjson::Value(m_constant), al);
    value->AddMember(rapidjson::Value("Linear", al), rapidjson::Value(m_linear), al);
    value->AddMember(rapidjson::Value("Quadratic", al), rapidjson::Value(m_quadratic), al);

    return value;
}

bool PointLight::LoadProp(const rapidjson::Value& value)
{
    Light::LoadProp(value);

    m_constant = value["Constant"].GetFloat();
    m_linear = value["Linear"].GetFloat();
    m_quadratic = value["Quadratic"].GetFloat();
    utility::JSONHelper::ReadVec(value["Position"], m_position);

    return false;
}


/**
 * SpotLight class
 */

SpotLight::SpotLight(const std::string& name, unsigned int id) :
    PointLight(name, id),
    m_direction(0.0f, -1.0f, 0.0f),
    m_cutoff(float(M_PI_2))
{}

SpotLight::SpotLight(const std::string& name,
                     unsigned int id,
                     const glm::vec4& ambient,
                     const glm::vec4& diffuse,
                     const glm::vec4& specular,
                     const glm::vec3& position,
                     float constant,
                     float linear,
                     float quadratic,
                     const glm::vec3& direction,
                     float cutoff) :
    PointLight(name, id,
               ambient, diffuse, specular,
               position, constant, linear, quadratic),
    m_direction(direction),
    m_cutoff(cutoff)
{}

void SpotLight::sendToShader(const Shader &shader) const
{
    char var[30];

    std::snprintf(var, 30, "spotLight[%u].ambient", m_id);
    shader.setUniformValue4f(var, m_ambient);

    std::snprintf(var, 30, "spotLight[%u].diffuse", m_id);
    shader.setUniformValue4f(var, m_diffuse);

    std::snprintf(var, 30, "spotLight[%u].specular", m_id);
    shader.setUniformValue4f(var, m_specular);

    std::snprintf(var, 30, "spotLight[%u].position", m_id);
    shader.setUniformValue3f(var, m_position);

    std::snprintf(var, 30, "spotLight[%u].constant", m_id);
    shader.setUniformValue1f(var, m_constant);

    std::snprintf(var, 30, "spotLight[%u].linear", m_id);
    shader.setUniformValue1f(var, m_linear);

    std::snprintf(var, 30, "spotLight[%u].quadratic", m_id);
    shader.setUniformValue1f(var, m_quadratic);

    std::snprintf(var, 30, "spotLight[%u].direction", m_id);
    shader.setUniformValue3f(var, m_direction);

    std::snprintf(var, 30, "spotLight[%u].cutoff", m_id);
    shader.setUniformValue1f(var, m_cutoff);
}

std::shared_ptr<rapidjson::Value> SpotLight::SerializeProp(rapidjson::MemoryPoolAllocator<>& al)
{
    auto value = PointLight::SerializeProp(al);

    utility::JSONHelper::SaveVec(*value, "Direction", m_direction, al);
    value->AddMember(rapidjson::Value("Cutoff", al), rapidjson::Value(m_cutoff), al);

    return value;
}

bool SpotLight::LoadProp(const rapidjson::Value& value)
{
    PointLight::LoadProp(value);

    utility::JSONHelper::ReadVec(value["Direction"], m_direction);
    m_cutoff = value["Cutoff"].GetFloat();

    return false;
}

} // namespace ressource

} // namespace skyengine
